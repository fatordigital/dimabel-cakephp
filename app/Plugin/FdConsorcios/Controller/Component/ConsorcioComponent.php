<?php

class ConsorcioComponent extends Component
{

    private $Controller;
    private $Consorcio;

    private $Selecionado;

    public function initialize(Controller $controller)
    {
        $this->Controller = $controller;
        $this->Consorcio = ClassRegistry::init('Consorcio');

        $this->ConsorcioSelecionado();
    }

    private function ConsorcioSelecionado()
    {
        $params = $this->Controller->request->params;
        if (isset($params['consorcio']) && !empty($params['consorcio'])) {
            $this->Selecionado = $this->Consorcio
                ->firstCache($params['consorcio'], array('conditions' => array('Consorcio.slug' => $params['consorcio'])))
                ->getCacheData();
        } else {
            $this->Selecionado = $this->Consorcio
                ->firstCache('consorcio_padrao', array('conditions' => array('Consorcio.principal' => 1)))
                ->getCacheData();
        }
        CakeSession::write('selecionado', $this->Selecionado);
    }

}