<?php

class FdConsorciosController extends FdConsorciosAppController
{

    public $uses = array('FdConsorcios.Consorcio');

    public function fatorcms_index($page = 1)
    {

        $this->FilterResults->addFilters(
            array(
                'filter' => array(
                    'OR' => array(
                        'Consorcio.nome' => array('operator' => 'LIKE', 'value' => array('before' => '%', 'after' => '%')),
                    )
                ),
                'filtro_nome' => array(
                    'Consorcio.nome' => array('operator' => 'LIKE', 'value' => array('before' => '%', 'after' => '%'))
                )
            )
        );

        $this->FilterResults->setPaginate('page', $page);
        $this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

        $paginate = $this->paginate();

        $this->set('paginate', $paginate);

    }

    public function fatorcms_add()
    {
        if ($this->request->is('post')) {
            $this->Consorcio->create($this->request->data);
            if ($this->Consorcio->save()) {
                $this->Session->setFlash(__('Registro salvo com sucesso.'), 'fatorcms_success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'), 'fatorcms_danger');
            }
        }
    }

    public function fatorcms_edit($id = null)
    {
        if ($this->request->is('PUT')) {
            $this->Consorcio->id;
            if ($this->Consorcio->save($this->request->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso.'), 'fatorcms_success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'), 'fatorcms_danger');
            }
        }

        $edit = $this->Consorcio->findById($id);
        if (!$edit) {
            $this->Session->setFlash(__('Registro não encontrado'), 'fatorcms_danger');
            $this->redirect(array('action' => 'index'));
        }

        $edit['Consorcio']['parcelas_carro'] = str_replace('[', '', str_replace(']', '', $edit['Consorcio']['parcelas_carro']));
        $edit['Consorcio']['parcelas_imoveis'] = str_replace('[', '', str_replace(']', '', $edit['Consorcio']['parcelas_imoveis']));

        $edit['Consorcio']['parcelas_apos_valor_imovel'] = str_replace('[', '', str_replace(']', '', $edit['Consorcio']['parcelas_apos_valor_imovel']));
        $edit['Consorcio']['parcelas_apos_valor_carro'] = str_replace('[', '', str_replace(']', '', $edit['Consorcio']['parcelas_apos_valor_carro']));

        $this->data = $edit;
    }


    public function fatorcms_delete($id = null)
    {
        $this->Consorcio->id = $id;
        if (!$this->Consorcio->exists()) {
            throw new NotFoundException('Registro inválido.');
        }
        $this->request->is('get');
        if ($this->Consorcio->delete()) {
            $this->_resetCaches();
            $this->Session->setFlash(__('Registro deletado.'), 'fatorcms_success');
            // $this->redirect(array('action' => 'index'));
            $this->_redirectFilter($this->referer());
        }
        $this->Session->setFlash(__('Registro não pode ser deletado.'), 'fatorcms_warning');
        // $this->redirect(array('action' => 'index'));
        $this->_redirectFilter($this->referer());
    }

    public function fatorcms_status()
    {
        if (!$this->request->is('post')) {
            throw new NotFoundException('Registro inválido.');
        }
        echo $this->_saveStatus('Consorcio', $this->request->data['id'], $this->request->data['value']);
        $this->_resetCaches();
        die;
    }
}