<?php echo $this->Html->script('fatorcms/jquery.stringToSlug.min'); ?>
<?php echo $this->Html->script('/js/fatorcms/core/jquery-character-counter/jquery.charactercounter'); ?>

<?php echo $this->Html->css('/js/fatorcms/core/bootstrap-fileupload/bootstrap-fileupload') ?>
<?php echo $this->Html->script('/js/fatorcms/core/bootstrap-fileupload/bootstrap-fileupload'); ?>

<?php $this->Html->addCrumb('Marcas', array('action' => 'index')) ?>
<?php $this->Html->addCrumb('Cadastrar Marca') ?>

<h3>Cadastrar Marca</h3>

<?php
    echo $this->Form->create('Consorcio', array('type' => 'file', 'role' => 'form', 'class' => 'minimal'));
        include 'form.ctp';
    echo $this->Form->end() ;
?>
