<div class="panel">
    <div class="panel-body">

        <div class="row">
            <div class="col-md-6 col-lg-6">
                <p class="alert alert-info">
                    <strong class="text-danger">*</strong> Campos obrigatórios <br>
                    <i class="fa fa-question-circle text-danger"></i> Descrição do que o campo vai impactar no site
                </p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-lg-3">
                <div class="form-group">
                    <label>Status</label>
                    <div class="icheck">
                        <?php echo $this->Form->input('status', array(
                            'type' => 'radio',
                            'options' => array(1 => 'Ativo', 0 => 'Inativo'),
                            'default' => 1,
                            'legend' => false,
                            'before' => '<div class="radio">',
                            'after' => '</div>',
                            'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="form-group">
                    <label>
                        Mostrar Consórcios
                    </label>
                    <div class="icheck">
                        <?php echo $this->Form->input('mostrar_marcas', array(
                            'type' => 'radio',
                            'options' => array(
                                1 => 'Ativo <i class="fa fa-question-circle text-danger" title="Os outros consórcios cadastrados e habilitados irão aparecer na página deste consórcio"></i>',
                                0 => 'Inativo <i class="fa fa-question-circle text-danger" title="Os consórcios cadastrado e habilitados ficarão invisíveis para este consórcio"></i>'
                            ),
                            'default' => 0,
                            'legend' => false,
                            'before' => '<div class="radio">',
                            'after' => '</div>',
                            'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="form-group">
                    <label>
                        Mostrar Veículos
                    </label>
                    <div class="icheck">
                        <?php echo $this->Form->input('mostrar_veiculos', array(
                            'type' => 'radio',
                            'options' => array(
                                1 => 'Ativo <i class="fa fa-question-circle text-danger" title="A vitrine de veículos ficará disponível quando acessada a página deste consórcio"></i>',
                                0 => 'Inativo <i class="fa fa-question-circle text-danger" title="Não irá aparercer a vitrine de veículos"></i>'
                            ),
                            'default' => 0,
                            'legend' => false,
                            'before' => '<div class="radio">',
                            'after' => '</div>',
                            'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="form-group">
                    <label>Mais Contratado</label>
                    <div class="icheck">
                        <?php echo $this->Form->input('mais_contratado', array(
                            'type' => 'radio',
                            'options' => array(
                                1 => 'Ativo <i class="fa fa-question-circle text-danger" title="Na seção da simulação este consórcio ficará destacado"></i>',
                                0 => 'Inativo <i class="fa fa-question-circle text-danger" title="O consórcio irá aparecer normalmente na seção de simulação"></i>'
                            ),
                            'default' => 0,
                            'legend' => false,
                            'before' => '<div class="radio">',
                            'after' => '</div>',
                            'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-6 col-lg-3">
                <div class="form-group">
                    <label>Principal</label>
                    <div class="icheck">
                        <?php echo $this->Form->input('principal', array(
                            'type' => 'radio',
                            'options' => array(
                                1 => 'Ativo <i class="fa fa-question-circle text-danger" title="Será a página de apresentação, página inicial para o público aberto"></i>',
                                0 => 'Inativo <i class="fa fa-question-circle text-danger" title="Ficará disponível apenas caso acessar diretamente ou quando o usuário selecionar este consórcio"></i>'
                            ),
                            'default' => 0,
                            'legend' => false,
                            'before' => '<div class="radio">',
                            'after' => '</div>',
                            'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="form-group">
                    <label>Meia parcela</label>
                    <div class="icheck">
                        <?php echo $this->Form->input('meia_parcela', array(
                            'type' => 'radio',
                            'options' => array(
                                1 => 'Ativo <i class="fa fa-question-circle text-danger" title="O valor apresentado pro cliente irá ser o [cálculo total / 2]"></i>',
                                0 => 'Inativo <i class="fa fa-question-circle text-danger" title="O valor apresentado pro cliente será o cálculo total"></i>'
                            ),
                            'default' => 0,
                            'legend' => false,
                            'before' => '<div class="radio">',
                            'after' => '</div>',
                            'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="form-group">
                    <label>Página inicial</label>
                    <div class="icheck">
                        <?php echo $this->Form->input('visivel', array(
                            'type' => 'radio',
                            'options' => array(
                                1 => 'Ativo <i class="fa fa-question-circle text-danger" title="O consórcio irá existir e irá aparecer na página inicial"></i>',
                                0 => 'Inativo <i class="fa fa-question-circle text-danger" title="O consórcio irá existir e será apresentado na hora de contratar, mas, não irá aparecer na página inicial"></i>'
                            ),
                            'default' => 0,
                            'legend' => false,
                            'before' => '<div class="radio">',
                            'after' => '</div>',
                            'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
                    </div>
                </div>
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="">
                        Nome <strong class="text-danger">*</strong>
                    </label>
                    <?php echo $this->Form->input('nome', array('required', 'class' => 'form-control', 'placeholder' => 'Nome do Consórcio', 'label' => false, 'legend' => false)) ?>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">
                                Principal parcela automóvel <strong class="text-danger">*</strong>
                                <i class="fa fa-question-circle text-danger" title="A parcela que irá aparecer na vitrine dos carros"></i>
                            </label>
                            <?php echo $this->Form->input('parcela_carro', array('required', 'maxlength' => 3, 'class' => 'form-control', 'placeholder' => 'Quantidade', 'label' => false, 'legend' => false)) ?>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">
                                Taxa Carro <strong class="text-danger">*</strong>
                                <i class="fa fa-question-circle text-danger"
                                   title="Importante preenchimento para apresentação dos valores das parcelas na VITRINE e na SIMULAÇÃO"></i>
                            </label>
                            <?php echo $this->Form->text('taxa_carro', array('required', 'maxlength' => 5, 'class' => 'form-control money1', 'placeholder' => 'Taxa Carro', 'label' => false, 'legend' => false)) ?>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">
                                Taxa Imóvel <strong class="text-danger">*</strong>
                                <i class="fa fa-question-circle text-danger"
                                   title="Importante preenchimento para apresentação dos valores na SIMULAÇÃO"></i>
                            </label>
                            <?php echo $this->Form->text('taxa_imovel', array('required', 'maxlength' => 5, 'class' => 'form-control money1', 'placeholder' => 'Taxa Imóvel', 'label' => false, 'legend' => false)) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="">
                                Parcelas Veículos
                                <i class="fa fa-question-circle text-danger"
                                   title="As parcelas serão apresentada como opção no valor pretendido do cliente"></i>
                            </label>
                            <?php echo $this->Form->text('parcelas_carro', array('class' => 'form-control', 'placeholder' => 'Parcelas Veículos', 'label' => false, 'legend' => false)) ?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="">
                                Parcelas Imóvel
                                <i class="fa fa-question-circle text-danger"
                                   title="As parcelas serão apresentada como opção no valor pretendido do cliente"></i>
                            </label>
                            <?php echo $this->Form->text('parcelas_imoveis', array('class' => 'form-control', 'placeholder' => 'Parcelas Imóvel', 'label' => false, 'legend' => false)) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-3">
                <div class="form-group">
                    <label for="">
                        Após valor veículo
                        <i class="fa fa-question-circle text-danger" title="Após o valor setado neste campo, após o valor simulado passar irá pegar o campo de parcelas referente ao veículo 'Parcelas após valor veículo'"></i>
                    </label>
                    <?php echo $this->Form->text('apos_valor_carro', array('class' => 'form-control money1', 'placeholder' => '0,00', 'label' => false, 'legend' => false)) ?>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="form-group">
                    <label for="">
                        Parcelas após valor veículo
                        <i class="fa fa-question-circle text-danger"
                           title="Para o site entender este campo, preencha o campo 'Parcelas após valor veículo'"></i>
                    </label>
                    <?php echo $this->Form->text('parcelas_apos_valor_carro', array('class' => 'form-control', 'placeholder' => '', 'label' => false, 'legend' => false)) ?>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="form-group">
                    <label for="">
                        Após valor imóvel
                        <i class="fa fa-question-circle text-danger" title="Após o valor setado neste campo, após o valor simulado passar irá pegar o campo de parcelas referente ao veículo 'Parcelas após valor imóvel'"></i>
                    </label>
                    <?php echo $this->Form->text('apos_valor_imovel', array('class' => 'form-control money1', 'placeholder' => '0,00', 'label' => false, 'legend' => false)) ?>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="form-group">
                    <label for="">
                        Parcelas após valor imóvel
                        <i class="fa fa-question-circle text-danger"
                           title="Para o site entender este campo, preencha o campo 'Após valor imóvel'"></i>
                    </label>
                    <?php echo $this->Form->text('parcelas_apos_valor_imovel', array('class' => 'form-control', 'placeholder' => '', 'label' => false, 'legend' => false)) ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <label>
                        Breve legenda
                    </label>
                    <?php echo $this->Form->textarea('legenda', array('legend' => false, 'label' => false, 'class' => 'form-control editor')) ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label>Imagem</label>
                    <div class="fileupload fileupload-new" data-provides="fileupload">

                        <?php if (isset($this->data['Consorcio']['imagem']) && $this->data['Consorcio']['imagem'] != ""): ?>
                            <div class="fileupload-new thumbnail" style="width: 200px; height: 119px;">
                                <img src="<?php echo $this->data['Consorcio']['full_imagem'] ?>" width="200"/>
                            </div>
                        <?php else: ?>
                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=nenhuma+imagem"
                                     alt=""/>
                            </div>
                        <?php endIf; ?>
                        <div class="fileupload-preview fileupload-exists thumbnail"
                             style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>

                        <?php if (isset($this->data['Consorcio']['imagem']) && $this->data['Consorcio']['imagem'] != ""): ?>
                            <div class="icheck minimal">
                                <?php echo $this->Form->input('Marca.imagem.remove', array('type' => 'checkbox', 'label' => 'Remover Imagem Atual', 'div' => false)); ?>
                            </div>
                        <?php endIf; ?>

                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=nenhuma+imagem" alt=""/>
                        </div>
                        <div class="fileupload-preview fileupload-exists thumbnail"
                             style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                        <div>
	                       <span class="btn btn-white btn-file">
	                           	<span class="fileupload-new"><i class="fa fa-paper-clip"></i> Selecionar imagem</span>
	                           	<span class="fileupload-exists"><i class="fa fa-undo"></i> Alterar</span>
                               <?php echo $this->Form->input("imagem", array('type' => 'file', 'class' => 'default', 'label' => false, 'div' => false)); ?>
                               <?php echo $this->Form->hidden("imagem_dir") ?>
	                       </span>
                            <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i
                                    class="fa fa-trash"></i> Remover</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group">
                    <label>Imagem Circular</label>
                    <div class="fileupload fileupload-new" data-provides="fileupload">

                        <?php if (isset($this->data['Consorcio']['imagem_circular']) && $this->data['Consorcio']['imagem_circular'] != ""): ?>
                            <div class="fileupload-new thumbnail" style="width: 200px; height: 119px;">
                                <img src="<?php echo $this->data['Consorcio']['full_imagem'] ?>" width="200"/>
                            </div>
                        <?php else: ?>
                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=nenhuma+imagem"
                                     alt=""/>
                            </div>
                        <?php endIf; ?>
                        <div class="fileupload-preview fileupload-exists thumbnail"
                             style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>

                        <?php if (isset($this->data['Consorcio']['imagem_circular']) && $this->data['Consorcio']['imagem_circular'] != ""): ?>
                            <div class="icheck minimal">
                                <?php echo $this->Form->input('Marca.imagem.remove', array('type' => 'checkbox', 'label' => 'Remover Imagem Atual', 'div' => false)); ?>
                            </div>
                        <?php endIf; ?>

                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=nenhuma+imagem" alt=""/>
                        </div>
                        <div class="fileupload-preview fileupload-exists thumbnail"
                             style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                        <div>
	                       <span class="btn btn-white btn-file">
	                           	<span class="fileupload-new"><i class="fa fa-paper-clip"></i> Selecionar imagem</span>
	                           	<span class="fileupload-exists"><i class="fa fa-undo"></i> Alterar</span>
                               <?php echo $this->Form->input("imagem_circular", array('type' => 'file', 'class' => 'default', 'label' => false, 'div' => false)); ?>
                               <?php echo $this->Form->hidden("imagem_circular_dir") ?>
	                       </span>
                            <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i
                                    class="fa fa-trash"></i> Remover</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php echo $this->Form->submit('Salvar', array('class' => 'btn btn-success', 'div' => false)) ?>

        <a href="<?php echo $this->Html->Url(array('action' => 'index')); ?>" class="btn btn-default">Cancelar</a>

    </div>
</div>