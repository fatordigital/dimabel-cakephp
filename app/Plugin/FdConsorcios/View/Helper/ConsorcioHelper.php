<?php

class ConsorcioHelper extends AppHelper
{
    private $Selecionado;
    private $Carro;
    private $Consorcio;

    public function __construct(View $view, $settings = array())
    {
        parent::__construct($view, $settings);
        $this->Consorcio = ClassRegistry::init('FdConsorcios.Consorcio');
        App::import('Model', 'FdCarros.Carro');
        $this->Carro = new Carro();
        $this->Selecionado = CakeSession::read('selecionado');
    }

    public function Atual($key)
    {
        return $this->Selecionado['Consorcio'][$key];
    }

    public function Url($url = null, $full = false)
    {
        $slug = $this->Selecionado['Consorcio']['principal'] != 1 ? '/' . $this->Selecionado['Consorcio']['slug'] : '';
        $current_url = parent::url($slug . $url, $full);
        return $current_url;
    }

    public function Vitrine($marca = null)
    {
        if (!is_null($marca)) {
            $carros = array();
        } else {
            $carros = $this->Carro->cacheConditions('Carro.destaque', array('conditions' => array('Carro.status' => 1, 'Carro.destaque' => 1)))
                ->getCacheData();
        }
        return $carros;
    }

    public function MostrarMarcas()
    {
        return $this->Selecionado['Consorcio']['mostrar_marcas'] == 1;
    }

    public function MostrarVeiculos()
    {
        return $this->Selecionado['Consorcio']['mostrar_veiculos'] == 1;
    }

    public function Body()
    {
        return $this->Selecionado['Consorcio']['principal'] == 1 ? 'caixa' : $this->Selecionado['Consorcio']['class'];
    }

    public function CircleLogo()
    {
        if ($this->Selecionado['Consorcio']['principal'] != 1) {
            return '<img src="' . $this->Selecionado['Consorcio']['full_imagem_circular'] . '" />';
        }
        return '';
    }

    public function Logo()
    {
        $id = $this->Selecionado['Consorcio']['id'];
        $image = $this->Selecionado['Consorcio']['imagem'];
        if (is_file(WWW_ROOT . 'files' . DS . 'consorcios' . DS . $id . DS . $image))
            return Router::url('/files/consorcios/' . $id . '/' . $image, true);
        else {
            return Router::url('/assets/images/logos/dimabel.png', true);
        }
    }

    public function LogoCircular()
    {
        $id = $this->Selecionado['Consorcio']['id'];
        $image = $this->Selecionado['Consorcio']['imagem_circular'];
        if (is_file(WWW_ROOT . 'files' . DS . 'consorcios' . DS . $id . DS . $image))
            return Router::url('/files/consorcios/' . $id . '/' . $image, true);
        else {
            return Router::url('/assets/images/logos/dimabel.png', true);
        }
    }

    public function TemplateLogo()
    {
        $image = 'dimabel-' . $this->Selecionado['Consorcio']['slug'] . '.png';
        if (is_file(WWW_ROOT . 'assets' . DS . 'images' . DS . 'logos' . DS . $image))
            return Router::url('/assets/images/logos/' . $image, true);
        else {
            return Router::url('/assets/images/logos/dimabel.png', true);
        }
    }

    public function Parcela($valor, $taxa_tipo, $parcela)
    {
        if ($valor > 0) {
            $valor_taxado = $valor * $this->Selecionado['Consorcio'][$taxa_tipo] / 100 + $valor;
            $valor_parcela = $valor_taxado / $parcela;
            return $valor_parcela;
        }
        return 0;
    }

    public function ParcelaConsorcio($valor, $parcela, $especifico, $taxa_tipo = 'taxa_carro')
    {
        $consorcios = $this->Consorcio->cache()
            ->getCacheData();

        $parcelas = array();
        foreach ($consorcios as $consorcio) {
            $valor_taxado = $valor * $consorcio['Consorcio'][$taxa_tipo] / 100 + $valor;
            $valor_parcela = $valor_taxado / $parcela;
            $parcelas[$consorcio['Consorcio']['slug']] = $valor_parcela;
        }

        return $parcelas[$especifico];
    }

    public function ParcelaTaxaFixa($valor, $parcela, $taxa)
    {
        $valor_taxado =  $valor * $taxa / 100 + $valor;
        $valor_parcela = $valor_taxado / $parcela;
        return $valor_parcela;
    }

    # Pega de todos os consórcios
    public function Parcelas($valor = 0.00, $taxa_tipo = 'taxa_carro', $parcela_tipo = 'parcela_carro', $menor = true, $consorcio_especifico = false)
    {
        if ($valor > 0) {

//            Acionar essa query para pegar todos os banco e buscar o menor valor
//            $consorcios = $this->Consorcio->cache()
//                ->getCacheData();

            $consorcios[] = $this->Selecionado;

            $parcelas = array();
            foreach ($consorcios as $consorcio) {
                $valor_taxado = $valor * $consorcio['Consorcio'][$taxa_tipo] / 100 + $valor;
                $valor_parcela = $valor_taxado / $consorcio['Consorcio'][$parcela_tipo];
                $parcelas[$consorcio['Consorcio']['slug']] = $valor_parcela;
            }

            if ($menor) {
                if ($consorcio_especifico == false)
                    return min($parcelas);
                else {
                    return $parcelas[$consorcio_especifico];
                }
            } else {
                if ($consorcio_especifico == false)
                    return max($parcelas);
                else {
                    return $parcelas[$consorcio_especifico];
                }
            }

        }

        return 0;
    }

}