<?php
App::uses('AppModel', 'Model');
App::uses('CakeSession', 'Model/Datasource');

class Consorcio extends FdConsorciosAppModel
{

    private $image_not_found = '/assets/images/sem-imagem.png';

    private $imagem_dir = 'consorcio';

    public $validate = array(
        'nome' => array('notBlank'),
        'slug' => array(
            'rule' => 'isUnique',
            'message' => 'Este Consórcio já consta nos registros'
        ),
        'taxa_carro' => array('notBlank'),
        'taxa_imovel' => array('notBlank'),
        'parcela_carro' => array('notBlank'),
        'parcela_imovel' => array('notBlank'),
        'class' => array('notBlank')
    );

    public $actsAs = array(
        'Containable',
        'Upload.Upload' => array(
            'imagem' => array(
                'thumbnailMethod' => 'php',
                'fields' => array(
                    'dir' => 'imagem_dir'
                ),
                'thumbnailSizes' => array(
                    'thumb' => '175x95'
                )
            ),
            'imagem_circular' => array(
                'thumbnailMethod' => 'php',
                'fields' => array(
                    'dir' => 'imagem_circular_dir'
                ),
                'thumbnailSizes' => array(
                    'thumb' => '175x95',
                    'circle' => '209x209'
                )
            )
        )
    );

    private $Cache;

    # Cria o cache das Consorcio
    public function cache()
    {
        if (Cache::read('Consorcio') === false) {
            $this->Cache = $this->find('all',
                array(
                    'recursive' => -1,
                    'conditions' => array(
                        'Consorcio.status' => 1
                    )
                )
            );
            Cache::write('Consorcio', $this->Cache);
        } else {
            $this->Cache = Cache::read('Consorcio');
        }
        return $this;
    }

    public function firstCache($key, $options)
    {
        if (Cache::read($key) === false) {
            $this->Cache = $this->find('first', $options);
            Cache::write($key, $this->Cache);
        } else {
            $this->Cache = Cache::read($key);
        }
        return $this;
    }

    public function allConditions($key, $options)
    {
        if (Cache::read($key) === false) {
            $this->Cache = $this->find('all', $options);
            Cache::write($key, $this->Cache);
        } else {
            $this->Cache = Cache::read($key);
        }
        return $this;
    }

    public function getCacheData()
    {
        return $this->Cache;
    }

    public function extract($path, $data = null)
    {
        return Set::extract($path, !is_null($data) ? $data : $this->Cache);
    }

    public function beforeSave($options = array())
    {
        $data = $this->data[$this->alias];

        if (!empty($data['nome'])) {
            $this->data[$this->alias]['slug'] = strtolower(Inflector::slug($data['nome'], '-'));
            if (empty($data['id'])) {
                $this->data[$this->alias]['class'] = $this->data[$this->alias]['slug'];
            }
        }

        App::import('Helper', 'String');
        $stringHelper = new StringHelper(new View());

        if (!empty($data['taxa_carro'])) {
            $this->data[$this->alias]['taxa_carro'] = $stringHelper->moedaToBco($data['taxa_carro']);
        }

        if (!empty($data['taxa_imovel'])) {
            $this->data[$this->alias]['taxa_imovel'] = $stringHelper->moedaToBco($data['taxa_imovel']);
        }

        if (!empty($data['apos_valor_carro'])) {
            $this->data[$this->alias]['apos_valor_carro'] = $stringHelper->moedaToBco($data['apos_valor_carro']);
        }

        if (!empty($data['apos_valor_imovel'])) {
            $this->data[$this->alias]['apos_valor_imovel'] = $stringHelper->moedaToBco($data['apos_valor_imovel']);
        }

        if (!empty($data['parcelas_carro'])) {
            $this->data[$this->alias]['parcelas_carro'] = '[' . $data['parcelas_carro'] . ']';
        }

        if (!empty($data['parcelas_imoveis'])) {
            $this->data[$this->alias]['parcelas_imoveis'] = '[' . $data['parcelas_imoveis'] . ']';
        }

        if (!empty($data['parcelas_apos_valor_imovel'])) {
            $this->data[$this->alias]['parcelas_apos_valor_imovel'] = '[' . $data['parcelas_apos_valor_imovel'] . ']';
        }

        if (!empty($data['parcelas_apos_valor_carro'])) {
            $this->data[$this->alias]['parcelas_apos_valor_carro'] = '[' . $data['parcelas_apos_valor_carro'] . ']';
        }

        if ($data['principal'] == 1) {
            $this->updateAll(array('principal' => 0));
        }

        parent::beforeSave($options);
    }

    public function afterFind($results, $primary = false)
    {
        foreach ($results as $index => $result) {
            $current = $result[$this->alias];
            if (isset($current['imagem'])) {
                if (is_file(WWW_ROOT . 'files' . DS . $this->imagem_dir . DS . 'imagem' . DS . $current['imagem_dir'] . DS . $current['imagem'])) {
                    $results[$index][$this->alias]['full_imagem'] = Router::url('/files/' . $this->imagem_dir . '/imagem/' . $current['imagem_dir'] . '/' . $current['imagem'], true);
                    $results[$index][$this->alias]['full_thumb'] = Router::url('/files/' . $this->imagem_dir . '/imagem/' . $current['imagem_dir'] . '/thumb_' . $current['imagem'], true);
                } else {
                    $results[$index][$this->alias]['full_imagem'] = Router::url($this->image_not_found, true);
                }
            } else {
                $results[$index][$this->alias]['full_imagem'] = Router::url($this->image_not_found, true);
            }

            if (isset($current['imagem_circular'])) {
                if (is_file(WWW_ROOT . 'files' . DS . $this->imagem_dir . DS . 'imagem_circular' . DS . $current['imagem_circular_dir'] . DS . $current['imagem_circular'])) {
                    $results[$index][$this->alias]['full_imagem_circular'] = Router::url('/files/' . $this->imagem_dir . '/imagem_circular/' . $current['imagem_circular_dir'] . '/' . $current['imagem_circular'], true);
                    $results[$index][$this->alias]['full_thumb_circular'] = Router::url('/files/' . $this->imagem_dir . '/imagem_circular/' . $current['imagem_circular_dir'] . '/circle_' . $current['imagem_circular'], true);
                } else {
                    $results[$index][$this->alias]['full_imagem_circular'] = Router::url($this->image_not_found, true);
                }
            } else {
                $results[$index][$this->alias]['full_imagem_circular'] = Router::url($this->image_not_found, true);
            }
        }
        return parent::afterFind($results, $primary);
    }

    public function beforeDelete($cascade = true)
    {
        $principal = $this->find('first', array('conditions' => array('Consorcio.id' => $this->id, 'Consorcio.principal' => 1)));
        if ($principal) {
            $consorcio = $this->find('first', array('conditions' => array('Consorcio.status' => 1, 'Consorcio.id !=' => $this->id)));
            if ($consorcio) {
                $this->updateAll(array('principal' => 1), array('Consorcio.id' => $consorcio['Consorcio']['id']));
            }
        }
        return parent::beforeDelete($cascade);
    }

}