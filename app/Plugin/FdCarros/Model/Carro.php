<?php
App::uses('AppModel', 'Model');
App::uses('CakeSession', 'Model/Datasource');

class Carro extends FdCarrosAppModel
{

    private $image_not_found = '/assets/images/sem-imagem.png';

    public $validate = array(
        'nome' => array('notBlank'),
        'preco' => array('notBlank'),
        'slug' => array(
            'rule' => 'isUnique',
            'message' => 'Este carro já consta nos registros'
        )
    );

    public $actsAs = array(
        'Containable',
        'Upload.Upload' => array(
            'imagem' => array(
                'thumbnailMethod' => 'php',
                'fields' => array(
                    'dir' => 'imagem_dir'
                ),
                'thumbnailSizes' => array(
                    'thumb' => '175x95'
                )
            )
        )
    );

    private $Cache;

    public $belongsTo = array(
        'Marca' => array(
            'className' => 'Marca',
            'foreignKey' => 'marca_id'
        )
    );

    # Cria o cache das marcas
    public function cache()
    {
        if (Cache::read('Carro') === false) {
            $this->Cache = $this->find('all',
                array(
                    'conditions' => array(
                        'Carro.status' => 1
                    )
                )
            );
            Cache::write('Carro', $this->Cache);
        } else {
            $this->Cache = Cache::read('Carro');
        }
        return $this;
    }

    public function cacheConditions($key, $options)
    {
        if (Cache::read($key) === false) {
            $this->Cache = $this->find('all', $options);
            Cache::write($key, $this->Cache);
        } else {
            $this->Cache = Cache::read($key);
        }
        return $this;
    }

    public function getCacheData()
    {
        return $this->Cache;
    }

    public function beforeSave($options = array())
    {
        $data = $this->data[$this->alias];

        if (!empty($data['nome'])) {
            $this->data[$this->alias]['slug'] = strtolower(Inflector::slug($data['nome'], '-'));
        }
        if (!empty($data['preco'])) {

            App::import('Helper', 'String');
            $StringHelper = new StringHelper(new View());

            $this->data[$this->alias]['preco'] = $StringHelper->moedaToBco($data['preco']);
        }

        parent::beforeSave($options);
    }

    public function afterFind($results, $primary = false)
    {
        foreach ($results as $index => $result) {
            $current = $result[$this->alias];
            if (isset($current['imagem'])) {
                if (is_file(WWW_ROOT . 'files' . DS . 'carro' . DS . 'imagem' . DS . $current['imagem_dir'] . DS . $current['imagem'])) {
                    $results[$index][$this->alias]['full_imagem'] = Router::url('/files/carro/imagem/' . $current['imagem_dir'] . '/' . $current['imagem'], true);
                    $results[$index][$this->alias]['full_thumb'] = Router::url('/files/carro/imagem/' . $current['imagem_dir'] . '/thumb_' . $current['imagem'], true);
                } else {
                    $results[$index][$this->alias]['full_imagem'] = Router::url($this->image_not_found, true);
                }
            }
        }
        return $results;
    }

}