<?php

class FdCarrosController extends FdCarrosAppController
{

    public $uses = array('FdCarros.Carro', 'FdMarcas.Marca');

    public function fatorcms_index($page = 1)
    {

        $this->FilterResults->addFilters(
            array(
                'filter' => array(
                    'OR' => array(
                        'Carro.nome' => array('operator' => 'LIKE', 'value' => array('before' => '%', 'after' => '%')),
                    )
                ),
                'filtro_nome' => array(
                    'Carro.nome' => array('operator' => 'LIKE', 'value' => array('before' => '%', 'after' => '%'))
                )
            )
        );

        $this->FilterResults->setPaginate('page', $page);
        $this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

        $paginate = $this->paginate();

        $this->set('paginate', $paginate);

    }

    public function fatorcms_add()
    {
        if ($this->request->is('post')) {
            $this->Carro->create($this->request->data);
            if ($this->Carro->save()) {
                $this->Session->setFlash(__('Registro salvo com sucesso.'), 'fatorcms_success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'), 'fatorcms_danger');
            }
        }

        $marcas = $this->Marca->find('list', array('fields' => array('Marca.id', 'Marca.nome'), 'conditions' => array('Marca.status' => 1)));
        $this->set(compact('marcas'));
    }

    public function fatorcms_edit($id = null)
    {
        if ($this->request->is('PUT')) {
            $this->Carro->id;
            if ($this->Carro->save($this->request->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso.'), 'fatorcms_success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'), 'fatorcms_danger');
            }
        }

        $edit = $this->Carro->findById($id);
        if (!$edit) {
            $this->Session->setFlash(__('Registro não encontrado'), 'fatorcms_danger');
            $this->redirect(array('action' => 'index'));
        }
        $this->data = $edit;

        $marcas = $this->Marca->find('list', array('fields' => array('Marca.id', 'Marca.nome'), 'conditions' => array('Marca.status' => 1)));
        $this->set(compact('marcas'));
    }


    public function fatorcms_delete($id = null) {
        $this->Carro->id = $id;
        if (!$this->Carro->exists()) {
            throw new NotFoundException('Registro inválido.');
        }
        $this->request->is('get');
        if ($this->Carro->delete()) {
            $this->_resetCaches();
            $this->Session->setFlash(__('Registro deletado.'), 'fatorcms_success');
            // $this->redirect(array('action' => 'index'));
            $this->_redirectFilter($this->referer());
        }
        $this->Session->setFlash(__('Registro não pode ser deletado.'), 'fatorcms_warning');
        // $this->redirect(array('action' => 'index'));
        $this->_redirectFilter($this->referer());
    }

    public function fatorcms_status(){
        if (!$this->request->is('post')){
            throw new NotFoundException('Registro inválido.');
        }
        echo $this->_saveStatus('Carro', $this->request->data['id'], $this->request->data['value']);
        $this->_resetCaches();
        die;
    }
}