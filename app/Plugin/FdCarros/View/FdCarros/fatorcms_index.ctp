<?php $this->Html->addCrumb('Carros'); ?>

<div class="row">
    <h3 class="col-lg-12 pull-left">
		Carros
		<?php echo $this->Html->link('Cadastrar Carro', array('action' => 'add'), array('class' => 'btn btn-info pull-right')); ?>
	</h3>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <div class="row-fluid">
                    <div class="col-sm-12 col-md-10 col-lg-11">
                        <?php echo $this->FilterForm->create(null, array('role' => 'form', 'class' => 'form-inline')) ?>
                            <div class="form-group">
                                <?php echo $this->FilterForm->input('filtro_nome', array('placeholder' => 'Filtrar por nome', 'class' => 'form-control')) ?>
                            </div>
                            <?php echo $this->FilterForm->submit('Filtrar', array('class' => 'btn btn-success', 'div' => false)) ?>
                            <?php echo $this->Html->link('Limpar Filtro', array('action' => 'index'), array('class' => 'btn btn-warning')) ?>
                        <?php echo $this->FilterForm->end() ?>
                    </div>
                    <div class="col-lg-1 visible-md visible-lg">
                       <?php echo $this->Element('/fatorcms/limit'); ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <table class="table table-hover general-table">
                    <thead>
                        <tr>
                            <th style="width:60px"><?php echo $this->Paginator->sort('id', '#') ?></th>
                            <th>Imagem</th>
                            <th><?php echo $this->Paginator->sort('nome') ?></th>
                            <th><?php echo $this->Paginator->sort('marca_id', 'Marca') ?></th>
                            <th><?php echo $this->Paginator->sort('preco', 'Preço') ?></th>
                            <th>Destaque</th>
                            <th>Mais vendido</th>
							<th><?php echo $this->Paginator->sort('status') ?></th>
                            <th style="width:180px">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($paginate) > 0): ?>
                            <?php foreach($paginate AS $data): ?>
                                <?php $current = $data['Carro'] ?>
                                <tr>
                                    <td><?php echo $current['id'] ?></td>
                                    <td>
                                        <img src="<?php echo $current['full_imagem'] ?>" width="60" alt="" />
                                    </td>
                                    <td><?php echo $current['nome'] ?></td>
                                    <td><?php echo $data['Marca']['nome'] ?></td>
                                    <td>R$ <?php echo $this->String->bcoToMoeda($current['preco']) ?></td>
                                    <td><?php echo $current['destaque'] ? 'Sim' : 'Não' ?></td>
                                    <td><?php echo $current['mais_vendido'] ? '<strong class="text-info">Sim</strong>' : 'Não' ?></td>
                                    <td>
                                        <input type="checkbox" class="atualiza-status" value="<?php echo $current['status'] == 1 ? 0 : 1 ?>"
                                               data-id="<?php echo $current['id'] ?>" data-url="<?php echo $this->Html->url(array('action' => 'status')) ?>"
                                               data-on-text="Sim" data-on-color="success" data-off-text="Não"
                                               data-off-color="danger"<?php echo $current['status'] == 1 ? ' checked="checked"' : '' ?>>
                                    </td>
                                    <td>
                                        <?php echo $this->Html->link('<i class="fa fa-edit"></i> Editar', array('action' => 'edit', $current['id']), array('class' => 'btn btn-primary btn-sm', 'escape' => false)) ?>
                                        <?php echo $this->Html->link('<i class="fa fa-trash-o"></i> Remover', array('action' => 'delete', $current['id']), array('class' => 'btn btn-danger btn-sm', 'escape' => false), __('Deseja mesmo remover o registro # %s?', $current['nome'])) ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="5" class="text-center">Nenhum registro encontrado.</td>
                            </tr>
                        <?php endIf; ?>
                    </tbody>
                </table>

                <?php echo $this->Element('/fatorcms/paginator'); ?>
            </div>
        </section>
    </div>
</div>