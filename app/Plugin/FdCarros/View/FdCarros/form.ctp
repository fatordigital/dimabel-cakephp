<div class="panel">
    <div class="panel-body">

        <div class="row">
            <div class="col-md-6 col-lg-4">
                <div class="form-group">
                    <label>Status</label>
                    <div class="icheck">
                        <?php echo $this->Form->input('status', array(
                            'type' => 'radio',
                            'options' => array(1 => 'Ativo', 0 => 'Inativo'),
                            'default' => 1,
                            'legend' => false,
                            'before' => '<div class="radio">',
                            'after' => '</div>',
                            'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="form-group">
                    <label>
                        Destaque
                    </label>
                    <div class="icheck">
                        <?php echo $this->Form->input('destaque', array(
                            'type' => 'radio',
                            'options' => array(
                                1 => 'Ativo <i class="fa fa-question-circle text-danger" title="Irá aparecer na página inicial e dentro da marca que ele for relacionado"></i>',
                                0 => 'Inativo <i class="fa fa-question-circle text-danger" title="Não irá aparecer na página inicial, mas, irá aparecer dentro da marca que ele for relacionado"></i>'
                            ),
                            'default' => 0,
                            'legend' => false,
                            'before' => '<div class="radio">',
                            'after' => '</div>',
                            'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="form-group">
                    <label>Mais vendido</label>
                    <div class="icheck">
                        <?php echo $this->Form->input('mais_vendido', array(
                            'type' => 'radio',
                            'options' => array(
                                1 => 'Ativo <i class="fa fa-question-circle text-danger" title="Na apresentação do carro o mesmo vai ficar destacado como mais vendido"></i>',
                                0 => 'Inativo <i class="fa fa-question-circle text-danger" title="Vai aparecer normalmente"></i>'
                            ),
                            'default' => 0,
                            'legend' => false,
                            'before' => '<div class="radio">',
                            'after' => '</div>',
                            'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="">
                        Nome <strong class="text-danger">*</strong>
                    </label>
                    <?php echo $this->Form->input('nome', array('required', 'class' => 'form-control', 'placeholder' => 'Nome do carro', 'label' => false, 'legend' => false)) ?>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="">
                        Marca <strong class="text-danger">*</strong>
                    </label>
                    <?php echo $this->Form->input('marca_id', array(
                        'required',
                        'options' => $marcas,
                        'class' => 'form-control',
                        'placeholder' => 'Nome do carro', 'label' => false, 'legend' => false
                    )) ?>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="">
                        Preço <strong class="text-danger">*</strong>
                    </label>
                    <?php echo $this->Form->text('preco', array('required', 'class' => 'form-control money1', 'placeholder' => '0,00', 'label' => false, 'legend' => false)) ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <label>Imagem</label>
                    <div class="fileupload fileupload-new" data-provides="fileupload">

                        <?php if(isset($this->data['Carro']['imagem']) && $this->data['Carro']['imagem'] != ""): ?>
                            <div class="fileupload-new thumbnail" style="width: 200px; height: 119px;">
                                <img src="<?php echo $this->data['Carro']['full_imagem'] ?>" width="200" />
                            </div>
                        <?php else: ?>
                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=nenhuma+imagem" alt="" />
                            </div>
                        <?php endIf; ?>
                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>

                        <?php if(isset($this->data['Carro']['imagem']) && $this->data['Carro']['imagem'] != ""): ?>
                            <div class="icheck minimal">
                                <?php echo $this->Form->input('Carro.imagem.remove', array('type' => 'checkbox', 'label' => 'Remover Imagem Atual', 'div' => false)); ?>
                            </div>
                        <?php endIf; ?>

                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=nenhuma+imagem" alt="" />
                        </div>
                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                        <div>
	                       <span class="btn btn-white btn-file">
	                           	<span class="fileupload-new"><i class="fa fa-paper-clip"></i> Selecionar imagem</span>
	                           	<span class="fileupload-exists"><i class="fa fa-undo"></i> Alterar</span>
                               <?php echo $this->Form->input("imagem", array('type' => 'file', 'class' => 'default', 'label' => false, 'div' => false)); ?>
                               <?php echo $this->Form->hidden("imagem_dir") ?>
	                       </span>
                            <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remover</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php echo $this->Form->submit('Salvar', array('class' => 'btn btn-success', 'div' => false)) ?>

        <a href="<?php echo $this->Html->Url(array('action' => 'index')); ?>" class="btn btn-default">Cancelar</a>

    </div>
</div>