<?php
	Router::connect("/fatorcms/paginas", array('plugin' => 'fd_paginas', 'controller' => 'fd_paginas', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));
    Router::connect("/fatorcms/paginas/:action/*", array('plugin' => 'fd_paginas', 'controller' => 'fd_paginas', 'prefix' => 'fatorcms', 'fatorcms' => true, 'abas'=> false));
    
    //abas
    Router::connect("/fatorcms/abas/paginas/:action/*", array('plugin' => 'fd_paginas', 'controller' => 'fd_paginas', 'prefix' => 'fatorcms', 'fatorcms' => true, 'abas'=> true));

    Router::connect("/fatorcms/pagina_tipos", array('plugin' => 'fd_paginas', 'controller' => 'fd_pagina_tipos', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));
    Router::connect("/fatorcms/pagina_tipos/:action/*", array('plugin' => 'fd_paginas', 'controller' => 'fd_pagina_tipos', 'prefix' => 'fatorcms', 'fatorcms' => true));
    

    