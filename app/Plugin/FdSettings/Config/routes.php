<?php
	Router::connect("/fatorcms/settings", array('plugin' => 'fd_settings', 'controller' => 'fd_settings', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));
    Router::connect("/fatorcms/settings/:action/*", array('plugin' => 'fd_settings', 'controller' => 'fd_settings', 'prefix' => 'fatorcms', 'fatorcms' => true));