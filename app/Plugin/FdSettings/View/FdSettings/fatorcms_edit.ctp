<?php $this->Html->addCrumb('Configurações', array('action' => 'index')) ?>
<?php $this->Html->addCrumb('Editar Configuração') ?>

<h3>Editar Configuração</h3>

<div class="panel">
	<div class="panel-body">
	<?php echo $this->Form->create('Setting', array('type' => 'file', 'role' => 'form', 'class' => 'minimal', 'novalidate' => true)) ?>
		<?php echo $this->Form->input('id') ?>
		<div class="row">
			<div class="col-md-6 col-lg-6">
				<div class="form-group">
					<label>Status</label>
					<div class="icheck">
						<?php echo $this->Form->input('status', array(
																	'type' => 'radio', 
																	'options' => array(1 => 'Ativo', 0 => 'Inativo'), 
																	'default' => 1, 
																	'legend' => false, 
																	'before' => '<div class="radio">', 
																	'after' => '</div>', 
																	'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<?php echo $this->Form->hidden('key', array('value' => $this->data['Setting']['key'])) ?>
					<?php echo $this->Form->input('key2', array('label' => 'Chave', 'div' => false, 'class' => 'form-control', 'value' => $this->data['Setting']['key'], 'disabled' => 'disabled')) ?>
				</div>
			</div>
		</div>
		<?php if($this->data['Setting']['label_visivel'] == true): ?>
			<div class="row">
				<div class="col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('label', array('label' => 'Label', 'div' => false, 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
		<?php endIF; ?>
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<?php echo $this->Form->input('value', array('label' => 'Valor', 'div' => false, 'class' => 'form-control')) ?>
				</div>
			</div>
		</div>
		<?php echo $this->Form->submit('Salvar', array('class' => 'btn btn-success', 'div' => false)) ?>
		<a href="<?php echo $this->Html->Url(array('action' => 'index')); ?>" class="btn btn-default">Cancelar</a>
	<?php echo $this->Form->end() ?>
	</div>
</div>