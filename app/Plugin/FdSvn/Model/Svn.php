<?php
App::uses('AppModel', 'Model');
App::uses('CakeSession', 'Model/Datasource');

class Svn extends FdSvnAppModel {

	public $useTable = "svn";

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'tabela' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo de preenchimento obrigatório.',
			),
		),
		'conteudo' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo de preenchimento obrigatório.',
			),
		),
		'registro_id' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo de preenchimento obrigatório.',
			),
		),
		'usuario_id' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo de preenchimento obrigatório.',
			),
		),
	);

//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Usuario' => array(
			'className' => 'FdUsuarios.Usuario',
			'foreignKey' => 'usuario_id',
			'conditions' => '',
            'fields' => '',
		),
	);

}