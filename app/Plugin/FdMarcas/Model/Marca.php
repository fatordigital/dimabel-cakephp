<?php
App::uses('AppModel', 'Model');
App::uses('CakeSession', 'Model/Datasource');

class Marca extends FdMarcasAppModel
{

    private $image_not_found = '/assets/images/sem-imagem.png';

    public $validate = array(
        'nome' => array('notBlank'),
        'slug' => array(
            'rule' => 'isUnique',
            'message' => 'Esta marca já consta nos registros'
        )
    );

    public $actsAs = array(
        'Containable',
        'Upload.Upload' => array(
            'imagem' => array(
                'thumbnailMethod' => 'php',
                'fields' => array(
                    'dir' => 'imagem_dir'
                ),
                'thumbnailSizes' => array(
                    'thumb' => '175x95'
                )
            )
        )
    );

    private $Cache;

    public $hasMany = array(
        'Carro' => array(
            'className' => 'Carro',
            'foreignKey' => 'marca_id'
        )
    );

    # Cria o cache das marcas
    public function cache()
    {
        if (Cache::read('Marca') === false) {
            $this->Cache = $this->find('all',
                array(
                    'conditions' => array(
                        'Marca.status' => 1
                    )
                )
            );
            Cache::write('Marca', $this->Cache);
        } else {
            $this->Cache = Cache::read('Marca');
        }
        return $this;
    }

    public function firstCache($key, $conditions)
    {
        if (Cache::read($key) === false) {
            $this->Cache = $this->find('first', $conditions);
            Cache::write($key, $this->Cache);
        } else {
            $this->Cache = Cache::read($key);
        }
        return $this;
    }

    public function getCacheData()
    {
        return $this->Cache;
    }

    public function beforeSave($options = array())
    {
        $data = $this->data[$this->alias];

        if (!empty($data['nome'])) {
            $this->data[$this->alias]['slug'] = strtolower(Inflector::slug($data['nome'], '-'));
        }

        parent::beforeSave($options);
    }

    public function afterFind($results, $primary = false)
    {
        foreach ($results as $index => $result) {
            $current = $result[$this->alias];
            if (isset($current['imagem'])) {
                if (is_file(WWW_ROOT . 'files' . DS . 'marca' . DS . 'imagem' . DS . $current['imagem_dir'] . DS . $current['imagem'])) {
                    $results[$index][$this->alias]['full_imagem'] = Router::url('/files/' . 'marca' . '/imagem/' . $current['imagem_dir'] . '/' . $current['imagem'], true);
                    $results[$index][$this->alias]['full_thumb'] = Router::url('/files/' . 'marca' . '/imagem/' . $current['imagem_dir'] . '/thumb_' . $current['imagem'], true);
                } else {
                    $results[$index][$this->alias]['full_imagem'] = Router::url($this->image_not_found, true);
                }
            } else {
                $results[$index][$this->alias]['full_imagem'] = Router::url($this->image_not_found, true);
            }
        }
        return parent::afterFind($results, $primary);
    }

}