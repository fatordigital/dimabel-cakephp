<?php echo $this->Html->script('fatorcms/jquery.stringToSlug.min'); ?>
<?php echo $this->Html->script('/js/fatorcms/core/jquery-character-counter/jquery.charactercounter'); ?>

<?php echo $this->Html->css('/js/fatorcms/core/bootstrap-fileupload/bootstrap-fileupload') ?>
<?php echo $this->Html->script('/js/fatorcms/core/bootstrap-fileupload/bootstrap-fileupload'); ?>

<?php $this->Html->addCrumb('Marca', array('action' => 'index')) ?>
<?php $this->Html->addCrumb('Editar Marca') ?>

<h3>Editar Marca</h3>

<?php
    echo $this->Form->create('Marca', array('type' => 'file', 'role' => 'form', 'class' => 'minimal'));
        echo $this->Form->hidden('id');
        include 'form.ctp';
    echo $this->Form->end() ;
?>
