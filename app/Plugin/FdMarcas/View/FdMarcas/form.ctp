<div class="panel">
    <div class="panel-body">

        <div class="row">
            <div class="col-md-6 col-lg-4">
                <div class="form-group">
                    <label>Status</label>
                    <div class="icheck">
                        <?php echo $this->Form->input('status', array(
                            'type' => 'radio',
                            'options' => array(1 => 'Ativo', 0 => 'Inativo'),
                            'default' => 1,
                            'legend' => false,
                            'before' => '<div class="radio">',
                            'after' => '</div>',
                            'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="">
                        Nome <strong class="text-danger">*</strong>
                    </label>
                    <?php echo $this->Form->input('nome', array('required', 'class' => 'form-control', 'placeholder' => 'Nome do Marca', 'label' => false, 'legend' => false)) ?>
                </div>
            </div>
        </div>
		
		<div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="">
                        Descri��o
                    </label>
                    <?php echo $this->Form->textarea('legenda', array('class' => 'form-control editor', 'label' => false, 'legend' => false)) ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="">
                        Descrição
                    </label>
                    <?php echo $this->Form->textarea('legenda', array('class' => 'form-control editor', 'label' => false, 'legend' => false)) ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <label>Imagem</label>
                    <div class="fileupload fileupload-new" data-provides="fileupload">

                        <?php if(isset($this->data['Marca']['imagem']) && $this->data['Marca']['imagem'] != ""): ?>
                            <div class="fileupload-new thumbnail" style="width: 200px; height: 119px;">
                                <img src="<?php echo $this->data['Marca']['full_imagem'] ?>" width="200" />
                            </div>
                        <?php else: ?>
                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=nenhuma+imagem" alt="" />
                            </div>
                        <?php endIf; ?>
                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>

                        <?php if(isset($this->data['Marca']['imagem']) && $this->data['Marca']['imagem'] != ""): ?>
                            <div class="icheck minimal">
                                <?php echo $this->Form->input('Marca.imagem.remove', array('type' => 'checkbox', 'label' => 'Remover Imagem Atual', 'div' => false)); ?>
                            </div>
                        <?php endIf; ?>

                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=nenhuma+imagem" alt="" />
                        </div>
                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                        <div>
	                       <span class="btn btn-white btn-file">
	                           	<span class="fileupload-new"><i class="fa fa-paper-clip"></i> Selecionar imagem</span>
	                           	<span class="fileupload-exists"><i class="fa fa-undo"></i> Alterar</span>
                               <?php echo $this->Form->input("imagem", array('type' => 'file', 'class' => 'default', 'label' => false, 'div' => false)); ?>
                               <?php echo $this->Form->hidden("imagem_dir") ?>
	                       </span>
                            <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remover</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php echo $this->Form->submit('Salvar', array('class' => 'btn btn-success', 'div' => false)) ?>

        <a href="<?php echo $this->Html->Url(array('action' => 'index')); ?>" class="btn btn-default">Cancelar</a>

    </div>
</div>