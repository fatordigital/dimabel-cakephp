<?php $this->Html->addCrumb('Rotas', array('action' => 'index')) ?>
<?php $this->Html->addCrumb('Editar Rota') ?>

<h3>Editar Rota</h3>

<div class="panel">
	<div class="panel-body">
	<?php echo $this->Form->create('Rota', array('type' => 'file', 'role' => 'form', 'class' => 'minimal', 'novalidate' => true)) ?>
		<?php echo $this->Form->input('id') ?>
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					<?php echo $this->Form->input('controller', array('label' => 'Controller', 'div' => false, 'class' => 'form-control', 'disabled' => 'disabled')) ?>
					<?php echo $this->Form->hidden('controller'); ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					<?php echo $this->Form->input('model', array('label' => 'Model', 'div' => false, 'class' => 'form-control', 'disabled' => 'disabled')) ?>
					<?php echo $this->Form->hidden('model'); ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					<?php echo $this->Form->input('action', array('label' => 'Action', 'div' => false, 'class' => 'form-control', 'disabled' => 'disabled')) ?>
					<?php echo $this->Form->hidden('action'); ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					<?php echo $this->Form->input('row_id', array('label' => 'Row', 'type' => 'text', 'div' => false, 'class' => 'form-control', 'disabled' => 'disabled')) ?>
					<?php echo $this->Form->hidden('action'); ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					<?php echo $this->Form->input('seo_url', array('label' => 'URL', 'div' => false, 'class' => 'form-control')) ?>
				</div>
			</div>
		</div>

		<?php if(isset($this->data[$this->data['Rota']['model']]) && count($this->data[$this->data['Rota']['model']]) > 0): ?>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<?php echo $this->Form->hidden("{$this->data['Rota']['model']}.id"); ?>
						<?php echo $this->Form->input("{$this->data['Rota']['model']}.seo_title", array('div' => false, 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<?php echo $this->Form->input("{$this->data['Rota']['model']}.seo_description", array('type' => 'textarea', 'div' => false, 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<?php echo $this->Form->input("{$this->data['Rota']['model']}.seo_keywords", array('type' => 'textarea', 'div' => false, 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
		<?php endIf; ?>

		<?php echo $this->Form->submit('Salvar', array('class' => 'btn btn-success', 'div' => false)) ?>
		<a href="<?php echo $this->Html->Url(array('action' => 'index')); ?>" class="btn btn-default">Cancelar</a>
	<?php echo $this->Form->end() ?>
	</div>
</div>