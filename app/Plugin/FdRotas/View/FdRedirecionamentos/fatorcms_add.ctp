<?php $this->Html->addCrumb('Redirecionamentos', array('action' => 'index')) ?>
<?php $this->Html->addCrumb('Cadastrar Redirecionamento') ?>

<h3>Criar Redirecionamento</h3>

<div class="panel">
	<div class="panel-body">
	<?php echo $this->Form->create('Redirecionamento', array('type' => 'file', 'role' => 'form', 'class' => 'minimal', 'novalidate' => true)) ?>
		<?php echo $this->Form->input('id') ?>
		<div class="row">
			<div class="col-md-6 col-lg-6">
				<div class="form-group">
					<?php echo $this->Form->input('tipo', array('div' => false, 'class' => 'form-control', 'options' => array('204'=>'204','301'=>'301','302'=>'302','303'=>'303','307'=>'307','403'=>'403','404'=>'404','503'=>'503'), 'default' => '301')) ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					<?php echo $this->Form->input('url_antiga', array(
																	'class' 				=> 'form-control popovers', 
																	'type'					=> 'text',
																	'data-original-title' 	=> 'Lembrete!', 
																	'data-content' 			=> "Cadastre apenas o referer da url. Ex: se a url antiga for a 'www.dominio.com.br/casa/k1', digite apenas 'casa/k1' nessa campo.", 
																	'data-placement' 		=> 'top', 
																	'data-trigger' 			=> 'hover'
																	)) ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					<?php echo $this->Form->input('url_nova', array(
																	'class' 				=> 'form-control popovers',
																	'type'					=> 'text',
																	'data-original-title' 	=> 'Lembrete!', 
																	'data-content' 			=> "Cadastre apenas o referer da url. Ex: se a url nova for a 'www.dominio.com.br/casas/casa-k1', digite apenas 'casas/casa-k1' nessa campo.", 
																	'data-placement' 		=> 'top', 
																	'data-trigger' 			=> 'hover'
																)) ?>
				</div>
			</div>
		</div>
		<?php echo $this->Form->submit('Salvar', array('class' => 'btn btn-success', 'div' => false)) ?>
		<a href="<?php echo $this->Html->Url(array('action' => 'index')); ?>" class="btn btn-default">Cancelar</a>
	<?php echo $this->Form->end() ?>
	</div>
</div>