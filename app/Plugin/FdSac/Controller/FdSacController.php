<?php

class FdSacController extends FdSacAppController {

	public $uses = array('FdSac.Sac');

/**
 * admin_index method
 *
 * @return void
 */
	public function fatorcms_index($page = 1) {

		// Lista os tipos de sac
		App::import('Model', 'FdSac.SacTipo');
		$this->SacTipo = new SacTipo();
		$sacTipos = $this->SacTipo->find('list', array('fields' => array('SacTipo.id', 'SacTipo.nome'), 'conditions' => array('SacTipo.status' => true), 'order' => array('SacTipo.nome ASC')));

		// Add filter
		$this->FilterResults->addFilters(
			array(
				'filter' => array(
					'OR' => array(
						'Sac.nome'   => array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' )),
						'Sac.email'   => array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' )),
					)
				),
				'filtro_nome' => array(
					'Sac.nome'     	=> array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' ))
				),
				'filtro_email' => array(
					'Sac.email'     	=> array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' ))
				),
				'filtro_sac_tipo_id' => array(
					'Sac.sac_tipo_id'    => array('select' => $this->FilterResults->select('Tipo de Sac...', $sacTipos))
				),
			)
		);

		$this->FilterResults->setPaginate('page', $page);
		// Define conditions
		$this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

		// Paginate
		$this->Sac->recursive = 0;
		$sacs = $this->paginate();

		$this->set('sacs', $sacs);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fatorcms_view($id = null) {
		if (!$this->Sac->exists($id)) {
            throw new NotFoundException('Registro inválido.');
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Sac->save($this->request->data, false)) {
				$this->Session->setFlash(__('Registro salvo com sucesso.'), 'fatorcms_success');
			} else {
				$this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'), 'fatorcms_danger');
			}
		}

		// set sac
		$options = array('conditions' => array('Sac.' . $this->Sac->primaryKey => $id));
		$sac = $this->Sac->find('first', $options);
		$this->request->data = $sac;
		$this->set(compact('sac'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fatorcms_delete($id = null) {
		$this->Sac->id = $id;
		if (!$this->Sac->exists()) {
            throw new NotFoundException('Registro inválido.');
		}
		$this->request->is('get');
		if ($this->Sac->delete()) {
			$this->Session->setFlash(__('Registro deletado.'), 'fatorcms_success');
			// $this->redirect(array('action' => 'index'));
			$this->_redirectFilter($this->referer());
		}
		$this->Session->setFlash(__('Registro não pode ser deletado.'), 'fatorcms_warning');
		// $this->redirect(array('action' => 'index'));
		$this->_redirectFilter($this->referer());
	}
}