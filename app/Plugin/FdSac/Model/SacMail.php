<?php
if (class_exists('App')) {
    App::uses('CakeEmail', 'Network/Email');
}

/**
 * Class SacMail
 *
 * @author Fator Digital
 * @version 0.1
 *
 * Classe criada para auxiliar no envio de e-mails, utilizando como base o padrão já existente.
 */
class SacMail
{
    private $dados;
    private $StringHelper;
    private $Template;
    private $SacTipo;
    private $Content = null;
    private $Sac;

    private $replyTo;
    private $Status;
    private $Email;
    private $CC;

    public function __construct($sac_tipo_id = null, $template_id = null)
    {
        App::import('Helper', 'String');
        $this->StringHelper = new StringHelper(new View());

        $this->Template = ClassRegistry::init('FdEmails.Template');
        $this->SacTipo = ClassRegistry::init('FdSac.SacTipo');
        $this->Sac = ClassRegistry::init('FdSac.Sac');

        if (!is_null($sac_tipo_id)) {
            $this->setSacTipo($sac_tipo_id);
        }
        if (!is_null($template_id)) {
            $this->setTemplate($template_id);
        }

        if (class_exists('CakeEmail')) {

            $this->Email = new CakeEmail('smtp');
            $this->Email->emailFormat('html');

            /* Seta o padrão ou passa na chamada */
            $this->setFrom();
        }
    }

    /**
     * @param $name
     * @param $arguments
     * @return $this
     *
     * Cria o metódo mágico __call que interceptar todos os eventos que a classe recebe
     */
    public function __call($name, $arguments)
    {
        if (preg_match('/^get(.+)/', $name, $matches)) {
            $var_name = strtolower($matches[1]);
            return $this->$var_name ? $this->$var_name : $arguments[0];
        }
        if (preg_match('/^set(.+)/', $name, $matches)) {
            $var_name_sneak = Inflector::underscore($matches[1]);
            $var_name = strtolower($var_name_sneak);
            $this->$var_name = $arguments[0];
        }
        return $this;
    }

    /**
     * @param $property
     * @return $this
     *
     * Metodo mágico que retorna a propriedade se caso ela existir
     */
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
        return $this;
    }

    /**
     * @param $property
     * @param $value
     * @return $this
     *
     * Intercepta todos os metódos que iniciam com set
     */
    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        } else {
            $property_to_upper = strtoupper($property);
            $this->dados[$property_to_upper] = $value;
        }
        return $this;
    }

    /**
     * @param null $content
     * @return $this
     *
     * String, id da template ou o caminho para um arquivo
     */
    public function setContent($content = null)
    {
        $template_render = (!is_null($content)) ? (is_int($content) ? $this->setTemplate($content) : $content) : APP . DS . WEBROOT_DIR . DS . 'templates' . DS . 'fale_conosco.html';
        $this->Content = (is_file($template_render)) ? file_get_contents($template_render) : $template_render;
        return $this;
    }

    /**
     * @param $id
     * @return $this
     * @throws Exception
     *
     * Seleciona o SacTipo
     */
    public function setSacTipo($id)
    {
        $this->SacTipo = $this->SacTipo->findById($id);
        if (!$this->SacTipo) {
            throw new Exception("SacTipo {$id} não encontrado.");
        }
        $this->dados['sac_tipo_id'] = $id;
        return $this;
    }

    /**
     * @param null $email
     * @return $this
     *
     * Seta os e-mails de cópia oculta
     */
    public function setCCs($email = null)
    {
        $this->CC[] = $email;
        return $this;
    }

    /**
     * @param $email
     * @return $this
     *
     * Seta o e-mail que irá receber a resposta
     */
    public function setReplyTo($email)
    {
        $this->replyTo = $email;
        return $this;
    }

    /**
     * @param $id
     * @return $this
     * @throws Exception
     *
     * Seleciona a template salva no banco de dados
     */
    public function setTemplate($id)
    {
        $this->Template = $this->Template->findById($id);
        if (!$this->Template) {
            throw new Exception("Template {$id} não encontrado.");
        }
        return $this;
    }

    /**
     * @return mixed
     *
     * Retorna o conteúdo após setado
     */
    public function getContent()
    {
        if ($this->Template) {
            $this->Content = $this->Template['Template']['conteudo'];
        }
        return $this->Content;
    }

    /**
     * @return mixed
     * @throws Exception
     *
     * Verifica quais as variáveis possíveis ser registradas
     */
    public function showAllKeys()
    {
        if (!is_null($this->getContent())) {
            preg_match_all('~{(.*?)}~si', $this->Content, $match);
            if (isset($match[0]) && count($match[0]) > 0) {
                return $match[0];
            } else {
                throw new Exception('Nenhuma chave encontrada no registro.');
            }
        } else {
            throw new Exception('Conteúdo está vazio.');
        }
    }

    /**
     * @return mixed
     *
     * Retorna todas os dados registrados
     */
    public function getDados()
    {
        return $this->dados;
    }

    /**
     * @return array
     *
     * Retorna todos os campos para serem inseridos do SAC
     */
    public function getSchema()
    {
        $schema = array_keys($this->Sac->schema());
        return $schema;
    }

    /**
     * @return mixed
     *
     * Converte todas as chaves da string para os valores passados em dados
     */
    public function templateValues()
    {
        return $this->StringHelper->stringReplace($this->getContent(), $this->getDados());
    }

    /**
     * Verifica se faltou alguma chave a ser preenchida
     */
    public function validate()
    {
        preg_match_all('~{(.*?)}~si', $this->templateValues(), $match);
        if (isset($match[0]) && count($match[0]) > 0) {
            echo '<pre>';
            echo 'Algumas chaves não foram encontradas. <br>';
            print_r($match[0]);
            exit;
        }
    }

    /**
     * @return bool
     *
     * Salva no banco de dados
     */
    public function Save()
    {
        $sac_fields = array_flip($this->getSchema());
        $data_fields = array_change_key_case($this->getDados(), CASE_LOWER);
        $save_fields = array_intersect_key($data_fields, $sac_fields);
        $this->Sac->create($save_fields);
        if ($this->Sac->save(null, false)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Monta o envio
     */
    public function SMTP()
    {
        if (class_exists('CakeEmail')) {

            if ($this->CC)
                $this->Email->cc($this->CC);

            return $this->Email->to($this->SacTipo['SacTipo']['email'])
                ->subject($this->SacTipo['SacTipo']['nome'])
                ->send($this->templateValues());

        } else {
            return false;
        }
    }

    public function setFrom($email = null, $nome = null)
    {
        $email = is_null($email) ? Configure::read('Site.from.email') : $email;
        $nome = is_null($nome) ? Configure::read('Site.from.nome') : $nome;
        $this->Email->from($email, $nome);
        return $this;
    }

    /**
     * @param bool $beforeSave
     * @param bool $afterSave
     * @return $this
     *
     * Envia os dados
     */
    public function Enviar($beforeSave = false, $afterSave = false)
    {
        $this->validate();

        if ($beforeSave)
            $this->Save();

        if ($this->Content != null) {
            $this->Status = $this->SMTP();
        }

        if ($afterSave)
            $this->Save();

        return $this;
    }

    /**
     * @return mixed
     *
     * Retorna o Status de envio
     */
    public function getStatus()
    {
        return $this->Status;
    }

}