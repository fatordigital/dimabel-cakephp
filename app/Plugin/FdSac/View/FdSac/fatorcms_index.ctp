<?php $this->Html->addCrumb('Sac'); ?>

<div class="row">
     <h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
        Sac 
    </h3>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <div class="row-fluid">
                    <div class="col-sm-12 col-md-10 col-lg-11">
                        <?php echo $this->FilterForm->create(null, array('role' => 'form', 'class' => 'form-inline')) ?>
                            <div class="form-group">
                                <?php echo $this->FilterForm->input('filtro_nome', array('placeholder' => 'Filtrar por nome', 'class' => 'form-control')) ?>
                            </div>
							<div class="form-group">
                                <?php echo $this->FilterForm->input('filtro_email', array('placeholder' => 'Filtrar por e-mail', 'class' => 'form-control')) ?>
                            </div>
                            <div class="form-group">
                                <?php echo $this->FilterForm->input('filtro_sac_tipo_id', array('placeholder' => 'Filtrar por tipo', 'class' => 'form-control')) ?>
                            </div>
                            <?php echo $this->FilterForm->submit('Filtrar', array('class' => 'btn btn-success', 'div' => false)) ?>
                            <?php echo $this->Html->link('Limpar Filtro', array('action' => 'index'), array('class' => 'btn btn-warning')) ?>
                        <?php echo $this->FilterForm->end() ?>
                    </div>
                    <div class="col-lg-1 visible-md visible-lg">
                       <?php echo $this->Element('/fatorcms/limit'); ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <table class="table table-hover general-table">
                    <thead>
                        <tr>
                            <th style="width:60px"><?php echo $this->Paginator->sort('id', '#') ?></th>
                            <th><?php echo $this->Paginator->sort('sac_tipo_id') ?></th>
                            <th><?php echo $this->Paginator->sort('nome') ?></th>
							<th><?php echo $this->Paginator->sort('email') ?></th>
							<th><?php echo $this->Paginator->sort('created', 'Criado') ?></th>
                            <th><?php echo $this->Paginator->sort('modified', 'Modificação') ?></th>
                            <th style="width:180px">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($sacs) > 0): ?>
                            <?php foreach($sacs AS $sac): ?>
                            <tr>
                                <td><?php echo $sac['Sac']['id'] ?></td>
                                <td><?php echo $sac['SacTipo']['nome'] ?></td>
                                <td><?php echo $sac['Sac']['nome'] ?></td>
								<td><?php echo $sac['Sac']['email'] ?></td>
								<td><?php echo $this->Time->format($sac['Sac']['created'], '%d/%m/%Y %H:%M:%S') ?></td>
                                <td><?php echo $this->Time->format($sac['Sac']['modified'], '%d/%m/%Y %H:%M:%S') ?></td>
                                <td>
                                    <?php echo $this->Html->link('<i class="fa fa-edit"></i> Detalhe', array('action' => 'view', $sac['Sac']['id']), array('class' => 'btn btn-primary btn-sm', 'escape' => false)) ?> 
                                    <?php echo $this->Html->link('<i class="fa fa-trash-o"></i> Remover', array('action' => 'delete', $sac['Sac']['id']), array('class' => 'btn btn-danger btn-sm', 'escape' => false), __('Deseja mesmo remover o registro # %s?', $sac['Sac']['nome'])) ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="7 class="text-center">Nenhum registro encontrado.</td>
                            </tr>
                        <?php endIf; ?>
                    </tbody>
                </table>

                <?php echo $this->Element('/fatorcms/paginator'); ?>
            </div>
        </section>
    </div>
</div>