<?php
	Router::connect("/fatorcms/sac", array('plugin' => 'fd_sac', 'controller' => 'fd_sac', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));
    Router::connect("/fatorcms/sac/:action/*", array('plugin' => 'fd_sac', 'controller' => 'fd_sac', 'prefix' => 'fatorcms', 'fatorcms' => true));

    Router::connect("/fatorcms/sac_tipos", array('plugin' => 'fd_sac', 'controller' => 'fd_sac_tipos', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));
    Router::connect("/fatorcms/sac_tipos/:action/*", array('plugin' => 'fd_sac', 'controller' => 'fd_sac_tipos', 'prefix' => 'fatorcms', 'fatorcms' => true));