<?php $this->Html->addCrumb('Logs', array('action' => 'index')) ?>
<?php $this->Html->addCrumb('Detalhe Log') ?>

<h3>Detalhe Log</h3>

<div class="panel">
	<div class="panel-body">
		<div class="row">
			<div class="col-lg-6">
				<strong>Info: </strong> <?php echo $log['Log']['info']; ?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<strong>Usuário: </strong> <a href="<?php echo $this->Html->Url(array('plugin' => 'fd_usuarios', 'controller' => 'fd_usuarios', 'action' => 'edit', $log['Usuario']['id'])); ?>"><?php echo $log['Usuario']['nome']; ?></a>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<strong>Ação: </strong> <?php echo $log['Log']['acao']; ?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<strong>Model: </strong> <?php echo $log['Log']['model']; ?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<strong>Criado: </strong> <?php echo $log['Log']['created']; ?>
			</div>
		</div>
	</div>
</div>