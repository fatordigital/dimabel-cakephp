<?php
	Router::connect("/fatorcms/banners", array('plugin' => 'fd_banners', 'controller' => 'fd_banners', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));
    Router::connect("/fatorcms/banners/:action/*", array('plugin' => 'fd_banners', 'controller' => 'fd_banners', 'prefix' => 'fatorcms', 'fatorcms' => true));
	
	Router::connect("/fatorcms/banner_tipos", array('plugin' => 'fd_banners', 'controller' => 'fd_banner_tipos', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));
    Router::connect("/fatorcms/banner_tipos/:action/*", array('plugin' => 'fd_banners', 'controller' => 'fd_banner_tipos', 'prefix' => 'fatorcms', 'fatorcms' => true));