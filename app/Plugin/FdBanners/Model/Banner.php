<?php
App::uses('AppModel', 'Model');
App::uses('CakeSession', 'Model/Datasource');

class Banner extends FdBannersAppModel {

	public $actsAs = array(
        'Upload.Upload' => array(			
            'file' => array(
				'fields' => array(
                    'dir' => 'dir',
					'type' => 'type',
					'size' => 'size',
                ),
				'thumbnailMethod' => 'php',
                'thumbnailSizes' => array(
                    'thumb' => '100x100',
                    '960' => '960w',
                    '400' => '400w'
                )
            ),
            'file_mobile' => array(
                'fields' => array(
                    'dir' => 'dir_mobile',
                    'type' => 'type_mobile',
                    'size' => 'size_mobile',
                ),
                'thumbnailMethod' => 'php',
                'thumbnailSizes' => array(
                    'thumb' => '100x100',
                    '400' => '400w'
                )
            )
        ),
    );

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'nome' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo de preenchimento obrigatório.',
			),
		),
        'data_inicio' => array(
            'preenchido' => array(
                'rule' => 'data_inicio_esta_vazio',
                'message' => 'Campo de preenchimento obrigatório',
            ),
        ),
        'data_fim' => array(
            'preenchido' => array(
                'rule' => 'data_fim_esta_vazio',
                'message' => 'Campo de preenchimento obrigatório',
            ),
        ),
        'file' => array(
            array(
                'rule' => 'isFileUpload',
                'message' => 'Campo de preenchimento obrigatório.',
                'on' => 'create'
                ),
            array(
                'rule' => array('isValidExtension', array('jpg', 'png', 'bmp', 'gif'), false),
                'message' => 'Extensão inválida. Utilize JPG, PNG, BMP e GIF.',
                'on' => 'create'
            )
        )
	);

//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'BannerTipo' => array(
			'className' => 'BannerTipo',
			'foreignKey' => 'banner_tipo_id'
		),
	);

/**
 * Filtra os banners do array de banners atraves dos tipos  e ids
 * @return Array
 */
    public function extraiBanners($array_banners, $tipo_banner, $banners_ids = null) {
        $return = "";
		foreach ($array_banners as $key => $v) {
            if ($banners_ids == null) {
                if ($v['BannerTipo']['codigo'] == $tipo_banner) {
                    $return[] = $v;
                }
            } elseif (is_array($banners_ids) && count($banners_ids) > 0) {
                foreach ($banners_ids as $id) {
                    if ($v['Banner']['id'] == $id) {
                        $return[] = $v;
                    }
                }
            }
        }
        return $return;
    }

    public function data_inicio_esta_vazio() {
        if(isset($this->data[$this->alias]['data_inicio']) && empty($this->data[$this->alias]['data_inicio'])){
            return false;
        }
        return true;
    }

    public function data_fim_esta_vazio() {
        if(isset($this->data[$this->alias]['data_fim']) && empty($this->data[$this->alias]['data_fim'])){
            return false;
        }
        return true;
    }

/**
 * beforeSave
 *
 * sobrecarga do metodo executado antes de salvar o registro
 */
    public function beforeSave($options = array()) {

        App::import("helper", "String");
        $this->String = new StringHelper(new View(null));

        if(isset($this->data[$this->alias]['sites']) && count($this->data[$this->alias]['sites']) > 0){
            $this->data[$this->alias]['sites'] = json_encode($this->data[$this->alias]['sites']);
        }

        if (isset($this->data[$this->alias]['data_inicio']) && $this->data[$this->alias]['data_inicio'] != "") {
            $this->data[$this->alias]['data_inicio'] = $this->String->dataFormatada("Y-m-d H:i:s",  $this->data[$this->alias]['data_inicio'].':00');
        }

        if (isset($this->data[$this->alias]['data_fim']) && $this->data[$this->alias]['data_fim'] != "") {
            $this->data[$this->alias]['data_fim'] = $this->String->dataFormatada("Y-m-d H:i:s",  $this->data[$this->alias]['data_fim'].':00');
        }

        parent::beforeSave($options = array());
    }

/**
 * afterSave
 *
 * sobrecarga do metodo executado depois de salvar o registro
 */
    public function afterSave($created, $options = array()) {

        parent::afterSave($created, $options = array());

        //get dados atualizados do banner
        $dados = $this->find('first', array('recursive' => -1, 'conditions' => array('id' => $this->id), 'fields' => array('file', 'dir', 'dir_mobile')));

        //begin rename files
        if(isset($this->data[$this->alias]['file']) && $this->data[$this->alias]['file'] != ""){
            if(isset($this->data[$this->alias]['nome'])){
                $pre_new_name = strtolower(Inflector::slug($this->data[$this->alias]['nome'], '-'));
                $pre_new_name = str_replace("_", "-", $pre_new_name);
                $path = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'files' . DS . 'banner' . DS . 'file';
            }
            $pre_old_name = $this->data[$this->alias]['file'];

            $new_name = $pre_new_name;
            $old_name = $pre_old_name;

            if(file_exists($path . DS . $dados[$this->alias]['dir'] . DS . $old_name)){
                $file = $path . DS . $dados[$this->alias]['dir'] . DS . $old_name;
                $infos = pathinfo($file);
                $new_file = $path . DS . $dados[$this->alias]['dir'] . DS . $new_name . '.' . $infos['extension'];
                $new_file_orginal = $path . DS . $dados[$this->alias]['dir'] . DS . 'original-' . $new_name . '.' . $infos['extension'];

                if(rename($file, $new_file)){

                    //load lib
                    include_once(ROOT . DS . APP_DIR . DS . 'Lib' . DS . 'Wideimage/WideImage.php');

                    $this->query('UPDATE banners SET file = "' . $new_name . '.' . $infos['extension'] . '" WHERE id = ' . $this->id);

                    copy($new_file, $new_file_orginal);

                    $caminho_novo   = $path . DS . $dados[$this->alias]['dir'];
                    $outFile        = $caminho_novo.DS.$pre_new_name . '.' . $infos['extension'];
                    if(strtolower($infos['extension']) == 'jpg'){
                        WideImage::load($new_file)->resize(1920)->crop('center', 'center', 1920, 875)->saveToFile($outFile, 90); 
                    }else{
                        WideImage::load($new_file)->resize(1920)->crop('center', 'center', 1920, 875)->saveToFile($outFile);
                    }

                    //outros renames
                    $file = $path . DS . $dados[$this->alias]['dir'] . DS . '960_' . $old_name;
                    $infos = pathinfo($file);
                    $new_file = $path . DS . $dados[$this->alias]['dir'] . DS . '960_' . $new_name . '.' . $infos['extension'];
                    rename($file, $new_file);

                    $file = $path . DS . $dados[$this->alias]['dir'] . DS . '400_' . $old_name;
                    $infos = pathinfo($file);
                    $new_file = $path . DS . $dados[$this->alias]['dir'] . DS . '400_' . $new_name . '.' . $infos['extension'];
                    rename($file, $new_file);

                    $file = $path . DS . $dados[$this->alias]['dir'] . DS . 'thumb_' . $old_name;
                    $infos = pathinfo($file);
                    $new_file = $path . DS . $dados[$this->alias]['dir'] . DS . 'thumb_' . $new_name . '.' . $infos['extension'];
                    rename($file, $new_file);
                }
            }
        }

        //begin rename files
        if(isset($this->data[$this->alias]['file_mobile']) && $this->data[$this->alias]['file_mobile'] != ""){
            if(isset($this->data[$this->alias]['nome'])){
                $pre_new_name = strtolower(Inflector::slug($this->data[$this->alias]['nome'], '-'));
                $pre_new_name = str_replace("_", "-", $pre_new_name);
                $path = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'files' . DS . 'banner' . DS . 'file_mobile';
            }
            $pre_old_name = $this->data[$this->alias]['file_mobile'];

            $new_name = $pre_new_name;
            $old_name = $pre_old_name;

            if(file_exists($path . DS . $dados[$this->alias]['dir_mobile'] . DS . $old_name)){
                $file = $path . DS . $dados[$this->alias]['dir_mobile'] . DS . $old_name;
                $infos = pathinfo($file);
                $new_file = $path . DS . $dados[$this->alias]['dir_mobile'] . DS . $new_name . '.' . $infos['extension'];
                $new_file_orginal = $path . DS . $dados[$this->alias]['dir_mobile'] . DS . 'original-' . $new_name . '.' . $infos['extension'];

                if(rename($file, $new_file)){

                    //load lib
                    include_once(ROOT . DS . APP_DIR . DS . 'Lib' . DS . 'Wideimage/WideImage.php');

                    $this->query('UPDATE banners SET file_mobile = "' . $new_name . '.' . $infos['extension'] . '" WHERE id = ' . $this->id);

                    copy($new_file, $new_file_orginal);

                    /*$caminho_novo   = $path . DS . $dados[$this->alias]['dir_mobile'];
                    $outFile        = $caminho_novo.DS.$pre_new_name . '.' . $infos['extension'];
                    if(strtolower($infos['extension']) == 'jpg'){
                        WideImage::load($new_file)->resize(1500)->crop('center', 'center', 1500, 370)->saveToFile($outFile, 80); 
                    }else{
                        WideImage::load($new_file)->resize(1500)->crop('center', 'center', 1500, 370)->saveToFile($outFile);
                    }*/

                    //outros renames
                    $file = $path . DS . $dados[$this->alias]['dir_mobile'] . DS . '400_' . $old_name;
                    $infos = pathinfo($file);
                    $new_file = $path . DS . $dados[$this->alias]['dir_mobile'] . DS . '400_' . $new_name . '.' . $infos['extension'];
                    rename($file, $new_file);

                    $file = $path . DS . $dados[$this->alias]['dir_mobile'] . DS . 'thumb_' . $old_name;
                    $infos = pathinfo($file);
                    $new_file = $path . DS . $dados[$this->alias]['dir_mobile'] . DS . 'thumb_' . $new_name . '.' . $infos['extension'];
                    rename($file, $new_file);
                }
            }
        }
    }

/**
 * afterFind
 *
 * sobrecarga do metodo executado depois de buscar o registro
 */
    public function afterFind($results, $primary = false) {
        App::import("helper", "String");
        $this->String = new StringHelper(new View(null));

        $params = Router::getParams();

        if (!empty($results)) {
            foreach ($results as $k => &$r) {
                if(isset($r[$this->alias])){
                    //tem sites? dou decode neles...
                    if(isset($r[$this->alias]['sites']) && $r[$this->alias]['sites'] != ""){
                        @$r[$this->alias]['sites'] = json_decode($r[$this->alias]['sites'], true);
                    }

                    if(isset($params['fatorcms']) && $params['fatorcms'] == true){
                        if(isset($r[$this->alias]['data_inicio']) && $r[$this->alias]['data_inicio'] != ""){
                            @$r[$this->alias]['data_inicio'] = $this->String->dataFormatada("d/m/Y H:i", $r[$this->alias]['data_inicio']);
                        }

                        if(isset($r[$this->alias]['data_fim']) && $r[$this->alias]['data_fim'] != ""){
                            @$r[$this->alias]['data_fim'] = $this->String->dataFormatada("d/m/Y H:i", $r[$this->alias]['data_fim']);
                        }
                    }
                }
            }
        }

        return $results;
    }
}