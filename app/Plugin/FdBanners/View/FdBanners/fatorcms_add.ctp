<?php echo $this->Html->css('/js/fatorcms/core/bootstrap-fileupload/bootstrap-fileupload') ?>
<?php echo $this->Html->script('/js/fatorcms/core/bootstrap-fileupload/bootstrap-fileupload'); ?>

<?php echo $this->Html->css('/js/fatorcms/core/bootstrap-datepicker/css/datepicker') ?>
<?php echo $this->Html->css('/js/fatorcms/core/bootstrap-datetimepicker/css/datetimepicker') ?>
<?php echo $this->Html->script('/js/fatorcms/core/bootstrap-datepicker/js/bootstrap-datepicker'); ?>
<?php echo $this->Html->script('/js/fatorcms/core/bootstrap-datepicker/js/bootstrap-datepicker'); ?>
<?php echo $this->Html->script('/js/fatorcms/core/bootstrap-datetimepicker/js/bootstrap-datetimepicker'); ?>
<?php echo $this->Html->script('/js/fatorcms/core/bootstrap-daterangepicker/moment.min'); ?>
<?php echo $this->Html->script('/js/fatorcms/core/bootstrap-daterangepicker/daterangepicker'); ?>

<?php echo $this->Html->script('/fd_banners/js/fatorcms/banner/crud.js?'.time()); ?>

<?php $this->Html->addCrumb('Banners', array('action' => 'index')) ?>
<?php $this->Html->addCrumb('Cadastrar Banner') ?>

<h3>Cadastrar Banner</h3>

<div class="panel">
	<div class="panel-body">
	<?php echo $this->Form->create('Banner', array('type' => 'file', 'role' => 'form', 'class' => 'minimal', 'novalidate' => true)) ?>
		<?php echo $this->Form->input('id') ?>
		<div class="row">
			<div class="col-md-6 col-lg-6">
				<div class="form-group">
					<label>Status</label>
					<div class="icheck">
						<?php echo $this->Form->input('status', array(
																	'type' => 'radio', 
																	'options' => array(1 => 'Ativo', 0 => 'Inativo'), 
																	'default' => 1, 
																	'legend' => false, 
																	'before' => '<div class="radio">', 
																	'after' => '</div>', 
																	'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4">
				<div class="form-group">
					<?php echo $this->Form->input('banner_tipo_id', array('options' => $bannerTipos, 'class' => 'form-control input-sm m-bot15')); ?>
				</div>
			</div>
		</div>
		<?php /*<div class="row">
			<div class="col-md-6 col-lg-6">
				<div class="form-group">
					<label>Em todas as páginas?</label>
					<div class="icheck">
						<?php echo $this->Form->input('pagina_geral', array(
																	'type' => 'radio', 
																	'options' => array(1 => 'Sim', 0 => 'Não'), 
																	'default' => 1, 
																	'legend' => false, 
																	'before' => '<div class="radio">', 
																	'after' => '</div>', 
																	'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
					</div>
				</div>
			</div>
		</div> */ ?>
		<?php /*
		<div class="row">
			<div class="col-lg-4">
				<div class="form-group">
					<?php echo $this->Form->input('pagina_id', array('options' => $pagina_id, 'class' => 'form-control input-sm m-bot15')); ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-lg-6">
				<div class="form-group">
					<label>Visível</label>
					<div class="icheck">
						<?php echo $this->Form->input('visivel', array(
																	'type' => 'radio', 
																	'options' => array('GERAL' => 'Geral', 'ALUNO' => 'Aluno', 'PROSPECT' => 'Prospect'), 
																	'default' => 'GERAL', 
																	'legend' => false, 
																	'before' => '<div class="radio">', 
																	'after' => '</div>', 
																	'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
					</div>
				</div>
			</div>
		</div> 
		<div class="row">
			<div class="col-md-6 col-lg-6">
				<div class="form-group">
					<label>Site</label>
					<div class="icheck">
						<?php 
							echo $this->Form->input('sites', array(
																	'type' => 'radio', 
																	'options' => $this->String->getSites(), 
																	'default' => 'portal',
																	'legend' => false, 
																	'before' => '<div class="radio">', 
																	'after' => '</div>', 
																	'separator' => '</div><div class="clearfix"></div><div class="radio">'
																)); 
															?>
					</div>
				</div>
			</div>
		</div> */ ?>
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<?php echo $this->Form->input('nome', array('div' => false, 'class' => 'form-control')) ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<?php echo $this->Form->input('data_inicio', array('type' => 'text', 'div' => false, 'class' => 'form-control form_datetime-adv')) ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<?php echo $this->Form->input('data_fim', array('type' => 'text', 'div' => false, 'class' => 'form-control form_datetime-adv')) ?>
				</div>
			</div>
		</div>	

		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<label>URL do Banner</label>
					<div class="icheck">
						<?php echo $this->Form->input('url_status', array(
																	'type' => 'radio', 
																	'options' => array(1 => 'URL Única', 0 => 'Múltiplas URLs'), 
																	'default' => 0, 
																	'legend' => false, 
																	'before' => '<div class="radio">', 
																	'after' => '</div>', 
																	'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
					</div>
				</div>
			</div>
		</div>
		
		<div class="url_multipla">
			<div class="row">
				<div class="col-lg-6">
					<div class="form-group bottom-line">
						<h3>Titulos e links do banner</h3>					
					</div>
				</div>			
			</div>	
			<div class="row">
				<div class="col-lg-6">
					<pre><?php echo $this->Html->image('fatorcms/exemplos/banners_titulos_e_links.jpg'); ?></pre>
				</div>
			</div>	
			<div class="row">
				<div class="col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('titulo_top_nome', array('label' => '1 - Título superior', 'div' => false, 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('titulo_top_url', array('label' => '1 - URL do título superior', 'div' => false, 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('titulo_bot_nome', array('label' => '2 - Título inferior', 'div' => false, 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('titulo_bot_url', array('label' => '2 - URL do título inferior', 'div' => false, 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
		</div>
		<div class="url_unica hide">
			<div class="row">
				<div class="col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('url', array('div' => false, 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
		</div>
		
		<script type="text/javascript">
			$(document).ready(function(){				
				$("[name='data[Banner][url_status]']").on('ifChecked', function(event){
					if($(this).val()=='0'){
						$('.url_unica').addClass('hide');
						$('.url_unica').removeClass('show');
						$('.url_multipla').addClass('show');
						$('.url_multipla').removeClass('hide');
					}else{
						$('.url_unica').addClass('show');
						$('.url_unica').removeClass('hide');
						$('.url_multipla').addClass('hide');
						$('.url_multipla').removeClass('show');
					}
				});
			});
		</script>

		<?php /*<div class="row">
			<div class="col-md-6 col-lg-6">
				<div class="form-group">
					<label>Possui vídeo ou link?</label>
					<div class="icheck">
						<?php echo $this->Form->input('url_status', array(
																	'type' => 'radio', 
																	'options' => array(1 => 'Sim', 0 => 'Não'), 
																	'default' => 0, 
																	'legend' => false, 
																	'before' => '<div class="radio">', 
																	'after' => '</div>', 
																	'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
					</div>
				</div>
			</div>
		</div>*/ ?>
		<?php /*<div class="row">
			<div class="col-md-6 col-lg-6">
				<div class="form-group">
					<label>Tipo de URL</label>
					<div class="icheck">
						<?php echo $this->Form->input('url_tipo', array(
																	'type' => 'radio', 
																	'options' => array('v' => 'Vídeo', 'l' => 'Link'), 
																	'default' => 'v', 
																	'legend' => false, 
																	'before' => '<div class="radio">', 
																	'after' => '</div>', 
																	'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<?php echo $this->Form->input('url_video', array('label' => 'ID do Vídeo do Youtube', 'div' => false, 'class' => 'form-control')) ?>
				</div>
			</div>
		</div> */?>
		
		<div class="row row-arquivo">
			<div class="col-lg-12">
				<div class="form-group">
					<label>Arquivo</label>
					<div class="fileupload fileupload-new" data-provides="fileupload">
	                    <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
	                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=nenhuma+imagem" alt="" />
	                    </div>
	                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
	                    <div>
	                       <span class="btn btn-white btn-file">
	                           	<span class="fileupload-new"><i class="fa fa-paper-clip"></i> Selecionar imagem</span>
	                           	<span class="fileupload-exists"><i class="fa fa-undo"></i> Alterar</span>
	                           	<?php echo $this->Form->input('file', array('type' => 'file', 'class' => 'default', 'label' => false, 'div' => false)); ?>
	                           	<?php echo $this->Form->hidden('dir') ?>
	                       </span>
	                        <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remover</a>
	                    </div>
	                </div>
	                <div class="col-lg-6">
	                	<div class="alert alert-warning">
	                		Tamanho: 1920px X 875px - Formatos: .jpg / .png / .gif
	                	</div>
	                </div>
				</div>
			</div>
		</div>
		<div class="row row-mobile">
			<div class="col-lg-12">
				<div class="form-group">
					<label>Mobile</label>
					<div class="fileupload fileupload-new" data-provides="fileupload">
	                    <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
	                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=nenhuma+imagem" alt="" />
	                    </div>
	                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
	                    <div>
	                       <span class="btn btn-white btn-file">
	                           	<span class="fileupload-new"><i class="fa fa-paper-clip"></i> Selecionar imagem</span>
	                           	<span class="fileupload-exists"><i class="fa fa-undo"></i> Alterar</span>
	                           	<?php echo $this->Form->input('file_mobile', array('type' => 'file', 'class' => 'default', 'label' => false, 'div' => false)); ?>
	                           	<?php echo $this->Form->hidden('dir_mobile') ?>
	                       </span>
	                        <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remover</a>
	                    </div>
	                </div>
	                <span class="label label-danger">Atenção!</span>
	                <span>
	                 	A visualiação da miniatura da imagem antes do upload é suportado no mais recente Firefox, Chrome, Opera, Safari e Internet Explorer 10 unicamente.
	                </span>
				</div>
			</div>
		</div>

		<div class="row row-ordem">
			<div class="col-lg-6">
				<div class="form-group">
					<?php echo $this->Form->input('ordem', array('div' => false, 'class' => 'form-control')) ?>
				</div>
			</div>
		</div>
		<?php echo $this->Form->submit('Salvar', array('class' => 'btn btn-success', 'div' => false)) ?>
	<?php echo $this->Form->end() ?>
	</div>
</div>