var site_topo 		= '1';
var site_pagina 	= '2';
var site_noticia 	= '3';
var site_sidebar 	= '4';
var site_curso 		= '5';
var site_floating 	= '6';

$(document).ready(function(){
	

	//sites
	/*gerencia_campos_url_status($("[name='data[Banner][url_status]']:checked").val());
	$("[name='data[Banner][url_status]']").on('ifChecked', function(event){
		gerencia_campos_url_status($(this).val());
	});*/
	
	$("[name='data[Banner][url_tipo]']").on('ifChecked', function(event){
		gerencia_campos_url_tipo($(this).val());
	});

	gerencia_campos_curso_tipo($("[name='data[Banner][banner_tipo_id]']").val());
	$("[name='data[Banner][banner_tipo_id]']").change(function(event){
		gerencia_campos_curso_tipo($(this).val());
	});

	//pagina
	gerencia_campos_pagina($("[name='data[Banner][banner_tipo_id]']").val());
	$("[name='data[Banner][banner_tipo_id]']").change(function(event){
		gerencia_campos_pagina($(this).val());
	});

	gerencia_campos_pagina_geral($("[name='data[Banner][pagina_geral]']:checked").val());
	$("[name='data[Banner][pagina_geral]']").on('ifChecked', function(event){
		gerencia_campos_pagina_geral($(this).val());
	});

	//dd/mm/yy
	$(".form_datetime-adv").datetimepicker({
	    format: "dd/mm/yyyy hh:ii",
	    autoclose: true,
	    todayBtn: true,
	    minuteStep: 10
	});
});

function gerencia_campos_pagina(value){
	$("[name='data[Banner][pagina_geral]']").closest('.row').hide();
	if (value == '2') {
		$("[name='data[Banner][pagina_geral]']").removeAttr("disabled").closest('.row').show();
		gerencia_campos_pagina_geral($("[name='data[Banner][pagina_geral]']:checked").val());
    } else {
        $("[name='data[Banner][pagina_geral]']").closest('.row').hide();
    }
}

function gerencia_campos_pagina_geral(value){
	$("[name='data[Banner][pagina_id]']").attr("disabled", "disabled").closest('.row').hide();
	if (value == '0') {
		$("[name='data[Banner][pagina_id]']").removeAttr("disabled").closest('.row').show();		
    } else {
        $("[name='data[Banner][pagina_id]']").attr("disabled", "disabled").closest('.row').hide();
    }
}

function gerencia_campos_curso_tipo(value){

	$("[name='data[Banner][data_inicio]']").attr("disabled", "disabled").closest('.row').hide();
	$("[name='data[Banner][data_fim]']").attr("disabled", "disabled").closest('.row').hide();
	//$("[name='data[Banner][url_status]']").attr("disabled", "disabled").closest('.row').hide();
	$("[name='data[Banner][visivel]']").attr("disabled", "disabled").closest('.row').hide();

	//site_topo
	if (value == site_topo) {
        $("[name='data[Banner][visivel]']").removeAttr("disabled").closest('.row').show();
    } else {
		// $("[name='data[Banner][visivel]']").attr("disabled", "disabled").closest('.row').hide();        
    }

    //site_floating
    if (value == site_floating) {
        $(".row-mobile").hide();
        $(".row-ordem").hide();
        // $("[name='data[Banner][url_status]']").attr("disabled", "disabled").closest('.row').hide();

        $("[name='data[Banner][data_inicio]']").removeAttr("disabled").closest('.row').show();
		$("[name='data[Banner][data_fim]']").removeAttr("disabled").closest('.row').show();

		//$("[name='data[Banner][url]']").removeAttr("disabled").closest('.row').show();   
    } else {
    	$(".row-mobile").show();
    	$(".row-ordem").show();
    	//$("[name='data[Banner][url_status]']").removeAttr("disabled").closest('.row').show();
    }
}

function gerencia_campos_url_status(value){
	//$("[name='data[Banner][url]']").attr("disabled", "disabled").closest('.row').hide();
	$("[name='data[Banner][url_tipo]']").closest('.row').hide();
	$("[name='data[Banner][url_video]']").attr("disabled", "disabled").closest('.row').hide();

	if (value == '1') {
        $("[name='data[Banner][url_tipo]']").removeAttr("disabled").closest('.row').show();

        gerencia_campos_url_tipo($("[name='data[Banner][url_tipo]']:checked").val());
    } else {
        $("[name='data[Banner][url_tipo]']").attr("disabled", "disabled").closest('.row').hide();
    }
}

function gerencia_campos_url_tipo(value){
	$("[name='data[Banner][url]']").attr("disabled", "disabled").closest('.row').hide();
	$("[name='data[Banner][url_video]']").attr("disabled", "disabled").closest('.row').hide();

	if (value == 'v') {
		$("[name='data[Banner][url]']").attr("disabled", "disabled").closest('.row').hide();
		$("[name='data[Banner][url_video]']").removeAttr("disabled").closest('.row').show();
    } else if(value == 'l') {
    	$("[name='data[Banner][url]']").removeAttr("disabled").closest('.row').show();    	
		$("[name='data[Banner][url_video]']").attr("disabled", "disabled").closest('.row').hide();
    }
}