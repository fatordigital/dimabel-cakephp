<?php
App::uses('AppModel', 'Model');
App::uses('CakeSession', 'Model/Datasource');

class Bloco extends FdBlocosAppModel {

	public $actsAs = array(
		'Containable',
	);

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'nome' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo de preenchimento obrigatório.',
			),
		),
		// 'slug' => array(
  //           'required' => array(
  //               'rule' => array('notBlank'),
  //               'message' => 'Campo de preenchimento obrigatório.',
  //               'required' => true,
  //           ),
  //           'unique' => array(
  //               'rule' => array('isUnique'),
  //               'message' => 'Esta chave já consta em nosso banco de dados',
  //               'on' => 'create',
  //           ),
  //       ),
        'seo_url' => array(
			'url_unica' => array(
	             'rule' => array('check_url_unica'),
	             'message' => 'Url já existente. Tenta outra.',
	        ),
		),
	);
	
/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'BlocoTipo' => array(
			'className' => 'BlocoTipo',
			'foreignKey' => 'bloco_tipo_id'
		),
	);

	public function beforeValidate($options = array()){

		//trata seo_url
		if (isset($this->data[$this->alias]['nome']) &&  ( !isset($this->data[$this->alias]['slug']) || $this->data[$this->alias]['slug'] == "") ) {
			$this->data[$this->alias]['slug'] = strtolower(Inflector::slug($this->data[$this->alias]['nome'], '-'));
		}

		if (!isset($this->data[$this->alias]['seo_url']) || $this->data[$this->alias]['seo_url'] == "") {
			$this->data[$this->alias]['seo_url'] = 'bloco/' . $this->data[$this->alias]['slug'];
		}

		if(stripos($this->data[$this->alias]['seo_url'], 'bloco/') === false){
			$this->data[$this->alias]['seo_url'] = 'bloco/' . $this->data[$this->alias]['seo_url'];
		}

		if (isset($this->data[$this->alias]['seo_title']) && $this->data[$this->alias]['seo_title'] == "") {
			$this->data[$this->alias]['seo_title'] = $this->data[$this->alias]['nome'];
		}

		// begin provisório
    	if(isset($this->data[$this->alias]['seo_url'])){
    		App::import('Model', 'FdRotas.Rota');
    		$this->Rota = new Rota();
    	
    		$rota = $this->Rota->find('all', array('conditions' => array('seo_url' => $this->data[$this->alias]['seo_url'])));
	    	if(!empty($rota) && is_null($this->data[$this->alias]['id'])){
	    		$this->data[$this->alias]['seo_url'] = $this->data[$this->alias]['seo_url'] . '-' . count($rota)+1;
	    	}
    	}
    	// end provisório
	}

/**
 * beforeSave
 *
 * sobrecarga do metodo executado antes de salvar o registro
 */
	public function beforeSave($options = array()) {
		//BEGIN: remoção do lixo do editor
	       	if(isset($this->data[$this->alias]['conteudo']) && $this->data[$this->alias]['conteudo'] != ""){
	       		$this->data[$this->alias]['conteudo'] = str_replace('<p></p>', '', $this->data[$this->alias]['conteudo']);
	       		$this->data[$this->alias]['conteudo'] = str_replace('<p><p>', '<p>', $this->data[$this->alias]['conteudo']);
	       		$this->data[$this->alias]['conteudo'] = str_replace('</p></p>', '</p>', $this->data[$this->alias]['conteudo']);
	       	}
       	//END: remoção do lixo do editor

	    if(isset($this->data[$this->alias]['sites']) && count($this->data[$this->alias]['sites']) > 0){
            $this->data[$this->alias]['sites'] = json_encode($this->data[$this->alias]['sites']);
        }

	    parent::beforeSave($options = array());
    }

/**
 * afterFind
 *
 * sobrecarga do metodo executado depois de buscar o registro
 */
    public function afterFind($results, $primary = false) {
        if (!empty($results)) {
            foreach ($results as $k => &$r) {
                if(isset($r[$this->alias])){
                    //tem sites? dou decode neles...
                    if(isset($r[$this->alias]['sites']) && $r[$this->alias]['sites'] != ""){
                        @$r[$this->alias]['sites'] = json_decode($r[$this->alias]['sites'], true);
                    }
                }
            }
        }

        return $results;
    }

	public function afterSave($created, $options = array()) {

    	parent::afterSave($created, $options = array());

    	//begin salva o registro da URL nas rotas
		$controller 	= 'Blocos';
		$model 			= 'Bloco';
		$action			= 'index';
		$params_id		= null;
		$params_value	= null;

		//salvo a rota da categoria
		if(isset($this->data[$this->alias]['seo_url'])){
			App::import('Model', 'Rota');
			$this->Rota = new Rota();

			$rt = $this->Rota->find('first', array('conditions' => array(
																		'AND' => array(
																				'row_id' => $this->data[$this->alias]['id'],
																				'model'  => $model
																			)
																	)
													)
									);

			if($rt){
				$rota['id'] 		= $rt['Rota']['id'];
			}else{
				$rota['id'] 		= null;
			}

			$rota['controller'] 	= $controller;
			$rota['model'] 			= $model;
			$rota['action'] 		= $action;
			$rota['params_id'] 		= $params_id;
			$rota['params_value'] 	= $params_value;
			$rota['row_id'] 		= $this->data[$this->alias]['id'];
			$rota['seo_url'] 		= $this->data[$this->alias]['seo_url'];
			$rota['buscavel'] 		= 0;
			$rota['sites'] 			= '["portal"]';

			$this->Rota->save($rota);
		}
		//end salva o registro da URL nas rotas
	}
}