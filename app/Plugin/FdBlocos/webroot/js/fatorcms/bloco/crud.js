$(document).ready(function(){
	$("[name='data[Bloco][nome]']").stringToSlug({
		getPut: '.slug'
	});

	$(".count_me").characterCounter({
		limit: '160',
		renderTotal: true,
		counterFormat: '[Recomenda-se utilizar no máximo 160 caracteres no resumo do conteúdo]. %1'
	});
});