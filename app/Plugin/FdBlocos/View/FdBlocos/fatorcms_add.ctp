<?php echo $this->Html->script('fatorcms/jquery.stringToSlug.min'); ?>
<?php echo $this->Html->script('/js/fatorcms/core/jquery-character-counter/jquery.charactercounter'); ?>

<?php echo $this->Html->css('/js/fatorcms/core/bootstrap-fileupload/bootstrap-fileupload') ?>
<?php echo $this->Html->script('/js/fatorcms/core/bootstrap-fileupload/bootstrap-fileupload'); ?>

<?php echo $this->Html->script('/fd_blocos/js/fatorcms/bloco/crud.js'); ?>

<?php $this->Html->addCrumb('Blocos', array('action' => 'index')) ?>
<?php $this->Html->addCrumb('Cadastrar Bloco') ?>

<h3>Cadastrar Bloco</h3>

<div class="panel">
	<div class="panel-body">
	<?php echo $this->Form->create('Bloco', array('type' => 'file', 'role' => 'form', 'class' => 'minimal', 'novalidate' => true)) ?>
		<?php echo $this->Form->input('id') ?>
		<div class="row">
			<div class="col-md-6 col-lg-6">
				<div class="form-group">
					<label>Status</label>
					<div class="icheck">
						<?php echo $this->Form->input('status', array(
																	'type' => 'radio', 
																	'options' => array(1 => 'Ativo', 0 => 'Inativo'), 
																	'default' => 1, 
																	'legend' => false, 
																	'before' => '<div class="radio">', 
																	'after' => '</div>', 
																	'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-lg-6">
				<div class="form-group">
					<label>Site</label>
					<div class="icheck">
						<?php echo $this->Form->input('sites', array(
																	'type' => 'radio', 
																	'options' => $this->String->getSites(), 
																	'default' => 'portal',
																	'legend' => false, 
																	'before' => '<div class="radio">', 
																	'after' => '</div>', 
																	'separator' => '</div><div class="clearfix"></div><div class="radio">'
																)); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4">
				<div class="form-group">
					<?php echo $this->Form->input('bloco_tipo_id', array('options' => $blocoTipos, 'class' => 'form-control input-sm m-bot15')); ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<?php echo $this->Form->input('nome', array('label' => 'Nome', 'div' => false, 'class' => 'form-control')) ?>
				</div>
			</div>
		</div>

		
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<?php echo $this->Form->input('slug', array('label' => 'Chave', 'div' => false, 'class' => 'form-control slug')) ?>
				</div>
			</div>
		</div>
		
		<div class="row" id="conteudo">
			<div class="col-lg-6">
				<div class="form-group">
					<?php echo $this->Form->input('conteudo', array('label' => 'Conteúdo', 'div' => false, 'class' => 'form-control editor')) ?>
				</div>
			</div>
		</div>

		<?php /*
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					<?php echo $this->Form->input('seo_title', array('div' => false, 'class' => 'form-control')) ?>
					<span class="help-block">Esse campo será alimentado com o nome do bloco, caso não seja preenchido.</span>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					<?php echo $this->Form->input('seo_description', array('type' => 'textarea', 'div' => false,  'class' => 'form-control count_me')) ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					<?php echo $this->Form->input('seo_keywords', array('div' => false, 'class' => 'form-control')) ?>
					<span class="help-block">Utilize ponto e vírgual (;) como separador das palavras-chave.</span>
				</div>
			</div>
		</div>
		*/ ?>
		<?php echo $this->Form->submit('Salvar', array('class' => 'btn btn-success', 'div' => false)) ?>
		<a href="<?php echo $this->Html->Url(array('action' => 'index')); ?>" class="btn btn-default">Cancelar</a>
	<?php echo $this->Form->end() ?>
	</div>
</div>