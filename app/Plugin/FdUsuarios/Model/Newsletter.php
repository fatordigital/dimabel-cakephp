<?php

/**
 * Usuario Model
 *
 * @property Usuario $Usuario
 */

class Newsletter extends FdUsuariosAppModel {

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'nome' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Informe o nome de exibição',
                'required' => true,
            ),
        ),
        'email' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'O e-mail obrigatório',
                'required' => true,
            ),
            'email' => array(
                'rule' => array('email', true),
                'message' => 'Informe um e-mail válido',
            ),
        ),
        // 'cpf' => array(            
        //     'valido' => array(
        //         'rule' => array('validaCpf'),
        //         'message' => 'Esse CPF é inválido.'
        //     ),
        //     'preenchido' => array(
        //         'rule' => array('notBlank'),
        //         'message' => 'Campo de preenchimento obrigatório',
        //         'required' => true,
        //         'on' => 'create'
        //     ),
        // ),
    );

    public function validaCpf() {
        $check = trim($this->data[$this->name]['cpf']);

        // sometimes the user submits a masked CPF
        if (preg_match('/^\d\d\d.\d\d\d.\d\d\d\-\d\d/', $check)) {
            $check = str_replace(array('-', '.', '/'), '', $check);
        } elseif (!ctype_digit($check)) {
            return false;
        }

        if (strlen($check) != 11) {
            return false;
        }

        // repeated values are invalid, but algorithms fails to check it
        for ($i = 0; $i < 10; $i++) {
            if (str_repeat($i, 11) === $check) {
                return false;
            }
        }

        $dv = substr($check, -2);
        for ($pos = 9; $pos <= 10; $pos++) {
            $sum = 0;
            $position = $pos + 1;
            for ($i = 0; $i <= $pos - 1; $i++, $position--) {
                $sum += $check[$i] * $position;
            }
            $div = $sum % 11;
            if ($div < 2) {
                $check[$pos] = 0;
            } else {
                $check[$pos] = 11 - $div;
            }
        }
        $dvRight = $check[9] * 10 + $check[10];

        return ($dvRight == $dv);
    }
    
/**
 * beforeValidate
 *
 * sobrecarga do metodo executado antes da validação dos dados
 */
    public function beforeValidate($options = array()){
        if (isset($this->data[$this->alias]['cpf']) && $this->data[$this->alias]['cpf'] != "") {
            $this->data[$this->alias]['cpf'] = preg_replace("/[^0-9]/", "", $this->data[$this->alias]['cpf']);
        }
    }
    
/**
 * beforeSave
 *
 * sobrecarga do metodo executado antes de salvar o registro
 */
    public function beforeSave($options = array()) {
        if(isset($this->data[$this->alias]['cpf']) && $this->data[$this->alias]['cpf'] != ""){
            $this->data[$this->alias]['cpf'] = preg_replace("/[^0-9]/", "", $this->data[$this->alias]['cpf']);
        }
		
		parent::beforeSave($options = array());
    }
}