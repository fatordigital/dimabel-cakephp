<?php
App::uses('AuthComponent', 'Controller/Component');

/**
 * Usuario Model
 *
 * @property Usuario $Usuario
 */

class Usuario extends FdUsuariosAppModel {

    const ADMINISTRADOR         = 1;
    const GERENTE               = 2;
    const PUBLICO               = 3;    
    const EDITOR_NOTICIAS       = 4;
    const EDITOR_EVENTOS        = 5;
    const EDITOR_CALENDARIO     = 6;
    const SUPER_ADMINISTRADOR   = 7;
    const SAC                   = 8;
    const CLIENTE               = 10;

    public $actsAs = array('Acl' => array('type' => 'requester'));

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'nome' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Informe o nome de exibição',
                'required' => true,
            ),
        ),
        'email' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'O e-mail obrigatório',
                'required' => true,
            ),
            'email' => array(
                'rule' => array('email', true),
                'message' => 'Informe um e-mail válido',
            ),
            'unique' => array(
                'rule' => array('isUnique'),
                'message' => 'Este e-mail já consta em nosso banco de dados',
                'on' => 'create',
            ),
        ),
        'senha_nova' => array(
            'confirm' => array(
                'rule' => array('confirmPassword'),
                'message' => 'As senhas não conferem'
            ),
            'minimoUpdate' => array(
                'rule' => array('between', 6, 8),
                'menssage' => 'A senha deve ter entre 6 à 8 caracteres',
                'allowEmpty' => true,
                'on' => 'update'
            ),
            'preenchido' => array(
                'rule' => 'senha_esta_vazio',
                'message' => 'Campo de preenchimento obrigatório',
            ),
        ),
        'grupo_id' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'O grupo de usuário é obrigatório'
            )
        ),
        'cpf' => array(            
            'valido' => array(
                'rule' => array('validaCpf'),
                'message' => 'Esse CPF é inválido.'
            ),
            'preenchido' => array(
                'rule' => array('cpf_esta_vazio'),
                'message' => 'Campo de preenchimento obrigatório',
                'required' => true,
                'on' => 'create'
            ),
            /*'unico' => array(
                'rule' => array('isUnique'),
                'message' => 'Este cpf já está cadastrado, insira outro CPF.',
                'on' => 'create'
            )*/
        ),
        'cnpj' => array(            
            'valido' => array(
                'rule' => array('validaCnpj'),
                'message' => 'O campo cnpj não contem um CNPJ válido',
            ),
            'preenchido' => array(
                'rule' => array('cnpj_esta_vazio'),
                'message' => 'Campo de preenchimento obrigatório'
            ),
            /*'unico' => array(
                'rule' => array('isUnique'),
                'message' => 'Este cnpj já está cadastrado, insira outro CNPJ.',
                'on' => 'create'
            )*/
        ),
        'data_nascimento' => array(
            'notBlank' => array(
                'rule' => array('data_nascimento_esta_vazio'),
                'message' => 'Campo de preenchimento obrigatório.',
                'required' => true,
                'on' => 'create'
            ),
        ),
        'telefone' => array(
            'notBlank' => array(
                'rule' => array('telefone_esta_vazio'),
                'message' => 'Campo de preenchimento obrigatório.',
                'required' => true,
                'on' => 'create'
            ),
        ),
    );

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
        'Grupo' => array(
            'className' => 'Grupo',
            'foreignKey' => 'grupo_id',
        )
    );

    // public $hasMany = array(
    //     'UsuarioEndereco' => array(
    //         'className' => 'UsuarioEndereco',
    //         'foreignKey' => 'usuario_id',
    //         'dependent' => false,
    //         'conditions' => '',
    //         'fields' => '',
    //         'order' => '',
    //         'limit' => '',
    //         'offset' => '',
    //         'exclusive' => '',
    //         'finderQuery' => '',
    //         'counterQuery' => ''
    //     )
    // );

    public function parentNode(){
        if (!$this->id && empty($this->data)){
            return null;
        }

        if (isset($this->data['Usuario']['grupo_id'])){
            $grupoId = $this->data['Usuario']['grupo_id'];
        } else {
            $grupoId = $this->field('grupo_id');
        }

        if (!$grupoId){
            return null;
        } else {
            return array('Grupo' => array('id' => $grupoId));
        }
    }

/**
 * Verifica se as senhas digitadas conferem
 *
 * @return boolean
 */
    public function validaRepetirSenha() {
        if (isset($this->data[$this->name]['senha_nova']) && isset($this->data[$this->name]['confirm']))
            return (@$this->data[$this->name]['senha_nova'] == @$this->data[$this->name]['confirm']);
        else
            return true;
    }

    public function senha_esta_vazio() {
        if(isset($this->data[$this->alias]['senha_nova']) && empty($this->data[$this->alias]['senha_nova'])){
            return false;
        }
        return true;
    }

    public function cpf_esta_vazio() {
        if(isset($this->data[$this->alias]['grupo_id']) && ($this->data[$this->alias]['grupo_id'] != '10')){
            return true;
        }

        if(isset($this->data[$this->alias]['pessoa_tipo']) && ($this->data[$this->alias]['pessoa_tipo'] == 'J') ){
            return true;
        }

        if(isset($this->data[$this->alias]['cpf']) && empty($this->data[$this->alias]['cpf'])){
            return false;
        }

        return true;
    }

    public function cnpj_esta_vazio() {
        if(isset($this->data[$this->alias]['pessoa_tipo']) && ($this->data[$this->alias]['pessoa_tipo'] == 'F') ){
            return true;
        }

        if(isset($this->data[$this->alias]['cnpj']) && empty($this->data[$this->alias]['cnpj'])){
            return false;
        }

        return true;
    }

    public function validaCpf() {
        if(isset($this->data[$this->alias]['grupo_id']) && ($this->data[$this->alias]['grupo_id'] != '10')){
            return true;
        }

        if(isset($this->data[$this->alias]['pessoa_tipo']) && ($this->data[$this->alias]['pessoa_tipo'] == 'J')){
            return true;
        }

        $check = trim($this->data[$this->name]['cpf']);

        // sometimes the user submits a masked CNPJ
        if (preg_match('/^\d\d\d.\d\d\d.\d\d\d\-\d\d/', $check)) {
            $check = str_replace(array('-', '.', '/'), '', $check);
        } elseif (!ctype_digit($check)) {
            return false;
        }

        if (strlen($check) != 11) {
            return false;
        }

        // repeated values are invalid, but algorithms fails to check it
        for ($i = 0; $i < 10; $i++) {
            if (str_repeat($i, 11) === $check) {
                return false;
            }
        }

        $dv = substr($check, -2);
        for ($pos = 9; $pos <= 10; $pos++) {
            $sum = 0;
            $position = $pos + 1;
            for ($i = 0; $i <= $pos - 1; $i++, $position--) {
                $sum += $check[$i] * $position;
            }
            $div = $sum % 11;
            if ($div < 2) {
                $check[$pos] = 0;
            } else {
                $check[$pos] = 11 - $div;
            }
        }
        $dvRight = $check[9] * 10 + $check[10];

        return ($dvRight == $dv);
    }

    public function validaCnpj() {
        if(isset($this->data[$this->alias]['pessoa_tipo']) && ($this->data[$this->alias]['pessoa_tipo'] == 'F')){
            return true;
        }

        $check = trim($this->data[$this->name]['cnpj']);
        // sometimes the user submits a masked CNPJ
        if (preg_match('/^\d\d.\d\d\d.\d\d\d\/\d\d\d\d\-\d\d/', $check)) {
            $check = str_replace(array('-', '.', '/'), '', $check);
        } elseif (!ctype_digit($check)) {
            return false;
        }

        if (strlen($check) != 14) {
            return false;
        }
        $firstSum = ($check[0] * 5) + ($check[1] * 4) + ($check[2] * 3) + ($check[3] * 2) +
            ($check[4] * 9) + ($check[5] * 8) + ($check[6] * 7) + ($check[7] * 6) +
            ($check[8] * 5) + ($check[9] * 4) + ($check[10] * 3) + ($check[11] * 2);

        $firstVerificationDigit = ($firstSum % 11) < 2 ? 0 : 11 - ($firstSum % 11);

        $secondSum = ($check[0] * 6) + ($check[1] * 5) + ($check[2] * 4) + ($check[3] * 3) +
            ($check[4] * 2) + ($check[5] * 9) + ($check[6] * 8) + ($check[7] * 7) +
            ($check[8] * 6) + ($check[9] * 5) + ($check[10] * 4) + ($check[11] * 3) +
            ($check[12] * 2);

        $secondVerificationDigit = ($secondSum % 11) < 2 ? 0 : 11 - ($secondSum % 11);

        return ($check[12] == $firstVerificationDigit) && ($check[13] == $secondVerificationDigit);
    }

    public function data_nascimento_esta_vazio() {
        if(isset($this->data[$this->alias]['grupo_id']) && ($this->data[$this->alias]['grupo_id'] != '10')){
            return true;
        }

        if(isset($this->data[$this->alias]['data_nascimento']) && empty($this->data[$this->alias]['data_nascimento'])){
            return false;
        }

        return true;
    }

    public function telefone_esta_vazio() {
        if(isset($this->data[$this->alias]['grupo_id']) && ($this->data[$this->alias]['grupo_id'] != '10')){
            return true;
        }
        
        if(isset($this->data[$this->alias]['telefone']) && empty($this->data[$this->alias]['telefone'])){
            return false;
        }

        return true;
    }
    
/**
 * beforeValidate
 *
 * sobrecarga do metodo executado antes da validação dos dados
 */
    public function beforeValidate($options = array()){
        if (isset($this->data[$this->alias]['cpf']) && $this->data[$this->alias]['cpf'] != "") {
            $this->data[$this->alias]['cpf'] = preg_replace("/[^0-9]/", "", $this->data[$this->alias]['cpf']);
        }

        if (isset($this->data[$this->alias]['cnpj']) && $this->data[$this->alias]['cnpj'] != "") {
            $this->data[$this->alias]['cnpj'] = preg_replace("/[^0-9]/", "", $this->data[$this->alias]['cnpj']);
        }
    }
    
/**
 * beforeSave
 *
 * sobrecarga do metodo executado antes de salvar o registro
 */
    public function beforeSave($options = array()) {
        if(isset($this->data[$this->alias]['cpf']) && $this->data[$this->alias]['cpf'] != ""){
            $this->data[$this->alias]['cpf'] = preg_replace("/[^0-9]/", "", $this->data[$this->alias]['cpf']);
        }
        if(isset($this->data[$this->alias]['cnpj']) && $this->data[$this->alias]['cnpj'] != ""){
            $this->data[$this->alias]['cnpj'] = preg_replace("/[^0-9]/", "", $this->data[$this->alias]['cnpj']);
        }
        if(isset($this->data[$this->alias]['telefone']) && $this->data[$this->alias]['telefone'] != ""){
            $this->data[$this->alias]['telefone'] = preg_replace("/[^0-9]/", "", $this->data[$this->alias]['telefone']);
        }
        if(isset($this->data[$this->alias]['celular']) && $this->data[$this->alias]['celular'] != ""){
            $this->data[$this->alias]['celular'] = preg_replace("/[^0-9]/", "", $this->data[$this->alias]['celular']);
        }
        if(isset($this->data['Usuario']['senha']) && $this->data['Usuario']['senha'] != ""){
            $this->data['Usuario']['senha'] = AuthComponent::password($this->data['Usuario']['senha']);
        }
        if(isset($this->data['Usuario']['senha_nova']) && $this->data['Usuario']['senha_nova'] != ""){
            $this->data['Usuario']['senha'] = AuthComponent::password($this->data['Usuario']['senha_nova']);
        }
        App::import("helper", "String");
        $this->String = new StringHelper(new View(null));
        $this->Session = new CakeSession();

        if (isset($this->data[$this->alias]['data_nascimento']) && $this->data[$this->alias]['data_nascimento'] != "") {
            $this->data[$this->alias]['data_nascimento'] = $this->String->dataFormatada("Y-m-d", $this->data[$this->alias]['data_nascimento']);
        }
        if (isset($this->data[$this->alias]['data_expedicao']) && $this->data[$this->alias]['data_expedicao'] != "") {
            $this->data[$this->alias]['data_expedicao'] = $this->String->dataFormatada("Y-m-d", $this->data[$this->alias]['data_expedicao']);
        }
        
        return true;
    }

/**
 * afterFind
 *
 * sobrecarga do metodo executado depois de buscar o registro
 */
    public function afterFind($results, $primary = false) {
        App::import("helper", "String");
        $this->String = new StringHelper(new View(null));

        if (!empty($results)) {
            foreach ($results as $k => &$r) {
                if(isset($r[$this->alias])){
                    if(isset($r[$this->alias]['data_nascimento']) && $r[$this->alias]['data_nascimento'] != ""){
                        @$r[$this->alias]['data_nascimento'] = $this->String->dataFormatada("d-m-Y", $r[$this->alias]['data_nascimento']);
                    }
                }
            }
        }

        return $results;
    }
    
    public function confirmPassword($password = null){
        if (isset($this->data[$this->name]['senha_nova']) && isset($this->data[$this->name]['confirma']))
            return (@$this->data[$this->name]['senha_nova'] == @$this->data[$this->name]['confirma']);
        else
            return true;
    }

    public function generatePAssword($length = 8){
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@";
        $password = substr( str_shuffle( $chars ), 0, $length );
        return $password;
    }


}