<?php
/**
 * Grupo Model
 *
 * @property Grupo $Grupo
 */

class Grupo extends FdUsuariosAppModel {

    public $actsAs = array('Acl' => array('type' => 'requester'));

    public $groups = array(
                            'SUPER_ADMIN'       => 7, 
                            'ADMIN'             => 1, 
                            'GERENTE'           => 2,
                            'PUBLICO'           => 3,
                            'EDITOR_NOTICIAS'   => 4,
                            'EDITOR_EVENTOS'    => 5,
                            'EDITOR_CALENDARIO' => 6,
                            'SAC'               => 8,
                            'CLIENTE'           => 9
                        );

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'nome' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'O nome do grupo é obrigatório'
            ),
        )
    );

//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
    public $hasMany = array(
        'Usuario' => array(
            'className' => 'Usuario',
            'foreignKey' => 'grupo_id',
            'dependent' => false,
        )
    );

    public function parentNode(){
        return null;
    }

    public function get_group($grupo){
        return $this->groups[$grupo];
    }
}