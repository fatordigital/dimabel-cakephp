<?php
App::uses('AuthComponent', 'Controller/Component');
/**
 * Estado Model
 *
 * @property Estado $Estado
 */
class Estado extends FdUsuariosAppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array();

	//The Associations below have been created with all possible keys, those that are not needed can be removed
/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array();

/**
 * beforeSave
 *
 * sobrecarga do metodo executado antes de salvar o registro
 */
	public function beforeSave($options = array()) {		
    }

/**
 * afterSave
 *
 * sobrecarga do metodo executado depois de salvar o registro
 */
    public function afterSave($created, $options = array()) {    	
    }

}
