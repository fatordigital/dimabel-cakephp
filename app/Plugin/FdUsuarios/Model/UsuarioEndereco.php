<?php
App::uses('AuthComponent', 'Controller/Component');
/**
 * UsuarioEndereco Model
 *
 * @property UsuarioEndereco $UsuarioEndereco
 */
class UsuarioEndereco extends FdUsuariosAppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'usuario_id' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo de preenchimento obrigatório.',
			),
		),
		'cep' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo de preenchimento obrigatório.',
				'required' => true,
                'on' => 'create'
			),
		),
		'endereco' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo de preenchimento obrigatório.',
				'required' => true,
                'on' => 'create'
			),
		),
		'numero' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo de preenchimento obrigatório.',
				'required' => true,
                'on' => 'create'
			),
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed
/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Usuario' => array(
			'className' => 'Usuario',
			'foreignKey' => 'usuario_id',
			'conditions' => '',
            'fields' => '',
		),
		'Cidade' => array(
		    'className' => 'Cidade',
		    'foreignKey' => 'cidade_id',
		    'dependent' => false
		),
		'Estado' => array(
		    'className' => 'Estado',
		    'foreignKey' => 'estado_id'
		)
	);

	public $hasMany = array(
        
    );

/**
 * beforeSave
 *
 * sobrecarga do metodo executado antes de salvar o registro
 */
	public function beforeSave($options = array()) {
		if(isset($this->data[$this->alias]['cep']) && $this->data[$this->alias]['cep'] != ""){
			$this->data[$this->alias]['cep'] = preg_replace("/[^0-9]/", "", $this->data[$this->alias]['cep']);
		}
        return true;
    }

/**
 * afterSave
 *
 * sobrecarga do metodo executado depois de salvar o registro
 */
    public function afterSave($created, $options = array()) {
    	if(isset($this->data[$this->alias]['cobranca']) && $this->data[$this->alias]['cobranca'] == true){
			if(isset($this->data[$this->alias]['usuario_id']) && $this->data[$this->alias]['usuario_id'] != ""){
    			$this->query("UPDATE usuario_enderecos SET cobranca = 0, modified = '". date('Y-m-d H:i:s') ."' WHERE id <> " . $this->id . " AND usuario_id = " . $this->field('usuario_id'));
    		}
    	}
    }

    public function afterFind($results, $primary = false) {
        
        if (!empty($results)) {
            foreach ($results as $k => $result) {
            	if(isset($result['Cidade'])){
                	$results[$k]['UsuarioEndereco']['cidade'] = $result['Cidade']['nome'];
                }
                if(isset($result['Estado'])){
                	$results[$k]['UsuarioEndereco']['estado'] = $result['Estado']['nome'];
            	}
            }
        }

        return $results;
    }

}
