<?php $this->Html->addCrumb('Grupos de Usuários', array('action' => 'index')) ?>
<?php $this->Html->addCrumb('Cadastrar Grupo de Usuário') ?>

<h3>Cadastrar Grupo de Usuário</h3>

<div class="panel">
	<div class="panel-body">
		<?php echo $this->Form->create('Grupo', array('role' => 'form', 'class' => 'minimal', 'novalidate' => true)) ?>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<label>Status</label>
						<div class="icheck">
							<?php echo $this->Form->input('status', array(
																		'type' => 'radio', 
																		'options' => array(1 => 'Ativo', 0 => 'Inativo'), 
																		'default' => 1, 
																		'legend' => false, 
																		'before' => '<div class="radio">', 
																		'after' => '</div>', 
																		'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('nome', array('class' => 'form-control')); ?>
					</div>
				</div>
			</div>
			<?php echo $this->Form->submit('Salvar', array('class' => 'btn btn-success', 'div' => false)) ?>
			<a href="<?php echo $this->Html->Url(array('action' => 'index')); ?>" class="btn btn-default">Cancelar</a>
		<?php echo $this->Form->end() ?>
	</div>
</div>