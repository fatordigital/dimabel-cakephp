<?php echo $this->Html->script('/fd_usuarios/js/fatorcms/usuario/form.js'); ?>

<script type="text/javascript">
	selectFieldsPessoaTipo('F');
</script>


<?php $this->Html->addCrumb('Usuários', array('action' => 'index')) ?>
<?php $this->Html->addCrumb('Cadastrar Usuário') ?>

<h3>Cadastrar Usuário</h3>

<div class="panel">
	<div class="panel-body">
		<?php echo $this->Form->create('Usuario', array('role' => 'form', 'class' => 'minimal', 'novalidate' => true)) ?>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<label>Status</label>
						<div class="icheck">
							<?php echo $this->Form->input('status', array(
																		'type' => 'radio', 
																		'options' => array(1 => 'Ativo', 0 => 'Inativo'), 
																		'default' => 1, 
																		'legend' => false, 
																		'before' => '<div class="radio">', 
																		'after' => '</div>', 
																		'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('grupo_id', array('options' => $grupos, 'class' => 'form-control', 'default' => 5)) ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('nome', array('label' => 'Nome', 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('nome_pai', array('label' => 'Nome do Pai', 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('nome_mae', array('label' => 'Nome do Mãe', 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('email', array('label' => 'E-mail', 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('data_nascimento', array('label' => 'Data de Nascimento', 'type' => 'text', 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('naturalidade', array('label' => 'Naturalidade', 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('rg', array('label' => 'RG', 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('orgao_emissor', array('label' => 'Orgão Emissor', 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('data_expedicao', array('label' => 'Data de Expedição', 'type' => 'text', 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<label>Tipo de Pessoa</label>
						<div class="icheck">
						<?php echo $this->Form->input('pessoa_tipo', array(
							            			'label' 	=> false,
							            			'legend'	=> false,
							            			'type'		=> 'radio',
							            			'required' 	=> 'required',
							            			'options' 	=> array('F' => 'Pessoa Física', 'J' => 'Pessoa Jurídica'),
							            			'default'	=> 'F',
							            			'class'		=> 'form-control',
							            			'before' => '<div class="radio">', 
																		'after' => '</div>', 
																		'separator' => '</div><div class="clearfix"></div><div class="radio">'
							            		)
							            ); ?>
						</div>
					</div>					
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('cpf', array('label' => 'CPF', 'class' => 'form-control')) ?>
					</div>
					<div class="form-group">
						<?php echo $this->Form->input('cnpj', array('label' => 'CNPJ', 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('telefone', array('label' => 'Telefone', 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('celular', array('label' => 'Celular', 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('senha_nova', array('label' => 'Senha de acesso', 'type'=>'password', 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('confirma', array('label' => 'Confirmar senha de acesso', 'type' => 'password', 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
			<?php /*
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('telefone', array('label' => 'Telefone', 'class' => 'form-control')) ?>
					</div>
				</div>
			</div> */ ?>
			<?php echo $this->Form->submit('Salvar', array('class' => 'btn btn-success', 'div' => false)) ?>
			<a href="<?php echo $this->Html->Url(array('action' => 'index')); ?>" class="btn btn-default">Cancelar</a>
		<?php echo $this->Form->end() ?>
	</div>
</div>

<script type="text/javascript">
    $(document).ready(function(){        
        $('#UsuarioDataNascimento').mask('00/00/0000'); 
        $('#UsuarioDataExpedicao').mask('00/00/0000'); 
        $('#UsuarioTelefone').mask('(00)0000-0000'); 
        $('#UsuarioCelular').mask('(00)00000-0000'); 
        $('#UsuarioCpf').mask('000.000.000-00', {reverse: true});
        $('#UsuarioRg').mask('000.000.00-00', {reverse: true});                
    });
</script>