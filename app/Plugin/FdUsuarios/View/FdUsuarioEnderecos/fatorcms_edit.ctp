<?php $this->Html->addCrumb('Endereços', array('action' => 'index')) ?>
<?php $this->Html->addCrumb('Editar Endereço') ?>

<h3>Editar Endereço</h3>

<div class="panel">
	<div class="panel-body">
		<?php echo $this->Form->create('UsuarioEndereco', array('role' => 'form', 'class' => 'minimal', 'novalidate' => true)) ?>
			<?php echo $this->Form->input('id') ?>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<label>Cobrança</label>
						<div class="icheck">
							<?php
								echo $this->Form->input('cobranca', array(
																		'type' 		=> 'radio',
																		'div'		=> false,
																		'legend'	=> false,
																		'options' 	=> array(
																							'1' => 'Residencial', 
																							'0' => 'Entrega'
																						),
																		'before' => '<div class="radio">', 
																		'after' => '</div>', 
																		'separator' => '</div><div class="clearfix"></div><div class="radio">'
																	)
														); 
							?>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('cep', array('label' => 'CEP', 'class' => 'form-control cep')) ?>
					</div>
					<div class="form-group">
						<?php echo $this->Form->input('endereco', array('label' => 'Endereço', 'class' => 'form-control')) ?>
					</div>
					<div class="form-group">
						<?php echo $this->Form->input('numero', array('label' => 'Número', 'class' => 'form-control')) ?>
					</div>
					<div class="form-group">
						<?php echo $this->Form->input('complemento', array('label' => 'Complemento', 'class' => 'form-control')) ?>
					</div>
					<div class="form-group">
						<?php echo $this->Form->input('bairro', array('class' => 'form-control')) ?>
					</div>
					<div class="form-group">
						<?php echo $this->Form->input('estado_id', array('class' => 'form-control', 'options' => $estados)); ?>
					</div>
					<div class="form-group">
						<?php echo $this->Form->input('cidade_id', array('class' => 'form-control', 'options' => $cidades)) ?>
					</div>
				</div>
			</div>
			<?php echo $this->Form->submit('Salvar', array('class' => 'btn btn-success', 'div' => false)) ?>
			<a href="<?php echo $this->Html->Url(array('action' => 'index')); ?>" class="btn btn-default">Cancelar</a>
		<?php echo $this->Form->end() ?>
	</div>
</div>

<script type="text/javascript">
    $(document).ready(function(){        
        //$('#UsuarioEnderecoCep').mask('00.000-000');                
    });
</script>