<?php $this->Html->addCrumb('Endereços') ?>

<div class="row">
    <h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
        Endereços 
        <?php echo $this->Html->link('Cadastrar Endereço', array('action' => 'add', 'usuario' => $this->params->named['usuario']), array('class' => 'btn btn-info pull-right')); ?>
    </h3>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <div class="row-fluid">
                    <div class="col-sm-12 col-md-10 col-lg-11">
                        <?php echo $this->FilterForm->create(null, array('role' => 'form', 'class' => 'form-inline')) ?>
                            <div class="form-group">
                                <?php echo $this->FilterForm->input('filter_cep', array('placeholder' => 'Filtrar por CEP', 'class' => 'form-control')) ?>
                            </div>
                            <div class="form-group">
                                <?php echo $this->FilterForm->input('filter_endereco', array('placeholder' => 'Filtrar por Endereço', 'class' => 'form-control')) ?>
                            </div>
                            <?php echo $this->FilterForm->submit('Filtrar', array('class' => 'btn btn-success', 'div' => false)) ?>
                            <?php echo $this->Html->link('Limpar Filtro', array('action' => $this->params->parans['action']), array('class' => 'btn btn-warning')) ?>
                        <?php echo $this->FilterForm->end() ?>
                    </div>
                    <div class="col-lg-1 visible-md visible-lg">
                       <?php echo $this->Element('/fatorcms/limit'); ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <table class="table table-hover general-table">
                    <thead>
                        <tr>
                            <th style="width:60px"><?php echo $this->Paginator->sort('id', '#') ?></th>                            
                            <th><?php echo $this->Paginator->sort('cep', 'CEP') ?></th>
                            <th><?php echo $this->Paginator->sort('endereco', 'Endereço') ?></th>
                            <th><?php echo $this->Paginator->sort('numero', 'Número') ?></th>
                            <th style="width:80px"><?php echo $this->Paginator->sort('modified', 'Modificado') ?></th>
                            <th class="text-center" style="width:180px">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($enderecos) > 0): ?>
                            <?php foreach($enderecos AS $endereco): ?>
                            <tr>
                                <td><?php echo $endereco['UsuarioEndereco']['id'] ?></td>                                
                                <td><?php echo $endereco['UsuarioEndereco']['cep'] ?></td>
                                <td><?php echo $endereco['UsuarioEndereco']['endereco'] ?></td>
                                <td><?php echo $endereco['UsuarioEndereco']['numero'] ?></td>
                                <td class="text-center"><?php echo $endereco['UsuarioEndereco']['modified']!='0000-00-00 00:00:00' ? $this->Time->format($endereco['UsuarioEndereco']['modified'], '%d/%m/%Y %H:%M:%S') : '---' ?></td>
                                <td>
                                    <?php echo $this->Html->link('<i class="fa fa-edit"></i> Editar', array('action' => 'edit', 'usuario' => $this->params->named['usuario'],  $endereco['UsuarioEndereco']['id']), array('class' => 'btn btn-primary btn-sm', 'escape' => false)) ?>                                  

                                    <?php echo $this->Html->link('<i class="fa fa-trash-o"></i> Remover', array('action' => 'delete', 'usuario' => $this->params->named['usuario'], $endereco['UsuarioEndereco']['id']), array('class' => 'btn btn-danger btn-sm', 'escape' => false), __('Deseja mesmo remover o registro # %s?', $endereco['UsuarioEndereco']['endereco'])) ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="6" class="text-center">Nenhum registro encontrado.</td>
                            </tr>
                        <?php endIf; ?>
                    </tbody>
                </table>

				<?php echo $this->Element('/fatorcms/paginator'); ?>
            </div>
        </section>
    </div>
</div>