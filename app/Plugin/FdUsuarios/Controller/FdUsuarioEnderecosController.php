<?php

class FdUsuarioEnderecosController extends FdUsuariosAppController {

    public $helpers = array('Estados', 'String');
    public $uses = array('FdUsuarios.UsuarioEndereco','FdUsuarios.Cidade','FdUsuarios.Estado');

    public function beforeFilter(){
        parent::beforeFilter();

        App::import('Model', 'FdUsuarios.UsuarioEndereco');
        $this->UsuarioEndereco = new UsuarioEndereco();
    }

/**
 * fatorcms_index method
 *
 * @return void
 */
    public function fatorcms_index($page = 1){

        if(!isset($this->params->named['usuario']) || $this->params->named['usuario'] == ""){
            $this->Session->setFlash(__('Paramêtros inválidos'), 'fatorcms_danger');
            $this->redirect(array('admin' => true, 'controller' => 'usuarios', 'action' => 'index'));
        }

        // Add filter
        $this->FilterResults->addFilters(
            array(
                'filter' => array(
                    'OR' => array(
                        'UsuarioEndereco.cep'       => array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' )),
                        'UsuarioEndereco.endereco' => array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' )),
                    )
                ),
                'filter_cep' => array(
                    'UsuarioEndereco.cep'       => array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' ))
                ),
                'filter_endereco' => array(
                    'UsuarioEndereco.endereco'     => array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' ))
                ),

            )
        );
        // Define conditions
        $this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

        // Paginate
        $options['conditions']  = $this->FilterResults->getConditions();
        if(isset($this->params->named['usuario']) && $this->params->named['usuario'] != ""){
            if(is_array($options['conditions'])){
                $options['conditions'] = array_merge($options['conditions'], array('UsuarioEndereco.usuario_id' => $this->params->named['usuario']));
            }else{
                $options['conditions'] = array('UsuarioEndereco.usuario_id' => $this->params->named['usuario']);
            }
        }

        $this->paginate = $options;

        //exportar? 
        if(isset($this->params->params['named']['acao']) && $this->params->params['named']['acao'] == "exportar"){
            $this->Reports->xls($this->UsuarioEndereco->find('all', array('conditions' => $options['conditions'])), 'Endereços');
        }

        // Paginate
        $this->UsuarioEndereco->recursive = 1;        
        $enderecos = $this->paginate();
        $this->set('enderecos', $enderecos);
    }

/**
 * fatorcms_add method
 *
 * @return void
 */
    public function fatorcms_add(){
        if(!isset($this->params->named['usuario']) || $this->params->named['usuario'] == ""){
            $this->Session->setFlash(__('Paramêtros inválidos'), 'fatorcms_danger');
            $this->redirect(array('admin' => true, 'controller' => 'usuarios', 'action' => 'index'));
        }

        // Cidades e Estados
        $cidades = $this->Cidade->find('list', array('fields' => array('Cidade.id', 'Cidade.nome'), 'order' => array('Cidade.nome ASC'))); 
        $this->set(compact('cidades'));   
        $estados = $this->Estado->find('list', array('fields' => array('Estado.id', 'Estado.nome'), 'order' => array('Estado.nome ASC')));
        $this->set(compact('estados'));

        if ($this->request->is('post')) {
            $this->UsuarioEndereco->create();

            if(isset($this->params->named['usuario']) && $this->params->named['usuario'] != ""){
                $this->request->data['UsuarioEndereco']['usuario_id'] = $this->params->named['usuario'];
            }

            if ($this->UsuarioEndereco->save($this->request->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso.'), 'fatorcms_success');
                $this->redirect(array('action' => 'index', 'usuario' => $this->params->named['usuario']));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'), 'fatorcms_danger');
            }
        }
    }

/**
 * fatorcms_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function fatorcms_edit($id = null){

        if (!$this->UsuarioEndereco->exists($id)) {
            throw new NotFoundException(__('Registro Inválido.'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {

            if(isset($this->params->named['usuario']) && $this->params->named['usuario'] != ""){
                $this->request->data['UsuarioEndereco']['usuario_id'] = $this->params->named['usuario'];
            }

            if ($this->UsuarioEndereco->save($this->request->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso.'), 'fatorcms_success');

                //custom do redirect para o FilterResults
                if($this->Session->read('referer') && stripos($this->Session->read('referer'), '/admin/')){
                    $this->redirect($this->Session->read('referer'));
                }else{
                    $this->redirect(array('action' => 'index', 'usuario' => $this->params->named['usuario']));
                }
                //$this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'), 'fatorcms_danger');
            }
        } else {
            $options = array('conditions' => array('UsuarioEndereco.' . $this->UsuarioEndereco->primaryKey => $id));
            $this->request->data = $this->UsuarioEndereco->find('first', $options);

            $this->Session->write('referer', $this->referer());
        }

        $conditions_cidades = array('conditions' => array());
        if(isset($this->request->data['UsuarioEndereco'])){            
            $conditions_cidades = array('Cidade.estado' => $this->request->data['UsuarioEndereco']['estado_id']);
        }        

        // Cidades e Estados
        $cidades = $this->Cidade->find('list', array('fields' => array('Cidade.id', 'Cidade.nome'), 'conditions' => $conditions_cidades, 'order' => array('Cidade.nome ASC'))); 
        $this->set(compact('cidades'));   
        $estados = $this->Estado->find('list', array('fields' => array('Estado.id', 'Estado.nome'), 'order' => array('Estado.nome ASC')));
        $this->set(compact('estados'));
    }

/**
 * fatorcms_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function fatorcms_delete($id = null){
        $this->UsuarioEndereco->id = $id;
        if (!$this->UsuarioEndereco->exists()) {
            throw new NotFoundException(__('Registro Inválido.'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->UsuarioEndereco->delete()) {
            $this->Session->setFlash('Registro deletado com sucesso.', 'fatorcms_success');

            $this->Session->setFlash('Registro deletado com sucesso.', 'fatorcms_success');
            // $this->redirect(array('action' => 'index'));
            $this->_redirectFilter($this->referer());
        }
        $this->Session->setFlash('O registro não pode ser removido.', 'fatorcms_warning');

        $this->_redirectFilter($this->referer());
    }

    public function fatorcms_ajax_get_cidades($estado = 23){
        $return = array();
        $this->autoRender = false;        
        if ($this->request->is('ajax')){
            $cidades = $this->Cidade->find('all', array('fields' => array('Cidade.id', 'Cidade.nome'), 'conditions' => array('Cidade.estado' => $estado), 'order' => array('Cidade.nome ASC'))); 
            $return = $cidades;          
        }
        $return = json_encode($cidades);
        return $return;        
    }

}