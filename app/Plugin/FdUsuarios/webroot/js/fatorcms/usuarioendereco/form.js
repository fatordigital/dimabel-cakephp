$(document).ready(function(){

	$('.ajax_estados').on('change',function(){
		var estado_id = $(this).val();
		
		$('.result_cidades').empty();
		$('.result_cidades').append('<option>Carrendo cidades...</option>');

		$.ajax({		  
			url: "/fatorcms/fd_usuarios/fd_usuario_enderecos/ajax_get_cidades/"+estado_id,		  
			dataType: "json"
		}).done(function(result){
			if(result){				
				$('.result_cidades').empty();
				$.each(result, function(i, item) {					
					$('.result_cidades').append('<option value="'+item.Cidade.id+'">'+item.Cidade.nome+'</option>');
				});
			}
  		});
	});

});