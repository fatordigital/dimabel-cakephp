$(document).ready(function(){
	$("[name='data[Usuario][pessoa_tipo]']").on('ifClicked', function(event){
		selectFieldsPessoaTipo($(this).val());
	});
});


function selectFieldsPessoaTipo(tipo_pessoa_val){
	$(document).ready(function(){
		var tipo_pessoa = tipo_pessoa_val;
		if(tipo_pessoa == 'J'){
			$("[name='data[Usuario][cpf]']").attr("disabled", "disabled").closest('.form-group').hide();
			$("[name='data[Usuario][cnpj]']").removeAttr("disabled").closest('.form-group').show();
		}else{
			$("[name='data[Usuario][cpf]']").removeAttr("disabled").closest('.form-group').show();
			$("[name='data[Usuario][cnpj]']").attr("disabled", "disabled").closest('.form-group').hide();
		}
	});
}