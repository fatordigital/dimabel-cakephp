<h1>Dashboard</h1>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6 col-lg-12">
                <table class="table">
                    <thead>
                    <tr>
                        <td>#</td>
                        <td>Nome</td>
                        <td>Telefone</td>
                        <td>E-mail</td>
                        <td>CPF</td>
                        <td>Valor</td>
                        <td>Consórcio</td>
                        <td>Categoria</td>
                        <td>Marca</td>
                        <td>Opções</td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($simuladores as $simulador) { ?>
                        <tr>
                            <td>#<?php echo $simulador['Simulador']['id'] ?></td>
                            <td><?php echo $simulador['Simulador']['nome'] ?></td>
                            <td><?php echo $simulador['Simulador']['telefone'] ?></td>
                            <td><?php echo $simulador['Simulador']['email'] ?></td>
                            <td><?php echo $simulador['Simulador']['cpf'] ?></td>
                            <td>R$ <?php echo $this->String->bcoToMoeda($simulador['Simulador']['valor']) ?></td>
                            <td><?php echo $simulador['Consorcio']['nome'] ?></td>
                            <td><?php echo $simulador['Categoria']['nome'] ?></td>
                            <td><?php echo $simulador['Marca']['nome'] ?></td>
                            <td>
                                <a href="<?php echo $this->Html->Url('/fatorcms/simuladores/show/' . $simulador['Simulador']['id']) ?>" class="btn btn-info btn-xs" title="Visualizar">
                                    <i class="glyphicon glyphicon-eye-open"></i>
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>