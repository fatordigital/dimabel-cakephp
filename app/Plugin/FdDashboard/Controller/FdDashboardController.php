<?php

class FdDashboardController extends FdDashboardAppController
{

    public $uses = array('FdSimuladores.Simulador');

    public function fatorcms_index()
    {

        $simuladores = $this->Simulador->find('all', array('limit' => 30, 'order' => array('Simulador.created' => 'desc')));
        $this->set(compact('simuladores'));

    }

}