<?php

class CartaContemplada extends FdCartaContempladaAppModel
{

    public $belongsTo = array(
        'Segmento' => array(
            'className' => 'Segmento',
            'foreignKey' => 'segmento_id'
        )
    );

    public $hasMany = array(
        'Reservas' => array(
            'className' => 'CartaReserva',
            'foreignKey' => 'carta_id'
        )
    );


    public function beforeSave($options = array())
    {
        $data = $this->data[$this->alias];

        App::import('Helper', 'String');
        $StringHelper = new StringHelper(new View());

        if (!empty($data['valor_carta'])) {
            $this->data[$this->alias]['valor_carta'] = $StringHelper->moedaToBco($data['valor_carta']);
        }
        if (!empty($data['valor_entrada'])) {
            $this->data[$this->alias]['valor_entrada'] = $StringHelper->moedaToBco($data['valor_entrada']);
        }
        if (!empty($data['valor_parcela'])) {
            $this->data[$this->alias]['valor_parcela'] = $StringHelper->moedaToBco($data['valor_parcela']);
        }

        parent::beforeSave($options);
    }


}