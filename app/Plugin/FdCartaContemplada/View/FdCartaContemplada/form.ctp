<div class="panel">
    <div class="panel-body">

        <div class="row">
            <div class="col-md-6 col-lg-4">
                <div class="form-group">
                    <label>Status</label>
                    <div class="icheck">
                        <?php echo $this->Form->input('status', array(
                            'type' => 'radio',
                            'options' => array(1 => 'Ativo', 0 => 'Inativo'),
                            'default' => 1,
                            'legend' => false,
                            'before' => '<div class="radio">',
                            'after' => '</div>',
                            'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="form-group">
                    <label for="">
                        Tipo
                    </label>
                    <div class="icheck">
                        <?php echo $this->Form->input('segmento_id', array(
                            'type' => 'radio',
                            'options' => array(1 => 'Imóvel', 2 => 'Veículo'),
                            'default' => 1,
                            'legend' => false,
                            'before' => '<div class="radio">',
                            'after' => '</div>',
                            'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="">
                        Nome Consórcio <strong class="text-danger">*</strong>
                    </label>
                    <?php echo $this->Form->input('consorcio', array('required', 'class' => 'form-control', 'placeholder' => 'Nome Consórcio', 'label' => false, 'legend' => false)) ?>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="">
                                Valor da Carta <strong class="text-danger">*</strong>
                            </label>
                            <?php echo $this->Form->text('valor_carta', array('required', 'class' => 'form-control money1', 'placeholder' => '0,00', 'label' => false, 'legend' => false)) ?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="">
                                Valor da Entrada <strong class="text-danger">*</strong>
                            </label>
                            <?php echo $this->Form->text('valor_entrada', array('required', 'class' => 'form-control money1', 'placeholder' => '0,00', 'label' => false, 'legend' => false)) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="">
                        Valor da Parcela <strong class="text-danger">*</strong>
                    </label>
                    <?php echo $this->Form->text('valor_parcela', array('required', 'class' => 'form-control money1', 'placeholder' => '0,00', 'label' => false, 'legend' => false)) ?>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="">
                        Parcelas <strong class="text-danger">*</strong>
                    </label>
                    <?php echo $this->Form->text('parcelas', array('required', 'class' => 'form-control', 'placeholder' => 'Quantidade Parcelas, apenas número', 'label' => false, 'legend' => false)) ?>
                </div>
            </div>
        </div>

        <?php echo $this->Form->submit('Salvar', array('class' => 'btn btn-success', 'div' => false)) ?>

        <a href="<?php echo $this->Html->Url(array('action' => 'index')); ?>" class="btn btn-default">Cancelar</a>

    </div>
</div>