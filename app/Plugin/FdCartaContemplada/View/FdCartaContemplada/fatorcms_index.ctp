<?php $this->Html->addCrumb('Carta Contemplada'); ?>

<div class="row">
    <h3 class="col-lg-12 pull-left">
        Carta Contemplada
        <?php echo $this->Html->link('Cadastrar Carta Contemplada', array('action' => 'add'), array('class' => 'btn btn-info pull-right')); ?>
    </h3>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <div class="row-fluid">
                    <div class="col-sm-12 col-md-10 col-lg-11">
                        <?php echo $this->FilterForm->create(null, array('role' => 'form', 'class' => 'form-inline')) ?>
                        <div class="form-group">
                            <?php echo $this->FilterForm->input('filtro_nome', array('placeholder' => 'Filtrar por nome', 'class' => 'form-control')) ?>
                        </div>
                        <?php echo $this->FilterForm->submit('Filtrar', array('class' => 'btn btn-success', 'div' => false)) ?>
                        <?php echo $this->Html->link('Limpar Filtro', array('action' => 'index'), array('class' => 'btn btn-warning')) ?>
                        <?php echo $this->FilterForm->end() ?>
                    </div>
                    <div class="col-lg-1 visible-md visible-lg">
                        <?php echo $this->Element('/fatorcms/limit'); ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body table-responsive">
                <table class="table general-table">
                    <thead>
                    <tr>
                        <th><?php echo $this->Paginator->sort('id', '#') ?></th>
                        <th><?php echo $this->Paginator->sort('consorcio', 'Consórcio') ?></th>
                        <th>Valor Carta</th>
                        <th>Valor Entrada</th>
                        <th>Valor Parcela</th>
                        <th>Parcelas</th>
                        <th>Tipo</th>
                        <th><?php echo $this->Paginator->sort('status') ?></th>
                        <th style="width:300px">Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if(count($paginate) > 0): ?>
                        <?php foreach($paginate AS $data): ?>
                            <?php $current = $data['CartaContemplada'] ?>
                            <tr>
                                <td><?php echo $current['id'] ?></td>
                                <td><?php echo $current['consorcio'] ?></td>
                                <td>R$ <?php echo $this->String->bcoToMoeda($current['valor_carta']) ?></td>
                                <td>R$ <?php echo $this->String->bcoToMoeda($current['valor_entrada']) ?></td>
                                <td>R$ <?php echo $this->String->bcoToMoeda($current['valor_parcela']) ?></td>
                                <td><?php echo $current['parcelas'] ?>x</td>
                                <td><?php echo $data['Segmento']['nome'] ?></td>
                                <td>
                                    <input type="checkbox" class="atualiza-status" value="<?php echo $current['status'] == 1 ? 0 : 1 ?>"
                                           data-id="<?php echo $current['id'] ?>" data-url="<?php echo $this->Html->url(array('action' => 'status')) ?>"
                                           data-on-text="Sim" data-on-color="success" data-off-text="Não"
                                           data-off-color="danger"<?php echo $current['status'] == 1 ? ' checked="checked"' : '' ?>>
                                </td>
                                <td>
                                    <a href="<?php echo $this->Html->Url(array('action' => 'reservas', $current['id'])) ?>" class="btn btn-primary btn-sm">
                                        <label class="label label-info"><?php echo count($data['Reservas']) ?></label>
                                        <i class="fa fa-edit"></i> Reservas
                                    </a>
                                    <?php echo $this->Html->link('<i class="fa fa-edit"></i> Editar', array('action' => 'edit', $current['id']), array('class' => 'btn btn-primary btn-sm', 'escape' => false)) ?>
                                    <?php echo $this->Html->link('<i class="fa fa-trash-o"></i> Remover', array('action' => 'delete', $current['id']), array('class' => 'btn btn-danger btn-sm', 'escape' => false), __('Deseja mesmo remover o registro # %s?', $current['id'])) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="8" class="text-center">Nenhum registro encontrado.</td>
                        </tr>
                    <?php endIf; ?>
                    </tbody>
                </table>

                <?php echo $this->Element('/fatorcms/paginator'); ?>
            </div>
        </section>
    </div>
</div>