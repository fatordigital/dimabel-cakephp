<?php $this->Html->addCrumb('Cartas Contempladas', array('action' => 'index')) ?>
<?php $this->Html->addCrumb('Editar Carta Contemplada') ?>

<h3>Editar Carta Contemplada</h3>

<?php
echo $this->Form->create('CartaContemplada', array('type' => 'file', 'role' => 'form', 'class' => 'minimal'));
include 'form.ctp';
echo $this->Form->end() ;
?>
