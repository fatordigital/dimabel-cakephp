<?php $this->Html->addCrumb('Cartas Contempladas', array('action' => 'index')) ?>
<?php $this->Html->addCrumb('Reservas') ?>

<h3>Reservas</h3>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body table-responsive">
                <table class="table general-table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nome</th>
                        <th>E-mail</th>
                        <th>Telefone</th>
                        <th>CPF</th>
                        <th>Data</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (isset($reservas)) { ?>
                        <?php foreach ($reservas as $reserva) { ?>
                            <tr>
                                <td><?php echo $reserva['CartaReserva']['id'] ?></td>
                                <td><?php echo $reserva['CartaReserva']['nome'] ?></td>
                                <td><?php echo $reserva['CartaReserva']['email'] ?></td>
                                <td><?php echo $reserva['CartaReserva']['telefone'] ?></td>
                                <td><?php echo $reserva['CartaReserva']['cpf'] ?></td>
                                <td><?php echo $this->String->dmY($reserva['CartaReserva']['created']) ?></td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </section>
    </div>
</div>