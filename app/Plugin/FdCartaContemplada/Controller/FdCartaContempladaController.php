<?php

class FdCartaContempladaController extends FdCartaContempladaAppController
{

    public $uses = array('FdCartaContemplada.CartaContemplada', 'FdCartaContemplada.CartaReserva');

    public function fatorcms_index($page = 1)
    {
        $this->FilterResults->addFilters(
            array(
                'filter' => array(
                    'OR' => array(
                        'Carro.nome' => array('operator' => 'LIKE', 'value' => array('before' => '%', 'after' => '%')),
                    )
                ),
                'filtro_nome' => array(
                    'Carro.nome' => array('operator' => 'LIKE', 'value' => array('before' => '%', 'after' => '%'))
                )
            )
        );

        $this->FilterResults->setPaginate('page', $page);
        $this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

        $paginate = $this->paginate();

        $this->set(compact('paginate'));
    }

    public function fatorcms_reservas($id)
    {
        $reservas = $this->CartaReserva->find('all', array('conditions' => array('CartaReserva.carta_id' => $id), 'order' => array('CartaReserva.id DESC')));
        $this->set(compact('reservas'));
    }

    public function fatorcms_add()
    {
        if ($this->request->is('post')) {
            $this->CartaContemplada->create($this->request->data['CartaContemplada']);
            if ($this->CartaContemplada->save()) {
                $this->Session->setFlash(__('Registro salvo com sucesso.'), 'fatorcms_success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'), 'fatorcms_danger');
            }
        }
    }

    public function fatorcms_edit($id)
    {
        if ($this->request->is('PUT')) {
            $this->CartaContemplada->id = $id;
            if ($this->CartaContemplada->save($this->request->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso.'), 'fatorcms_success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'), 'fatorcms_danger');
            }
        }

        $edit = $this->CartaContemplada->findById($id);
        if (!$edit) {
            $this->Session->setFlash(__('Registro não encontrado'), 'fatorcms_danger');
            $this->redirect(array('action' => 'index'));
        }
        $this->data = $edit;
    }

    public function fatorcms_delete($id)
    {
        $this->CartaContemplada->id = $id;
        if (!$this->CartaContemplada->exists()) {
            throw new NotFoundException('Registro inválido.');
        }
        $this->request->is('get');
        if ($this->CartaContemplada->delete()) {
            $this->_resetCaches();
            $this->Session->setFlash(__('Registro deletado.'), 'fatorcms_success');
            // $this->redirect(array('action' => 'index'));
            $this->_redirectFilter($this->referer());
        }
        $this->Session->setFlash(__('Registro não pode ser deletado.'), 'fatorcms_warning');
        // $this->redirect(array('action' => 'index'));
        $this->_redirectFilter($this->referer());
    }

    public function fatorcms_status()
    {
        if (!$this->request->is('post')) {
            throw new NotFoundException('Registro inválido.');
        }
        echo $this->_saveStatus('CartaContemplada', $this->request->data['id'], $this->request->data['value']);
        $this->_resetCaches();
        die;
    }

}