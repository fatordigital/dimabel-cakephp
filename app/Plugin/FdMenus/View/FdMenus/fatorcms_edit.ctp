<?php echo $this->Html->css('/js/fatorcms/core/bootstrap-fileupload/bootstrap-fileupload') ?>
<?php echo $this->Html->script('/js/fatorcms/core/bootstrap-fileupload/bootstrap-fileupload'); ?>

<?php $this->Html->addCrumb('Menus', array('action' => 'index')) ?>
<?php $this->Html->addCrumb('Editar Menu') ?>

<h3>Editar Menu</h3>

<div class="panel">
	<div class="panel-body">
	<?php echo $this->Form->create('Menu', array('type' => 'file', 'role' => 'form', 'class' => 'minimal')) ?>
		<?php echo $this->Form->input('id') ?>
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					<label>Status</label>
					<div class="icheck">
						<?php echo $this->Form->input('status', array(
																	'type' => 'radio', 
																	'options' => array(1 => 'Ativo', 0 => 'Inativo'), 
																	'default' => 1, 
																	'legend' => false, 
																	'before' => '<div class="radio">', 
																	'after' => '</div>', 
																	'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					<?php echo $this->Form->input('nome', array('class' => 'form-control')) ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					<?php echo $this->Form->input('slug', array('class' => 'form-control')) ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					<?php echo $this->Form->input('descricao', array('class' => 'form-control', 'label' => 'Descrição')) ?>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="form-group"><h4>Destaque Lateral</h4></div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					<?php echo $this->Form->input('banner_titulo', array('class' => 'form-control', 'label' => 'Título')) ?>
				</div>
				<div class="form-group">
					<label>Banner</label>
					<strong>Tamanho:</strong><br>
					Largura: 320px <br>
					Altura: 160px <br> <br>
					<div class="fileupload fileupload-new" data-provides="fileupload">
						
						<?php if(isset($menu) && $menu['Menu']['thumb'] != ""): ?>
							<div class="fileupload-new thumbnail" style="width: 200px; height: 119px;">
								<img src="<?php echo $this->Html->Url('/'. $menu['Menu']['thumb_path'] .'/'. $menu['Menu']['thumb_dir'] . '/thumb_' . $menu['Menu']['thumb'] , true) ?>" width="200" />
							</div>
						<?php else: ?>
		                    <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
		                    	<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=nenhuma+imagem" alt="" />
		                    </div>
	                    <?php endIf; ?>
	                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>

                        <?php if(isset($menu) && $menu['Menu']['thumb'] != ""): ?>
                        <div class="icheck minimal">
                        	<?php echo $this->Form->input('Menu.thumb.remove', array('type' => 'checkbox', 'label' => 'Remover Imagem Atual', 'div' => false)); ?>
                        </div>
                        <?php endIf; ?>
	                    <div>
	                       <span class="btn btn-white btn-file">
	                           	<span class="fileupload-new"><i class="fa fa-paper-clip"></i> Selecionar imagem</span>
	                           	<span class="fileupload-exists"><i class="fa fa-undo"></i> Alterar</span>
	                           	<?php echo $this->Form->input("thumb", array('type' => 'file', 'class' => 'default', 'label' => false, 'div' => false)); ?>
	                           	<?php echo $this->Form->hidden("thumb_dir") ?>
	                       </span>
	                        <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remover</a>
	                    </div>
	                </div>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('banner_label', array('class' => 'form-control', 'label' => 'Label do botão')) ?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('banner_link', array('class' => 'form-control', 'label' => 'Link do botão')) ?>
				</div>
				<div class="form-group">
					<label>Target</label>
					<div class="icheck">
						<?php echo $this->Form->input('banner_link_target', array(
																	'type' => 'radio', 
																	'options' => array('_blank' => '_blank', '_self' => '_self'), 
																	'default' => '_self', 
																	'legend' => false, 
																	'before' => '<div class="radio">', 
																	'after' => '</div>', 
																	'separator' => '</div><div class="clearfix"></div><div class="radio">
																')); ?>
					</div>
				</div>
			</div>
		</div>


		<?php echo $this->Form->submit('Salvar', array('class' => 'btn btn-success', 'div' => false)) ?>
		<a href="<?php echo $this->Html->Url(array('action' => 'index')); ?>" class="btn btn-default">Cancelar</a>
	<?php echo $this->Form->end() ?>
	</div>
</div>