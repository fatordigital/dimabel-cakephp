<?php
App::uses('AppModel', 'Model');
App::uses('CakeSession', 'Model/Datasource');

class Menu extends FdMenusAppModel {

	public $actsAs = array(	
		'Upload.Upload' => array(
            'thumb' => array(
				'fields' => array(
                    'dir' => 'thumb_dir',
					'type' => 'thumb_type',
					'size' => 'thumb_size',
                ),
				'thumbnailMethod' => 'php',
                'thumbnailSizes' => array(
                    'thumb' => '320x160',
                )
            ),
        )
	);

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'nome' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo de preenchimento obrigatório.',
			),
		),
		'slug' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo de preenchimento obrigatório.',
			),
		),
	);
	
/**
 * hasMany associations
 *
 * @var array
 */
    public $hasMany = array(
        'Link' => array(
            'className' => 'Link',
            'foreignKey' => 'menu_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => 'Link.lft ASC',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => '',
        ),
    );

    public function afterSave($created, $options = array()) {
    	parent::afterSave($created, $options = array());

    	$menu = $this->find('first', array('recursive' => -1, 'conditions' => array('id' => $this->id), 'fields' => array('thumb', 'thumb_dir')));

    	//begin rename files
    	if(isset($this->data[$this->alias]['thumb']) && $this->data[$this->alias]['thumb'] != ""){
    		if(isset($this->data[$this->alias]['nome'])){
	    		$pre_new_name = strtolower(Inflector::slug($this->data[$this->alias]['nome'], '-'));
				$pre_new_name = str_replace("_", "-", $pre_new_name);
				$path = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'files' . DS . 'menu' . DS . 'thumb';
	    	}
    		$pre_old_name = $this->data[$this->alias]['thumb'];

    		$new_name = $pre_new_name;
			$old_name = $pre_old_name;

			if(file_exists($path . DS . $menu[$this->alias]['thumb_dir'] . DS . $old_name)){
				$file = $path . DS . $menu[$this->alias]['thumb_dir'] . DS . $old_name;
				$infos = pathinfo($file);
				$new_file = $path . DS . $menu[$this->alias]['thumb_dir'] . DS . $new_name . '.' . $infos['extension'];

				if(rename($file, $new_file)){

					//load lib
        			include_once(ROOT . DS . APP_DIR . DS . 'Lib' . DS . 'Wideimage/WideImage.php');

					$this->query('UPDATE menus SET thumb = "' . $new_name . '.' . $infos['extension'] . '" WHERE id = ' . $this->id);

					$caminho_novo 	= $path . DS . $menu[$this->alias]['thumb_dir'];
					$outFile    	= $caminho_novo.DS.$pre_new_name . '.' . $infos['extension'];
					if(strtolower($infos['extension']) == 'jpg'){
                        WideImage::load($new_file)->resize(1920)->crop('center', 'top', 1920, 770)->saveToFile($outFile, 80); 
                        WideImage::load($new_file)->resize(483)->crop('center', 'top', 483, 250)->saveToFile($outFile, 80); 
                    }else{
                        WideImage::load($new_file)->resize(1920)->crop('center', 'top', 1920, 770)->saveToFile($outFile);
                    }

					//outros renames
					$file = $path . DS . $menu[$this->alias]['thumb_dir'] . DS . 'thumb_' . $old_name;
					$infos = pathinfo($file);
					$new_file = $path . DS . $menu[$this->alias]['thumb_dir'] . DS . 'thumb_' . $new_name . '.' . $infos['extension'];
					rename($file, $new_file);
				}
			}
		}

	}
}