<?php $this->Html->addCrumb('Simulações', array('action' => 'index')); ?>
<?php $this->Html->addCrumb('Detalhe Simulação'); ?>

<div class="row">
    <h3 class="col-lg-12 pull-left">
        Detalhe Simulação
    </h3>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                Página: <a class="text-info" style="font-weight: bold" href="<?php echo $show['Simulador']['url_ref'] ?>"><?php echo $show['Simulador']['url_ref'] ?></a>
                <br><br>
                Categoria: <strong class="text-info"><?php echo $show['Categoria']['nome'] ?></strong>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h3 class="text-info">Cliente</h3>
                <br>
            </div>
            <div class="col-sm-3">
                <div class="form-inline">
                    <strong>Nome</strong>: <strong class="text-primary"><?php echo $show['Simulador']['nome'] ?></strong>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-inline">
                    <strong>Telefone</strong>: <strong class="text-primary"><?php echo $show['Simulador']['telefone'] ?></strong>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-inline">
                    <strong>E-mail</strong>: <strong class="text-primary"><?php echo $show['Simulador']['email'] ?></strong>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-inline">
                    <strong>CPF</strong>: <strong class="text-primary"><?php echo $show['Simulador']['cpf'] ?></strong>
                </div>
            </div>
            <div class="col-lg-12">
                <hr>
            </div>
        </div>

        <?php if ($show['Simulador']['carro_id'] != '') { ?>
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="text-info">Carro</h3>
                    <br>
                </div>
                <div class="col-sm-3">
                    <div class="form-inline">
                        <strong>Nome</strong>: <strong class="text-primary"><?php echo $show['Carro']['nome'] ?></strong>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-inline">
                        <strong>Marca</strong>: <strong class="text-primary"><?php echo $show['Marca']['nome'] ?></strong>
                    </div>
                </div>
                <div class="col-lg-12">
                    <hr>
                </div>
            </div>
        <?php } ?>

        <?php foreach ($show['SimuladorConsorcio'] as $simulador_consorcio){ ?>
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="text-info">Consórcio <strong><?php echo $simulador_consorcio['nome'] ?></strong></h3>
                    <br>
                </div>
                <div class="col-sm-3">
                    <div class="form-inline">
                        <strong>Parcela</strong>: <strong class="text-primary"><?php echo $simulador_consorcio['parcela'] ?></strong>x
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-inline">
                        <strong>Valor Simulador</strong>: R$ <strong class="text-primary"><?php echo $this->String->bcoToMoeda($show['Simulador']['valor']) ?></strong>
                    </div>
                </div>
                <?php if ($simulador_consorcio['meia_parcela'] == 1) { ?>
                <div class="col-sm-3">
                    <div class="form-inline">
                        <strong>Valor total da parcela</strong>: R$ <strong class="text-primary">
                            <?php $valor_corrente = $consorcioHelper->ParcelaTaxaFixa($show['Simulador']['valor'], $simulador_consorcio['parcela'], $simulador_consorcio[$show['Simulador']['categoria_id'] == 1 ? 'taxa_carro' : 'taxa_imovel']); ?>
                            <?php echo $this->String->bcoToMoeda($valor_corrente); ?>
                        </strong>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-inline">
                        <strong>Valor meia parcela</strong>: R$ <strong class="text-primary">
                            <?php $valor_corrente = $consorcioHelper->ParcelaTaxaFixa($show['Simulador']['valor'], $simulador_consorcio['parcela'], $simulador_consorcio[$show['Simulador']['categoria_id'] == 1 ? 'taxa_carro' : 'taxa_imovel']); ?>
                            <?php echo $this->String->bcoToMoeda($valor_corrente / 2); ?>
                        </strong>
                    </div>
                </div>
                <?php } else { ?>
                    <div class="col-sm-3">
                        <div class="form-inline">
                            <strong>Valor Parcela</strong>: R$ <strong class="text-primary">
                                <?php $valor_corrente = $consorcioHelper->ParcelaTaxaFixa($show['Simulador']['valor'], $simulador_consorcio['parcela'], $simulador_consorcio[$show['Simulador']['categoria_id'] == 1 ? 'taxa_carro' : 'taxa_imovel']); ?>
                                <?php echo $this->String->bcoToMoeda($valor_corrente); ?>
                            </strong>
                        </div>
                    </div>
                <?php } ?>
                <div class="col-lg-12">
                    <hr>
                </div>
            </div>
        <?php } ?>

        <?php echo $this->Form->create('Simulador', array('method' => 'POST')) ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="">
                        Observação
                    </label>
                    <?php echo $this->Form->textarea('observacao', array('class' => 'form-control', 'rows' => 5, 'placeholder' => 'Digite aqui uma observação sobre essa simulação')) ?>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success">
                        Salvar observação
                    </button>
                </div>
            </div>
        </div>
        <?php echo $this->Form->end() ?>
    </div>
</div>