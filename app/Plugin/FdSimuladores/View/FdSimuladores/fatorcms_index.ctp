<?php $this->Html->addCrumb('Simulações'); ?>

<div class="row">
    <h3 class="col-lg-12 pull-left">
		Simulações
	</h3>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <div class="row-fluid">
                    <div class="col-sm-12 col-md-10 col-lg-11">
                        <?php echo $this->FilterForm->create(null, array('role' => 'form', 'class' => 'form-inline')) ?>
                        <div class="form-group">
                            <?php echo $this->FilterForm->input('filtro_nome', array('placeholder' => 'Filtrar por nome', 'class' => 'form-control')) ?>
                        </div>
                        <?php echo $this->FilterForm->submit('Filtrar', array('class' => 'btn btn-success', 'div' => false)) ?>
                        <?php echo $this->Html->link('Limpar Filtro', array('action' => 'index'), array('class' => 'btn btn-warning')) ?>
                        <?php echo $this->FilterForm->end() ?>
                    </div>
                    <div class="col-lg-1 visible-md visible-lg">
                        <?php echo $this->Element('/fatorcms/limit'); ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>


<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6 col-lg-12">
                <table class="table">
                    <thead>
                    <tr>
                        <td>#</td>
                        <td>Nome</td>
                        <td>Telefone</td>
                        <td>E-mail</td>
                        <td>CPF</td>
                        <td>Valor</td>
                        <td>Consórcio</td>
                        <td>Categoria</td>
                        <td>Marca</td>
                        <td>Opções</td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($paginate as $simulador) { ?>
                        <tr>
                            <td>#<?php echo $simulador['Simulador']['id'] ?></td>
                            <td><?php echo $simulador['Simulador']['nome'] ?></td>
                            <td><?php echo $simulador['Simulador']['telefone'] ?></td>
                            <td><?php echo $simulador['Simulador']['email'] ?></td>
                            <td><?php echo $simulador['Simulador']['cpf'] ?></td>
                            <td>R$ <?php echo $this->String->bcoToMoeda($simulador['Simulador']['valor']) ?></td>
                            <td><?php echo $simulador['Consorcio']['nome'] ?></td>
                            <td><?php echo $simulador['Categoria']['nome'] ?></td>
                            <td><?php echo $simulador['Marca']['nome'] ?></td>
                            <td>
                                <a href="<?php echo $this->Html->Url(array('action' => 'show', $simulador['Simulador']['id'])) ?>" class="btn btn-info btn-xs" title="Visualizar">
                                    <i class="glyphicon glyphicon-eye-open"></i>
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>