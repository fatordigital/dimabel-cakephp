<?php
App::uses('AppModel', 'Model');
App::uses('CakeSession', 'Model/Datasource');

class Simulador extends FdSimuladoresAppModel
{

    public $useTable = 'simuladores';

    public $actsAs = array('Containable');

    private $String;

    public $belongsTo = array(
        'Marca' => array(
            'className' => 'Marca',
            'foreignKey' => 'marca_id'
        ),
        'Consorcio' => array(
            'className' => 'Consorcio',
            'foreignKey' => 'consorcio_id'
        ),
        'Categoria' => array(
            'className' => 'Categoria',
            'foreignKey' => 'categoria_id'
        ),
        'Carro' => array(
            'className' => 'Carro',
            'foreignKey' => 'carro_id'
        )
    );

    public $hasMany = array(
        'SimuladorConsorcio' => array(
            'className' => 'SimuladorConsorcio',
            'foreignKey' => 'simulador_id'
        )
    );

    public function getEmailData($id)
    {
        App::import('Helper', 'String');
        $this->String = new StringHelper(new View());

        $simulador = $this->findById($id);
        return array(
            $simulador['Consorcio']['nome'],
            $simulador['Categoria']['nome'],
            $simulador['Marca']['nome'],
            $simulador['Simulador']['nome'],
            $simulador['Simulador']['telefone'],
            $simulador['Simulador']['email'],
            $simulador['Simulador']['cpf'],
            $this->String->bcoToMoeda($simulador['Simulador']['valor']),
            $simulador['Simulador']['mensagem'],
            $simulador['Simulador']['url_ref']
        );
    }

    public function getConsorcioCarroData($id)
    {
        App::import('Helper', 'String');
        $this->String = new StringHelper(new View());

        App::import('Helper', 'FdConsorcios.Consorcio');
        $ConsorcioHelper = new ConsorcioHelper(new View());

        $simulador = $this->findById($id);
        return array(
            $simulador['Consorcio']['nome'],
            $simulador['Categoria']['nome'],
            $simulador['Marca']['nome'],
            $simulador['Simulador']['nome'],
            $simulador['Simulador']['telefone'],
            $simulador['Simulador']['email'],
            $simulador['Simulador']['cpf'],
            $this->String->bcoToMoeda($simulador['Simulador']['valor']),
            $this->String->bcoToMoeda($ConsorcioHelper->Parcela($simulador['Carro']['preco'], 'taxa_carro', $simulador['Simulador']['parcela'])),
            $simulador['Simulador']['url_ref'],
            $simulador['Carro']['nome'],
            $simulador['Simulador']['parcela']
        );
    }

    public function getSimuladorData($id)
    {
        App::import('Helper', 'String');
        $this->String = new StringHelper(new View());

        App::import('Helper', 'FdConsorcios.Consorcio');
        $ConsorcioHelper = new ConsorcioHelper(new View());

        $simulador = $this->findById($id);
        return array(
            $simulador['Consorcio']['nome'],
            $simulador['Categoria']['nome'],
            $simulador['Simulador']['nome'],
            $simulador['Simulador']['telefone'],
            $simulador['Simulador']['cpf'],
            $simulador['Simulador']['email'],
            $this->String->bcoToMoeda($simulador['Simulador']['valor']),
            $this->String->bcoToMoeda($ConsorcioHelper->Parcela($simulador['Simulador']['valor'], 'taxa_carro', $simulador['Simulador']['parcela'])),
            $simulador['Simulador']['parcela'],
            $simulador['Simulador']['url_ref']
        );
    }

}