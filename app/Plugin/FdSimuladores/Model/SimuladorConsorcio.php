<?php
App::uses('AppModel', 'Model');
App::uses('CakeSession', 'Model/Datasource');

class SimuladorConsorcio extends FdSimuladoresAppModel
{

    public $actsAs = array('Containable');

    public $belongsTo = array(
        'Simulador' => array(
            'className' => 'Simulador',
            'foreignKey' => 'simulador_id'
        )
    );


}