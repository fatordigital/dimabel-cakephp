<?php

class FdSimuladoresController extends FdSimuladoresAppController
{

    public $uses = array('FdSimuladores.Simulador');


    public function fatorcms_index($page = 1)
    {
        $this->paginate = array('order' => array('Simulador.created' => 'DESC'));

        $this->FilterResults->addFilters(
            array(
                'filter' => array(
                    'OR' => array(
                        'Simulador.nome' => array('operator' => 'LIKE', 'value' => array('before' => '%', 'after' => '%')),
                    )
                ),
                'filtro_nome' => array(
                    'Simulador.nome' => array('operator' => 'LIKE', 'value' => array('before' => '%', 'after' => '%'))
                )
            )
        );

        $this->FilterResults->setPaginate('page', $page);
        $this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

        if (!isset($_GET['finalizado'])) {
            $this->paginate = array(
                'conditions' => array(
                    'Simulador.finalizado' => 0
                )
            );
        } else {
            $this->paginate = array(
                'conditions' => array(
                    'Simulador.finalizado' => 1
                )
            );
        }


        $paginate = $this->paginate();

        $this->set('paginate', $paginate);

    }

    public function fatorcms_show($id)
    {
        App::import('Helper', 'FdConsorcios.Consorcio');
        $consorcioHelper = new ConsorcioHelper(new View());
        $this->set(compact('consorcioHelper'));

        if ($this->request->is('PUT')) {
            $show = $this->Simulador->findById($id);
            if (!$show) {
                $this->Session->setFlash(__('Registro não encontrado.'), 'fatorcms_danger');
                $this->redirect(array('action' => 'index'));
            }

            $this->Simulador->id = $id;
            if ($this->Simulador->save(array('Simulador' => array('observacao' => $this->request->data['Simulador']['observacao'])))) {
                $this->Session->setFlash(__('Obervação salva com sucesso'), 'fatorcms_success');
                $this->redirect($this->referer());
            } else {
                $this->Session->setFlash(__('Não foi possível salvar a observação.'), 'fatorcms_danger');
                $this->redirect($this->referer());
            }
        }

        $show = $this->Simulador->findById($id);

        if (!$show) {
            $this->Session->setFlash(__('Registro não encontrado.'), 'fatorcms_danger');
            $this->redirect(array('action' => 'index'));
        }

        $this->data = $show;

        $this->set('show', $show);
    }

    public function fatorcms_add()
    {

    }

    public function fatorcms_edit($id = null)
    {

    }


    public function fatorcms_delete($id = null)
    {

    }

    public function fatorcms_status()
    {

    }
}