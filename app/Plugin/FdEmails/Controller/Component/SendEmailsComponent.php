<?php

App::uses('CakeEmail', 'Network/Email');

class SendEmailsComponent extends Component
{

    public $controller = null;
    public $email = '';
    public $templates = array();

    private $SacTipo;
    private $String;
    private $Template;
    private $Email;
    private $Smtp;
    private $Ccs = array();

    public function initialize(Controller $controller)
    {
        $this->required();

        $this->controller = $controller;
        $this->templates = $this->get_templates();
    }

    # Carrega as necessidades para o funcionamento do componente
    private function required()
    {
        App::import('Helper', 'String');
        $this->String = new StringHelper(new View(null));

        App::import('Model', 'FdSac.SacTipo');
        $this->SacTipo = new SacTipo();

        App::import('Model', 'FdEmails.Template');
        $this->Template = new Template();

        App::import('Model', 'FdEmails.Email');
        $this->Email = new Email();

        $this->Smtp = new CakeEmail('smtp');
        $this->ccs();
    }

    # Carrega todas as templates e gerar o cache das mesmas (só irá gerar cache, caso na config esteja habilitado)
    private function get_templates()
    {
        if (Cache::read('templates') === false) {
            $templates = $this->Template->find('list', array('fields' => array('Template.slug', 'Template.conteudo',), 'conditions' => array('status' => true)));
            Cache::write('templates', $templates);
        } else {
            $templates = Cache::read('templates');
        }

        return $templates;
    }

    # Salva o LOG de envio
    private function set_log($to, $subject, $content, $status)
    {
        return $this->Email->save(array('id' => null, 'to' => $to, 'subject' => $subject, 'content' => $content, 'status' => $status));
    }

    private function ccs()
    {
        $ccs = explode(';', Configure::read('Site.Email.CC'));
        foreach ($ccs as $cc) {
            if (!empty($cc)) {
                $this->Ccs[] = trim($cc);
            }
        }
        if (CakeSession::read('email_debug')) {
            $this->Ccs[] = CakeSession::read('email_debug');
        }
        $this->Smtp->bcc($this->Ccs);
        return $this;
    }

    public function send($to, $subject = null, $content)
    {
        if (is_null($subject))
            $subject = 'Contato realizado pelo site';

        $this->Smtp->emailFormat('html');

        if (!$to)
            $to = Configure::read('Site.Ambiente') == 'dev' ? Configure::read('Site.Email.ToDev') : Configure::read('Site.Sac.Email');

        $this->Smtp->to($to);
        $this->Smtp->subject($subject);
		$this->Smtp->bcc('andrew.rodrigues@fatordigital.com.br', 'Fator Digital');
//        $this->Smtp->replyTo();


        if ($this->Smtp->send($content))
            $status = true;
        else {
            $status = false;
        }

        $this->set_log(json_encode($to), $subject, $content, $status);

        return $status;
    }

    public function _enviaSac($data)
    {
        $content_email = str_replace(
            array(
                '{SITE_URL}', '{DATA}', '{NOME}', '{EMAIL}', '{TELEFONE}', '{MENSAGEM}'
            ),
            array(
                Router::url('/', true),
                date('d/m/Y H:i:s'),
                $data['nome'],
                $data['email'],
                $data['telefone'],
                $data['mensagem']
            ), $this->templates['fale_conosco']
        );

        $SacTipo = $this->SacTipo->find('first', array('conditions' => array('SacTipo.id' => $data['sac_tipo_id'])));
        if ($SacTipo) {
            $to = explode(';', $SacTipo['SacTipo']['email']);
            if (count($to) > 1) {
                return $this->send($to, 'Fale Conosco' . (isset($data['assunto']) && !empty($data['assunto']) ? ' - ' . $data['assunto'] : ''), $content_email);
            } else {
                return $this->send($SacTipo['SacTipo']['email'], 'Fale Conosco' . (isset($data['assunto']) && !empty($data['assunto']) ? ' - ' . $data['assunto'] : ''), $content_email);
            }
        }
        return false;
    }

    public function OutroModelo($data)
    {
        $content_email = str_replace(
            array(
                '{CONSORCIO}', '{CATEGORIA}', '{MARCA}', '{NOME}', '{TELEFONE}', '{EMAIL}', '{VALOR}', '{MENSAGEM}', '{SITE_URL}'
            ),
            $data,
            $this->templates['outro_modelo']
        );

        $SacTipo = $this->SacTipo->find('first', array('conditions' => array('SacTipo.slug' => 'outro_modelo')));
        if ($SacTipo) {
            $to = explode(';', $SacTipo['SacTipo']['email']);
            if (count($to) > 1) {
                return $this->send($to, 'Dúvidas/Outro Modelo', $content_email);
            } else {
                return $this->send($SacTipo['SacTipo']['email'], 'Dúvidas/Outro Modelo', $content_email);
            }
        }
        return false;

    }

    public function ContratarCarro($data)
    {
        $content_email = str_replace(
            array(
                '{CONSORCIO}', '{CATEGORIA}', '{MARCA}', '{NOME}', '{TELEFONE}', '{EMAIL}',  '{CPF}', '{VALOR_PARCELA}', '{VALOR}', '{SITE_URL}', '{CARRO}', '{PARCELA}'
            ),
            $data,
            $this->templates['contratar_carro']
        );

        $SacTipo = $this->SacTipo->find('first', array('conditions' => array('SacTipo.slug' => 'consorcio_carro')));
        if ($SacTipo) {
            $to = explode(';', $SacTipo['SacTipo']['email']);
            if (count($to) > 1) {
                return $this->send($to, 'Simular Consórcio', $content_email);
            } else {
                return $this->send($SacTipo['SacTipo']['email'], 'Consórcio Carro', $content_email);
            }
        }
        return false;

    }


    public function Simulador($data)
    {
        $content_email = str_replace(
            array(
                '{CONSORCIO}', '{CATEGORIA}', '{NOME}', '{TELEFONE}', '{CPF}',  '{EMAIL}', '{VALOR}', '{VALOR_PARCELA}', '{PARCELA}', '{SITE_URL}'
            ),
            $data,
            $this->templates['simular_consorcio']
        );

        $SacTipo = $this->SacTipo->find('first', array('conditions' => array('SacTipo.slug' => 'simular_consorcio')));
        if ($SacTipo) {
            $to = explode(';', $SacTipo['SacTipo']['email']);
            if (count($to) > 1) {
                return $this->send($to, 'Simular Consórcio', $content_email);
            } else {
                return $this->send($SacTipo['SacTipo']['email'], 'Consórcio Carro', $content_email);
            }
        }
        return false;
    }

    public function PreContrato($data)
    {
        $consorcio = CakeSession::read('selecionado');
        $content_email = str_replace(
            array(
                '{CONSORCIO}', '{TIPO}', '{NOME}', '{TELEFONE}', '{EMAIL}', '{VALOR}'
            ),
            array(
                $consorcio['Consorcio']['nome'],
                $data['simulation_type'] == 'automovel' ? 'Automóvel' : 'Imóvel',
                $data['nome'],
                $data['telefone'],
                $data['email'],
                'R$ ' . $data['value']
            ),
            $this->templates['pre_contrato']
        );

        $SacTipo = $this->SacTipo->find('first', array('conditions' => array('SacTipo.slug' => 'simular_consorcio')));
        if ($SacTipo) {
            $to = explode(';', $SacTipo['SacTipo']['email']);
            if (count($to) > 1) {
                return $this->send($to, 'Simular Consórcio', $content_email);
            } else {
                return $this->send($SacTipo['SacTipo']['email'], 'Pré Contrato', $content_email);
            }
        }
        return false;
    }

}
