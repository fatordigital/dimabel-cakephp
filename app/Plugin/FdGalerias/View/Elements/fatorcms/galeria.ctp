<script style="text/javascript">
    var APP_ROOT = "<?php echo $this->Html->Url('/', true); ?>"

    function salvar(element){
        var id      = element.closest('tr').find('.file_id').val();
        var legenda = element.closest('tr').find('.file_legenda').val();
        element.html('<span>Salvando...</span>');
        $.ajax({
              type: "POST",
              url: APP_ROOT + '/fatorcms/galerias/images_save/',
              data: {'id': id, 'legenda': legenda},
              success: function(){
                element.html('<span>Salvar<span>');
              },
        });

        return false;
    }
</script>

<!-- The file upload form used as target for the file upload widget -->
<form id="fileupload" action="#" method="POST" enctype="multipart/form-data">
    <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
    <div class="row fileupload-buttonbar" style="width: 80%; margin-left: 0px">
        <div class="col-lg-10">
            <!-- The fileinput-button span is used to style the file input field as button -->
            <span class="btn btn-success fileinput-button">
                <!-- <i class="glyphicon glyphicon-plus"></i> -->
                <span>Adicionar aquivos...</span>
                <input type="file" name="files[]" multiple>
            </span>
            <button type="submit" class="btn btn-primary start">
                <!-- <i class="glyphicon glyphicon-upload"></i> -->
                <span>Iniciar upload</span>
            </button>
            <button type="reset" class="btn btn-warning cancel">
                <!-- <i class="glyphicon glyphicon-ban-circle"></i> -->
                <span>Cancelar upload</span>
            </button>
            <button type="button" class="btn btn-danger delete">
                <!-- <i class="glyphicon glyphicon-trash"></i> -->
                <span>Deletar</span>
            </button>
            <input type="checkbox" class="toggle">
            <!-- The global file processing state -->
            <span class="fileupload-process"></span>
        </div>
        <!-- The global progress state -->
        <div class="col-lg-5 fileupload-progress fade">
            <!-- The global progress bar -->
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                <div class="progress-bar progress-bar-success" style="width:0%;"></div>
            </div>
            <!-- The extended global progress state -->
            <div class="progress-extended">&nbsp;</div>
        </div>
    </div>
    <!-- The table listing the files available for upload/download -->
    <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
</form>

<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td style="width: 100px">
            <span class="preview"></span>
        </td>
        <td>
            <input name="galeria_id[]" type="hidden" value="<?php echo $galeria_id; ?>">
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger"></strong>
        </td>
        <td class="legend">
            <textarea name="legenda[]" required></textarea>
        </td>
        <td>
            <p class="size">Carregando...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td>
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled>
                    <span>Iniciar</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel">
                    <span>Cancelar</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td style="width: 100px">
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                {% } %}
            </span>
        </td>
        <td>
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                    <span>{%=file.name%}</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td class="legend">
            <textarea name="legenda[]" class="file_legenda">{%=file.legenda%}</textarea>
            <input type="hidden" value="{%=file.id%}" class="file_id">
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
            <span class="title"><strong>{%=file.ordem||''%}</strong></span>
        </td>
        <td>
            <a onclick="javascript:salvar($(this))" class="btn btn-primary btn-salvar">
                <span>Salvar</span>
            </a>
            {% if (file.deleteUrl) { %}
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}&galeria_id=<?php echo $galeria_id; ?>"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <span>Deletar</span>
                </button>
                <input type="checkbox" name="delete" value="1" class="toggle">
            {% } else { %}
                <button class="btn btn-warning cancel">
                    <span>Cancelar</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<?php
    //echo $this->Html->css('/js/fatorcms/core/jQueryFileUpload/css/bootstrap.min');
    echo $this->Html->css('/js/fatorcms/core/jQueryFileUpload/css/style');
    echo $this->Html->css('/js/fatorcms/core/jQueryFileUpload/css/blueimp-gallery.min');
    echo $this->Html->css('/js/fatorcms/core/jQueryFileUpload/css/jquery.fileupload');
    echo $this->Html->css('/js/fatorcms/core/jQueryFileUpload/css/jquery.fileupload-ui');
    //echo $this->Html->css('/js/common/jQueryFileUpload/css/jquery.fileupload-noscript');
    //echo $this->Html->css('/js/common/jQueryFileUpload/css/jquery.fileupload-ui-noscript');

    //echo $this->Html->script('/js/fatorcms/core/jquery-2.0.2.min');
    echo $this->Html->script('/js/fatorcms/core/jQueryFileUpload/js/vendor/jquery.ui.widget');
    echo $this->Html->script('/js/fatorcms/core/jQueryFileUpload/js/tmpl.min');
	echo $this->Html->script('/js/fatorcms/core/jQueryFileUpload/js/load-image.min');
    echo $this->Html->script('/js/fatorcms/core/jQueryFileUpload/js/canvas-to-blob.min');
    echo $this->Html->script('/js/fatorcms/core/jQueryFileUpload/js/bootstrap.min');
    echo $this->Html->script('/js/fatorcms/core/jQueryFileUpload/js/jquery.blueimp-gallery.min');
    echo $this->Html->script('/js/fatorcms/core/jQueryFileUpload/js/jquery.iframe-transport');
    echo $this->Html->script('/js/fatorcms/core/jQueryFileUpload/js/jquery.fileupload');
    echo $this->Html->script('/js/fatorcms/core/jQueryFileUpload/js/jquery.fileupload-process');
    echo $this->Html->script('/js/fatorcms/core/jQueryFileUpload/js/jquery.fileupload-image');
    echo $this->Html->script('/js/fatorcms/core/jQueryFileUpload/js/jquery.fileupload-audio');
    echo $this->Html->script('/js/fatorcms/core/jQueryFileUpload/js/jquery.fileupload-video');
    echo $this->Html->script('/js/fatorcms/core/jQueryFileUpload/js/jquery.fileupload-validate');
    echo $this->Html->script('/js/fatorcms/core/jQueryFileUpload/js/jquery.fileupload-ui');
    echo $this->Html->script('/js/fatorcms/core/jQueryFileUpload/js/main');
    echo $this->fetch('script');
?>
<script type="text/javascript">
   $(document).ready(function(){
        'use strict';
        // if(typeof fileupload == 'function'){
            $('#fileupload').fileupload({
                url: APP_ROOT + 'fatorcms/galerias/upload/' + <?php echo $galeria_id; ?>
            }).on('fileuploadsubmit', function (e, data) {
                data.formData = data.context.find(':input').serializeArray();
            });

            // Enable iframe cross-domain access via redirect option:
            $('#fileupload').fileupload(
                'option',
                'redirect',
                window.location.href.replace(
                    /\/[^\/]*$/,
                    '/cors/result.html?%s'
                )
            );

            // Load existing files:
            $('#fileupload').addClass('fileupload-processing');
            $.ajax({
                // Uncomment the following to send cross-domain cookies:
                //xhrFields: {withCredentials: true},
                url: $('#fileupload').fileupload('option', 'url'),
                dataType: 'json',
                context: $('#fileupload')[0]
            }).always(function () {
                $(this).removeClass('fileupload-processing');
            }).done(function (result) {
                $(this).fileupload('option', 'done')
                    .call(this, $.Event('done'), {result: result});
            });
        // }

        // $('.btn-salvar').click(function(){
        //     alert($(this).closest('tr').find('.file_id'));
        // });


    });

    // function salvar(element){
    //     alert(element.closest('tr').find('.file_id'));
    // }
</script>
<style>
p.name{
    width: 150px;

}
.legend textarea{
    border: medium none;
    padding: 3px 6px;
    resize: none;
    width: 100%;
}
.icheckbox_minimal, .iradio_minimal{
    display: inline-block;
}
</style>