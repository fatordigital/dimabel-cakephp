var MaskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
    maskOptions = {
        onKeyPress: function (val, e, field, options) {
            field.mask(MaskBehavior.apply({}, arguments), options);
        }
    };

function goal(goal) {
    if (ENV_DEBUG == false && typeof ga != 'undefined') {
        ga('send', 'pageview', goal);
    }
}

var wait = false;

$('img.fd_its_svg').each(function () {
    var $img = $(this),
        imgID = $img.attr('id'),
        imgClass = $img.attr('class'),
        imgURL = $img.attr('src');

    $.get(imgURL, function (data) {
        // Get the SVG tag, ignore the rest
        var $svg = $(data).find('svg');

        // Add replaced image's ID to the new SVG
        if (typeof imgID !== 'undefined') {
            $svg = $svg.attr('id', imgID);
        }
        // Add replaced image's classes to the new SVG
        if (typeof imgClass !== 'undefined') {
            $svg = $svg.attr('class', imgClass + ' replaced-svg');
        }

        // Remove any invalid XML tags as per http://validator.w3.org
        $svg = $svg.removeAttr('xmlns:a');

        // Check if the viewport is set, else we gonna set it if we can.
        if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
            $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
        }

        // Replace image with new SVG
        $img.replaceWith($svg);

    }, 'xml');

});


function openModal (carta_id) {
    if (carta_id != '') {

        $('#carta-id').val(carta_id);

        var modalDropback = $('.fd_modal_dropback'),
            closeModal = $('.fd_modal_close'),
            modal = $('.fd_modal');
        if (!modal.hasClass('opened')) {
            // modal.find('form').attr('data-current', current_form);
            // modal.find('.fd_modal_title img').attr('src', img);
            modalDropback.fadeIn(350);
            modal.fadeIn(250);
        }
    } else {
        window.location.reload();
    }
}


var url = baseUrl + '/assets/js/lib/values.json',
    local = 'automovel',
    startValue,
    handle = $("#custom-handle");

function fnJSON(local) {

    var data = ConfigDimabel;

    var $texto1 = $('.fd_header_overlay .fd_row_2 .fd_s:first-of-type'),
        $texto2 = $('.fd_header_overlay .fd_row_2 .fd_s:last-of-type'),
        $textAux = 'mil',
        maxValue, minValue, step, textMaxValue;


    startValue = data[local]['startValue'];
    maxValue = data[local]['maxValue'];
    minValue = data[local]['minValue'];
    step = data[local]['step'];

    if (maxValue >= 1000000) {
        $textAux = 'mi';
        textMaxValue = maxValue / 1000000;
    } else {
        $textAux = 'mil';
        textMaxValue = maxValue / 1000;
    }

    startValue = startValue / 1000;
    maxValue = maxValue / 1000;
    minValue = minValue / 1000;
    step = step / 1000;


    chamada(parseInt(minValue), parseInt(maxValue), step, startValue);
    handle.attr('data-content', 'R$ ' + startValue + '.000,00');

    $texto1.text('R$ ' + minValue + 'mil');
    $texto2.text('R$ ' + textMaxValue + $textAux);

    $('.fd_header_overlay h1').text(data[local]["h1"]);
}

function chamada(min, max, step, value) {
    /* BEGIN: SLIDER RANGE */
    $("#slider").slider({
        min: min,
        max: max,
        step: step,
        value: value,
        create: function () {
            handle.attr('data-content', 'R$ ' + $(this).slider("value") + '.000,00');
            $('.fd_form_value').val($(this).slider("value") + '.000,00');
        },
        slide: function (event, ui) {

            if (local === 'automovel') {
                if (ui.value >= 100000) {
                    $("#slider").slider("option", "step", step * 4);
                }
            }

            if (local === 'imovel') {
                if (ui.value >= 100000) {
                    $("#slider").slider("option", "step", step * 2);
                }
                if (ui.value >= 500000) {
                    $("#slider").slider("option", "step", step * 4);
                }
                if (ui.value >= 1000000) {
                    $("#slider").slider("option", "step", step * 10);
                }
                console.log(ui.value.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'}));
            }

            handle.attr('data-content', (ui.value * 1000).toLocaleString('pt-br',{style: 'currency', currency: 'BRL'}));
            $('.fd_form_value').val(ui.value + '.000,00');
        }
    }).draggable();
    /* END: SLIDER RANGE */
}

var carregando = false;

function newsletter(e) {
    var $sendData = $(this).serialize();
    var $validator = $(this).data('validator');
    if ($validator.errorList.length == 0) {
        var $form = $(this);
        if (!carregando) {
            carregando = true;
            $.ajax({
                dataType: 'json',
                data: $sendData,
                type: 'POST',
                url: $(this).attr('action'),
                success: function (r) {
                    goal(r.goal);
                    swal({
                        type: 'success',
                        title: r.message,
                        showConfirmButton: false,
                        timer: 2500
                    });
                    $form.trigger('reset');
                    carregando = false;
                },
                error: function (r) {
                    r = r.responseJSON;
                    swal({
                        type: 'error',
                        title: r.message,
                        showConfirmButton: false,
                        timer: 2500
                    });
                    carregando = false;
                }
            });
        }
    }
    return false;
}

function duvidas_outro_modelo(e) {
    if (!carregando) {
        var $sendData = $(this).serialize();
        var $form = $(this);
        carregando = true;
        $.ajax({
            url: $(this).attr('action'),
            dataType: 'json',
            data: $sendData,
            type: 'post',
            success: function (r) {
                goal(r.goal);
                swal({
                    type: 'success',
                    title: r.message,
                    showConfirmButton: false,
                    timer: 2500
                });
                $form.trigger('reset');
                carregando = false;
            },
            error: function (r) {
                r = r.responseJSON;
                swal({
                    type: 'error',
                    title: r.message,
                    showConfirmButton: false,
                    timer: 2500
                });
                carregando = false;
            }
        });
    }

    return false;
}

function simulador($this, $event) {
    $event.preventDefault();
    setTimeout(function(){
        if (wait == false) {
            var bt_simulador = $('[data-simulador], .submit-modal');

            wait = true;

            var $data = window.current_data;
            var $form = window.current_form;
            var $modalForm = $('.fd_modal').find("form");
            var $validator = $($this).data('validator');


            if ($validator.errorList.length < 1) {

                bt_simulador.html('AGUARDE...');

                if (Object.keys($validator.invalid).length) {
                    wait = false;
                    return false;
                } else {
                    var $sendData = $form.serialize() + '&' + $($this).serialize();
                    $.ajax({
                        url: $($this).attr('action') + '/' + $data.slug,
                        type: 'POST',
                        data: $sendData,
                        dataType: 'json',
                        success: function (r) {
                            goal(r.goal);
                            $('.fd_modal_dropback').trigger('click');
                            swal({
                                type: 'success',
                                title: r.message,
                                showConfirmButton: false,
                                timer: 2500
                            });
                            $modalForm.trigger('reset');
                            bt_simulador.html('CONTRATAR');
                            wait = false;
                        },
                        error: function (r) {
                            r = r.responseJSON;
                            swal({
                                type: 'error',
                                title: r.message,
                                showConfirmButton: false,
                                timer: 2500
                            });
                            bt_simulador.html('CONTRATAR');
                            wait = false;
                        }
                    });
                }
            } else {
                wait = false;
            }
        }
    }, 200);
}

function fill_hiddens(form) {

    var validator = $(form).data('validator');
    setTimeout(function(){
        if (validator.errorList.length < 1) {
            $('.fill').each(function (e) {
                var name = $(this).attr('name');
                var current = $(form).find('[name="' + name + '"]');
                if (current) {
                    $(this).val(current.val());
                }
            }).promise().done(function () {

                var form_submit = $($(form).data('current'));
                form_submit.removeAttr('onsubmit');
                form_submit.submit();
            });
        }
    }, 200);
    event.preventDefault();
}

$.validator.addMethod('valor',function(value,element,param) {
    var $return = true;
    var data = ConfigDimabel[start_current];
    // var valor = $(e).find('input[name="value"]').val();
    var valor = value;
    valor = valor.replace(/\D+/g, '');

    var min = parseInt(data.minValue + '00');
    var max = parseInt(data.maxValue + '00');

    if ( min > valor) {
        $return = false;
    }

    if ( max < valor) {
        $return = false;
    }

    return $return;
}, function(params, element){
    var data = ConfigDimabel[start_current];
    return 'Min: ' + data.textMin + ' - Max: ' + data.textMax
});


var start_current = 'automovel';

$(document).ready(function () {

    $('.fd_page_content.fd_home_marcas .btn.fd_item').click( function () {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });

    $('select[name="simulation_type"]').change(function(){
        start_current = $(this).val();
        var data = ConfigDimabel[start_current];

        if (start_current == 'imovel') {
            $('.fd_last').css({display: 'block'});
            $('.fd_first').css({display: 'none'});
        } else {
            $('.fd_first').css({display: 'block'});
            $('.fd_last').css({display: 'none'});
        }
    });

    $('.fd_money').mask('000.000.000.000.000,00', {reverse: true});

    var a = $('.fd_form_header_default .fd_row_1 a'),
        last = $('header .fd_last'),
        first = $('header .fd_first'),
        DuvidaOutroModelo = $('#DuvidaOutroModelo'),
        FormNewsletter = $('#form-newsletter'),
        FormContatoRodape = $('#form-rodape-contato');

    if (DuvidaOutroModelo) {
        DuvidaOutroModelo.submit(duvidas_outro_modelo);
    }

    if (FormContatoRodape) {
        FormContatoRodape.submit(newsletter);
    }

    if (FormNewsletter) {
        FormNewsletter.submit(newsletter);
    }
	
	$('.chat-box').click(function(){
        $zopim.livechat.window.show();
    });

    if (!$('header').hasClass('fd_header_interna')) {
        fnJSON(local);

        a.on('click', function () {

            var local = $(this).data('form');

            a.removeClass('fd_active').attr('disabled', false);

            if (local === 'imovel') {

                first.fadeOut(350);
                last.fadeIn(350);
                $(this).addClass('fd_active').attr('disabled', 'disabled');

                $('input[name="simulation_type"]').val('imovel');

                fnJSON(local);

            } else if (local === 'automovel') {

                first.fadeIn(350);
                last.fadeOut(350);
                $(this).addClass('fd_active').attr('disabled', 'disabled');

                $('input[name="simulation_type"]').val('automovel');

                fnJSON(local);
            }

        });
    }

    var $owl = $('.fd_carousel'),
        $owlHeader = $('.fd_carousel_topo'),
        nav = $('.navbar.navbar-default'),
        $eq = $('.fd_eq'),
        $tel = $('.fd_phone_mask'),
        $btnMobile = $('.navbar-toggle'),
        $dorp = $('.fd_has_submenu>a');


    $dorp.on('click', function () {
        $(this).parent().toggleClass('opened');
    });


    /* BEGIN: MASK */
    $tel.mask(MaskBehavior, maskOptions);
    $('.fd_cpf').mask('000.000.000-00', {reverse: true});
    /* END: MASK */

    /* BEGIN: MODAL */
    var om = $('[data-modal]'),
        cc = $('[data-carro]'),
        modalDropback = $('.fd_modal_dropback'),
        closeModal = $('.fd_modal_close'),
        modal = $('.fd_modal'),
        exist = false,
        z, img;


    om.on('click', function () {

        img = $(this).closest('.fd_item').find('.fd_item_topo img').attr('src');

        if (!modal.hasClass('opened')) {

            z = $(this).data('modal');

            var current_form = '#contratar-' + z;

            modal.find('form').attr('data-current', current_form);

            modal.find('.fd_modal_title img').attr('src', img);

            modalDropback.fadeIn(350);
            modal.delay(250).addClass(z).fadeIn(250);

        }
    });

    om.on('click', function () {

        img = $(this).closest('.fd_item').find('.fd_item_topo img').attr('src');

        if (!modal.hasClass('opened')) {

            window.current_data = $(this).data('simulador');
            if (window.current_data) {
                window.current_form = $('#contratar-' + current_data.slug);
                $('#carta-de-credito').html('R$ ' + current_data.preco);
                $('#titulo-contratar').attr('class', 'fd_modal ' + current_data.class);
            }

            z = $(this).data('modal');

            modal.find('.fd_modal_title img').attr('src', img);

            modalDropback.fadeIn(350);
            modal.delay(250).addClass(z).fadeIn(250);

        }
    });


    modalDropback.on('click', function () {
        $(this).delay(250).fadeOut(350);
        modal.fadeOut(250, function () {
            modal.removeClass(z);
        });
    });

    closeModal.on('click', function () {
        modalDropback.delay(250).fadeOut(350);
        modal.fadeOut(250, function () {
            modal.removeClass(z);
        });
    });

    /* END: MODAL */

    /* BEGIN: MENU MOBILE */
    var dropback = $('.fd_menu_dropback');

    $btnMobile.on('click', function () {
        $(this).toggleClass('opened');
        $('.fd_menu').toggleClass('opened');
        dropback.toggleClass('opened');
    });

    dropback.on('click', function () {
        $(this).toggleClass('opened');
        $('.fd_menu').toggleClass('opened');
        $btnMobile.toggleClass('opened');
    });
    /* END: MENU MOBILE */

    /* BEGIN: OWL CAROUSEL CONFIGS */

    $owlHeader.owlCarousel({
        loop: true,
        margin: 0,
        nav: false,
        navText: ["Prev", "Next"],
        dots: false,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        autoplay: true,
        responsive: {
            0: {
                items: 1
            }
        }
    });

    $owl.owlCarousel({
        loop: false,
        margin: 0,
        nav: false,
        autoplay: false,
        dots: false,
        responsive: {
            0: {
                items: 4
            },
            1200: {
                items: 8
            }
        }
    });
    /* END: OWL CAROUSEL CONFIG */

    /* BEGIN: TOOLTIP */
    $('[data-toggle="tooltip"]').tooltip()
    /* END: TOOLTIP */

    /* BEGIN: AFFIX */
    var $bar = $('header .fd_header_phone');

    if ($bar.offset()) {
        $bar.affix({
            offset: {
                top: $bar.offset().top
            }
        });
    }
    /* END: AFFIX */

    /* BEGIN: PAGE-SCROLL*/
    // $('a.page-scroll').bind('click', function(event) {
    //     var $anchor = $(this);
    //     $('html, body').stop().animate({
    //         scrollTop: $($anchor.attr('href')).offset().top - 100
    //     }, 1200, 'easeInOutExpo');
    //     event.preventDefault();
    // });
    /* END: PAGE-SCROLL */

    /* BEGIN: SCROLLSPY */
    $('body').scrollspy({target: '.navbar.navbar-default'});
    /* END SCROLLSPY */


    /* BEGIN: LABEL PARA INPUT */
    var inputs = $('[class*="fd_input"]');
    inputs.blur(function () {
        if ($(this).val()) {
            $(this).addClass('used');
        }
        else {
            $(this).removeClass('used');
        }
    });
    /* END: LABEL PARA INPUT */

    /* BEGIN: EQUAL HEIGHT */
    var options = {
        byRow: true,
        property: 'height',
        target: null,
        remove: false
    };

    $eq.matchHeight(options);

    if (window.matchMedia('(min-width: 768px)').matches) {
        $eq.matchHeight(options);
    }
    /* END: EQUAL HEIGHT */

    /* BEGIN: SEND FORM */
    $.validator.methods.email = function (value, element) {
        return this.optional(element) || /[a-z]+@[a-z]+\.[a-z]+/.test(value);
    };

    $('.form-validate').each(function () {
        $(this).validate();
    });


    $('.fd_btn_next').on('click', function () {

        var t = $('.fd_row_4'),
            c = $('.fd_row_3');

        c.css({
            'margin-left': '-2000px'
        });
        t.css({
            'left': 0
        })
    });

    /* END: SEND FORM */

});


var mapStyle = [
    {
        "featureType": "administrative.land_parcel",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "simplified"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
            {
                "hue": "#f49935"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "hue": "#fad959"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "hue": "#a1cdfc"
            },
            {
                "saturation": 30
            },
            {
                "lightness": 49
            }
        ]
    }
];

var baseIcon = baseUrl + '/assets/images/icons/',
    sizeWidth, sizeHeight;

if ($(window).width() < 768) {
    baseIcon = baseIcon + 'pin_mapa_mobile.png';
    sizeWidth = 151;
    sizeHeight = 113;
} else {
    baseIcon = baseIcon + 'pin_mapa.png';
    sizeWidth = 250;
    sizeHeight = 185;
}
var locations = [
    {
        mapa: {
            lat: -29.7703267,
            long: -51.1595922,
            zoom: 17,
            endereco: 'Av. João Correa, 503, sala 2 São Leopoldo/RS - CEP 93040-035',
            // enderecoHTML: 'Av. João Correa, 503, sala 2 <br /><strong>São Leopoldo/RS</strong> - CEP 93040-035',
            controlPosition: google.maps.ControlPosition.RIGHT_BOTTOM,
            id: 'mapa'
        }
    }
];


// When the window has finished loading create our google map below
locations.map(function (obj, index) {
    // console.log(obj, index);
    if (document.getElementById(obj.mapa.id)) {
        google.maps.event.addDomListener(window, 'load', init(obj));
    }
});


function init(locations) {
    // Get the HTML DOM element that will contain your map
    // We are using a div with id="map" seen below in the <body>
    var mapElement = document.getElementById(locations.mapa.id);

    if (mapElement) {

        // Basic options for a simple Google Map
        // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
        var mapOptions = {
            // How zoomed in you want the map to start at (always required)
            zoom: locations.mapa.zoom,
            scrollwheel: false,
            navigationControl: false,
            mapTypeControl: false,
            scaleControl: false,
            draggable: true,
            disableDoubleClickZoom: true,
            streetViewControl: false,
            zoomControl: true,
            zoomControlOptions: {
                position: locations.mapa.controlPosition
            },
            // The latitude and longitude to center the map (always required)
            center: new google.maps.LatLng(locations.mapa.lat, locations.mapa.long),
            // How you would like to style the map.
            // This is where you would paste any style found on Snazzy Maps.
            styles: mapStyle
        };
        var image = {
            url: baseIcon,
            size: new google.maps.Size(sizeWidth, sizeHeight)
        };


        // Create the Google Map using our element and options defined above
        var map = new google.maps.Map(mapElement, mapOptions);

        // Let's also add a marker while we're at it
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations.mapa.lat + 0.0001, locations.mapa.long + 0.0005),
            map: map,
            title: locations.mapa.endereco,
            icon: image
        });
    }
}