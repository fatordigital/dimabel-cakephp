$(document).ready(function(){
		
	//begin: Formulário de Pré Inscrição

		var slug = function(str) {
		  str = str.replace(/^\s+|\s+$/g, ''); // trim
		  str = str.toLowerCase();

		  // remove accents, swap ñ for n, etc
		  var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
		  var to   = "aaaaaeeeeeiiiiooooouuuunc------";
		  for (var i=0, l=from.length ; i<l ; i++) {
		    str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
		  }

		  str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
		    .replace(/\s+/g, '-') // collapse whitespace and replace by -
		    .replace(/-+/g, '-'); // collapse dashes

		  return str;
		};

		//carrega as informações dos campus
		$.ajax({
			dataType: "json",
			type: "GET",
			url: APP_ROOT + 'api/get_campi',
			success: function (data, textStatus){
				$('.form_pre_inscricao #ApiNensId').html("").append($('<option>', {
				    value: '',
				    text: 'Selecione o campus'
				}));
				$.each(data, function(index, itemData) {
				  	$('.form_pre_inscricao #ApiNensId').append($('<option>', {
					    value: index,
					    text: itemData
					}));
				});
			}
		});

		if($('.form_pre_inscricao #ApiNensId:selected').val() != ""){
			//get_cursos($('.form_pre_inscricao #ApiNensId:selected').val());
		}
		$('.form_pre_inscricao #ApiNensId').change(function(){
			//get_cursos($(this).val());
			var $output = '';
			var $target = $('#inscricao-target');
			if ($(this).val() == 1) {
				$output = '<select name="curso_interesse" id="curs_id" class="control form-control" data-rule-required="true" data-msg-required="Selecione o curso de interesse"><option value="">Selecione o curso de interesse</option><option value="1|1">BACHARELADO EM ADMINISTRAÇÃO</option><option value="1|227">BACHARELADO EM CIÊNCIA DA COMPUTAÇÃO</option><option value="1|8">BACHARELADO EM CIÊNCIAS CONTÁBEIS</option><option value="1|55">BACHARELADO EM CIÊNCIAS ECONÔMICAS</option><option value="1|9">BACHARELADO EM DIREITO</option><option value="1|223">CURSO SUPERIOR DE TECNOLOGIA EM COMÉRCIO EXTERIOR</option><option value="1|228">CURSO SUPERIOR DE TECNOLOGIA EM JOGOS DIGITAIS</option><option value="1|225">CURSO SUPERIOR DE TECNOLOGIA EM PRODUÇÃO MULTIMÍDIA</option><option value="1|224">CURSO SUPERIOR DE TECNOLOGIA EM SISTEMAS PARA INTERNET</option><option value="1|169">TEC. ANALISE E DESENVOLVIMENTO</option><option value="1|144">TECNOLOGIA EM LOGÍSTICA</option><option value="1|150">TECNOLOGIA GEST. DA QUALIDADE</option><option value="1|167">TECNOLOGO DE RECURSOS HUMANOS</option><option value="1|221">TECNÓLOGO EM GESTÃO COMERCIAL</option><option value="1|177">TECNÓLOGO EM GESTÃO FINANCEIRA</option><option value="1|212">TECNÓLOGO EM GESTÃO HOSPITALAR</option><option value="1|213">TECNÓLOGO EM GESTÃO PÚBLICA</option><option value="1|179">TECNÓLOGO EM MARKETING</option><option value="1|222">TECNÓLOGO EM PROCESSOS GERENCIAIS</option><option value="1|220">TECNÓLOGO EM REDES DE COMPUTADORES</option><option value="1|211">TECNÓLOGO EM SERVIÇOS PENAIS</option></select>';
			}
			if ($(this).val() == 4) {
				$output = '<select name="curso_interesse" id="curs_id" class="control form-control" data-rule-required="true" data-msg-required="Selecione o curso de interesse"><option value="">Selecione o curso de interesse</option><option value="1|218">BACHARELADO EM EDUCAÇÃO FÍSICA</option><option value="1|200">BACHARELADO EM ENFERMAGEM</option><option value="1|214">BACHARELADO EM ESTÉTICA E COSMÉTICA</option><option value="1|219">BACHARELADO EM FISIOTERAPIA</option><option value="1|216">BACHARELADO EM NUTRIÇÃO</option><option value="1|217">BACHARELADO EM SERVIÇO SOCIAL</option><option value="1|56">GRADUAÇÃO EM PSICOLOGIA</option><option value="1|232">TECNÓLOGO EM ESTÉTICA E COSMÉTICA</option></select>';
			}
			if ($(this).val() == 7) {
				$output = '<select name="curso_interesse" id="curs_id" class="control form-control" data-rule-required="true" data-msg-required="Selecione o curso de interesse"><option value="">Selecione o curso de interesse</option><option value="1|231">BACHARELADO EM COMUNICAÇÃO SOCIAL - PUBLICIDADE E PROPAGANDA</option><option value="1|230">BACHARELADO EM JORNALISMO</option><option value="1|229">CURSO SUPERIOR DE TECNOLOGIA EM DESIGN GRÁFICO</option></select>';
			}
			$target.html($output);
		});

		$(".form_pre_inscricao form").submit(function(e) {

			$('.form_pre_inscricao .success_message').hide();
			$('.form_pre_inscricao .error_message').hide();

			if($(".form_pre_inscricao form").valid()){
		        e.preventDefault();

		        $.ajax({
		                url: APP_ROOT + 'api/set_data_pre_inscricao',
		                type: 'post',
		                // dataType: 'json',
		                data: $(this).serialize(),
		                success: function(data) {
		                   	if(data.substring(data.length-1) == 1){
		                   		//goal
		                   		var goal_tipo = 'pre-inscricao';
		                   		var goal_objetivo = slug($('#ApiCursId option:selected').text());
		                   		var goal = '/goal/' + goal_tipo + '/' + goal_objetivo;
		                   		console.log(goal);
								// ga('send','pageview', goal);
								dataLayer.push({
									'event':'VirtualPageview',
									'sendPageview': goal
								});

								//temporário
								$('#inscricao-target').html('<select name="curso_interesse" class="control form-control" data-rule-required="true" data-msg-required="Selecione o curso de interesse" id="ApiCursId"> <option value="">Escolha o curso</option> </select>');

								//reset
		                   		$(".form_pre_inscricao form")[0].reset();
		                   		$('.form_pre_inscricao .success_message').show();
		                   	}else{
		                   		$('.form_pre_inscricao .error_message').show();
		                   	}
		                }
		        });
			}	       

	    });


	//end: Formulário de Pré Inscrição

});

function get_cursos(campus){
	$('.form_pre_inscricao #ApiCursId').html("").append($('<option>', {
	    value: '',
	    text: 'Carregando...'
	}));
	$.ajax({
		dataType: "json",
		type: "GET",
		url: APP_ROOT + 'api/get_cursos/' + campus,
		success: function (data, textStatus){
			$('.form_pre_inscricao #ApiCursId').html("").append($('<option>', {
			    value: '',
			    text: 'Selecione o curso'
			}));
			$.each(data, function(index, itemData) {
			  	$('.form_pre_inscricao #ApiCursId').append($('<option>', {
				    value: index,
				    text: itemData
				}));
			});
		}
	});
}