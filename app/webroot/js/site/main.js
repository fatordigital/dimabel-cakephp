$(document).ready(function(){
		
	//begin: Formulário de Manifestar Interesse

		//carrega as informações dos campus
		$.ajax({
			dataType: "json",
			type: "GET",
			url: APP_ROOT + 'api/get_campi',
			success: function (data, textStatus){
				$('.form_manifestar_interesse #nens_id').html("").append($('<option>', {
				    value: '',
				    text: 'Selecione o campus'
				}));
				$.each(data, function(index, itemData) {
				  	$('.form_manifestar_interesse #nens_id').append($('<option>', {
					    value: index,
					    text: itemData
					}));
				});
			}
		});

		if($('.form_manifestar_interesse #nens_id:selected').val() != ""){
			get_cursos($('.form_manifestar_interesse #nens_id:selected').val(), $('.form_manifestar_interesse #nens_id').data('ens-id'));
		}
		$('.form_manifestar_interesse #nens_id').change(function(){
			get_cursos($(this).val(), $(this).data('ens-id'));
		});


		$(".form_manifestar_interesse #frmContact").submit(function(e) {

			$('.success_message').hide();
			$('.error_message').hide();

			if($(".form_manifestar_interesse #frmContact").valid()){
		        e.preventDefault();

		        $.ajax({
		                url: APP_ROOT + 'api/set_data',
		                type: 'post',
		                // dataType: 'json',
		                data: $(this).serialize(),
		                success: function(data) {
		                   	if(data.substring(data.length-1) == 1){
		                   		//reset
		                   		$(".form_manifestar_interesse #frmContact")[0].reset();
		                   		$('.success_message').show();

		                   		//goal
		                   		var goal_tipo = 'sidebar-curso';
		                   		var goal_objetivo = 'manifestar-interesse';
		                   		var goal = '/goal/' + goal_objetivo + '/' + goal_objetivo;
								// ga('send','pageview', goal);
								dataLayer.push({
									'event':'VirtualPageview',
									'sendPageview': goal
								});
		                   	}else{
		                   		$('.error_message').show();
		                   	}
		                }
		        });
			}
	       

	    });


	//end: Formulário de Manifestar Interesse

});

function get_cursos(campus, nens){
	$('.form_manifestar_interesse #curs_id').html("").append($('<option>', {
	    value: '',
	    text: 'Carregando...'
	}));



	$.ajax({
		dataType: "json",
		type: "GET",
		url: APP_ROOT + 'api/get_cursos/' + campus + '/' + nens,
		success: function (data, textStatus){
			$('.form_manifestar_interesse #curs_id').html("").append($('<option>', {
			    value: '',
			    text: 'Selecione o curso de interesse'
			}));
			$.each(data, function(index, itemData) {
			  	$('.form_manifestar_interesse #curs_id').append($('<option>', {
				    value: index,
				    text: itemData
				}));
			});
		}
	});
}