$(document).ready(function(){

	if($('.escolha-idioma').length > 0) {
		$( ".escolha-idioma" ).tabs();
	}

	if($('.data').length > 0) {
		$('.data').datepicker({
			dateFormat: 'dd/mm/yy'
		});
	}

	if($('.editor').length > 0) {
		$('.editor').redactor({
            toolbarFixed: true,
            toolbarFixedTopOffset: 80, // pixels
			fileUpload: '/js/fatorcms/redactor10/phps/files.php',
			imageUpload: '/js/fatorcms/redactor10/phps/images.php',
			minHeight: 250,
			autoresize: true,
            focus: false,
            // cleanSpaces: false,
            paragraphize: true,
            // removeEmpty: ['p'],
            visual: true,
            linebreaks: false,
            pastePlainText: false,
            enterKey: true,
            cleanOnPaste: false,
            cleanStyleOnEnter: false,
            replaceDivs: false,
            removeAttr:  [
                ['style']
            ],
            removeWithoutAttr: [],
            formatting: ['p', 'blockquote', 'a'],
            formattingAdd: [
                {
                    tag: 'h3',
                    title: 'H3 - Título com barra laranja lateral',
                    class: ''
                },
                {
                    tag: 'h4',
                    title: 'H4 - Título azul centralizado',
                    class: ''
                }/*,
                {
                    tag: 'ul',
                    title: 'Lista com pontos vermlehos',
                    class: 'ul-style'
                },
                {
                    tag: 'a',
                    title: 'Link Vermelho com PDF',
                    class: 'link-my-pdf-big'
                },
                {
                    tag: 'a',
                    title: 'Link Vermelho com XLS',
                    class: 'link-my-xls-big'
                },
                {
                    tag: 'a',
                    title: 'Link Vermelho com DOC',
                    class: 'link-my-doc-big'
                },*/
            ]            
		});
	}
	
	$('#limit').change(function() {
        if ($(this).val() != "") {
            window.location = $(this).val();
        } else {
            window.location = $('#limit').attr('rel').replace(/\/limit:(.*)/gi, '');
        }
    });

     setMasks();
});

function setMasks(){
    $('input.date').mask('11/11/1111');
    $('input.time').mask('00:00:00');
    $('input.date_time').mask('99/99/9999 00:00:00');
    $('input.cep').mask('99999-999');
    $('input.cpf').mask('999.999.999-99');
    $('input.cnpj').mask('99.999.999.9999/99');
    $('input.phone').mask('9999-99999');
    $('input.phone_with_ddd').mask('(99) 9999-9999');
    $('input.phone_br').mask('(99) 9999-99999');
    $('input.card_credit').mask('9999999999999999');
    $('input.code_validate').mask('999');
    $('input.number').mask('99999999999999999999999999#');
    $('input.money1').mask('000.000.000.000.000,00', {reverse: true});
}