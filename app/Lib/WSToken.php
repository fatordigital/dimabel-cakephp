<?php
	/*
	 * Classe de geração de chave de segurança para recuperação e registro de dados através do ws.
	 *
	 * @author Juliano S. De Cezaro <juliano_cezaro@uniritter.edu.br>
	 * @version 1.0.0
	 */
	class WSToken
	{
		/*
		 * Método de geração do digito verificador do token.
		 *
		 * @author Juliano S. De Cezaro <juliano_cezaro@uniritter.edu.br>
		 * @version 1.0.0
		 */
		public function getTokenNumberChkId($_token)
		{
			$soma = 0;
			$t = 0;
			for($j=0; $j < strlen($_token); $j++){
				$soma += ord($_token[$j]);
			}
			$s = (string)$soma;
			$l = strlen($soma) -1;
			for($i = 0; $i < $l; $i++){
				$t += $s[$i];
			}
			$t *= ($s[$l] ? $s[$l] : 1);
			return str_pad(($t < 99 ? $t : 99), 2, "0", STR_PAD_LEFT);
		}
		
		/*
		 * Método de geração do token.
		 *
		 * @author Juliano S. De Cezaro <juliano_cezaro@uniritter.edu.br>
		 * @version 1.0.0
		 */
		public function getTokenNumber()
		{
			$TOKEN_LEN = 15;
			$specials = array(33, 35, 36, 37, 42, 63, 64);   
			$string = '';     
			for ($i = 0; $i < $TOKEN_LEN; $i++){
				switch(rand(0,3)){
					case 0:
						$string .= rand(0,9);
						break;
					case 1:
						$string .= strtolower(chr(rand(65, 91)));
						break;
					case 2:
						$string .= chr(rand(65, 91));
						break;
					case 3:
						$string .= chr($specials[rand(0, 6)]);
						break;
				}
			}
			return $string."-".WSToken::getTokenNumberChkId($string);
		}
		
		/*
		 * Método de geração da chave com os dados necessários e o token de validação.
		 *
		 * @author Juliano S. De Cezaro <juliano_cezaro@uniritter.edu.br>
		 * @version 1.0.0
		 */
		public function getKey($_params = null)
		{
			if(is_array($_params) && count($_params)>0){
				foreach ($_params as $key => $value) {
					$arr[$key] = $value;
				}
			}
			unset($arr['token']);
			$arr['token'] = WSToken::getTokenNumber();
			return base64_encode(serialize($arr));
		}
	}
?>