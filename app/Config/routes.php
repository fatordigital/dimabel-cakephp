<?php
App::import('Model', 'FdConsorcios.Consorcio');
$Consorcio = new Consorcio();

function procura_consorcio_na_url(Consorcio $Consorcio)
{
    $consorcios = $Consorcio->cache()
        ->extract('{n}.Consorcio.slug');

    $url = Router::url();
    $peaces = explode('/', $url);

    if (isset($peaces[1]) && !empty($peaces)) {
        if (in_array($peaces[1], $consorcios)) {
            return $peaces[1];
        }
    }
    return '';
}

# Begin FatorCMS
Router::connect('/fatorcms', array('fatorcms' => true, 'plugin' => 'fd_dashboard', 'controller' => 'fd_dashboard', 'action' => 'index', 'prefix' => 'fatorcms'));
Router::connect('/fatorcms/login', array('fatorcms' => true, 'plugin' => 'fd_dashboard', 'controller' => 'usuarios', 'action' => 'login'));
Router::connect('/fatorcms/logout', array('fatorcms' => true, 'plugin' => 'fd_dashboard', 'controller' => 'usuarios', 'action' => 'logout'));
# End FatorCMS

Router::connect('/', array('controller' => 'home', 'action' => 'index'));

Router::connect('/share/*', array('controller' => 'share', 'action' => 'index'));
//Router::connect('/:consorcio', array('controller' => 'home', 'action' => 'index'), array('consorcio' => 'caixa|itau|hsconsorcios'));

Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));
Router::connect('/sitemap.xml', array('admin' => false, 'controller' => 'sitemaps', 'action' => 'index'));
Router::connect('/sitemap', array('admin' => false, 'controller' => 'sitemaps', 'action' => 'index'));


# Begin Rotas dinâmicas

// Pega a URL atual e transforma ela em array
$request_url = Router::url();
$explode = explode("/", $request_url);
// Pega a URL atual e transforma ela em array

// Istância a variável $url
$url = null;
// Istância a variável $url

// Está etapa, pega a URL e transforma ela em URL????
while (count($explode) > 0) {
    if (stripos($_SERVER['SCRIPT_NAME'], $request_url) === false && stripos($_SERVER['SCRIPT_NAME'], $explode[1]) == true) {
        unset($explode[count($explode) - 1]);
        if (count($explode) > 0) {
            $request_url = '';
            foreach ($explode as $ex) {
                if ($ex != "") {
                    $request_url .= '/' . $ex;
                }
            }
        }
    } else {
        $uri = str_replace($request_url . '/', '', $_SERVER['REQUEST_URI']);

        if (strlen($uri) > 1 && substr($uri, 0, 1) == '/') {
            $uri = substr($uri, 1);
        }
        $explode = array();
    }
}
$url = $uri;
// Está etapa, pega a URL e transforma ela em URL????

// Verifica se foi passado paramêtros em GET
if ($_SERVER['QUERY_STRING'] != "") {
    $url = str_replace('?' . $_SERVER['QUERY_STRING'], '', $url);
}
// Verifica se foi passado paramêtros em GET

$consorcio = procura_consorcio_na_url($Consorcio);


$only = $Consorcio->cache()
    ->extract('{n}.Consorcio.slug');


if ($consorcio)
    $url = str_replace($consorcio . '/', '', $url);

//remove parametros
$tem_paramentro = false;
while (strpos(basename($url), ':') != "") {
    $tem_paramentro = true;
    $url = str_replace(basename($url), '', $url);
}

while (substr($url, -1) == '/') {
    $url = substr($url, 0, -1);
}

$url = urldecode($url);

App::import('Model', 'FdRotas.Rota');
$modelRota = new Rota();
$rotas = $modelRota->getRotas(Configure::read('site'));

foreach ($rotas as $rota) {
    if ($only) {
        Router::connect("/:consorcio",
            array('controller' => 'home', 'action' => 'index'),
            array('consorcio' => implode('|', $only)));

        if (isset($rotas[$url])) {
            Router::connect("/:consorcio/{$rotas[$url]['seo_url']}/*",
                array('fatorcms' => false, 'plugin' => false, 'controller' => $rotas[$url]['controller'], 'action' => $rotas[$url]['action'], $rotas[$url]['row_id'], $url),
                array('consorcio' => implode('|', $only)));
            Router::connect("/:consorcio/{$rotas[$url]['seo_url']}",
                array('fatorcms' => false, 'plugin' => false, 'controller' => $rotas[$url]['controller'], 'action' => $rotas[$url]['action'], $rotas[$url]['row_id'], $url),
                array('consorcio' => implode('|', $only)));

            Router::connect("/{$rotas[$url]['seo_url']}",
                array('fatorcms' => false, 'plugin' => false, 'controller' => $rotas[$url]['controller'], 'action' => $rotas[$url]['action'], $rotas[$url]['row_id'], $url),
                array());
        }

    } else {
        if (isset($rotas[$url])) {
            Router::connect("/{$rotas[$url]['seo_url']}",
                array('fatorcms' => false, 'plugin' => false, 'controller' => $rotas[$url]['controller'], 'action' => $rotas[$url]['action'], $rotas[$url]['row_id'], $url),
                array());
        }
    }
}
# End Rotas dinâmicas

Router::connect("/fale-conosco", array('fatorcms' => false, 'plugin' => false, 'controller' => 'sac', 'action' => 'add'));
Router::connect("/fale-conosco/", array('fatorcms' => false, 'plugin' => false, 'controller' => 'sac', 'action' => 'add'));
Router::connect("/fale-conosco/*", array('fatorcms' => false, 'plugin' => false, 'controller' => 'sac', 'action' => 'add'));

Router::connect("/:consorcio/fale-conosco", array('fatorcms' => false, 'plugin' => false, 'controller' => 'sac', 'action' => 'add'), array('consorcio' => implode('|', $only)));
Router::connect("/:consorcio/fale-conosco/", array('fatorcms' => false, 'plugin' => false, 'controller' => 'sac', 'action' => 'add'), array('consorcio' => implode('|', $only)));
Router::connect("/:consorcio/fale-conosco/*", array('fatorcms' => false, 'plugin' => false, 'controller' => 'sac', 'action' => 'add'), array('consorcio' => implode('|', $only)));

Router::connect("/:consorcio/automoveis/*", array('fatorcms' => false, 'plugin' => false, 'controller' => 'marca', 'action' => 'detalhe'), array('consorcio' => implode('|', $only)));
Router::connect("/automoveis/*", array('fatorcms' => false, 'plugin' => false, 'controller' => 'marca', 'action' => 'detalhe'), array());

Router::connect("/:consorcio/contratar-carro/*", array('fatorcms' => false, 'plugin' => false, 'controller' => 'api', 'action' => 'contratar_carro'), array('consorcio' => implode('|', $only)));
Router::connect("/contratar-carro/*", array('fatorcms' => false, 'plugin' => false, 'controller' => 'api', 'action' => 'contratar_carro'), array());

Router::connect("/:consorcio/newsletter/*", array('fatorcms' => false, 'plugin' => false, 'controller' => 'api', 'action' => 'newsletter'), array('consorcio' => implode('|', $only)));
Router::connect("/newsletter/*", array('fatorcms' => false, 'plugin' => false, 'controller' => 'api', 'action' => 'newsletter'), array());

Router::connect("/:consorcio/contato/*", array('fatorcms' => false, 'plugin' => false, 'controller' => 'api', 'action' => 'contato'), array('consorcio' => implode('|', $only)));
Router::connect("/contato/*", array('fatorcms' => false, 'plugin' => false, 'controller' => 'api', 'action' => 'contato'), array());

Router::connect("/:consorcio/simulacao/*", array('fatorcms' => false, 'plugin' => false, 'controller' => 'simulador', 'action' => 'index'), array('consorcio' => implode('|', $only)));
Router::connect("/simulacao/*", array('fatorcms' => false, 'plugin' => false, 'controller' => 'simulador', 'action' => 'index'), array());

Router::connect("/:consorcio/simulacao-contratar/*", array('fatorcms' => false, 'plugin' => false, 'controller' => 'api', 'action' => 'contratar_simulador'), array('consorcio' => implode('|', $only)));
Router::connect("/simulacao-contratar/*", array('fatorcms' => false, 'plugin' => false, 'controller' => 'api', 'action' => 'contratar_simulador'), array());

Router::connect("/:consorcio/duvidas-outro-modelo/*", array('fatorcms' => false, 'plugin' => false, 'controller' => 'api', 'action' => 'duvida_outro_modelo'), array('consorcio' => implode('|', $only)));
Router::connect("/duvidas-outro-modelo/*", array('fatorcms' => false, 'plugin' => false, 'controller' => 'api', 'action' => 'duvida_outro_modelo'), array());

Router::connect("/:consorcio/cartas-contempladas/*", array('fatorcms' => false, 'plugin' => false, 'controller' => 'carta', 'action' => 'index'), array('consorcio' => implode('|', $only)));
Router::connect("/cartas-contempladas/*", array('fatorcms' => false, 'plugin' => false, 'controller' => 'carta', 'action' => 'index'), array());

CakePlugin::routes();

require CAKE . 'Config' . DS . 'routes.php';
