<?php

App::uses('AppController', 'Controller');

class ApiController extends AppController
{
    public $components = array('FdEmails.SendEmails');

    private $Simulador;
    private $SimuladorConsorcio;
    private $Carro;
    private $Marca;
    private $String;
    private $Newsletter;
    private $Sac;


    public function __construct(CakeRequest $cakeRequest, CakeResponse $cakeResponse)
    {
        parent::__construct($cakeRequest, $cakeResponse);
    }

    public function contratar_carro($carro = null)
    {
        header('Content-type: application/json');

        App::import('Model', 'FdSimuladores.Simulador');
        $this->Simulador = new Simulador();

        App::import('Model', 'FdCarros.Carro');
        $this->Carro = new Carro();

        $consorcio = CakeSession::read('selecionado');
        $carro = $this->Carro->find('first', array('conditions' => array('Carro.slug' => $carro)));

        if (!$carro) {
            http_response_code(400);
            die(json_encode(array('error' => true, 'message' => 'Ops, um erro interno aconteceu, tente novamente. Caso o erro persista, atualize a página.')));
        }

        $save = array(
            'id' => null,
            'nome' => $this->request->data['nome'],
            'carro_id' => (int)$carro['Carro']['id'],
            'marca_id' => (int)$carro['Carro']['marca_id'],
            'consorcio_id' => (int)$consorcio['Consorcio']['id'],
            'categoria_id' => 1,
            'telefone' => $this->request->data['telefone'],
            'email' => $this->request->data['email'],
            'cpf' => $this->request->data['cpf'],
            'parcela' => $this->request->data['parcela'][$this->request->data['carro']],
            'valor' => $carro['Carro']['preco'],
            'url_ref' => $this->request->referer()
        );

        if ($this->Simulador->save($save)) {

            $this->SendEmails->ContratarCarro($this->Simulador->getConsorcioCarroData($this->Simulador->id));

            http_response_code(200);
            die(json_encode(array('error' => false, 'message' => 'Obrigado, aguarde e instante entraremos em contato com você.', 'goal' => '/goal/contratar/' . $consorcio['Consorcio']['slug'])));
        }
        http_response_code(400);
        die(json_encode(array('error' => true, 'message' => 'Ops, falha ao salvar os dados, tente novamente.')));
    }


    public function duvida_outro_modelo($marca = null)
    {
        header('Content-type: application/json');

        App::import('Model', 'FdMarcas.Marca');
        $this->Marca = new Marca();

        App::import('Model', 'FdSimuladores.Simulador');
        $this->Simulador = new Simulador();

        App::import('Helper', 'String');
        $this->String = new StringHelper(new View(), null);

        $consorcio = CakeSession::read('selecionado');
        $marca = $this->Marca->find('first', array('conditions' => array('Marca.slug' => $marca)));

        if (!$marca) {
            http_response_code(400);
            die(json_encode(array('error' => true, 'message' => 'Ops, um erro interno aconteceu, tente novamente. Caso o erro persista, atualize a página.')));
        }

        $save = array(
            'id' => null,
            'nome' => $this->request->data['nome'],
            'consorcio_id' => (int)$consorcio['Consorcio']['id'],
            'categoria_id' => 1,
            'marca_id' => $marca['Marca']['id'],
            'telefone' => $this->request->data['telefone'],
            'email' => $this->request->data['email'],
            'mensagem' => $this->request->data['mensagem'],
            'cpf' => null,
            'parcela' => null,
            'valor' => $this->String->moedaToBco($this->request->data['valor-consorcio']),
            'url_ref' => $this->request->referer()
        );

        if ($this->Simulador->save($save)) {

            $this->SendEmails->OutroModelo($this->Simulador->getEmailData($this->Simulador->id));

            http_response_code(200);
            die(json_encode(array('error' => false, 'message' => 'Obrigado, aguarde e instante entraremos em contato com você.', 'goal' => '/goal/duvida/' . $consorcio['Consorcio']['slug'])));
        }

        http_response_code(400);
        die(json_encode(array('error' => true, 'message' => 'Ops, falha ao salvar os dados, tente novamente.')));

    }

    public function newsletter()
    {
        header('Content-type: application/json');

        App::import('Model', 'FdUsuarios.Newsletter');
        $this->Newsletter = new Newsletter();

        $newsletter = $this->Newsletter->find('first', array('conditions' => array('Newsletter.email' => $this->request->data['email'])));
        if (!$newsletter) {
            $this->Newsletter->create($this->request->data);
            if ($this->Newsletter->save()) {
                http_response_code(200);
                die(json_encode(array('error' => false, 'message' => 'Agradecemos sua inscrição!', 'goal' => '/goal/newsletter')));
            } else {
                http_response_code(400);
                die(json_encode(array('error' => true, 'message' => 'Ops, falha ao salvar os dados, tente novamente.')));
            }

        } else {
            http_response_code(200);
            die(json_encode(array('error' => false, 'message' => 'Você já está inscrito, obrigado!')));
        }

    }

    public function contato()
    {
        header('Content-type: application/json');

        App::import('Model', 'FdSac.Sac');
        $this->Sac = new Sac();

        $this->request->data['sac_tipo_id'] = 1;
        $this->request->data['url'] = $this->request->referer();
        $this->Sac->create($this->request->data);
        if ($this->Sac->save()) {
            $this->SendEmails->_enviaSac($this->request->data);
            http_response_code(200);
            die(json_encode(array('error' => false, 'message' => 'Obrigado, aguarde e instante entraremos em contato com você.', 'goal' => '/goal/contato')));
        } else {
            http_response_code(400);
            die(json_encode(array('error' => true, 'message' => 'Não foi possível enviar seu contato, tente novamente.')));
        }
    }


    public function contratar_simulador()
    {
        header('Content-type: application/json');

        App::import('Model', 'FdSimuladores.Simulador');
        $this->Simulador = new Simulador();

        App::import('Model', 'FdSimuladores.SimuladorConsorcio');
        $this->SimuladorConsorcio = new SimuladorConsorcio();

        App::import('Model', 'FdConsorcios.Consorcio');
        $this->Consorcio = new Consorcio();

        $consorcio = $this->Consorcio->firstCache($this->request->data['consorcio'], array('conditions' => array('Consorcio.slug' => $this->request->data['consorcio'])))
            ->getCacheData();

        if (!$consorcio) {
            http_response_code(400);
            die(json_encode(array('error' => true, 'message' => 'Ops, um erro interno aconteceu, tente novamente. Caso o erro persista, atualize a página.')));
        }

        $save = array(
            'Simulador' => array(
                'id' => CakeSession::read('simulador_id'),
                'nome' => $this->request->data['nome'],
                'consorcio_id' => (int)$consorcio['Consorcio']['id'],
                'categoria_id' => $this->request->data['categoria'] == 'automovel' ? 1 : 2,
                'telefone' => $this->request->data['telefone'],
                'email' => $this->request->data['email'],
                'cpf' => $this->request->data['cpf'],
                'parcela' => $this->request->data['parcela'],
                'valor' => $this->request->data['valor'],
                'finalizado' => 1,
                'url_ref' => $this->request->referer()
            ),
            'SimuladorConsorcio' => array(
                array(
                    'nome' => $consorcio['Consorcio']['nome'],
                    'slug' => $consorcio['Consorcio']['slug'],
                    'parcela' => $this->request->data['parcela'],
                    'taxa_carro' => $consorcio['Consorcio']['taxa_carro'],
                    'taxa_imovel' => $consorcio['Consorcio']['taxa_imovel'],
                    'parcela_carro' => $consorcio['Consorcio']['parcela_carro'],
                    'parcela_imovel' => $consorcio['Consorcio']['parcela_imovel'],
                    'mostrar_marcas' => intval($consorcio['Consorcio']['mostrar_marcas']),
                    'mostrar_veiculos' => intval($consorcio['Consorcio']['mostrar_veiculos']),
                    'mais_contratado' => $consorcio['Consorcio']['mais_contratado'],
                    'meia_parcela' => $consorcio['Consorcio']['meia_parcela'],
                    'status' => intval($consorcio['Consorcio']['status'])
                )
            )
        );

        if ($this->Simulador->saveAll($save)) {

            $this->SendEmails->Simulador($this->Simulador->getSimuladorData($this->Simulador->id));

            http_response_code(200);
            die(json_encode(array('error' => false, 'message' => 'Obrigado, aguarde e instante entraremos em contato com você.', 'goal' => '/goal/contratar/' . $this->request->data['categoria'] . '/' . $consorcio['Consorcio']['slug'])));
        }

        http_response_code(400);
        die(json_encode(array('error' => true, 'message' => 'Ops, falha ao salvar os dados, tente novamente.')));

    }

}