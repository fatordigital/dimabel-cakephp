<?php

class PaginasController extends AppController
{

    public $components = array('Cookie');

    public function detalhe($id = null)
    {
        App::import('Model', 'FdPaginas.Pagina');
        $this->Pagina = new Pagina();

        if (!$this->Pagina->exists($id))
            throw new NotFoundException(__('Página inválida.'));

        $options = array(
            'conditions' => array(
                'Pagina.' . $this->Pagina->primaryKey => $id
            ),
            'contain' => array(
                'PaginaTipo', 'Aba' => array(
                    'conditions' => array(
                        'status' => true
                    ),
                    'order' => 'aba_ordem ASC'
                )
            )
        );

        $pagina = $this->Pagina->find('first', $options);
        if (isset($pagina['Aba'])) {
            foreach ($pagina['Aba'] as $idx => $aba) {
                if (isset($aba['galeria_id']) && !empty($aba['galeria_id'])) {

                    App::import('Model', 'FdGalerias.Galeria');
                    $this->Galeria = new Galeria();

                    App::import('Model', 'FdGalerias.GaleriaImagem');
                    $galeria = $this->Galeria->find('first', array(
                            'conditions' => array(
                                'Galeria.id' => $aba['galeria_id']
                            )
                        )
                    );

                    if (!empty($galeria)) {
                        $pagina['Aba'][$idx]['GaleriaImagem'] = $galeria['GaleriaImagem'];
                    }
                }
            }
        }

        $this->set('title', $pagina['Pagina']['nome']);
        $this->set(compact('pagina'));

        if (isset($pagina['Pagina']['view_layout']) && $pagina['Pagina']['view_layout'] != 'default') {
            $this->render($pagina['Pagina']['view_layout']);
        }
    }

}