<?php

class CartaController extends AppController
{

    public $uses = array('FdCartaContemplada.CartaContemplada', 'FdCartaContemplada.CartaReserva');

    public function index()
    {
        $this->set('title', 'Conheça as nossas cartas contempladas');

        $this->salvar();

        $Query = $this->CartaContemplada->find('all', array('conditions' => array('CartaContemplada.status != 2'), 'order' => array('CartaContemplada.created DESC')));
        $this->set('cartas', $Query);
    }

    public function salvar()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data['Carta'];
            if (!empty($data['fd'])) {
                $this->Flash->error("Não foi possível receber seus dados :(");
            } else {

                App::import('String', 'Helper');
                $StringHelper = new StringHelper(new View());

                $Carta = $this->CartaContemplada->findById($data['carta_id']);

                if (!$Carta)
                    $this->Flash->error("Não foi possível receber seus dados, tente novamente.");
                else {
                    $this->CartaReserva->create($data);
                    if ($this->CartaReserva->save()) {

                        $data['valor_carta'] = '' . $StringHelper->bcoToMoeda($Carta['CartaContemplada']['valor_carta']);
                        $data['valor_entrada'] = '' . $StringHelper->bcoToMoeda($Carta['CartaContemplada']['valor_entrada']);
                        $data['valor_parcela'] = '' . $StringHelper->bcoToMoeda($Carta['CartaContemplada']['valor_parcela']);
                        $data['parcelas'] = $Carta['CartaContemplada']['parcelas'];
                        $data['tipo'] = $Carta['Segmento']['nome'];
                        $data['consorcio'] = $Carta['CartaContemplada']['consorcio'];

                        $this->CartaContempladaEmail($data);

                        $this->request->data['Carta'] = array();

                        $this->Flash->goal('/goal/carta-contemplada', array('key' => 'goal'));
                        $this->Flash->success("Obrigado, em breve entraremos em contato.", array('key' => 'success'));
                    } else {
                        $this->Flash->error("Não foi possível receber seus dados, tente novamente.");
                    }
                }
            }
        }
    }

    private function CartaContempladaEmail($data)
    {
        App::import('Model', 'FdSac.SacMail');
        $Email = new SacMail(6, 6);
        $Email->setSiteNome(Configure::read('Site.Nome'))
            ->setSiteUrl(Router::url('/', true))
            ->setNome($data['nome'])
            ->setTelefone($data['telefone'])
            ->setEmail($data['email'])
            ->setValorCarta($data['valor_carta'])
            ->setValorEntrada($data['valor_entrada'])
            ->setValorParcela($data['valor_parcela'])
            ->setParcelas($data['parcelas'])
            ->setTipo($data['tipo'])
            ->setConsorcio($data['consorcio'])
            ->Enviar();

        return $Email->getStatus();
    }


}