<?php

class HomeController extends AppController
{

    public $components = array('Cookie');
    public $helpers = array('String');

    private $Marca;
    private $Consorcio;

    public function index()
    {

        $title_for_layout = '';
        $this->set(compact('title_for_layout'));

        App::import('Model', 'FdUsuarios.Newsletter');
        $this->Newsletter = new Newsletter();

        if ($this->request->is('post')) {

            if ($this->request->data['Newsletter']['fd'] == "") {
                if (isset($this->request->data['Newsletter'])) {

                    $newsletter = $this->Newsletter->find('first', array('recursive' => -1, 'conditions' => array('Newsletter.email' => $this->request->data['Newsletter']['email'])));
                    if (!empty($newsletter)) {
                        $this->request->data['Newsletter']['id'] = $newsletter['Newsletter']['id'];
                    }

                    if ($this->Newsletter->save($this->request->data['Newsletter'])) {
                        // $this->request->data['Newsletter'] = array();
                        $this->redirect(array('controller' => 'certificado', 'action' => 'index', 'nome' => $this->request->data['Newsletter']['nome']));

                    } else {
                        $this->Session->setFlash('A consulta não pode ser feita. Tente novamente.', 'default', array('class' => 'alert alert-danger'), 'certificado');
                    }
                }
            } else {
                $this->Session->setFlash('A consulta não pode ser feita. Tente novamente.', 'default', array('class' => 'alert alert-danger'), 'certificado');
            }
        }

        $this->blog();

        App::import('Model', 'FdMarcas.Marca');
        $this->Marca = new Marca();

        App::import('Model', 'FdConsorcios.Consorcio');
        $this->Consorcio = new Consorcio();

        $marcas = $this->Marca->cache()
            ->getCacheData();
        
        $consorcios = $this->Consorcio->allConditions('consorcio_visivel', array('conditions' => array('Consorcio.status' => 1, 'Consorcio.visivel' => 1)))
            ->getCacheData();
        
        $this->set(compact('marcas', 'consorcios'));
    }

    private function blog()
    {
        App::import('Model', 'Contato');
        $contato = new Contato();

        $posts = $contato->query('SELECT wp_posts.ID, wp_term_taxonomy.term_taxonomy_id , wp_posts.post_title, wp_posts.post_name, wp_posts.post_date, wp_posts.post_content,
        wp_terms.`name` AS CATEGORY_NAME, wp_terms.slug AS CATEGORY_SLUG FROM wp_posts
        LEFT JOIN wp_term_relationships ON wp_posts.ID = wp_term_relationships.object_id
        LEFT JOIN wp_term_taxonomy ON wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id
        LEFT JOIN wp_terms ON wp_terms.term_id = wp_term_taxonomy.term_id
        WHERE wp_posts.post_status = "publish" GROUP BY ID ORDER BY ID DESC LIMIT 2');

        $this->set(compact('posts'));

    }

}