<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package        app.Controller
 * @link        http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    public $components = array(
        'Acl',
        'Auth',
        'Session',
        'Flash',
        'Paginator',
        'ImportDb',
        'Reports',
        'Cookie',
        'FilterResults.FilterResults' => array(
            'auto' => array(
                'paginate' => false,
                'explode' => true,
            ),
            'explode' => array(
                'character' => ' ',
                'concatenate' => 'AND',
            ),
        ),
        'FdMenus.Menuu',
        'FdEmails.SendEmails',
    );

    public $helpers = array(
        'Session',
        'Time',
        'String',
        'FilterResults.FilterForm' => array(
            'operators' => array(
                'LIKE' => 'containing',
                'NOT LIKE' => 'not containing',
                'LIKE BEGIN' => 'starting with',
                'LIKE END' => 'ending with',
                '=' => 'equal to',
                '!=' => 'different',
                '>' => 'greater than',
                '>=' => 'greater or equal to',
                '<' => 'less than',
                '<=' => 'less or equal to'
            )
        ),
        'Html' => array('className' => 'MyHtml'),
        'FdMenus.Menus',
    );

    public function __construct(CakeRequest $cakeRequest, CakeResponse $cakeResponse)
    {
        if (!strstr(Router::url(), 'fatorcms')) {
            $this->components[] = 'FdConsorcios.Consorcio';
            $this->helpers[] = 'FdConsorcios.Consorcio';
        }
        parent::__construct($cakeRequest, $cakeResponse);

        if (isset($_GET['email_debug']) && !empty($_GET['email_debug'])) {
            CakeSession::write('email_debug', $_GET['email_debug']);
        } else if (isset($_GET['email_debug_off'])) {
            CakeSession::delete('email_debug');
        }

    }

    protected function _configureAcl()
    {
        //Configure AuthComponent
        $this->Auth->authenticate = array(
            AuthComponent::ALL => array(
                'userModel' => 'FdUsuarios.Usuario',
                'fields' => array('username' => 'email', 'password' => 'senha'),
                'scope' => array('Usuario.status' => true)
            ),
            'Form'
        );

        $this->Auth->authorize = array(
            'Actions' => array(
                'actionPath' => 'controllers',
                'userModel' => 'FdUsuarios.Usuario',
            ),
        );

        //Definicoes do methods de autenticação
        if ($this->_isAdminMode()) {
            $this->Auth->autoRedirect = true;
            $this->Auth->loginAction = array('fatorcms' => true, 'plugin' => 'fd_usuarios', 'controller' => 'fd_usuarios', 'action' => 'login');
            $this->Auth->loginRedirect = array('fatorcms' => true, 'plugin' => 'fd_dashboard', 'controller' => 'fd_dashboard', 'action' => 'index');
            $this->Auth->logoutRedirect = array('fatorcms' => true, 'plugin' => 'fd_usuarios', 'controller' => 'fd_usuarios', 'action' => 'login', 'prefix' => 'fatorcms');
            $this->Auth->authError = 'Efetue login com permissão nessa sessão para prosseguir.';
        } else {
            $this->Auth->autoRedirect = false;
            $this->Auth->loginAction = array('fatorcms' => true, 'plugin' => 'fd_usuarios', 'controller' => 'fd_usuarios', 'action' => 'login');
            $this->Auth->loginRedirect = array('fatorcms' => false, 'plugin' => 'fd_usuarios', 'controller' => 'home', 'action' => 'index');
            $this->Auth->logoutRedirect = array('fatorcms' => false, 'plugin' => 'fd_usuarios', 'controller' => 'home', 'action' => 'index');
            $this->Auth->authError = 'Faça login para prosseguir.';
        }

        //Begin permissons
        if ($this->Auth->user() && $this->Auth->user('grupo_id') == 1) {
            // Role: Admin
            $this->Auth->allowedActions = array('*');
        } else {
            if ($this->Auth->user()) {
                $roleId = $this->Auth->user('grupo_id');
            } else {
                $roleId = 4; // Role: Public
            }

            $thisControllerNode = $this->Acl->Aco->node($this->Auth->actionPath . $this->name);

            if ($thisControllerNode) {
                $thisControllerNode = $thisControllerNode['0'];

                $thisControllerActions = $this->Acl->Aco->find('list', array(
                    'conditions' => array(
                        'Aco.parent_id' => $thisControllerNode['Aco']['id'],
                    ),
                    'fields' => array(
                        'Aco.id',
                        'Aco.alias',
                    ),
                    'recursive' => '-1',
                ));

                $thisControllerActionsIds = array_keys($thisControllerActions);

                $allowedActions = $this->Acl->Aco->Permission->find('list', array(
                    'conditions' => array(
                        'Permission.aro_id' => $roleId,
                        'Permission.aco_id' => $thisControllerActionsIds,
                        'Permission._create' => 1,
                        'Permission._read' => 1,
                        'Permission._update' => 1,
                        'Permission._delete' => 1,
                    ),
                    'fields' => array(
                        'id',
                        'aco_id',
                    ),
                    'recursive' => '-1',
                ));
                $allowedActionsIds = array_values($allowedActions);
            }

            $allow = array();
            if (isset($allowedActionsIds) &&
                is_array($allowedActionsIds) &&
                count($allowedActionsIds) > 0
            ) {
                foreach ($allowedActionsIds AS $i => $aId) {
                    $allow[] = $thisControllerActions[$aId];
                }
            }

            $this->Auth->allowedActions = $allow;
        }
        //End permissions

        //gamba (solucionar ACL)
        // if($this->_isAdminMode() && $this->Auth->user() && $this->Auth->user('grupo_id') != 1){
        //  $this->Session->setFlash(__('Acesso Negado'), 'default', array('class' => 'error_message'));
        //  $this->redirect(array('controller' => 'home', 'action' => 'index', 'admin' => false));
        // }

        // Allow all actions. CakePHP 2.0
        // $this->Auth->allow('*');
        // Allow all actions. CakePHP 2.1
        // $this->Auth->allow();
    }

    /**
     * Migalha
     *
     * @var array
     * @access protected
     */
    protected $_breadcrumb = array();

    /**
     * Adição de migalha
     *
     *
     * @param string $nome Nome da migalha
     * @param mixed $url Url de acesso (opcional). Deve seguir o padrão do cake para url
     * @access protected
     * @return AppController Próprio Objeto para encadeamento
     */
    protected function _addBreadcrumb($nome, $url = null)
    {
        if (empty($url)) {
            $url = 'javascript:void(0);';
        }
        $this->_breadcrumb[] = array('nome' => $nome, 'url' => $url);
        return $this;
    }

    /*
     * Verifico que o usuario está no admin
     *
     * @return bollean Resultado da operação
     */
    protected function _isAdminMode()
    {
        $adminRoute = Configure::read('Routing.prefixes');
        if (isset($this->params['prefix']) && in_array($this->params['prefix'], $adminRoute)) {
            return true;
        }
        return false;
    }

    /**
     * Mêtodo setUrlReferer
     *
     * @return Array
     */
    protected function _setUrlReferer()
    {
        $referer_tmp = $this->Session->read('referer');
        if ($referer_tmp == "" || $referer_tmp === FALSE) {
            $this->Session->write('referer', $this->referer());
        }
    }

    /**
     * Mêtodo _resetCaches
     *
     * @return Array
     */
    protected function _resetCaches()
    {
        Cache::clearGroup('site');
    }

    /**
     * Mêtodo _saveStatus
     *
     * @return Array
     */
    protected function _saveStatus($model, $id, $status)
    {
        $this->{$model}->id = $id;
        $this->{$model}->saveField('status', $status);
        if ($status == 1) {
            return 0;
        } else {
            return 1;
        }
    }

    /**
     * Mêtodo _redirectFilter
     * responsavel por gerenciar os redirects dos filters
     * @return Array
     */
    protected function _redirectFilter($referer = null)
    {
        if ($referer == null) {
            $referer = $this->referer();
        }

        //custom do redirect para o FilterResults
        if ($referer && stripos($referer, '/fatorcms/')) {
            $this->redirect($referer);
        } else {
            $this->redirect(array('action' => 'index'));
        }
    }

    /**
     * Carrega os Blocos
     * @return Array
     */

    private function _carregaBlocos()
    {
        App::import('Model', 'FdBlocos.Bloco');
        $modelBloco = new Bloco();

        $bloco = array();
        if (Cache::read('bloco') === false) {
            $blocos = $modelBloco->find('all', array(
                    'recursive' => -1,
                    'conditions' => array('Bloco.status' => true)
                )
            );

            if (!empty($blocos)) {
                foreach ($blocos as $key => $bloc) {
                    $bloco[$bloc['Bloco']['slug']] = $bloc['Bloco']['conteudo'];
                }
            }
            Cache::write('bloco', $bloco);
        } else {
            $bloco = Cache::read('bloco');
        }

        //blocos
        $this->set('bloco', $bloco);
        $this->set('bloco_de_conteudo', $bloco);
    }

    private function _getEventos()
    {
        App::import('Model', 'FdEventos.Evento');
        $this->Evento = new Evento();
        $eventos_hoje = $this->Evento->find('all', array('recursive' => -1, 'fields' => array('Evento.id', 'Evento.nome', 'Evento.conteudo', 'Evento.data_inicio', 'Evento.data_fim', 'Evento.seo_url'), 'conditions' => array('Evento.status' => true, 'OR' => array(array('Evento.data_inicio =' => date('Y-m-d')), array('Evento.data_inicio <' => date('Y-m-d'), 'Evento.data_fim >=' => date('Y-m-d'))))));
        $eventos_proximos = $this->Evento->find('all', array('recursive' => -1, 'order' => 'data_inicio ASC', 'fields' => array('Evento.id', 'Evento.nome', 'Evento.conteudo', 'Evento.data_inicio', 'Evento.data_fim', 'Evento.seo_url'), 'conditions' => array('Evento.status' => true, 'Evento.data_inicio >' => date('Y-m-d'), 'Evento.evento_tipo' => 'evento'), 'limit' => 10));

        if (isset($this->params->params['tipo_calendario'])) {
            $conditions = array('Evento.status' => true, 'Evento.evento_tipo' => $this->params->params['tipo_calendario']);
        } else {
            $conditions = array('Evento.status' => true);
        }

        $all_eventos = $this->Evento->find('all', array('recursive' => -1, 'fields' => array('Evento.id', 'Evento.data_inicio', 'Evento.data_fim', 'Evento.evento_tipo'), 'conditions' => $conditions));

        $eventos = array();
        foreach ($all_eventos as $key => $value) {
            if ($value['Evento']['data_fim'] != null) {
                $data_inicio = date('Y-n-j', strtotime($value['Evento']['data_inicio']));
                $eventos[$value['Evento']['evento_tipo']][$data_inicio] = $data_inicio;

                while ($value['Evento']['data_inicio'] < $value['Evento']['data_fim']) {
                    $value['Evento']['data_inicio'] = date('Y-m-d', strtotime($value['Evento']['data_inicio'] . ' + 1 day'));
                    $data_inicio = date('Y-n-j', strtotime($value['Evento']['data_inicio']));

                    $eventos[$value['Evento']['evento_tipo']][$data_inicio] = $data_inicio;
                }
            } else {
                $data_inicio = date('Y-n-j', strtotime($value['Evento']['data_inicio']));
                $eventos[$value['Evento']['evento_tipo']][$data_inicio] = $data_inicio;
            }
        }

        //oders
        foreach ($eventos as $key => $value) {
            ksort($eventos[$key]);
        }

        //set
        if (isset($eventos['evento'])) {
            $this->set('datas_eventos_eventos', $eventos['evento']);
        } else {
            $this->set('datas_eventos_eventos', array());
        }

        if (isset($eventos['academico'])) {
            $this->set('datas_eventos_academico', $eventos['academico']);
        } else {
            $this->set('datas_eventos_academico', array());
        }

        // debug($eventos);die;

        $this->set(compact('eventos_proximos', 'eventos_hoje'));
    }

    private function removeSessaoSimulador()
    {
        $controller = $this->request->param('controller');
        if ($controller != 'simulador' && $controller != 'api' && !strstr($controller, 'fd_')) {
            CakeSession::delete('simulador_id');
        }
    }

    // sobrescrita de gancho para antes da execução das filtragens
    public function beforeFilter()
    {
        // Configue ACL
        $this->_configureAcl();
        
        // Set user logado
        $this->set('auth', $this->Auth->user());

        // Load infos settings
        $this->loadModel('FdSettings.Setting');
        $this->Setting->loadSettings();

        if (!$this->request->is('ajax')) {

            //altero o layout se o usuário estiver no admin
            if ($this->_isAdminMode()) {
                //layout
                $this->layout = 'fatorcms';
                //menu
                $this->set('menus_for_layout', $this->Menuu->carregaMenu(array('admin')));

                if (isset($this->params->query['reset']) && $this->params->query['reset'] == 'ok') {
                    $this->Session->setFlash(__('Dados atualizados com sucesso.'), 'fatorcms_success');
                    $this->redirect('/' . $this->params->url);
                }
            } else {
                //layout
                $this->layout = 'default';
                //menu
                $menus_for_layout = $this->Menuu->carregaMenu(array('site_institucional'));
                $this->set('menus_for_layout', $menus_for_layout);

                // Lista dos blocos de conteudo
                $this->_carregaBlocos();

                // Lista os eventos
                // $this->_getEventos();

                if ((isset($_GET['noticia']) && $_GET['noticia'] != "") || (isset($this->params->query['noticia']) && $this->params->query['noticia'] != "")) {
                    throw new NotFoundException(__('Notícia inválida.'));
                }

                if ($this->request->is('post')) {
                    App::import('Model', 'FdSac.Sac');
                    $this->Sac = new Sac();

                    if (isset($this->request->data['SacInterna'])) {

                        if ($this->request->data['SacInterna']['fd'] == "") {
                            $this->request->data['SacInterna']['controller'] = $this->params->params['controller'];
                            $this->request->data['SacInterna']['url'] = $this->params->url;
                            $this->request->data['SacInterna']['sac_tipo_id'] = 2;
                            if ($this->Sac->save($this->request->data['SacInterna'])) {
                                $this->SendEmails->_enviaSacInterna($this->request->data['SacInterna']);
                                $this->request->data['SacInterna'] = array();
                                $this->Session->setFlash('Contato realizado com sucesso.', 'default', array('class' => 'success_message'));
                                $this->set('send_goal_sac_interna', true);
                            } else {
                                $this->Session->setFlash('O contato não pode ser salvo. Tente novamente.', 'default', array('class' => 'error_message'));
                                $this->set('send_goal_sac_interna', false);
                            }
                        } else {
                            $this->Session->setFlash('O contato não pode ser salvo. Tente novamente.', 'default', array('class' => 'error_message'));
                            $this->set('send_goal_sac_interna', false);
                        }

                    }
                }
            }
        }

        $this->removeSessaoSimulador();
    }

    private function carregarMarcas()
    {
        App::import('Model', 'FdMarcas.Marca');
        $marcaModel = new Marca();

        $marcas = $marcaModel->cache()
            ->getCacheData();

        $this->set(compact('marcas'));
    }

    // sobrescrita de gancho para antes da renderização da view
    public function beforeRender()
    {

        $this->carregarMarcas();

		/*
        if($_SERVER['SERVER_NAME'] != "localhost" && $_SERVER['REMOTE_ADDR'] != "::1" && $_SERVER['SERVER_ADDR'] != "127.0.0.1" && $_SERVER['SERVER_ADDR'] != "192.168.0.221"){
            $url = env('SERVER_NAME') . env('REQUEST_URI');
            if (!preg_match('/www/', $url) && !preg_match('/ceiadenatal/', $url)) {
                $url = 'http://www.' . $url;
                $this->redirect($url, 301);
            }
        }
		*/

        if (Configure::read('debug') == 0) {
            if (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] != 'on') {
                if ((strpos($_SERVER['HTTP_HOST'], 'www.') === false)) {
                    $this->redirect(str_replace('http://', 'https://www.', Router::url(null, true)));
                } else {
                    $this->redirect(str_replace('http://', 'https://', Router::url(null, true)));
                }
            } else {
                if ((strpos($_SERVER['HTTP_HOST'], 'www.') === false)) {
                    $this->redirect(str_replace('https://', 'https://www.', Router::url(null, true)));
                }
            }
        }

        $this->set('parcela_disponiveis', json_decode(Configure::read('Site.Parcelas'), true));

        // envia a migalha para a visualização
        $this->set('breadcrumbs', $this->_breadcrumb);
    }
}