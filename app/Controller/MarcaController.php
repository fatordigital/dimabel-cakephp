<?php

class MarcaController extends AppController
{

    private $Marca;

    public function __construct(CakeRequest $cakeRequest, CakeResponse $cakeResponse)
    {
        parent::__construct($cakeRequest, $cakeResponse);
        $this->Marca = ClassRegistry::init('FdMarcas.Marca');
    }

    public function detalhe($marca)
    {
        $marca = $this->Marca->firstCache($marca, array('conditions' => array('Marca.slug' => $marca)))
            ->getCacheData();

        if (!$marca)
            return $this->redirect($this->referer() != '/' ? $this->referer() : Router::url('/', true));
        

        $this->set('title', $marca['Marca']['nome']);
        $this->set(compact('marca'));
    }

}