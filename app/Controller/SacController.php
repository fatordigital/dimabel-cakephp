<?php

class SacController extends AppController
{

    public $components = array('FdEmails.SendEmails');

    public function add()
    {

        $this->set('title', 'Fale Conosco');

        App::import('Model', 'FdSac.Sac');
        $this->Sac = new Sac();
        if ($this->request->is('post')) {
            if ($this->request->data['Sac']['fd'] == "") {
                if (isset($this->request->data['Sac'])) {
                    $this->request->data['Sac']['sac_tipo_id'] = 1;
                    if ($this->Sac->save($this->request->data['Sac'])) {
                        $this->SendEmails->_enviaSac($this->request->data['Sac']);

                        $this->Flash->success('Contato realizado com sucesso.', array('key' => 'success'));
                        $this->Flash->goal('/goal/contato', array('key' => 'goal'));

                        $this->redirect($this->referer());
                    } else {
                        $this->Flash->error('Não foi possível enviar seu contato, tente novamente.');
                    }
                }
            } else {
                $this->Flash->error('Não foi possível enviar seu contato, tente novamente.');
                $this->redirect($this->referer());
            }
        }
    }
}