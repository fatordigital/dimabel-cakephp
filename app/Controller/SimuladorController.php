<?php

class SimuladorController extends AppController
{

    public $components = array('FdEmails.SendEmails');

    private $Consorcio;
    private $SimuladorConsorcio;
    private $String;
    private $Carro;
    private $Simulador;
    private $parcelas = 'parcelas_imoveis';
    private $apos_valor = 'apos_valor_imovel';
    private $apos_valor_parcela = 'parcelas_apos_valor_imovel';

    public function index()
    {

        if (!isset($this->request->data['value']) || $this->request->data['value'] == '') {
            if (!isset($this->request->data['carro']) || $this->request->data['carro'] == '') {
                $this->Flash->error('Para acessar está área é necessário fazer uma simulação.');
                $this->redirect('/');
            }
        }

        App::import('Helper', 'String');
        $this->String = new StringHelper(new View());

        if (isset($this->request->data['carro']) && !empty($this->request->data['carro'])) {
            App::import('Model', 'FdCarros.Carro');
            $this->Carro = new Carro();
            $carro = $this->Carro->find('first', array('recursive' => -1, 'fields' => array('Carro.id', 'Carro.marca_id', 'Carro.preco'), 'conditions' => array('Carro.slug' => $this->request->data['carro'])));
            if (!$carro) {
                $this->Flash->error('Ops, não foi possível continuar com a operação, tente novamente ou entre em contato conosco.');
                $this->redirect($this->referer());
            } else {

                $this->parcelas = 'parcelas_carro';
                $this->apos_valor = 'apos_valor_carro';
                $this->apos_valor_parcela = 'parcelas_apos_valor_carro';

                $this->request->data['value'] = $this->String->bcoToMoeda($carro['Carro']['preco']);
                $this->request->data['marca_id'] = $carro['Carro']['marca_id'];
                $this->request->data['carro_id'] = $carro['Carro']['id'];
            }
        }

        if ($this->request->data['simulation_type'] == 'automovel') {
            $this->parcelas = 'parcelas_carro';
            $this->apos_valor = 'apos_valor_carro';
            $this->apos_valor_parcela = 'parcelas_apos_valor_carro';
        } else {
            $this->parcelas = 'parcelas_imoveis';
        }

        App::import('Model', 'FdConsorcios.Consorcio');
        $this->Consorcio = new Consorcio();

        App::import('Model', 'FdSimuladores.SimuladorConsorcio');
        $this->SimuladorConsorcio = new SimuladorConsorcio();

        App::import('Model', 'FdSimuladores.Simulador');
        $this->Simulador = new Simulador();

        $request_data = $this->request->data;
        $request_data['valor'] = $this->String->moedaToBco($request_data['value']);

        $this->pre_contrato($request_data);

        $title = 'Confira o resultado da sua simulação de consórcio';
        $consorcios = $this->Consorcio->cache()
            ->getCacheData();

        /*Ordenação*/
        $consorcios = Hash::sort($consorcios, '{n}.Consorcio.ordem', 'asc');

        $valor_request = $this->String->bcoToMoeda($request_data['valor']);

        $unidade = $this->String->unidade($valor_request);
        $decimal = $this->String->decimal($valor_request);

        $value = $this->String->moedaToBco($request_data['value']);

        $share_data = array(
            'title' => $title,
            'consorcios' => $consorcios,
            'value' => $value,
            'unidade' => $unidade,
            'decimal' => $decimal,
            'request_data' => $request_data
        );

        if (isset($request_data['carro_id'])) {
            $this->parcelas = 'parcelas_carro';
            $share_data['carro_id'] = $request_data['carro_id'];
            $share_data['marca_id'] = $request_data['marca_id'];
        }

        $this->set('parcela_segmento', $this->parcelas);
        $this->set('apos_valor', $this->apos_valor);
        $this->set('parcelas_apos', $this->apos_valor_parcela);
        $this->set($share_data);

    }

    private function pre_contrato(array $data)
    {
        if (!CakeSession::read('simulador_id')) {
            $this->Simulador->create();
            if ($this->Simulador->save($data)) {
                $this->SendEmails->PreContrato($data);
                $this->Flash->goal('/goal/simulador/' . $data['simulation_type'], array('key' => 'goal'));
                CakeSession::write('simulador_id', $this->Simulador->getLastInsertId());
            }
        }
    }

}