<?php echo $this->assign('title', 'Busca por ' . $this->params['named']['item']); ?>

</header>
<main>
    <section class="conteudo institucional">

        <div class="container-fluid bg-curos">
            <div class="row">
                <div class="box-banner">
                    <img src="assets/images/banners/banner-curso.jpg" class="img-responsive">
                    <div class="filter-img"></div>  
                </div>                      
                <div class="col-xs-12 box-caption">                     
                    <div class="container">                             
                        <div class="row">
                            <h1 class="text-center">Busca</h1>
                            <p><a href="<?php echo $this->Html->Url('/',true) ?>">Home</a> » Busca</p>
                        </div>
                    </div>                      
                </div>              
            </div>              
        </div>
        <div class="container">
            <div class="row row-conteudo busca">
                <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                    <div class="col-xs-12">
                        <?php if($totais_count>0): ?>
                        <h3><?php echo $totais_count ?> resultado(s) encontrado(s)</h3>                        
                        <?php else: ?>
                        <h3>Nenhum resultado encontrado</h3>                        
                        <?php endif; ?>
                        <br><br>
                        
                        <?php if(isset($cursos) && sizeof($cursos)>0): ?>
                        <?php foreach($cursos as $curso): ?>
                        <div class="box-result">
                            <time><?php echo $curso['Rota']['model'] ?></time>
                            <a href="<?php echo $this->Html->Url('/'.$curso['Rota']['seo_url'],true); ?>"><h5><?php echo $curso['Rota']['nome'] ?></h5></a>
                            <p>
                                <?php echo $this->String->limitStringByWords($curso['Rota']['conteudo'],100,200); ?> 
                            </p>
                            <a href="<?php echo $this->Html->Url('/'.$curso['Rota']['seo_url'],true); ?>">Leia Mais</a>
                        </div>
                        <?php endforeach; ?>
                        <?php endif; ?>

                        <?php if(isset($rotas) && sizeof($rotas)>0): ?>
                        <?php foreach($rotas as $rota): ?>
                        <div class="box-result">
                            <time><?php echo $rota['Rota']['model'] ?></time>
                            <a href="<?php echo $this->Html->Url('/'.$rota['Rota']['seo_url'],true); ?>"><h5><?php echo $rota['Rota']['nome'] ?></h5></a>
                            <p>
                                <?php echo $this->String->limitStringByWords($rota['Rota']['conteudo'],100,200); ?> 
                            </p>
                            <a href="<?php echo $this->Html->Url('/'.$rota['Rota']['seo_url'],true); ?>">Leia Mais</a>
                        </div>
                        <?php endforeach; ?>
                        <?php endif; ?>

                    </div>
                    <div class="col-xs-12">
                        <?php if(isset($this->params['paging']['Rota']['pageCount']) && $this->params['paging']['Rota']['pageCount'] > 1): ?>
                            <nav aria-label="Page navigation" class="navigation">
                                <ul class="pagination">
                                    <li>
                                      <?php echo $this->Paginator->prev(__('«', true), array('aria-hidden' => 'true'), null, array()); ?>
                                    </li>
                                    <?php echo $this->Paginator->numbers(array('separator' => ' ', 'tag' => 'li', 'currentTag' => 'a')); ?>
                                    <?php if(($this->Paginator->current()+9) < $this->Paginator->counter(array('format' => '{:pages}'))): ?>
                                        <li>
                                            <?php echo $this->Paginator->last($this->Paginator->counter(array('format' => '{:pages}')), array(), null, array()); ?>
                                        </li>
                                    <?php endIf; ?>
                                    <li>                          
                                        <?php echo $this->Paginator->next(__('»', true), array('aria-hidden' => 'true'), null, array()); ?>
                                    </li>
                                </ul>
                            </nav>
                        <?php endIf; ?>                        
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">                  
                    <?php echo $this->element('site/sidebar_paginas') ?>
                </div>                      
            </div>              
        </div>                      
    </section>      
</main>