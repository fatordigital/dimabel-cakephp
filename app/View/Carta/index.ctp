<?php echo $this->assign('title', 'Cartas contempladas'); ?>

<?php echo $this->Element('site/header_interna') ?>

<section class="fd_page_content fd_page_products">
    <div class="container">
        <div class="row">

            <div class="col-xs-12 over-table">
                <div class="fd_content">

                    <table style="display: inline-table;" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr class="td_prin">
                            <th colspan="7" align="center" style="text-align:center;">Categoria</th>
                            <th colspan="7" align="center" style="text-align:center;">Crédito</th>
                            <th colspan="7" align="center" style="text-align:center;">Entrada</th>
                            <th colspan="7" align="center" style="text-align:center;">Parcela</th>
                            <th colspan="7" align="center" style="text-align:center;">Prazo</th>
                            <th colspan="7" align="center" style="text-align:center;">Administradora</th>
                            <th colspan="7" align="center" style="text-align:center;">Status</th>
                        </tr>
                        <?php if (isset($cartas) && count($cartas)) { ?>
                            <?php foreach ($cartas as $carta) { ?>
                                <tr>
                                    <td colspan="7" align="center"><?php echo $carta['Segmento']['nome'] ?></td>
                                    <td colspan="7" align="center">R$ <?php echo $this->String->bcoToMoeda($carta['CartaContemplada']['valor_carta']) ?></td>
                                    <td colspan="7" align="center">R$ <?php echo $this->String->bcoToMoeda($carta['CartaContemplada']['valor_entrada']) ?></td>
                                    <td colspan="7" align="center">R$ <?php echo $this->String->bcoToMoeda($carta['CartaContemplada']['valor_parcela']) ?></td>
                                    <td colspan="7" align="center"><?php echo $carta['CartaContemplada']['parcelas'] ?>x</td>
                                    <td colspan="7" align="center"><?php echo $carta['CartaContemplada']['consorcio'] ?></td>
                                    <td colspan="7" align="center">
                                        <?php if ($carta['CartaContemplada']['status'] == 1) { ?>
                                            <a href="javascript:void(0)" class="btn fd_button_orange fd_button_cart" onclick="openModal(<?php echo $carta['CartaContemplada']['id'] ?>)">
                                                Disponível
                                            </a>
                                        <?php } else { ?>
                                            <a href="javascript:void(0)" class="btn fd_button_orange fd_button_dark_orange fd_button_cart">
                                                Reservado
                                            </a>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        <?php } else { ?>
                            <tr>
                                <td colspan="7"></td>
                            </tr>
                        <?php } ?>
                    </table>


                </div><!-- end of /.fd_content -->
            </div><!-- end of /.cols.fd_eq -->

        </div><!-- end of /.row -->

    </div><!-- end of /.container -->
</section>

<div class="fd_modal_dropback"></div>
<div class="fd_modal caixa" style="min-height: 435px">
    <a href="javascript:void(0)" class="fd_modal_close">
        <img src="<?php echo $this->Html->Url('/assets/images/svg/cross-out.svg', true) ?>" alt="">
    </a>
    <div class="fd_modal_title">
        <img src="<?php echo $this->Html->Url('/assets/images/logos/dimabel.png', true) ?>" alt="Dimabel">
    </div>
    <div class="fd_modal_content">
        <p>
            Preencha os dados abaixo para continuar
        </p>

        <?php echo $this->Form->create('Carta', array('class' => 'fd_modal_form form-validate', 'url' => Router::url(null, true), 'type' => 'POST')) ?>
            <div class="fd_key">
                <?php echo $this->Form->hidden('fd', array('value' => '')) ?>
            </div>
            <div class="fd_key">
                <?php echo $this->Form->hidden('carta_id', array('value' => '', 'id' => 'carta-id')) ?>
            </div>
            <div class="form-group fd_group">
                <label class="fd_label_modal" id="lbl_nome">nome</label>
                <?php
                echo $this->Form->text('nome',
                    array(
                        'class' => 'form-control fd_input_modal',
                        'required',
                        'data-rule-required' => 'true',
                        'data-msg-required' => 'Campo obrigatório',
                        'data-rule-maxlength' => 30,
                        'data-msg-maxlength' => 'Preencha o campo corretamente',
                        'aria-required' => 'true',
                        'autocomplete' => 'off'
                    )
                )
                ?>
            </div>
            <div class="form-group fd_group">
                <label class="fd_label_modal" id="lbl_telefone">telefone</label>
                <?php
                echo $this->Form->text('telefone',
                    array(
                        'class' => 'form-control fd_input_modal fd_phone_mask',
                        'pattern' => '([\(]\d{2}[\)]) (\d{5}[\-]\d{4}|\d{4}[\-]\d{4})',
                        'required',
                        'data-rule-required' => 'true',
                        'data-msg-required' => 'Campo obrigatório',
                        'aria-required' => 'true',
                        'autocomplete' => 'off'
                    )
                )
                ?>
            </div>
            <div class="form-group fd_group">
                <label class="fd_label_modal" id="lbl_email">e-mail</label>
                <?php
                echo $this->Form->text('email',
                    array(
                        'class' => 'form-control fd_input_modal',
                        'pattern' => '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$',
                        'required',
                        'data-rule-required' => 'true',
                        'data-msg-required' => 'Campo obrigatório',
                        'aria-required' => 'true',
                        'autocomplete' => 'off'
                    )
                )
                ?>
            </div>
            <div class="form-group fd_group">
                <label class="fd_label_modal" id="lbl_cpf">preencha seu cpf</label>
                <?php
                echo $this->Form->text('cpf',
                    array(
                        'class' => 'form-control fd_input_modal fd_cpf',
                        'pattern' => '\d{3}\.\d{3}\.\d{3}-\d{2}',
                        'required',
                        'data-rule-required' => 'true',
                        'data-msg-required' => 'Campo obrigatório',
                        'aria-required' => 'true',
                        'autocomplete' => 'off'
                    )
                )
                ?>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn fd_button_orange pull-right">CONTRATAR</button>
                </div>
            </div>
        <?php echo $this->Form->end() ?>
    </div>
</div>