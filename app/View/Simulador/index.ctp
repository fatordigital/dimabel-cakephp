<?php echo $this->Element('site/header_interna') ?>
<section class="fd_page_content fd_page_simulation">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-xs-offset-0 col-md-10 col-md-offset-1">
                <h2 class="text-center">
                    <strong><?php echo $request_data['nome'] ;?></strong>, separamos as melhores opções para o seu consórcio de <br> <span><small>R$ </small><?php echo $unidade; ?>,<small><?php echo $decimal ;?></small></span>
                </h2>
            </div><!-- end of /.cols -->
        </div><!-- end of /.row -->
    </div><!-- end of /.container -->


    <div class="container fd_options">
        <div class="row">
            <?php foreach ($consorcios as $consorcio) { ?>
                <form action="<?php echo $this->Consorcio->Url('/', true) ?>" method="POST" id="contratar-<?php echo $consorcio['Consorcio']['slug'] ?>">
                    <input type="hidden" name="consorcio" value="<?php echo $consorcio['Consorcio']['slug'] ?>" />
                    <input type="hidden" name="valor" value="<?php echo $value ?>" />
                    <input type="hidden" name="categoria" value="<?php echo $request_data['simulation_type'] ?>" />
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="fd_item<?php echo $consorcio['Consorcio']['mais_contratado'] ? ' has-flag' : '' ?>">
                            <div class="fd_item_topo <?php echo $consorcio['Consorcio']['class'] ?>">
                                <img src="<?php echo $consorcio['Consorcio']['full_imagem'] ?>" alt="<?php echo $consorcio['Consorcio']['nome'] ?>">
                                <?php echo (!empty($consorcio['Consorcio']['legenda'])) ? $consorcio['Consorcio']['legenda'] : '' ?>
                            </div>

                            <?php if ($consorcio['Consorcio'][$apos_valor] > 0 &&  $consorcio['Consorcio'][$apos_valor] < $value && $consorcio['Consorcio'][$parcelas_apos] != '') { ?>
                                <?php $checked = true; ?>
                                <?php foreach (json_decode($consorcio['Consorcio'][$parcelas_apos], true) as $i => $apos_valor_parcela) { ?>
                                    <div class="fd_radio">
                                        <input id="fd_radio_caixa-<?php echo $consorcio['Consorcio']['slug'] ?>-<?php echo $apos_valor_parcela ?>" value="<?php echo $apos_valor_parcela; ?>" <?php echo $i == 0 ? 'checked' : '' ?> name="parcela" type="radio" />
                                        <label for="fd_radio_caixa-<?php echo $consorcio['Consorcio']['slug'] ?>-<?php echo $apos_valor_parcela ?>" class="fd_radio_label">
                                    <span>
                                        <strong><?php echo $apos_valor_parcela ?> PARCELAS</strong><br />
                                        <?php if ($consorcio['Consorcio']['meia_parcela'] == 1) { ?>
                                            R$ <?php echo $this->String->bcoToMoeda($this->Consorcio->ParcelaConsorcio($value, $apos_valor_parcela, $consorcio['Consorcio']['slug']) / 2) ?>
                                        <?php } else { ?>
                                            R$ <?php echo $this->String->bcoToMoeda($this->Consorcio->ParcelaConsorcio($value, $apos_valor_parcela, $consorcio['Consorcio']['slug'])) ?>
                                        <?php } ?>
                                    </span>
                                        </label>
                                    </div><!-- end of /.fd_radio -->
                                <?php } ?>
                            <?php } ?>

                            <?php foreach ($consorcio['Consorcio'][$parcela_segmento] != '' ? json_decode($consorcio['Consorcio'][$parcela_segmento], true) : $parcela_disponiveis as $i => $parcela) { ?>
                                <div class="fd_radio">
                                    <input id="fd_radio_caixa-<?php echo $consorcio['Consorcio']['slug'] ?>-<?php echo $i ?>" value="<?php echo $parcela; ?>" name="parcela" type="radio" <?php echo !isset($checked) && $i == 0 ? 'checked' : '' ?> />
                                    <label for="fd_radio_caixa-<?php echo $consorcio['Consorcio']['slug'] ?>-<?php echo $i ?>" class="fd_radio_label">
                                    <span>
                                        <strong><?php echo $parcela ?> PARCELAS</strong><br />
                                        <?php if ($consorcio['Consorcio']['meia_parcela'] == 1) { ?>
                                            R$ <?php echo $this->String->bcoToMoeda($this->Consorcio->ParcelaConsorcio($value, $parcela, $consorcio['Consorcio']['slug']) / 2) ?>
                                        <?php } else { ?>
                                            R$ <?php echo $this->String->bcoToMoeda($this->Consorcio->ParcelaConsorcio($value, $parcela, $consorcio['Consorcio']['slug'])) ?>
                                        <?php } ?>
                                    </span>
                                    </label>
                                </div><!-- end of /.fd_radio -->
                            <?php } ?>

                            <a href="javascript:void(0);" class="btn fd_button_warning"
                               data-simulador='<?php echo json_encode(array(
                                   'nome' => $consorcio['Consorcio']['nome'],
                                   'slug' => $consorcio['Consorcio']['slug'],
                                   'imagem' => $consorcio['Consorcio']['full_imagem'],
                                   'class' => $consorcio['Consorcio']['class_modal'],
                                   'preco' => $this->String->bcoToMoeda($value))) ?>'
                               data-modal="<?php echo $consorcio['Consorcio']['slug'] ?>">contratar</a>

                        </div><!-- end of /.fd_item -->
                    </div><!-- end of /.cols -->
                </form>
            <?php } ?>
        </div><!-- end of /.row -->
    </div><!-- end of /.container -->

    <div class="container">
        <hr class="fd_hr">
        <div class="row">
            <div class="col-xs-12">
                <h3 class="text-center">
                    Quer fazer uma nova simulação? <a href="<?php echo $this->Consorcio->Url('/', true) ?>" class="fd_link">Clique aqui!</a>
                </h3>
            </div>
        </div><!-- end of /.row -->
    </div><!-- end of /.container -->
</section><!-- end of /.fd_page_content.fd_page_simulation -->


<?php echo $this->Element('modal/contratar', array('imovel' => $this->Consorcio->Url('/simulacao-contratar', true))) ?>