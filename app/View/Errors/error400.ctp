<?php
$this->assign('class_body', ' fd_has_error');
?>
<section class="error-page">
    <div class="container-flex">
        <div class="container">
            <h5>
                PÁGINA NÃO<br />
                <strong>ENCONTRADA</strong>
            </h5>
            <h6>
                O LINK QUE VOCÊ CLICOU PODE ESTAR INDISPONÍVEL OU A PÁGINA PODE TER SIDO REMOVIDA.
            </h6>
            <p>
                VISITE A <a href="<?php echo $this->Html->Url('/', true) ?>">PÁGINA INICIAL</a> OU ENTRE EM <a href="<?php echo $this->Html->Url('/fale-conosco', true) ?>" class="page-scroll">CONTATO</a> CONOSCO SOBRE O PROBLEMA
            </p>
        </div>
    </div>
</section>