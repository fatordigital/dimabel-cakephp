<?php echo $this->Element('site/header') ?>

<main class="fd_home">
    <section class="fd_page_content fd_inst">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-9 col-md-7">
                    <h2>
                        Por que escolher consórcio para adquirir seus bens?
                    </h2>
                    <p class="border-left">
                        Comprar um imóvel ou de um carro exige planejamento financeiro e nem sempre é fácil poupar uma quantia alta. Por isso, parcelar muitas vezes acaba sendo a melhor solução.
                        <br>
                        O consórcio possibilita que você pague, todo mês, um valor estabelecido previamente por uma empresa administradora.
                        <br>
                        No fim, você adquire uma carta de crédito no valor do bem pretendido. É uma ótima alternativa ao financiamento.
                        <br>
                        Uma das vantagens é que, mensalmente, é realizado um sorteio entre os consorciados e, se contemplado, você ganha a oportunidade de adquirir sua carta de crédito no momento em que é sorteado, não precisando aguardar até o fim do consórcio.
                        <br>
                        Se interessou? Faça uma cotação e realize o seu sonho!
                    </p>
                    <div class="fd_flex_column">
                        <div class="fd_fake_button" style="cursor: pointer;">
                            <figure>
                                <img src="<?php echo $this->Html->Url('/assets/images/icons/car-icon.png', true) ?>" alt="Ícone automóvel" />
                            </figure>
                            <span onclick="window.location.href = '<?php echo $this->Html->Url('/produtos', true) ?>'">
                                AUTOMÓVEL
                            </span>
                        </div><!-- end of /.fd_fake_button -->
                        <div class="fd_fake_button" style="cursor: pointer;">
                            <figure>
                                <img src="<?php echo $this->Html->Url('/assets/images/icons/building-icon.png', true) ?>" alt="Ícone imóvel" />
                            </figure>
                            <span onclick="window.location.href = '<?php echo $this->Html->Url('/produtos', true) ?>'">
                                IMÓVEL
                            </span>
                        </div><!-- end of /.fd_fake_button -->
                    </div><!-- end of /.fd_flex_column -->
                </div><!-- end of /.cols -->

                <div class="col-xs-12 col-sm-3 col-md-5 hidden-xs">
                    <figure>
                        <img src="<?php echo $this->Html->Url('/assets/images/backgrounds/consorcio.png', true) ?>" alt="Imagem ilustrativa de carro" />
                    </figure>
                </div><!-- end of /.cols -->
            </div><!-- end of /.row -->
        </div><!-- end of /.container -->
    </section><!-- end of /.fd_page_content.fd_inst -->

    <section class="fd_page_content fd_home_marcas">

        <?php if (isset($consorcios) && $this->Consorcio->MostrarMarcas()) { ?>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 fd_flex fd_eq">
                        <h2>As marcas que trabalhamos:</h2>
                    </div><!-- end of /.cols -->
                    <div class="col-xs-12 col-sm-7 col-md-7 fd_flex_column no-padding fd_eq">
                        <?php foreach ($consorcios as $consorcio) { ?>
                            <a href="<?php echo $this->Html->Url('/' . ($consorcio['Consorcio']['principal'] != 1 ? $consorcio['Consorcio']['slug'] : '')) ?>">
                                <figure>
                                    <img src="<?php echo $consorcio['Consorcio']['full_imagem_circular'] ?>"
                                         alt="<?php echo $consorcio['Consorcio']['nome'] ?>" />
                                </figure>
                            </a>
                        <?php } ?>
                    </div><!-- end of /.cols -->
                </div><!-- end of /.row -->
            </div><!-- end of /.container -->
        <?php } ?>

        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-5 col-md-5 fd_flex fd_eq">
                    <h2>Os benefícios que trazemos:</h2>
                </div><!-- end of /.cols.fd_flex.fd_eq -->
                <div class="col-xs-12 col-sm-7 col-md-7 no-padding fd_eq">
                    <div class="fd_item">
                        sem juros
                    </div>
                    <div class="fd_item">
                        sem comprovação de renda
                    </div>
                    <div class="fd_item">
                        sem entrada
                    </div>
                    <div class="fd_item">
                        diversidade de planos
                    </div>
                    <div class="fd_item">
                        taxas mais acessíveis
                    </div>
                    <div class="fd_item">
                        atendimento pós-venda
                    </div>
                    <div class="fd_item">
                        consultoria especializada
                    </div>
                    <a href="javacript:void(0)" class="btn fd_item">
                        simulação online
                    </a>
                </div><!-- end of /.cols.no-padding.fd_eq -->
            </div><!-- end of /.row -->
        </div><!-- end of /.container -->

    </section><!-- end of /.fd_page_content.fd_home_marcas -->

    <?php echo $this->Element('site/form_newsletter') ?>

    <?php
        if ($this->Consorcio->MostrarVeiculos()) {
            echo $this->Element('site/vitrine', array('carros' => $this->Consorcio->Vitrine()));
        }
    ?>

    <?php if(isset($posts) && $posts) { ?>
    <section class="fd_page_content fd_home_blog">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h2>Blog Dimabel</h2>
                    <h5>Acompanhe as dicas sobre consórcios</h5>
                </div>
            </div>
            <div class="row">
                <?php foreach ($posts as $post) { ?>
                    <div class="col-xs-12 col-sm-6">
                        <article>
                            <a href="<?php echo '/blog/'. $post['wp_posts']['post_name']; ?>">
                                <figure>
                                    <img src="<?php echo str_replace('http://', (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://'), $this->String->image($post['wp_posts']['ID'], 'guid')); ?>" alt="Qual a diferença entre os lances de um consórcio?">
                                </figure>
                            </a>
                            <div class="fd_eq">
                                <h3>
                                    <a href="<?php echo '/blog/'. $post['wp_posts']['post_name']; ?>">
                                        <?php echo $post['wp_posts']['post_title']; ?>
                                    </a>
                                </h3>
                                <p>
                                    <a href="<?php echo '/blog/'. $post['wp_posts']['post_name']; ?>">
                                        <?php echo substr(strip_tags($post['wp_posts']['post_content']), 0, 180); ?>...
                                    </a>
                                </p>
                            </div>
                            <a href="<?php echo '/blog/'. $post['wp_posts']['post_name']; ?>" class="btn fd_button_orange">leia mais</a>
                        </article>
                    </div>
                <?php } ?>
            </div>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <a href="<?php echo $this->Html->Url('/blog');?>" class="btn fd_btn_orange">Veja todos os posts</a>
                </div>
            </div>
        </div>
    </section>
    <?php } ?>

    <?php echo $this->Element('site/contato') ?>

</main>