<?php if(Configure::read("Site.Google.GTM") != ""): ?>
	<script type="text/javascript">					        	
        $(document).ready(function(){
    		dataLayer.push({
				'event':'VirtualPageview',
				'sendPageview':'<?php echo '/goal/' . $goal_tipo . '/' . $goal_objetivo ?>'
			});				                
        });
    </script>
<?php elseif(Configure::read("Site.Google.Analytics") != ""): ?>
	 <script type="text/javascript">					        	
        $(document).ready(function(){
    		var goal = '<?php echo '/goal/' . $goal_tipo . '/' . $goal_objetivo ?>';
        	ga('send','pageview', goal);			                
        });
    </script>
<?php endIf; ?>	