<section class="fd_page_content fd_manual">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-5 col-md-3 col-lg-4 hidden-xs">
                <figure>
                    <img src="<?php echo $this->Html->Url('/assets/images/backgrounds/notebook.png', true) ?>" alt="Imagem exemplo dimabel telefone" />
                </figure>
            </div>
            <div class="col-xs-12 col-sm-7 col-md-9 col-lg-8">
                <h2>Assine nossa newsletter e receba nossas novidades sobre consórcios.</h2>
                <?php /*<p>
                    Como escolher o melhor consórcio? Como aumentar as chances de ser contemplado? Qual o melhor lance? Separamos um guia com diversas dicas para você obter o máximo de benefícios em seu próximo consórcio!
                </p>*/ ;?>
                <form action="<?php echo $this->Consorcio->Url('/newsletter', true) ?>" method="POST" class="form-validate" id="form-newsletter">
                    <div class="form-group fd_group">
                        <input
                            type="text"
                            name="nome"
                            class="form-control fd_input"
                            pattern="([A-zÀ-ž\s]){1,}"
                            required="required"
                            data-rule-required="true"
                            data-msg-required="Preencha o campo corretamente"
                            aria-required="true"
                            autocomplete="off"
                        />
                        <label for="" class="fd_label">NOME</label>
                    </div>
                    <div class="form-group fd_group">
                        <input
                            type="text"
                            name="email"
                            class="form-control fd_input"
                            pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
                            required="required"
                            data-rule-required="true"
                            data-rule-email="true"
                            data-msg-required="Preencha o campo corretamente"
                            data-msg-email="Preencha o campo corretamente"
                            aria-required="true"
                            autocomplete="off"

                        />
                        <label for="" class="fd_label">E-MAIL</label>
                    </div>
                    <input type="hidden" class="fd_key">
                    <button type="submit" class="btn fd_button_warning">RECEBER</button>
                </form>
            </div>
        </div>
    </div>
</section>