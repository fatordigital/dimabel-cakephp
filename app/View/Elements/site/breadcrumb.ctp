<?php if(isset($breadcrumbs) && is_array($breadcrumbs) && count($breadcrumbs) > 0): ?>
    <ol class="breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/#">
        <li typeof="v:Breadcrumb">
            <a href="<?php echo $this->Html->Url('/', true); ?>" title="Home" rel="v:url" property="v:title">Home</a>
        </li>
        <?php foreach ($breadcrumbs as $k => $bread): ?>
            <?php if ($bread['url'] != "javascript:void(0);"): ?>
                <?php
                $url = '';
                if(is_array($bread['url'])){
                    $url = $this->Html->Url($bread['url']);
                }else{
                    $url = $this->Html->Url('/').$bread['url'];
                }
                ?>
                <li typeof="v:Breadcrumb" <?php echo (isset($bread['active']) && $bread['active'] == true) ? ' class="active"' : ''; ?>>
                    <a href="<?php echo $url; ?>" title="<?php echo $bread['nome']; ?>" rel="v:url" property="v:title"> <?php echo $bread['nome']; ?></a>
                </li>
            <?php else: ?>
                <li typeof="v:Breadcrumb" <?php echo (isset($bread['active']) && $bread['active'] == true) ? ' class="active"' : ''; ?>>
                    <a href="javascript:void(0);" title="<?php echo $bread['nome']; ?>" rel="v:url" property="v:title"> <?php echo $bread['nome']; ?></a>
                </li>
            <?php endif; ?>
        <?php endforeach; ?>
    </ol>
<?php endif; ?>