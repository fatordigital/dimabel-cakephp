<header class="fd_header_marcas" style="background-image: url('assets/images/backgrounds/header-marcas.jpg')">

    <div class="fd_header_overlay">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-7">
                    <h1>
                        <?php echo $title; ?>
                    </h1>
                    <?php
                        if (isset($marca['Marca']['legenda'])) {
                            echo $marca['Marca']['legenda'];
                        }
                    ?>
                </div>
                <div class="col-xs-12 col-sm-6 col-sm-offset-0 col-md-4 col-md-offset-1">
                    <?php echo $this->Form->create(array('url' => $form_url, 'id' => 'DuvidaOutroModelo')) ?>
                        <div class="fd_form_header">
                            Dúvidas? Procurando outro modelo?
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group fd_group">
                                    <input name="nome" type="text" class="form-control fd_input" required />
                                    <label for="" class="fd_label">nome</label>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group fd_group">
                                    <input name="telefone" type="text" class="form-control fd_input fd_phone_mask" required />
                                    <label for="" class="fd_label">telefone</label>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group fd_group">
                                    <input name="email" type="text" class="form-control fd_input" required />
                                    <label for="" class="fd_label">e-mail</label>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group fd_group">
                                    <input name="valor-consorcio" type="text" class="form-control fd_input fd_money" required />
                                    <label for="" class="fd_label">valor aproximado do consórcio</label>
                                </div>
                                <div class="form-group fd_group">
                                    <input name="mensagem" type="text" class="form-control fd_input" maxlength="120" required />
                                    <label for="" class="fd_label">mensagem</label>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <button type="submit" class="btn fd_button_orange pull-right">
                                    ENVIAR MENSAGEM
                                </button>
                            </div>
                        </div>
                    <?php echo $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>

</header>