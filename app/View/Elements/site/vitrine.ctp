<?php if (isset($carros)) { ?>
    <section class="fd_page_content fd_vitrine">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <figure>
                        <img src="<?php echo $this->Html->Url('/assets/images/svg/sports-car.svg', true) ?>" alt="Ícone de carro" class="img-responsive center-block fd_its_svg" />
                    </figure>
                    <h2>
                        Buscando o carro dos sonhos?
                    </h2>
                    <p class="clr_orange">
                        Na Dimabel você pode encontrar o consórcio ideal para adquirir aquele veículo sonhado. Confira abaixo o que nossos clientes mais tem comprado:
                    </p>
                </div>
            </div>
            <div class="row">
                <?php foreach ($carros as $carro) { ?>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <a href="<?php echo $this->Consorcio->Url('/automoveis/' . $carro['Marca']['slug'] . '/' . $carro['Carro']['slug'], true) ?>" class="fd_item">
                            <?php if ($carro['Carro']['mais_vendido'] == 1) { ?>
                                <div class="fd_flag">mais vendido</div>
                            <?php } ?>
                            <h3><?php echo $carro['Carro']['nome'] ?></h3>

                            <figure>
                                <img src="<?php echo $carro['Carro']['full_imagem'] ?>"
                                     width="195"
                                     alt="<?php echo $carro['Carro']['nome'] ?>">
                            </figure>
                            <p>
                                R$ <?php echo $this->String->bcoToMoeda($this->Consorcio->Parcelas($carro['Carro']['preco'], 'taxa_carro', 'parcela_carro')) ?>
                            </p>
                        </a>
                    </div>
                <?php } ?>
            </div>
            <hr>
            <?php if (isset($marcas)) { ?>
                <div class="row">
                    <div class="col-xs-12 col-sm-1 col-md-1">
                        <figure class="fd_img">
                            <img src="<?php echo $this->Html->Url('/assets/images/svg/sports-car.svg', true) ?>" alt="Ícone de carro" class="img-responsive center-block fd_its_svg" />
                        </figure>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-2 no-padding">
                        <div>
                            <h2>Busque por marca</h2>
                            <p>
                                Clique nas marcas para visualizar os valores de parcela de consórcio para cada modelo!
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-sm-9 col-md-9">
                        <div class="fd_carousel owl-carousel owl-theme">
                            <?php foreach ($marcas as $marca) { ?>
                                <?php if ($marca['Carro']) { ?>
                                    <a href="<?php echo $this->Consorcio->Url('/automoveis/' . $marca['Marca']['slug'], true) ?>" class="item">
                                        <figure>
                                            <img src="<?php echo $marca['Marca']['full_imagem'] ?>"
                                                 alt="<?php echo $marca['Marca']['nome'] ?>"/>
                                        </figure>
                                        <span><?php echo $marca['Marca']['nome'] ?></span>
                                    </a>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </section>
<?php } ?>
