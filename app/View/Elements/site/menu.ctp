<nav class="navbar navbar-default">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle fd_btn_menu">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo $this->Html->Url('/') ?>">
                <img src="<?php echo $this->Consorcio->TemplateLogo() ?>" alt="Dimabel Consórcios"/>
            </a>
        </div>

        <div class="fd_menu">
            <ul class="nav navbar-nav">
                <li class="fd_logo_mobile">
                    <a href="/">
                        <img src="<?php echo $this->Html->Url('/assets/images/logos/dimabel.png', true) ?>" alt="Dimabel Consórcios"/>
                    </a>
                </li>

                <?php if (true) { ?>
                    <?php foreach($menus_for_layout['site_institucional']['threaded'] as $l) { ?>
<!--                        --><?php //if ($l['Link']['seo_url'] == 'produtos' && isset($marcas)) { ?>
<!--                            <li class="fd_has_submenu">-->
<!--                                <a href="javascript:void(0)">marcas de veículos <i class="fa fa-angle-down"></i></a>-->
<!--                                <ul>-->
<!--                                    --><?php //foreach ($marcas as $marca) { ?>
<!--                                        --><?php //if ($marca['Carro']) { ?>
<!--                                            <li>-->
<!--                                                <a href="--><?php //echo $this->Consorcio->Url('/automoveis/' . $marca['Marca']['slug'], true) ?><!--" class="item">-->
<!--                                                    <figure>-->
<!--                                                        <img src="--><?php //echo $marca['Marca']['full_imagem'] ?><!--"-->
<!--                                                             alt="--><?php //echo $marca['Marca']['nome'] ?><!--" style="max-width: 45px" />-->
<!--                                                    </figure>-->
<!--                                                    <span>--><?php //echo $marca['Marca']['nome'] ?><!--</span>-->
<!--                                                </a>-->
<!--                                            </li>-->
<!--                                        --><?php //} ?>
<!--                                    --><?php //} ?>
<!--                                </ul>-->
<!--                            </li>-->
<!--                        --><?php //} ?>
                        <li>
                            <a href="<?php echo $this->Consorcio->Url('/' . $l['Link']['seo_url'], true) ?>" class="page-scroll">
                                <?php echo $l['Link']['title'] ?>
                            </a>
                        </li>
                    <?php } ?>
                <?php } else { ?>
<!--                    --><?php //if ($this->params->controller == 'marca' && isset($marcas)) { ?>
<!--                        <li class="fd_has_submenu">-->
<!--                            <a href="javascript:void(0)">marcas de veículos <i class="fa fa-angle-down"></i></a>-->
<!--                            <ul>-->
<!--                                --><?php //foreach ($marcas as $marca) { ?>
<!--                                    --><?php //if ($marca['Carro']) { ?>
<!--                                        <li>-->
<!--                                            <a href="--><?php //echo $this->Consorcio->Url('/automoveis/' . $marca['Marca']['slug'], true) ?><!--" class="item">-->
<!--                                                <figure>-->
<!--                                                    <img src="--><?php //echo $marca['Marca']['full_imagem'] ?><!--"-->
<!--                                                         alt="--><?php //echo $marca['Marca']['nome'] ?><!--" style="max-width: 45px" />-->
<!--                                                </figure>-->
<!--                                                <span>--><?php //echo $marca['Marca']['nome'] ?><!--</span>-->
<!--                                            </a>-->
<!--                                        </li>-->
<!--                                    --><?php //} ?>
<!--                                --><?php //} ?>
<!--                            </ul>-->
<!--                        </li>-->
<!--                    --><?php //} ?>
                <?php } ?>

            </ul>
        </div><!-- /.navbar-collapse -->

        <div class="fd_phone_item pull-right">
            <span>dúvidas? <strong>central de vendas dimabel</strong></span>
            <div class="fd_phone_item_content">
                <figure>
                    <img src="<?php echo $this->Html->Url('/assets/images/icons/phone_small.png', true) ?>" alt="Ligue para">
                </figure>
                <h5><?php echo Configure::read('Site.central_de_vendas') ?></h5>
            </div>
        </div>
    </div><!-- /.container-fluid -->
    <div class="fd_menu_dropback"></div>
</nav>