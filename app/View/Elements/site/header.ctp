<header>

    <div class="fd_header_bg fd_first fd_active" style="background-image: url(<?php echo $this->Html->Url('/assets/images/backgrounds/automotive.jpg', true) ?>)"></div>
    <div class="fd_header_bg fd_last" style="background-image: url(<?php echo $this->Html->Url('/assets/images/backgrounds/imovel.jpg', true) ?>)"></div>

    <div class="fd_header_overlay">

        <div class="fd_flex_column">
            <h1 class="tlt">
                Encontre o melhor consórcio de automóvel.
            </h1>
            <?php echo $this->Consorcio->CircleLogo() ?>
        </div>

        <h4>
            A Dimabel é uma plataforma online de consórcios que ajudará você a encontrar a melhor opção para realizar o seu sonho.
        </h4>


        <form action="<?php echo $this->Consorcio->Url('/simulacao') ?>" method="POST" class="fd_form_header_default form-validate">
            <div class="fd_row_1" style="border-right: 1px solid #CECEBF">
                <div class="fd_text_1">
                    <div class="form-group fd_group">
                        <label class="label">ESTOU BUSCANDO O CONSÓRCIO DE UM</label>
                        <select name="simulation_type" class="form-control fd_input">
                            <option value="automovel">Automóvel</option>
                            <option value="imovel">Imóvel</option>
                        </select>
                    </div>
                </div>
                <div class="fd_text_1 valor-de">
                    <div class="form-group fd_group">
                        <label class="label">NO VALOR DE</label>
                        <input type="text" class="form-control fd_input fd_money" name="value" placeholder="No valor de" required data-msg-required="Obrigatório" data-rule-valor="true" />
                        <label id="valueError" class="errorCustom hide">:error</label>
                    </div>
                </div>
            </div>
            <div class="fd_row_2">
                <div class="fd_text_6">
                    <div class="form-group fd_group">
                        <label>MEU NOME</label>
                        <input type="text" name="nome" class="form-control fd_input" placeholder="Prencha seu nome" required data-msg-required="Obrigatório" />
                    </div>
                </div>
                <div class="fd_text_6">
                    <div class="form-group fd_group">
                        <label>MEU E-MAIL</label>
                        <input type="text" name="email" class="form-control fd_input" placeholder="Preencha seu e-mail" data-rule-email="true" data-msg-email="Utilize um e-mail válido" required data-msg-required="Obrigatório" />
                    </div>
                </div>
                <div class="fd_text_6">
                    <div class="form-group fd_group">
                        <label>MEU TELEFONE</label>
                        <input type="text" name="telefone" class="form-control fd_input fd_phone_mask" placeholder="Preencha seu telefone" required data-msg-required="Obrigatório" />
                    </div>
                </div>
                <div class="fd_text_6" style="padding-top: 25px;margin-top: 0;">
                    <button type="submit" class="receber_simulacao">
                        RECEBER SIMULAÇÃO!
                    </button>
                </div>
            </div>
            <input type="hidden" class="fd_key" />
        </form>


        <figure class="fd_header_mouse">
            <img src="<?php echo $this->Html->Url('/assets/images/icons/mouse.png', true) ?>" alt="Rolar a página" />
        </figure>

    </div>


    <?php echo $this->Element('site/navbar-phone') ?>

</header>