<footer>

    <section class="fd_footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <div class="fd_item">
                        <figure>
                            <img src="<?php echo $this->Html->Url('/assets/images/logos/dimabel.png', true) ?>" alt="Logo <?php echo Configure::read('Site.Seo.Title') ?>" />
                            <figcaption>
                                CNPJ: <?php echo Configure::read('Site.CNPJ') ?> - <?php echo Configure::read('Site.Nome') ?>
                            </figcaption>
                        </figure>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="fd_item">
                        <p>
                            Dúvidas? Sugestões? Reclamações? Converse<br /> com nossa equipe de atendimento!
                        </p>
                        <figure>
                            <img src="<?php echo $this->Html->Url('/assets/images/icons/phone.png', true) ?>" alt="Ícone de telefone" />
                        </figure>
                        <h5><?php echo Configure::read('Site.SAC') ?></h5>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="fd_item">
                        <figure>
                            <img src="<?php echo $this->Html->Url('/assets/images/google-safe-browser.png', true) ?>" alt="Certificado de segurança Google" />
                        </figure>
                        <p>
                            Verifique a segurança desse site através do Google Safe Browsing, clicando no selo ao lado.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- end of /.fd_footer_bottom -->

    <section class="fd_signature_content">
        <div class="container">
            <a href="https://www.fatordigital.com.br/" target="_blank" class="pull-right" title="Fator Digital">
                Fator Digital
                <span id="fd_signature" style="width: 200px; height: 50px; margin-top: -5px; float: right; margin-right: 20px;"></span>
            </a>
        </div>
    </section>

</footer>