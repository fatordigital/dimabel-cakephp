<section class="fd_page_content fd_contato">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 fd_eq">
                <div class="fd_content">
                    <div class="fd_item">
                        <figure>
                            <img src="<?php echo $this->Html->Url('/assets/images/svg/pin.svg', true) ?>" alt="Ponto de indicação no mapa" class="fd_its_svg" />
                        </figure>
                        <div class="fd_content">
                            <p>
                                Quer adquirir o consórcio pessoalmente? Visite nossa sede!
                            </p>
                            <address>
                                Av. João Correa, 503, sala 2<br />
                                <strong>São Leopoldo/RS</strong> - CEP 93040-035
                            </address>
                        </div>
                    </div>
                    <div class="fd_item">
                        <figure>
                            <img src="<?php echo $this->Html->Url('/assets/images/icons/phone.png', true) ?>" alt="Ícone de telefone" />
                        </figure>
                        <p>
                            Quer tirar todas as suas dúvidas sobre consórcios?<br />
                            <big><?php echo Configure::read('Site.SAC') ?></big>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-sm-6 fd_eq">
                <div class="fd_form">
                    <form action="<?php echo $this->Consorcio->Url('/contato', true) ?>" method="POST" class="form-validate" id="form-rodape-contato">
                        <h2>
                            Entre em contato com a Dimabel Consórcios
                        </h2>
                        <div class="form-group fd_group">
                            <input
                                type="text"
                                name="nome"
                                class="form-control fd_input_bottom"
                                pattern="([A-zÀ-ž\s]){1,}"
                                required="required"
                                data-rule-required="true"
                                data-msg-required="Preencha o campo corretamente"
                                data-rule-minlength="3"
                                data-msg-minlength="Preencha o campo corretamente"
                                data-rule-maxlength="30"
                                data-msg-maxlength="Preencha o campo corretamente"
                                aria-required="true"
                                autocomplete="off"
                            />
                            <label class="fd_label_bottom">Nome</label>
                        </div>
                        <div class="form-group fd_group">
                            <input
                                type="text"
                                name="email"
                                class="form-control fd_input_bottom"
                                pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
                                required="required"
                                data-rule-required="true"
                                data-rule-email="true"
                                data-msg-required="Preencha o campo corretamente"
                                data-msg-email="Preencha o campo corretamente"
                                aria-required="true"
                                autocomplete="off"
                            />
                            <label class="fd_label_bottom">E-mail</label>
                        </div>
                        <div class="form-group fd_group">
                            <input
                                type="text"
                                name="telefone"
                                class="form-control fd_input_bottom fd_phone_mask"
                                pattern="([\(]\d{2}[\)]) (\d{5}[\-]\d{4}|\d{4}[\-]\d{4})"
                                required="required"
                                data-rule-required="true"
                                data-rule-minlength="14"
                                data-rule-maxlength="15"
                                data-msg-required="Preencha o campo corretamente"
                                maxlength="15"
                                autocomplete="off"
                                aria-required="true"
                            />
                            <label class="fd_label_bottom">Telefone</label>
                        </div>
                        <div class="form-group fd_group">
                                <textarea
                                    type="text"
                                    name="mensagem"
                                    class="form-control fd_input_bottom"
                                    pattern="([A-zÀ-ž\s]){1,}"
                                    required="required"
                                    data-rule-required="true"
                                    data-msg-required="Preencha o campo corretamente"
                                    aria-required="true"
                                    autocomplete="off"
                                ></textarea>
                            <label class="fd_label_bottom">Mensagem</label>
                        </div>
                        <input type="hidden" class="fd_key">
                        <button type="submit" class="btn fd_button_white">enviar mensagem</button>
                        <div class="fd_form_message"></div>
                    </form>
                </div>
            </div>
        </div>
        <!--            <div id="mapa" class=""></div>-->
    </div>
</section>