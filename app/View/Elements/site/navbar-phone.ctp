<section class="fd_header_phone">
    <div class="left">
        <figure>
            <img src="<?php echo $this->Html->Url('/assets/images/logos/dimabel-small.png', true) ?>" alt="Dimabel">
        </figure>
        <p>
            QUER UM <span class="text-white">ATENDIMENTO</span> MAIS EXCLUSIVO?
        </p>
    </div>
    <div class="right">

        <div class="fd_item">
            <figure>
                <img src="<?php echo $this->Html->Url('/assets/images/icons/phone.png', true) ?>" alt="Ligue para a Dimabel">
            </figure>
            <p>
                <span>Ligue para nossa central de vendas</span>
                <strong>
                    <?php echo Configure::read('Site.central_de_vendas') ?>
                </strong>
            </p>
        </div>

        <div class="fd_item">

            <p>
                <span>
                    Marcas que trabalhamos
                </span>
            </p>
            <a href="">
                <figure>
                    <img src="<?php echo $this->Html->Url('/assets/images/marcas/caixa-consorcios-bar.jpg', true) ?>" alt="Caixa Consórcios" />
                </figure>
            </a>
            <a href="">
                <figure>
                    <img src="<?php echo $this->Html->Url('/assets/images/marcas/hs-consorcios-bar.jpg', true) ?>" alt="Dimabel whatsapp" />
                </figure>
            </a>
        </div>

        <?php /*
        <div class="fd_item">
            <figure>
                <img src="<?php echo $this->Html->Url('/assets/images/icons/whatsapp.png', true) ?>" alt="Dimabel whatsapp" />
            </figure>
            <p>
                <span>Envie um whats para nós</span>
                <strong>
                    <?php echo Configure::read('Site.WhatsApp') ?>
                </strong>
            </p>
        </div>
        */;?>

        <div class="fd_item chat-box">
            <figure>
                <img src="<?php echo $this->Html->Url('/assets/images/icons/chat_online.png') ?>" alt="Chat online com a Dimabel">
            </figure>
            <p>
                <span>Converse com a gente</span>
                <strong>
                    <small>Chat online</small>
                </strong>
            </p>
        </div>

    </div>
</section>