<script type="text/javascript">
    swal({
        type: 'error',
        title: '<?php echo $message ?>',
        showConfirmButton: false,
        timer: 2500
    });
</script>