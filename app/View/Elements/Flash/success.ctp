<script type="text/javascript">
    swal({
        type: 'success',
        title: '<?php echo $message; ?>',
        showConfirmButton: false,
        timer: 2500
    });
</script>