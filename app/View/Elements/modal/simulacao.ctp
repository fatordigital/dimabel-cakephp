<div class="fd_modal_dropback"></div>
<div class="fd_modal">
    <a href="javascript:void(0)" class="fd_modal_close">
        <img src="/assets/images/svg/cross-out.svg" alt="">
    </a>
    <div class="fd_modal_title">
        <img src="assets/images/logos/hs-consorcios.png" alt="HS Consórcios">
    </div>
    <div class="fd_modal_content">
        <p>
            Parabéns! Você está  prestes a conhecer a forma mais econômica de adquirir bens e/ou investimentos. A <span>HS Consórcios</span> terá o maior prazer em ter você como cliente!
        </p>
        <p>
            Preencha os dados abaixo e em instantes você receberá um e-mail de confirmação e o contato de um representante para efetivar sua contratação!
        </p>
        <form action="javascript:void(0)" method="POST" class="fd_modal_form form-validate">
            <div class="form-group fd_group">
                <label class="fd_label_modal" id="lbl_nome">nome</label>
                <input
                    type="text"
                    name="nome"
                    class="form-control fd_input_modal"
                    aria-describedby="lbl_nome"
                    pattern="([A-zÀ-ž\s]){3,}"
                    required="required"
                    data-rule-required="true"
                    data-msg-required="Preencha o campo corretamente"
                    data-rule-maxlength="30"
                    data-msg-maxlength="Preencha o campo corretamente"
                    aria-required="true"
                    autocomplete="off"
                    value=""
                >
            </div>
            <div class="form-group fd_group">
                <label class="fd_label_modal" id="lbl_telefone">telefone</label>
                <input
                    type="text"
                    name="telefone"
                    class="form-control fd_input_modal fd_phone_mask"
                    pattern="([\(]\d{2}[\)]) (\d{5}[\-]\d{4}|\d{4}[\-]\d{4})"
                    required="required"
                    data-rule-required="true"
                    data-rule-minlength="14"
                    data-rule-maxlength="15"
                    data-msg-required="Preencha o campo corretamente"
                    data-msg-minlength="Preencha o campo corretamente"
                    data-msg-maxlength="Preencha o campo corretamente"
                    maxlength="15"
                    autocomplete="off"
                    aria-required="true"
                    value=""
                >
            </div>
            <div class="form-group fd_group">
                <label class="fd_label_modal" id="lbl_email">e-mail</label>
                <input
                    type="text"
                    name="email"
                    class="form-control fd_input_modal"
                    aria-describedby="lbl_email"
                    pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
                    required="required"
                    data-rule-required="true"
                    data-rule-email="true"
                    data-msg-required="Preencha o campo corretamente"
                    data-msg-email="Preencha o campo corretamente"
                    aria-required="true"
                    autocomplete="off"
                    value=""
                >
            </div>
            <div class="form-group fd_group">
                <label class="fd_label_modal" id="lbl_cpf">preencha seu cpf</label>
                <input
                    type="text"
                    name="cpf"
                    class="form-control fd_input_modal fd_cpf"
                    aria-describedby="lbl_cpf"
                    pattern="\d{3}\.\d{3}\.\d{3}-\d{2}"
                    required="required"
                    data-rule-required="true"
                    data-rule-minlength="14"
                    data-rule-maxlength="14"
                    data-msg-required="Preencha o campo corretamente"
                    data-msg-minlength="Preencha o campo corretamente"
                    data-msg-maxlength="Preencha o campo corretamente"
                >
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <h6>
                        VALOR DA CARTA DE CRÉDITO <strong>R$ <?php echo $_GET['value'] ;?></strong>
                    </h6>
                </div>
                <div class="col-xs-12">
                    <button type="submit" class="btn fd_button_orange pull-right">CONTRATAR</button>
                </div>
            </div>
        </form>
    </div>
</div>