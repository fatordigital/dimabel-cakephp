<div class="fd_modal_dropback"></div>
<div class="fd_modal dimabel" style="min-height: auto;">
    <a href="javascript:void(0)" class="fd_modal_close">
        <img src="<?php echo $this->Html->Url('/assets/images/svg/cross-out.svg', true) ?>" alt="Fechar">
    </a>
    <div class="fd_modal_title caixa_consorcios">
        <img src="<?php echo $this->Html->Url('/assets/images/logos/dimabel.png', true) ?>" alt="Dimabel">
    </div>
    <div class="fd_modal_content">
        <p>
            Preencha os dados abaixo para continuar
        </p>
        <form action="<?php echo $this->Consorcio->Url('/simulacao', true) ?>" method="POST" class="fd_modal_form form-validate" id="contratar-modal-carro" onsubmit="fill_hiddens(this)">
            <div class="form-group fd_group">
                <label class="fd_label_modal" id="lbl_nome">nome</label>
                <input
                    type="text"
                    name="nome"
                    class="form-control fd_input_modal"
                    aria-describedby="lbl_nome"
                    pattern="([A-zÀ-ž\s]){3,}"
                    required="required"
                    data-rule-required="true"
                    data-msg-required="Preencha o campo corretamente"
                    data-rule-maxlength="30"
                    data-msg-maxlength="Preencha o campo corretamente"
                    aria-required="true"
                    autocomplete="off"
                    value="<?php echo isset($this->data['nome']) ? $this->data['nome'] : '' ?>"
                >
            </div>
            <div class="form-group fd_group">
                <label class="fd_label_modal" id="lbl_telefone">telefone</label>
                <input
                    type="text"
                    name="telefone"
                    class="form-control fd_input_modal fd_phone_mask"
                    pattern="([\(]\d{2}[\)]) (\d{5}[\-]\d{4}|\d{4}[\-]\d{4})"
                    required="required"
                    data-rule-required="true"
                    data-rule-minlength="14"
                    data-rule-maxlength="15"
                    data-msg-required="Preencha o campo corretamente"
                    data-msg-minlength="Preencha o campo corretamente"
                    data-msg-maxlength="Preencha o campo corretamente"
                    maxlength="15"
                    autocomplete="off"
                    aria-required="true"
                    value="<?php echo isset($this->data['telefone']) ? $this->data['telefone'] : '' ?>"
                >
            </div>
            <div class="form-group fd_group">
                <label class="fd_label_modal" id="lbl_email">e-mail</label>
                <input
                    type="text"
                    name="email"
                    class="form-control fd_input_modal"
                    aria-describedby="lbl_email"
                    pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
                    required="required"
                    data-rule-required="true"
                    data-rule-email="true"
                    data-msg-required="Preencha o campo corretamente"
                    data-msg-email="Preencha o campo corretamente"
                    aria-required="true"
                    autocomplete="off"
                    value="<?php echo isset($this->data['email']) ? $this->data['email'] : '' ?>"
                >
            </div>
            <div class="form-group fd_group">
                <label class="fd_label_modal" id="lbl_cpf">preencha seu cpf</label>
                <input
                    type="text"
                    name="cpf"
                    value="<?php echo isset($this->data['cpf']) ? $this->data['cpf'] : '' ?>"
                    class="form-control fd_input_modal fd_cpf"
                    aria-describedby="lbl_cpf"
                    pattern="\d{3}\.\d{3}\.\d{3}-\d{2}"
                    required="required"
                    data-rule-required="true"
                    data-rule-minlength="14"
                    data-rule-maxlength="14"
                    data-msg-required="Preencha o campo corretamente"
                    data-msg-minlength="Preencha o campo corretamente"
                    data-msg-maxlength="Preencha o campo corretamente"
                >
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn fd_button_orange pull-right">CONTINUAR</button>
                </div>
            </div>
        </form>
    </div>
</div>
