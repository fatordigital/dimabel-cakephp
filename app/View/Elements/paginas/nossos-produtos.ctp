<?php echo $this->Element('site/header_interna') ?>
<section class="fd_page_content fd_page_products">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-7 fd_eq fd_flex_column">
                <div class="fd_content">
                    <h2>
                        Consórcio de veículos
                    </h2>
                    <p>
                        Somos uma empresa de soluções financeiras focada na necessidade global do cliente, constituída por profissionais experientes e certificados, com forte atuação nas maiores instituições financeiras do país.
                    </p>
                    <p>
                        A Dimabel Consórcios remete a uma filosofia de como fazer bem as coisas, cativando o seu público e sempre procurando a excelência em seus serviços. Não nascemos para ser mais uma, nascemos para ser a melhor.
                    </p>
                    <p>
                        Por isso a Dimabel busca a excelência no atendimento através de profissionais qualificados e uma missão única entre as empresas de seu segmento, realizando um pós venda diferenciado e garantindo que os clientes superem suas expectativas ao contratar um consórcio conosco.
                    </p>
                    <a href="https://www.dimabel.com.br/blog/" target="_blank" class="btn fd_button_orange">
                        <img src="assets/images/icons/car-icon-invert.png"/>
                        LEIA EM NOSSO BLOG SOBRE CONSÓRCIO DE VEÍCULOS
                    </a>
                </div><!-- end of /.fd_content -->
            </div><!-- end of /.cols.fd_eq -->
            <div class="col-xs-12 col-sm-4 col-md-5 fd_eq">
                <figure>
                    <img src="assets/images/backgrounds/automotive-product.jpg" alt="Consórcio de veículos">
                </figure>
            </div><!-- end of /.cols.fd_eq -->
        </div><!-- end of /.row -->

        <div class="row">
            <div class="col-xs-12 col-xs-push-0 col-sm-8 col-sm-push-4 col-md-7 col-md-push-5 fd_eq fd_flex_column">
                <div class="fd_content">
                    <h2>
                        Consórcio Imobiliário
                    </h2>
                    <p>
                        Somos uma empresa de soluções financeiras focada na necessidade global do cliente, constituída por profissionais experientes e certificados, com forte atuação nas maiores instituições financeiras do país.
                    </p>
                    <p>
                        A Dimabel Consórcios remete a uma filosofia de como fazer bem as coisas, cativando o seu público e sempre procurando a excelência em seus serviços. Não nascemos para ser mais uma, nascemos para ser a melhor.
                    </p>
                    <p>
                        Por isso a Dimabel busca a excelência no atendimento através de profissionais qualificados e uma missão única entre as empresas de seu segmento, realizando um pós venda diferenciado e garantindo que os clientes superem suas expectativas ao contratar um consórcio conosco.
                    </p>
                    <a href="https://www.dimabel.com.br/blog/" target="_blankad" class="btn fd_button_orange">
                        <img src="assets/images/icons/building-icon-invert.png" />
                        LEIA EM NOSSO BLOG SOBRE CONSÓRCIO IMOBILIÁRIO
                    </a>
                </div><!-- end of /.fd_content -->
            </div><!-- end of /.cols.fd_eq -->
            <div class="col-xs-12 col-xs-pull-0 col-sm-4 col-sm-pull-8 col-md-5 col-md-pull-7 fd_eq">
                <figure>
                    <img src="assets/images/backgrounds/residence-product.jpg" alt="Consórcio Imobiliário">
                </figure>
            </div><!-- end of /.cols.fd_eq -->
        </div><!-- end of /.row -->
    </div><!-- end of /.container -->
</section>