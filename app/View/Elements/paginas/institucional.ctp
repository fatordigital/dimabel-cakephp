<?php echo $this->Element('site/header_interna') ?>
<section class="fd_page_content fd_page_institucional">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-7 fd_eq fd_content">
                <h2>Dimabel Consórcios</h2>
                <p>
                    Somos uma empresa de soluções financeiras focada na necessidade global do cliente, constituída por profissionais experientes e certificados, com forte atuação nas maiores instituições financeiras do país.
                </p>
                <p>
                    A Dimabel Consórcios remete a uma filosofia de como fazer bem as coisas, cativando o seu público e sempre procurando a excelência em seus serviços. Não nascemos para ser mais uma, nascemos para ser a melhor.
                </p>
                <p>
                    Por isso a Dimabel busca a excelência no atendimento através de profissionais qualificados e uma missão única entre as empresas de seu segmento, realizando um pós venda diferenciado e garantindo que os clientes superem suas expectativas ao contratar um consórcio conosco.
                </p>
            </div><!-- end of /.cols.fd_eq.fd_content -->
            <div class="col-xs-12 col-sm-5 fd_eq">
                <figure>
                    <img src="<?php echo $this->Html->Url('/assets/images/backgrounds/institucional.png', true) ?>" alt="Imagem ilustrativa Casa" />
                </figure>
            </div><!-- end of /.cols.fd_eq -->
        </div><!-- end of /.row -->
    </div><!-- end of /.container -->

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <h5>
                    Missão
                </h5>
                <p>
                    Ofertar crédito nas várias modalidades com as melhores condições e segurança, visando o equilíbrio financeiro e contribuindo para melhorar a qualidade de vida do cliente.
                </p>
            </div><!-- end of /.cols -->
            <div class="col-xs-12 col-sm-4">
                <h5>
                    Visão
                </h5>
                <p>
                    Planejamento financeiro acessível, com acompanhamento personalizado para alcançar seus objetivos.
                </p>
            </div><!-- end of /.cols -->
            <div class="col-xs-12 col-sm-4">
                <h5>
                    Valores
                </h5>
                <p>
                    Sempre valorizar e respeitar o cliente, Conseguir excelência com simplicidade, Integridade e confiança frente ao mercado.
                </p>
            </div><!-- end of /.cols -->
        </div><!-- end of /.row -->
    </div><!-- end of /.container -->

</section>

<?php echo $this->Element('site/form_newsletter') ?>
