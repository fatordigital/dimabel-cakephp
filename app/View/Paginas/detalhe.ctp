<?php

if (isset($pagina)) {
    if ($pagina['Pagina']['exibir_abas'] == 0) {
        if ($pagina['Aba']) {
            echo $this->assign('title', $title);
            echo str_replace('{CONTEUDO_DINAMICO}', $this->Element('paginas/' . $pagina['Aba'][0]['dinamico_elemento']), $pagina['Aba'][0]['conteudo']);
        }
    }
}