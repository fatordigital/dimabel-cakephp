<?php header("Content-Type: text/xml; charset=UTF-8"); ?>
<?php echo '<?'; ?>xml version="1.0" encoding="UTF-8"<?php echo '?'; ?>>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
	<url>
		<loc><?php echo Router::url('/', TRUE); ?></loc>
		<changefreq>daily</changefreq>
		<priority>1.0</priority>
	</url>
	<?php foreach($urls as $key => $url): ?>
		<url>
			<loc><?php echo Router::url('/'.$url['url'], TRUE); ?></loc>
			<?php if(!empty($url['lastmod'])): ?>
				<lastmod><?php echo $url['lastmod']; ?></lastmod>
			<?php endif; ?>
			<?php if(!empty($url['priority'])): ?>
				<priority><?php echo $url['priority']; ?></priority>
			<?php endif; ?>
			<?php if(!empty($url['changefreq'])): ?>
				<changefreq><?php echo $url['changefreq']; ?></changefreq>
			<?php endif; ?>
		</url>
	<?php endforeach; ?>
</urlset>