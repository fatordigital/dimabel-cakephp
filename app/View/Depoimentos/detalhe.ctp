<main>
	<section class="conteudo depoimento">
		<div class="container-fluid">
			<div class="row">
				<div class="box-img">
					<img src="<?php echo $this->Html->Url('/' . $depoimento['Depoimento']['thumb_path'] . '/' . $depoimento['Depoimento']['thumb_dir'] . '/1920_' . $depoimento['Depoimento']['thumb'], true); ?>" class="img-responsive">
					<div class="efeito"></div>
				</div>	
				<div class="col-xl-12 box-caption">
					<div class="col-lg-offset-1 col-md-offset-1 col-lg-6 col-md-6 col-xs-12">
						<h2>
							<?php echo str_replace(array('<br>','<br/>','<br />'), '', $depoimento['Depoimento']['depoimento_resumo']) ?>
						</h2>
						<p class="pull-right">
							<?php 
								$nome = explode(' ', $depoimento['Depoimento']['nome']);
								$firstname = $nome[0]; unset($nome[0]);								
								$lastname = implode(' ', $nome); unset($nome);
							?>
							<strong><?php echo $firstname ?></strong> <?php echo $lastname ?>
						</p>
						<div class="col-xs-12">
							<ul class="icons-depoimento">

								<?php if(isset($depoimento['Depoimento']['info_cursou']) && !empty($depoimento['Depoimento']['info_cursou'])): ?>
								<li>
									<img src="assets/images/icons/chapeu.png" class="img-responsive">
									<p><?php echo $depoimento['Depoimento']['info_cursou']; ?></p>
								</li>
								<?php endif; ?>

								<?php if(isset($depoimento['Depoimento']['info_formatura']) && !empty($depoimento['Depoimento']['info_formatura'])): ?>
								<li>
									<img src="assets/images/icons/calendario.png" class="img-responsive">
									<p><?php echo $depoimento['Depoimento']['info_formatura']; ?></p>
								</li>
								<?php endif; ?>

								<?php if(isset($depoimento['Depoimento']['info_trabalho']) && !empty($depoimento['Depoimento']['info_trabalho'])): ?>
								<li>
									<img src="assets/images/icons/predio.png" class="img-responsive">
									<p><?php echo $depoimento['Depoimento']['info_trabalho']; ?></p>
								</li>
								<?php endif; ?>
							</ul>
						</div>								
					</div>						
				</div>
			</div>				
		</div>		
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<p>
						<?php echo $depoimento['Depoimento']['depoimento_conteudo'] ?>
					</p>
				</div>
			</div>
		</div>
	</section>

	<?php if(isset($outros_depoimentos) && sizeof($outros_depoimentos)>0): ?>
	<section class="outros-cursos">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h2>Outros depoimentos de Ex-alunos da Factum</h2>
				</div>
			</div>
		</div>
		<div class="box-thumb">
			<div class="container">
				<div class="row">
					<?php foreach($outros_depoimentos as $outro_depoimento): ?>
					<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
						<div class="thumbnail">
							<img src="<?php echo $this->Html->Url('/' . $outro_depoimento['Depoimento']['thumb_path'] . '/' . $outro_depoimento['Depoimento']['thumb_dir'] . '/483_' . $outro_depoimento['Depoimento']['thumb'], true); ?>" alt="<?php echo $outro_depoimento['Depoimento']['nome'] ?>">
							<div class="caption">							
								
								<?php echo $outro_depoimento['Depoimento']['depoimento_resumo'] ?>
								
								<small><strong><?php echo $outro_depoimento['Depoimento']['nome'] ?></strong>, <?php echo $outro_depoimento['Depoimento']['legenda'] ?></small>
							</div>
						</div>
					</div>
					<?php endforeach; ?>
				</div>						
			</div>
		</div>
	</section>
	<?php endif; ?>

</main>		