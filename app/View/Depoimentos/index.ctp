<?php //debug($this->params); die; ?> 

<main>
	<section class="conteudo institucional">

		<div class="container-fluid bg-curos">
			<div class="row">
				<div class="box-banner">
					<img src="<?php echo $this->Html->Url('/assets/images/banners/banner-curso.jpg'); ?>" class="img-responsive">
					<div class="filter-img"></div>	
				</div>						
				<div class="col-xs-12 box-caption">						
					<div class="container">								
						<div class="row">
							<h1 class="text-center">Depoimentos</h1>
							<p>Home » A Factum » Depoimentos</p>						  													
						</div>
					</div>						
				</div>				
			</div>				
		</div>
		<div class="container">
			<div class="row row-conteudo">

				<?php if(isset($depoimentos) && sizeof($depoimentos)>0): ?>

				<?php $count = 1; foreach($depoimentos as $depoimento): ?>	

				<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
					<div class="thumbnail">				

						<img src="<?php echo $this->Html->Url('/' . $depoimento['Depoimento']['thumb_path'] . '/' . $depoimento['Depoimento']['thumb_dir'] . '/483_' . $depoimento['Depoimento']['thumb'], true); ?>" alt="<?php $depoimento['Depoimento']['nome']; ?>" />

						<div class="caption">								
							
							<?php echo $depoimento['Depoimento']['depoimento_resumo'] ?>
							
							<p><span><strong><?php echo $depoimento['Depoimento']['nome'] ?></strong>, <?php echo $depoimento['Depoimento']['legenda'] ?></span></p>
							<a href="<?php echo $this->Html->Url('/depoimento/'.$depoimento['Depoimento']['id']) ?>" class="btn btn-primary center-block">VEJA O DEPOIMENTO</a>
						</div>
					</div>
				</div>
				
				<?php if($count%3==0): ?>
				<div class="clearfix"></div>
				<?php endif; ?>

				<?php $count++; endforeach; ?>
				
				<?php if(isset($this->params['paging']['Depoimento']['pageCount']) && $this->params['paging']['Depoimento']['pageCount'] > 1): ?>
		    	<?php
					$this->Paginator->options(
		    			array(
						    'url'=> array(
						        'controller' => 'depoimentos',
						        'action' => 'index'
						    )
						)
					);
				?>
				<div class="col-xs-12">
					<nav aria-label="Page navigation" class="navigation">
					  <ul class="pagination">
					    <li>
					      <?php echo $this->Paginator->prev(__('«', true), array('aria-hidden' => 'true'), null, array()); ?>
					    </li>
			            <?php echo $this->Paginator->numbers(array('separator' => ' ', 'tag' => 'li', 'currentTag' => 'a')); ?>
			            <?php if(($this->Paginator->current()+9) < $this->Paginator->counter(array('format' => '{:pages}'))): ?>
			            	<li>
				            	<?php echo $this->Paginator->last($this->Paginator->counter(array('format' => '{:pages}')), array(), null, array()); ?>
				            </li>
			            <?php endIf; ?>
					    <li>					      
					      	<?php echo $this->Paginator->next(__('»', true), array('aria-hidden' => 'true'), null, array()); ?>
					    </li>
					  </ul>
					</nav>
				</div>
				<?php endIf; ?>				

				<?php else: ?>

				<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">

					<p>Nenhum depoimento encontrado.</p>
					<br><br>

				</div>

				<?php endif; ?>
			</div>				
		</div>						
	</section>		
</main>	