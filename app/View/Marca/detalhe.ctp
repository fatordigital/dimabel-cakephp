<?php $this->assign('title', $title) ?>
<?php echo $this->Element('site/header_marcas', array('form_url' => $this->Consorcio->Url('/duvidas-outro-modelo/' . $marca['Marca']['slug'], true))) ?>
<section class="fd_page_content fd_page_products_type">
    <div class="container">

        <figure class="fd_product_brand">
            <img src="<?php echo $marca['Marca']['full_imagem'] ?>"
                 alt="<?php echo $marca['Marca']['nome'] ?>" style="max-width: 65px;" />
        </figure>

        <div class="row">

            <?php foreach ($marca['Carro'] as $carro) { ?>
                <form action="<?php echo $this->Consorcio->Url('/simulacao', true) ?>" method="POST" id="contratar-<?php echo $carro['slug'] ?>" onsubmit="return false;">
                    
                    <input type="hidden" name="carro" value="<?php echo $carro['slug'] ?>" />
                    <input type="hidden" name="simulation_type" class="fd_form_indice" value="automovel">

                    <input type="hidden" name="nome" class="fill" />
                    <input type="hidden" name="telefone" class="fill" />
                    <input type="hidden" name="email" class="fill" />
                    <input type="hidden" name="cpf" class="fill" />

                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 fd_eq">
                        <div class="fd_item<?php echo $carro['mais_vendido'] ? ' fd_has_flag' : '' ?>">
                            <h3><?php echo $carro['nome'] ?></h3>

                            <?php if (is_file(WWW_ROOT . 'files' . DS . 'carro' . DS . 'imagem' . DS . $carro['imagem_dir'] . DS . $carro['imagem'])) { ?>
                                <figure>
                                    <img src="<?php echo $this->Html->Url('/files/carro/imagem/' . $carro['imagem_dir'] . '/' . $carro['imagem'], true) ?>"
                                         width="195"
                                         alt="<?php echo $carro['nome'] ?>">
                                </figure>
                            <?php } else { ?>
                                <figure>
                                    <img src="<?php echo $this->Html->Url('/assets/images/sem-imagem.png', true) ?>" alt="<?php echo $carro['nome'] ?>" />
                                </figure>
                            <?php } ?>

                            <div class="fd_radio">
                                <label class="fd_radio_label">
                                    <span class="total_carro">
                                        <br />
                                        R$ <?php echo $this->String->bcoToMoeda($carro['preco']) ?>
                                    </span>
                                </label>
                            </div><!-- end of /.fd_radio -->

                            <?php foreach ($this->Consorcio->Atual('parcelas_carro') != '' ? json_decode($this->Consorcio->Atual('parcelas_carro'), true) : $parcela_disponiveis as $i => $parcela) { ?>
                            <div class="fd_radio">
                                <input name="parcela" id="fd_radio-<?php echo $carro['slug'] ?>-<?php echo $i ?>" value="<?php echo $parcela ?>" type="radio"<?php echo $i == 0 ? 'checked' : '' ?> />
                                <label for="fd_radio-<?php echo $carro['slug'] ?>-<?php echo $i ?>" class="fd_radio_label">
                                    <span>
                                        <strong><?php echo $parcela ?> PARCELAS</strong><br />
                                        R$ <?php echo $this->String->bcoToMoeda($this->Consorcio->parcela($carro['preco'], 'taxa_carro', $parcela)) ?>
                                    </span>
                                </label>
                            </div><!-- end of /.fd_radio -->
                            <?php } ?>

                            <button type="submit" class="btn fd_button_orange" data-carro="<?php echo $carro['slug'] ?>" data-modal="<?php echo $carro['slug'] ?>">
                                Contratar
                            </button>

                        </div><!-- end of /.fd_item -->
                    </div><!-- end of /.cols.fd_eq -->
                </form>
            <?php } ?>



        </div><!-- end of /.row -->
    </div><!-- end of /.container -->
</section>

<?php echo $this->Element('site/form_newsletter') ?>
<?php echo $this->Element('modal/dados-pessoais') ?>
