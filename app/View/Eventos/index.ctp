<?php echo $this->assign('title', 'Calendário Acadêmico'); ?>

<main>
	<section class="conteudo institucional">

		<div class="container-fluid bg-curos">
			<div class="row">
				<div class="box-banner">
					<img src="<?php echo $this->Html->Url('/assets/images/banners/banner-curso.jpg'); ?>" class="img-responsive">
					<div class="filter-img"></div>	
				</div>						
				<div class="col-xs-12 box-caption">						
					<div class="container">								
						<div class="row">
							<h1 class="text-center">Eventos</h1>
							<p><a href="<?php echo $this->Html->Url('/'); ?>">Home</a> » <a href="<?php echo $this->Html->Url('/eventos'); ?>">Eventos</a></p>
						</div>
					</div>						
				</div>				
			</div>				
		</div>
		<div class="container">
			<div class="row row-conteudo calendario">
				<div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
					<div class="col-xs-12">

					<?php if(isset($eventos) && count($eventos) > 0): ?>
						<?php foreach($eventos as $k => $evento): ?>
						<a href="<?php echo $this->Html->Url('/' . $evento['Evento']['seo_url']); ?>">
							<div class="box-my-evento">
								<div class="col-xs-1">
									<span class="icon-calendar"></span>
								</div>
								<div class="col-xs-11">
									<time>
									<?php if($evento['Evento']['data_fim'] != "" || ($evento['Evento']['data_fim'] != "" && $evento['Evento']['data_fim'] != $evento['Evento']['data_inicio']) ){ ?>

									<?php if($evento['Evento']['data_inicio']==$evento['Evento']['data_fim']){ ?>

										<?php echo date('d', strtotime($evento['Evento']['data_inicio'])); ?> de <?php echo $this->String->mes(date('m', strtotime($evento['Evento']['data_inicio']))); ?>

									<?php }else{ ?>

										<?php echo date('d', strtotime($evento['Evento']['data_inicio'])); ?> de <?php echo $this->String->mes(date('m', strtotime($evento['Evento']['data_inicio']))); ?> a 
										<?php echo date('d', strtotime($evento['Evento']['data_fim'])); ?> de <?php echo $this->String->mes(date('m', strtotime($evento['Evento']['data_fim']))); ?>

									<?php } ?>		

				    				<?php }else{ ?>
				    					<?php echo date('d', strtotime($evento['Evento']['data_inicio'])); ?> de <?php echo $this->String->mes(date('m', strtotime($evento['Evento']['data_inicio']))); ?>
				    				<?php } ?>
									</time>
								</div>
								<div class="col-xs-12"><p><?php echo $evento['Evento']['nome']; ?></p></div>
							</div>
						</a>
						<?php endForeach; ?>
	    			<?php else: ?>
	    				<br /><br />
	    				Nenhum evento agendado para hoje.
	    			<?php endIf; ?>

					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<?php echo $this->element('site/calendario') ?>
					<?php echo $this->element('site/sidebar_paginas') ?>
				</div>						
			</div>				
		</div>						
	</section>		
</main>		

<?php /*

<section class="row row-title-page">    
	<div class="container">
	  <div class="col-xs-12">
	    <h1><?php echo ($this->params->params['tipo_calendario'] == 'academico') ? 'Calendário Acadêmico' : 'Calendário de Eventos'; ?></h1>
	  </div>
	</div>
	<div class="row-white">
	  <div class="container">
	    <div class="col-xs-6 pull-right"></div>
	  </div>
	</div>
</section>  
<section class="row row-content row-curso">
	<div class="container">
	  <div>
	    <!-- Nav tabs -->
	    <ul class="nav nav-tabs" role="tablist">
	      <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Calendário</a></li>
	    </ul>

	  <!-- Nav content -->
	  <div class="tab-content">
	      <div role="tabpanel" class="tab-pane fade in active" id="home">
	        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">

	        	<?php if(isset($eventos) && count($eventos) > 0): ?>
					<?php foreach($eventos as $k => $evento): ?>
		    			<div class="box-day box-day-<?php echo $evento['Evento']['evento_tipo']; ?>">	
		    				<h4>
		    					<span class="glyphicon glyphicon-calendar"></span>
		    					<?php if($evento['Evento']['data_fim'] != "" || ($evento['Evento']['data_fim'] != "" && $evento['Evento']['data_fim'] != $evento['Evento']['data_inicio']) ){ ?>

									<?php if($evento['Evento']['data_inicio']==$evento['Evento']['data_fim']){ ?>

										<?php echo date('d', strtotime($evento['Evento']['data_inicio'])); ?> de <?php echo $this->String->mes(date('m', strtotime($evento['Evento']['data_inicio']))); ?>

									<?php }else{ ?>

										<?php echo date('d', strtotime($evento['Evento']['data_inicio'])); ?> de <?php echo $this->String->mes(date('m', strtotime($evento['Evento']['data_inicio']))); ?> a 
										<?php echo date('d', strtotime($evento['Evento']['data_fim'])); ?> de <?php echo $this->String->mes(date('m', strtotime($evento['Evento']['data_fim']))); ?>

									<?php } ?>		

			    				<?php }else{ ?>
			    					<?php echo date('d', strtotime($evento['Evento']['data_inicio'])); ?> de <?php echo $this->String->mes(date('m', strtotime($evento['Evento']['data_inicio']))); ?>
			    				<?php } ?>
		    				</h4>		    				
		    				<h3><?php echo $evento['Evento']['nome']; ?></h3>
		    				
		    				<?php if($evento['Evento']['descricao_resumida'] != ""): ?>
		    					<p><?php echo $evento['Evento']['descricao_resumida']; ?></p>
		    				<?php endIf; ?>
		    				
		    				<?php if(trim(strip_tags($evento['Evento']['conteudo'])) != ""): ?>
		    					<a href="<?php echo $this->Html->Url('/' . $evento['Evento']['seo_url']); ?>" class="btn-yellow btn-vest upper pull-right" title="<?php echo $evento['Evento']['nome']; ?>">Leia Mais</a>
		    				<?php endIf; ?>
		    			</div>
		    		<?php endForeach; ?>
	    		<?php else: ?>
	    			<br /><br />
	    			Nenhum evento agendado para hoje.
	    		<?php endIf; ?>

	        </div><!-- end conteudo tab -->
	        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 sidebar"> 
	          	<?php echo $this->element('site/calendario') ?>

	          	<?php if(isset($downloads) && count($downloads) > 0): ?>
					<div class="documents">

						<h2>Documentos</h2>
						<p>Faça download dos calendários acadêmicos em formato PDF</p>
						
						<?php foreach($downloads as $k => $download): ?>
							<a href="<?php echo $this->Html->Url('/' . $download['Download']['path'] . '/' . $download['Download']['dir'] . '/' . $download['Download']['file'], true); ?>" title="<?php echo $download['Download']['nome']; ?>" target="_blank">
								<div class="icon-pdf">
									<img src="assets/images/icons/icon-pdf.png" class="img-responsive">
									<p>
										<?php echo $download['Download']['nome']; ?>
									</p>
								</div>
							</a>
						<?php endForeach; ?>
						
					</div>
				<?php endIf; ?>

				

	          	<?php echo $this->element('site/redes-sociais-mini') ?>
	        </div><!-- end sidebar -->
	      </div>         
	  </div>
	</div>
</section><!-- end content -->
*/ ?>