<?php echo $this->assign('title', $evento['Evento']['nome']); ?>

<main>
	<section class="conteudo institucional">
		<div class="container-fluid bg-curos">
			<div class="row">
				<div class="box-banner">
					<img src="<?php echo $this->Html->Url('/assets/images/banners/banner-curso.jpg'); ?>" class="img-responsive">
					<div class="filter-img"></div>	
				</div>						
				<div class="col-xs-12 box-caption">						
					<div class="container">								
						<div class="row">
							<h1 class="text-center">Eventos</h1>
							<p><a href="<?php echo $this->Html->Url('/'); ?>">Home</a> » <a href="<?php echo $this->Html->Url('/eventos'); ?>">Eventos</a> » <?php echo $evento['Evento']['nome']; ?></p>
						</div>
					</div>						
				</div>				
			</div>				
		</div>
		<div class="container">
			<div class="row row-conteudo calendario">
				<div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
					<h2><?php echo $evento['Evento']['nome']; ?></h2>
					<p><?php echo $evento['Evento']['conteudo']; ?></p>

					<?php if($evento['Evento']['status_inscricao']==='1'): ?>
					<div class="col-lg-12 col-lg-12 col-sm-12 col-xs-12">
						<?php echo $this->element('site/evento_inscricao') ?>
					</div>
					<?php endif; ?>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<?php echo $this->element('site/calendario') ?>
					<?php echo $this->element('site/sidebar_paginas') ?>
				</div>							
			</div>				
		</div>						
	</section>		
</main>	

<?php /*

<?php echo $this->element('site/banner_curso', array()); ?>



<section class="row row-title-page">    
	<div class="container">
	  <div class="col-xs-12">
	    <h1><?php echo ($this->params->params['tipo_calendario'] == 'academico') ? 'Calendário Acadêmico' : 'Calendário de Eventos'; ?></h1>
	  </div>
	</div>
	<div class="row-white">
	  <div class="container">
	    <div class="col-xs-6 pull-right"></div>
	  </div>
	</div>
</section>  
<section class="row row-content row-curso">
	<div class="container">
	  <div>
	    <!-- Nav tabs -->
	    	<ul class="nav nav-tabs" role="tablist">
	      		<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Calendário</a></li>
	    	</ul>

		  	<!-- Nav content -->
		  	<div class="tab-content">
		      	<div role="tabpanel" class="tab-pane fade in active" id="home">
			        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
			          	<div class="day">
			            	<h4> <?php echo date('d', strtotime($this->String->dataFormatada("Y-m-d", $evento['Evento']['data_inicio']))); ?> </h4>
							<h6><?php echo $this->String->mes(date('m', strtotime($this->String->dataFormatada("Y-m-d", $evento['Evento']['data_inicio'])))); ?></h6>	
			          	</div>
		          		<h4 class="title-detail"><?php echo $evento['Evento']['nome']; ?></h4>
						<p><?php echo $evento['Evento']['conteudo']; ?></p>
			                        
			        </div><!-- end conteudo tab -->
			        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 sidebar"> 
			        	<a href="<?php echo $this->Html->Url(array('controller' => 'eventos', 'action' => 'index')) ?>" class="return-calendar">
							<h5><i class="glyphicon glyphicon-chevron-left"></i><i class="glyphicon glyphicon-chevron-left"></i> Retornar à <span>listagem</span></h5>
						</a>	

						<?php echo $this->element('site/calendario') ?>

						<?php if(isset($downloads) && count($downloads) > 0): ?>
							<div class="documents">

								<h2>Documentos</h2>
								<p>Faça download dos calendários acadêmicos em formato PDF</p>
								
								<?php foreach($downloads as $k => $download): ?>
									<a href="<?php echo $this->Html->Url('/' . $download['Download']['path'] . '/' . $download['Download']['dir'] . '/' . $download['Download']['file'], true); ?>" title="<?php echo $download['Download']['nome']; ?>" target="_blank">
										<div class="icon-pdf">
											<img src="assets/images/icons/icon-pdf.png" class="img-responsive">
											<p>
												<?php echo $download['Download']['nome']; ?>
											</p>
										</div>
									</a>
								<?php endForeach; ?>
								
							</div>
						<?php endIf; ?>



						<?php echo $this->element('site/redes-sociais-mini') ?>	          	
			        </div><!-- end sidebar -->
			    </div>         
		  	</div>
		</div>
	</div>
</section><!-- end content -->
*/ ?>