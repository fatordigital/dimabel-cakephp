<?php echo $this->assign('title', 'Fale Conosco'); ?>

<?php echo $this->Element('site/header_interna') ?>

<section class="fd_page_content fd_page_contact">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-xs-offset-0 col-sm-10 col-sm-offset-1 col-md-7 col-md-offset-0">
                <p>
                    A sede da Dimabel está estabelecida em São Leopoldo, há 20km de distância de Porto Alegre. Com uma equipe formada por profissionais qualificados, atendemos o Brasil inteiro comercializando consórcios de marcas conceituadas.  Quer conhecer melhor a Dimabel? Faça uma vista ou um contato diretamente através do formulário ou de nossos telefones!
                </p>

                <?php echo $this->Form->create('Sac', array('url' => Router::url(null, true), 'class' => 'fd_form form-validate')) ?>
                    <div class="fd_key">
                        <?php echo $this->Form->hidden('fd') ?>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-5">
                            <div class="form-group fd_group">
                                <?php
                                    echo $this->Form->text('nome',
                                        array(
                                            'class' => 'form-control fd_input',
                                            'pattern' => '([A-zÀ-ž\s]){1,}',
                                            'required' => 'required',
                                            'data-rule-required' => 'true',
                                            'data-msg-required' => 'Preencha o campo corretamente',
                                            'aria-required' => 'true',
                                            'autocomplete' => 'off'
                                        )
                                    )
                                ?>
                                <label class="fd_label">NOME</label>
                            </div>
                            <div class="form-group fd_group">
                                <?php
                                echo $this->Form->text('email',
                                    array(
                                        'class' => 'form-control fd_input',
                                        'pattern' => '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$',
                                        'required' => 'required',
                                        'data-rule-required' => 'true',
                                        'data-rule-email' => 'true',
                                        'data-msg-required' => 'Preencha o campo corretamente',
                                        'data-msg-email' => 'Preencha o campo corretamente',
                                        'aria-required' => 'true',
                                        'autocomplete' => 'off'
                                    )
                                )
                                ?>
                                <label class="fd_label">E-MAIL</label>
                            </div>
                            <div class="form-group fd_group">
                                <?php
                                echo $this->Form->text('telefone',
                                    array(
                                        'class' => 'form-control fd_input fd_phone_mask',
                                        'pattern' => '([\(]\d{2}[\)]) (\d{5}[\-]\d{4}|\d{4}[\-]\d{4})',
                                        'required' => 'required',
                                        'data-rule-required' => 'true',
                                        'data-msg-required' => 'Preencha o campo corretamente',
                                        'aria-required' => 'true',
                                        'autocomplete' => 'off',
                                        'maxlength' => 15,
                                        'data-rule-minlength' => 14,
                                        'data-rule-maxlength' => 15
                                    )
                                )
                                ?>
                                <label class="fd_label">TELEFONE</label>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-7">
                            <div class="form-group fd_group">
                                <?php
                                echo $this->Form->textarea('mensagem',
                                    array(
                                        'class' => 'form-control fd_input',
                                        'pattern' => '([A-zÀ-ž\s]){1,}',
                                        'required' => 'required',
                                        'data-rule-required' => 'true',
                                        'data-msg-required' => 'Preencha o campo corretamente',
                                        'aria-required' => 'true',
                                        'autocomplete' => 'off'
                                    )
                                )
                                ?>
                                <label class="fd_label">MENSAGEM</label>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <input type="hidden" class="fd_key">
                            <button type="submit" class="btn fd_button_warning pull-right">Enviar</button>
                        </div>
                    </div>
                <?php echo $this->Form->end() ?>
            </div>
            <div class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-1">

                <div class="fd_item">
                    <figure>
                        <img src="<?php echo $this->Html->Url('/assets/images/icons/pin.png', true) ?>" alt="Endereço">
                    </figure>
                    <div class="fd_content">
                        <p>
                            Quer adquirir o consórcio pessoalmente? Visite nossa sede!
                        </p>
                        <address>
                            Av. João Correa, 503, sala 2<br />
                            <strong>São Leopoldo/RS</strong> - CEP 93040-035
                        </address>
                    </div>
                </div>

                <div class="fd_item">
                    <figure>
                        <img src="<?php echo $this->Html->Url('/assets/images/icons/phone_big.png', true) ?>" alt="Telefone">
                    </figure>
                    <p>
                        Quer tirar todas as suas dúvidas sobre consórcios?<br />
                        <big><?php echo Configure::read('Site.central_de_vendas') ?></big>
                    </p>
                </div>

                <div class="fd_item">
                    <figure>
                        <img src="<?php echo $this->Html->Url('/assets/images/icons/whatsapp.png', true) ?>" alt="Whatsapp">
                    </figure>
                    <p>
                        Envie um whats para nós<br />
                        <big><?php echo Configure::read('Site.WhatsApp') ?></big>
                    </p>
                </div>

            </div>
        </div>
    </div>
    <div id="mapa"></div>
</section>

<?php
$this->start('script');
echo $this->Html->script('/assets/js/secoes/sac');
$this->end();
?>
