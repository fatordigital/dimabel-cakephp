<?php
class EstadosHelper extends Helper {

	public function getEstadosBrasileiros(){

		$opt_estados = array(
			// '' 	 => 'UF',
			'AC' => 'Acre',
			'AL' => 'Alagoas',
			'AP' => 'Amapá',
			'AM' => 'Amazonas',
			'BA' => 'Bahia',
			'CE' => 'Ceará',
			'DF' => 'Distrito Federal',
			'ES' => 'Espirito Santo',
			'GO' => 'Goiás',
			'MA' => 'Maranhão',
			'MT' => 'Mato Grosso',
			'MS' => 'Mato Grosso do Sul',
			'MG' => 'Minas Gerais',
			'PA' => 'Pará',
			'PB' => 'Paraíba',
			'PR' => 'Paraná',
			'PE' => 'Pernambuco',
			'PI' => 'Piauí',
			'RJ' => 'Rio de Janeiro',
			'RN' => 'Rio Grande do Norte',
			'RS' => 'Rio Grande do Sul',
			'RO' => 'Rondônia',
			'RR' => 'Roraima',
			'SC' => 'Santa Catarina',
			'SP' => 'São Paulo',
			'SE' => 'Sergipe',
			'TO' => 'Tocantins'
		);

		return $opt_estados;

	}

	public function getNomeEstado($uf){
		switch( $uf ){
			case 'AC':
			return 'Acre';

			case 'AL':
			return 'Alagoas';

			case 'AP':
			return 'Amapá';

			case 'AM':
			return 'Amazonas';

			case 'BA':
			return 'Bahia';

			case 'CE':
			return 'Ceará';

			case 'DF':
			return 'Distrito Federal';

			case 'ES':
			return 'Espirito Santo';

			case 'GO':
			return 'Goiás';

			case 'MA':
			return 'Maranhão';

			case 'MT':
			return 'Mato Grosso';

			case 'MS':
			return 'Mato Grosso do Sul';

			case 'MG':
			return 'Minas Gerais';

			case 'PA':
			return 'Pará';

			case 'PB':
			return 'Paraíba';

			case 'PR':
			return 'Paraná';

			case 'PE':
			return 'Pernambuco';

			case 'PI':
			return 'Piauí';

			case 'RJ':
			return 'Rio de Janeiro';

			case 'RN':
			return 'Rio Grande do Norte';

			case 'RS':
			return 'Rio Grande do Sul';

			case 'RO':
			return 'Rondônia';

			case 'RR':
			return 'Roraima';

			case 'SC':
			return 'Santa Catarina';

			case 'SP':
			return 'São Paulo';

			case 'SE':
			return 'Sergipe';

			case 'TO':
			return 'Tocantins';
		}
	}
}