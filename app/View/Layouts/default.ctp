<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php echo $this->Html->charset() ?>
    <title>
        <?php
            //seo_title
            if($this->fetch('title')){
                $seo['title'] = '';
                if(isset($this->params->params['page']) && $this->params->params['page'] > 1){
                    $seo['title'] = $this->fetch('title') . ' | Página ' . $this->params->params['page'];
                }else{
                    $seo['title'] = $this->fetch('title');
                }

                $seo['title'] .= ' | '. Configure::read('Site.Seo.Title');
            }elseif($title_for_layout != ""){
                if(isset($seo) && isset($seo['title'])){
                }else{
                    $seo['title'] = $title_for_layout;
                }

                if(isset($this->params->params['page']) && $this->params->params['page'] > 1){
                    $seo['title'] .= ' | Página ' . $this->params->params['page'];
                }

                $seo['title'] .= ' | '. Configure::read('Site.Seo.Title');
            }else{
                $seo['title'] = Configure::read('Site.Seo.Title');
            }
        ?>
        <?php echo $seo['title']; ?>
    </title>

    <?php
        //seo_keywords
        if($this->fetch('keywords')){
            $seo['keywords'] = $this->fetch('keywords');
        }else{
            $seo['keywords'] = Configure::read('Site.Seo.Keywords');
        }

        //seo_description
        if($this->fetch('description')){
            $seo['description'] = $this->fetch('description');
        }else{
            $seo['description'] = Configure::read('Site.Seo.Description');
        }

        //meta
        echo $this->Html->meta('keywords', $seo['keywords']);
        echo $this->Html->meta('description', $seo['description']);
    ?>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php echo $this->element('site/metatags', array('seo' => $seo)); ?>

    <?php  echo $this->fetch('meta'); ?>

    <script type="text/javascript">
        var APP_ROOT = "<?php echo $this->Html->Url('/', true); ?>";
    </script>

    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo $this->Html->Url('/assets/images/favicon/apple-icon-57x57.png'); ?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo $this->Html->Url('/assets/images/favicon/apple-icon-60x60.png'); ?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo $this->Html->Url('/assets/images/favicon/apple-icon-72x72.png'); ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $this->Html->Url('/assets/images/favicon/apple-icon-76x76.png'); ?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo $this->Html->Url('/assets/images/favicon/apple-icon-114x114.png'); ?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo $this->Html->Url('/assets/images/favicon/apple-icon-120x120.png'); ?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo $this->Html->Url('/assets/images/favicon/apple-icon-144x144.png'); ?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $this->Html->Url('/assets/images/favicon/apple-icon-152x152.png'); ?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $this->Html->Url('/assets/images/favicon/apple-icon-180x180.png'); ?>">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo $this->Html->Url('/assets/images/favicon/android-icon-192x192.png'); ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $this->Html->Url('/assets/images/favicon/favicon-32x32.png'); ?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo $this->Html->Url('/assets/images/favicon/favicon-96x96.png'); ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $this->Html->Url('/assets/images/favicon/favicon-16x16.png'); ?>">
    <link rel="manifest" href="<?php echo $this->Html->Url('/assets/favicon/manifest.json'); ?>">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo $this->Html->Url('/assets/images/favicon/ms-icon-144x144.png'); ?>">
    <meta name="theme-color" content="#ffffff">
    
    <!-- BEGIN: FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans:100,200,300,400,600,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <!-- END: FONTS -->

    <!-- Stylesheet -->
    <link rel="stylesheet" type="text/css" href="<?php echo $this->Html->Url('/assets/css/lib/reset.css', true); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo $this->Html->Url('/assets/css/lib/bootstrap.min.css', true); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo $this->Html->Url('/assets/css/lib/owl.carousel.min.css', true); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo $this->Html->Url('/assets/css/lib/owl.theme.default.css', true); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo $this->Html->Url('/assets/css/lib/sweetalert.min.css', true); ?>">
    <link rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <?php echo $this->fetch('css'); ?>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->Html->Url('/assets/css/main.css', true); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo $this->Html->Url('/assets/css/custom.css', true); ?>">
    <!-- Stylesheet -->

    <script type="text/javascript">
        var baseUrl = '<?php echo $this->Html->Url('/', true) ?>',
            baseImg = '<?php echo $this->Html->Url('/assets/images/', true) ?>',
            baseFiles = '<?php echo $this->Html->Url('/files/', true) ?>',
            ConfigDimabel = <?php echo Configure::read('Site.Config.Valores') ?>,
            Atual = '<?php echo $this->Consorcio->Atual('slug') ?>';
    </script>

    <?php if (Configure::read('debug') == 2) { ?>
        <script>
            let ENV_DEBUG = true;
        </script>
    <?php } else { ?>
        <script>
            let ENV_DEBUG = false;
        </script>
    <?php } ?>

    <?php
    if (Configure::read('debug') == 0) {
        echo Configure::read('Site.Google.GTM');
        echo Configure::read('Site.Google.Analytics');
        echo Configure::read('Site.Chat');
        echo Configure::read('Site.Hubspot');
        echo $this->Flash->render('goal');
    }
    ?>

    <?php if (Configure::read('debug') == 0) { ?>
        <link rel="manifest" href="<?php echo $this->Html->Url('/manifest.json', true) ?>" />
        <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
        <script>
            var OneSignal = window.OneSignal || [];
            OneSignal.push(function() {
                OneSignal.init({
                    appId: "5fec3021-ce6b-4a6d-8cc5-aecb00a3a6d7",
                    autoRegister: true
                });
            });
        </script>
    <?php  } ?>

</head>

<body class="<?php echo $this->Consorcio->Body() ?><?php echo $this->fetch('class_body') ?>">


    <?php echo $this->element('site/menu') ?>

    <?php echo $this->fetch('content') ?>

    <?php echo $this->element('site/footer') ?>

	<?php echo $this->element('sql_dump'); ?>


    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBLaAx8JweUAoiMwVIcgoeWvvxvflHAi7E&v=3.20&sensor=false"></script>
    <script type="text/javascript" src="<?php echo $this->Html->Url('/assets/js/lib/jquery-2.2.4.min.js', true) ?>"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.17.0/axios.min.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="<?php echo $this->Html->Url('/assets/js/lib/jquery.touch.min.js', true) ?>"></script>
    <script type="text/javascript" src="<?php echo $this->Html->Url('/assets/js/lib/bootstrap.min.js', true) ?>"></script>
    <script type="text/javascript" src="<?php echo $this->Html->Url('/assets/js/lib/mask.min.js', true) ?>"></script>
    <script type="text/javascript" src="<?php echo $this->Html->Url('/assets/js/lib/jquery.validate.min.js', true) ?>"></script>
    <script type="text/javascript" src="<?php echo $this->Html->Url('/assets/js/lib/owl.carousel.min.js', true) ?>"></script>
    <script type="text/javascript" src="<?php echo $this->Html->Url('/assets/js/lib/jquery.match-height.js', true) ?>"></script>
    <script type="text/javascript" src="<?php echo $this->Html->Url('/assets/js/lib/swiffy-v7.4.js', true) ?>"></script>
    <script type="text/javascript" src="<?php echo $this->Html->Url('/assets/js/lib/fator.js', true) ?>"></script>
    <script type="text/javascript" src="<?php echo $this->Html->Url('/assets/js/lib/sweetalert.min.js', true) ?>"></script>
    <script type="text/javascript" src="<?php echo $this->Html->Url('/assets/js/lib/smoothscroll.js', true) ?>"></script>
    <?php echo $this->fetch('script'); ?>
    <?php echo $this->Flash->render('success')  ?>
    <?php echo $this->Flash->render('error')  ?>
    <script type="text/javascript" src="<?php echo $this->Html->Url('/assets/js/main.js', true) ?>"></script>
    <script>
        $(document).ready(function(){
            var stage = new swiffy.Stage(document.getElementById('fd_signature'),
                swiffyobject, {});
            stage.setBackground(null);
            stage.start();
        });
    </script>
</body>

</html>