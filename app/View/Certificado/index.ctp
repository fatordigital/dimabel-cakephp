<?php echo $this->element('site/header'); ?>
<main>
    
    <section class="certificados">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>Selecione o Certificado que você deseja emitir</h1>
                    <?php if(count($certificados) > 1): ?>
                    <p>
                        Ops! Verificamos em nosso sistema que você participou de mais de um evento da Educamax. 
                        Clique e selecione qual certificado você deseja imprimir:
                    </p>
                    <?php endIf; ?>

                    <?php if(count($certificados)): ?>
                    
                        <?php foreach($certificados as $k => $particip): ?>
                            <?php if(count($certificados) > 1): ?>
                                <div class="clear"></div>
                                <h3 style="margin-top: 10px; margin-bottom: 0px; float: left;"><?php echo $k; ?></h3>
                            <?php endIf; ?>
                            <ul class="listaCertificados">
                                <?php foreach($particip as $certificado): ?>        
                                    <li>
                                        <h2>“<?php echo $certificado['Evento']['nome'] ?>”</h2>
                                        <ul>
                                            <li>Tipo: <?php echo ucwords($certificado['Evento']['evento_tipo']); ?></li>
                                            <li>Local: <?php echo $certificado['Evento']['local'] ?></li>
                                            <li>Cidade: <?php echo $certificado['Evento']['cidade'] ?></li>
                                            <li>Data: <?php echo implode('/', array_reverse(explode('-', $certificado['Evento']['data']))); ?></li>
                                        </ul>
                                        <form action="<?php echo $this->Html->Url(array('action' => 'pdf', $certificado['EventoParticipante']['id'])); ?>">
                                        <button class="btn"><img src="<?php echo $this->Html->Url('/'); ?>assets/images/svg/doc.svg" class="img-responsive"> EMITIR CERTIFICADO</button>
                                        </form>
                                    </li>
                                <?php endForeach; ?>
                            </ul>
                        <?php endForeach; ?>

                    <?php else: ?>
                        <p class="text-center">
                            Nenhum certificado encontrado! 
                            <br />
                            Caso tenha participado do evento, por favor, entre em contato pelo formulário abaixo!
                        </p>
                    <?php endIf; ?>
                </div>
            </div>
        </div>
    </section>

    <?php echo $this->element('site/contato'); ?>    
</main>
<?php echo $this->element('site/footer'); ?>