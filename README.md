![FatorCMS](http://www.fatordigital.com.br/logo_fatorcms_234x55.png "FatorCMS")

-------
# Educamax

# Descrição:

Projeto CORE da Educamax

# Orientações:

# Plugins:
- Acl
- AclExtras
- FdBlocos
- FdDashboard
- FdDepoimentos
- FdEventos
- FdGalerias
- FdLogs
- FdMenus
- FdPaginas
- FdRotas
- FdSac
- FdSettings
- FdSites
- FdSvn
- FdUsuarios
- FilterResults
- PhpExcel
- Upload

# Libraries
- Error / AppExceptionRenderer
- Wideimage
- CustomUploadHandler
- GaleriaUploadHandler