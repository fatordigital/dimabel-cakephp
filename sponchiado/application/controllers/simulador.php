<?php

defined('BASEPATH') OR exit('No direct script access allowed');
include 'controller.php';

class Simulador extends Controller {

    private $simulador_data;
    private $valores = array(
        //Imovéis
        '1' => array(
            '180' => array(
                'min' => 100000.00,
                'name' => 'Imóvel',
                'propaganda' => 'Seu Consórcio Imobiliário',
                'tax' => 22
            )
        ),
        //Automóveis
        '2' => array(
            '60' => array(
                'min' => 100000.00,
                'name' => 'Automóvel',
                'propaganda' => 'Seu Consórcio de Veículos',
                'tax' => 11
            ),
            '72' => array(
                'min' => 100000.00,
                'name' => 'Automóvel',
                'propaganda' => 'Seu Consórcio de Veículos',
                'tax' => 12
            ),
            '84' => array(
                'min' => 100000.00,
                'name' => 'Automóvel',
                'propaganda' => 'Seu Consórcio de Veículos',
                'tax' => 13
            ),
            '100' => array(
                'min' => 100000.00,
                'name' => 'Automóvel',
                'propaganda' => 'Seu Consórcio de Veículos',
                'tax' => 16
            ),
        ),
        //Caminhões
        '3' => array(
            '80' => array(
                'min' => 100000.00,
                'name' => 'Automóvel',
                'propaganda' => 'Seu Consórcio de Veículos',
                'tax' => 12
            ),
            '100' => array(
                'min' => 100000.00,
                'name' => 'Automóvel',
                'propaganda' => 'Seu Consórcio de Veículos',
                'tax' => 14
            ),
            '120' => array(
                'min' => 100000.00,
                'name' => 'Automóvel',
                'propaganda' => 'Seu Consórcio de Veículos',
                'tax' => 16
            )
        ),
        //Máquinas
        '4' => array(
            '80' => array(
                'min' => 100000.00,
                'name' => 'Automóvel',
                'propaganda' => 'Seu Consórcio de Veículos',
                'tax' => 12
            ),
            '100' => array(
                'min' => 100000.00,
                'name' => 'Automóvel',
                'propaganda' => 'Seu Consórcio de Veículos',
                'tax' => 14
            ),
            '120' => array(
                'min' => 100000.00,
                'name' => 'Automóvel',
                'propaganda' => 'Seu Consórcio de Veículos',
                'tax' => 16
            )
        )
    );

    public function __construct() {
        parent::__construct();
        $this->simulador_data['db'] = $this->db;
    }

    public function index() {
        $this->validate();
        $this->template->load('template', 'simulador', $this->simulador_data);
    }

    public function submit_plan() {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $validate = $this->validate();
            $post = $this->input->post();
            $tipo = $this->valores[$post['tipo']][$post['parcelas']];
            if ($validate) {
                $valf = coinToBco($post['valor']);
                $valf = (($valf * $tipo['tax']) / 100) + $valf;
                $valn = ($valf / $post['parcelas']);
                $val = bcoToCoin($valn);

                $insert = ''
                    . 'INSERT INTO simulators ('
                    . 'type, value, plots, '
                    . 'name, email, phone, '
                    . 'created_at, updated_at'
                    . ') VALUES ('
                    . '"' . $post['tipo'] . '", "' . $post['valor'] . '", "' . $post['parcelas'] . '", '
                    . '"' . $post['nome'] . '", "' . $post['email'] . '", "' . $post['telefone'] . '", '
                    . 'NOW(), NOW())';
                $this->db->query($insert);

                $body = ''
                    . '<b>Mensagem enviado por:</b><br/>' . $post['nome']
                    . '<br/><br/>'
                    . '<b>E-mail:</b><br/>' . $post['email']
                    . '<br/><br/> <b>Fone:</b><br/>' . $post['telefone']
                    . '<br/><br/>'
                    . '<b>Tipo de Consórcio:</b><br/>' . $tipo['propaganda']
                    . '<br/><br/>'
                    . '<b>Valor do Consórcio:</b><br/>R$ ' . $post['valor']
                    . '<br/><br/>'
                    . '<b>Parcelas do Consórcio:</b><br/> ' . $post['parcelas'] . ' meses '
                    . '<br/><br/>'
                    . '<b>Valor por Mês:</b><br/>R$ ' . $val . ' por mês';

                $this->session->set_userdata($post);
                if ($this->send_php_mailer(array('email' => $post['email'], 'name' => $post['nome']), $body, array('subject' => 'Simulador Consórcio Sponchiado'))) {
                    $this->session->set_userdata(array('plan' => 1));
                    $this->session->set_userdata($tipo);
//                    $this->session->set_userdata($post);
                    $this->session->set_userdata(array('valor_parcela' => $val));
                    $this->session->set_userdata(array('goal' => "ga('send', 'pageview', '/goal/simulacao-sponchiado');"));
                    alert('success', 'Tudo Ok, logo entraremos em contato com você, Obrigado!');
                    redirect(site_url('/simulador'));
                }
            } else {
                $this->load->view('simulador', $this->simulador_data);
            }
        } else {
            alert('error', 'Página não encontrada.');
        }
        $this->template->load('template', 'simulador');
    }

    public function submit_con() {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $session = $this->session->all_userdata();
            $post = $this->input->post();
            if (!empty($post['cpf'])) {
                //Revisar e colocar ao vir de uma ssession do formulário anterior
//            $insert = 'INSERT INTO simulators (`type`, `value`, `plots`, `name`, `email`, `phone`, `created_at`, `updated_at`)
//            VALUES ("'.$session['propaganda'].'","'.$session['valor'].'","'.$session['parcelas'].'","'.$session['nome'].'","'.$session['email'].'","'.$session['telefone'].'", NOW(), NOW())';
//            $this->db->query($insert);


                $body = "<b>Mensagem enviado por:</b>
                <br/>
                {$session['nome']}
                <br/><br/>
                <b>E-mail:</b>
                <br/>
                {$session['email']}
                <br/><br/>
                <b>Fone:</b>
                <br/>
                {$session['telefone']}
                <br/><br/>	
                <b>CPF:</b>
                <br/>
                {$post['cpf']}
                <br/><br/>		
                <b>Tipo de Consórcio:</b>
                <br/>
                {$session['propaganda']}
                <br/><br/>		
                <b>Valor do Consórcio:</b>
                <br/>
                R$ {$session['valor']}			
                <br/><br/>		
                <b>Parcelas do Consórcio:</b>
                <br/>
                {$session['parcelas']} meses<br/><br/><b>Valor por Mês:</b><br/>R$ {$session['valor_parcelas']} por mês";

                if ($this->send_php_mailer(array('name' => $session['nome'], 'email' => $session['email']), $body, array('subject' => 'Consórcio Contratado'))) {
                    $this->session->unset_userdata('plan');
                    alert('success', 'Tudo Ok, logo entraremos em contato com você, Obrigado!');
                    redirect('/simulador');
                } else {
                    alert('error', 'Ops, houve um erro na hora de enviar seus dados, tente novamente.');
                    redirect('/simulador');
                }
            } else {
                alert('error', 'Por favor, preencha seu CPF.');
                redirect('/simulador');
            }
        }
    }

    public function validate() {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $post = $this->input->post();
            $tipo = $this->valores[$post['tipo']];

            if (!captcha_fator())
                redirect($post['return_url']);

            if (empty($post['parcelas']) || $post['parcelas'] == 'na') {
                alert('error', 'Por favor, selecione a quantidade de parcelas.');
                if (isset($post['from_home']))
                    redirect($post['return_url']);
                return false;
            }

            if (!phone_validate($post['telefone'])) {
                alert('error', 'Por favor, adicione um telefone válido.');
                if (isset($post['from_home']))
                    redirect($post['return_url']);
                return false;
            }

            if ($tipo['min'] > coinToBco($post['valor'])) {
                alert('error', 'Por favor, adicione um valor acima de R$ ' . bcoToCoin($tipo['min']));
                if (isset($post['from_home']))
                    redirect($post['return_url']);
                return false;
            }

            if (!filter_var($post['email'], FILTER_VALIDATE_EMAIL)) {
                alert('error', 'Por favor, adicione um e-mail válido.');
                if (isset($post['from_home']))
                    redirect($post['return_url']);
                return false;
            }
            return true;
        }
    }

}
