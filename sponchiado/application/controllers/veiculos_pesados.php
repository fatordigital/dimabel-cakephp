<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include 'controller.php';

class Veiculos_pesados extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->view('veiculos_pesados');
    }


}
