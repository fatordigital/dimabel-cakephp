<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('coinToBco')) {
    function coinToBco($get_valor)
    {
        if (strstr($get_valor, ',')) {
            $source = array(
                '.',
                ','
            );
            $replace = array(
                '',
                '.'
            );
            $valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto
        } else
            $valor = $get_valor;

        return $valor; //retorna o valor formatado para gravar no banco
    }
}

if (!function_exists('bcoToCoin')) {
    function bcoToCoin($value)
    {
        return number_format($value, 2, ',', '.');
    }
}

if (!function_exists('simpleInstallment')) {
    function simpleInstallment($value, $installment)
    {
        $division = coinToBco($value) / (int)$installment;
        return bcoToCoin($division);
    }
}

if (!function_exists('captcha_fator')) {
    function captcha_fator($other_key = null)
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST[!is_null($other_key) ? $other_key : 'fd'])) {
                $captcha_field = $_POST[!is_null($other_key) ? $other_key : 'fd'];
                if (!empty($captcha_field) || $captcha_field != '') {
                    alert('error', 'Não foi possível enviar receber seus dados, por favor, tente novamente. Caso o erro persista, tente novamente mais tarde.');
                    return false;
                }
            } else if (!isset($_POST[!is_null($other_key) ? $other_key : 'fd'])) {
                alert('error', 'Não foi possível enviar receber seus dados, por favor, tente novamente. Caso o erro persista, tente novamente mais tarde.');
                return false;
            }
            return true;
        }
    }
}

if (!function_exists('return_refer')) {
    function return_refer()
    {
        if (isset($_POST['return_url']) && !empty($_POST['return_url'])) {
            redirect($_POST['return_url']);
        } else if (isset($_GET['return_url']) && !empty($_GET['return_url'])) {
            redirect($_POST['return_url']);
        } else {
            redirect(site_url());
        }
    }
}

if (!function_exists('old')) {
    function old($input_name = null)
    {
        if (!is_null($input_name)) {
            $errors = $GLOBALS['ci']->session->flashdata('error_data');
            if (isset($errors[$input_name]))
                return $errors[$input_name];
            else if (isset($errors[$input_name]))
                return $errors[$input_name];
        }
        return '';
    }
}

if (!function_exists('session')) {
    function alert($type, $msg)
    {
        $GLOBALS['ci']->session->set_flashdata($type, $msg);
    }
}

if (!function_exists('phone_validate')) {
    function phone_validate($number = null)
    {
        if (!is_null($number)) {
            if (preg_match('/^\(\d{2}\) \d{4}-\d{4}$/', $number)) {
                return true;
            }
            if (preg_match('/^\(\d{2}\) \d{5}-\d{4}$/', $number)) {
                return true;
            }
        }
        return false;
    }
}

if (!function_exists('setting')) {
    function settings ($key) {
        if ($GLOBALS['ci']->settings) {
            if (isset($GLOBALS['ci']->settings[$key])) {
                return $GLOBALS['ci']->settings[$key];
            }
        }
        return '';
    }
}


function base64_encode_image ($url = null) {
    $img_url = $url;
    $b64_url = 'php://filter/read=convert.base64-encode/resource='.$img_url;
    $b64_img = file_get_contents($b64_url);
    return 'data:image/' . pathinfo($img_url, PATHINFO_EXTENSION) . ';base64,' . $b64_img;
}


function image($object_id, $db, $key = null)
{
    $query = $db->query('
    SELECT p.*
      FROM wp_postmeta AS pm
     INNER JOIN wp_posts AS p ON pm.meta_value=p.ID 
     WHERE pm.post_id = ' . $object_id . '
       AND pm.meta_key = "_thumbnail_id"
     ORDER BY p.post_date DESC 
     LIMIT 1
    ');
    if ($query->num_rows > 0) {
        $return = $query->row();
        if (is_null($key))
            return $return;
        else {
            return $return->{$key};
        }
    }
}
