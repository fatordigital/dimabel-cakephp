<?php
$cam = "";
$bt1 = '';
$bt2 = '';
$bt3 = '';
$bt4 = '';
$bt5 = '';
$bt6 = 'active';
$bt7 = '';
$bt8 = '';
$bt9 = '';

$sel = '';
?>
<!-- Page Wrap -->
<div class="page" id="top">    
    <?php include_once("includes/menu.php"); ?>
    <!-- Head Section -->
    <section class="small-section bg-dark-alfa-90 bg-scroll" data-background="<?php echo getLink(); ?>views/imagens/simular.jpg">
        <div class="relative container align-left">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="section-heading mb-10 mb-xs-0">Simulador</h1>
                    <div class="hs-line-4 uppercase">O que deseja Adquirir?</div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Head Section -->
    <!-- Section -->
    <section class="page-section" style="padding:80px 0;">
        <div class="relative container">
            <div class="section-text">
                <?php if ($this->session->userdata('plan')) { ?>
                    <div class="row">
                        <h4 style="margin:10px 0; font-weight:700; color:#0072ab;" class="hide"><?php echo $this->session->userdata('propaganda'); ?></h4>
                        <h6 style="margin:10px 0; font-weight:700; color:#f6832b;" class="hide">Valor do Consórcio: R$ <?php echo $this->session->userdata('valor'); ?></h6>
                        <h6 style="margin:10px 0; font-weight:700; color:#f6832b;" class="hide">Número de Parcelas: <?php echo $this->session->userdata('parcelas'); ?> meses</h6>
                        <h6 style="margin:10px 0; font-weight:700; color:#f6832b;" class="hide">Valor da Parcelas: R$ <?php echo $this->session->userdata('valor_parcela'); ?> por mês</h6>
                        <h4 style="margin:39px 0 10px 0; font-weight:700; color:#0072ab;" class="hide">Quero Contratar o Consórcio:</h4>
                        <!-- BEGIN NOVO BLOCO COM VALORES -->
                        <div class="container-result">
                            <div class="row"><div class="col-xs-12"><h4 class="text-center"><?php echo $this->session->userdata('propaganda'); ?></h4></div></div>
                            <div class="row">
                                <div class="col-xs-12 col-md-7"><h2>Valor do Consórcio: <strong><?php echo 'R$'.$this->session->userdata('valor'); ?></strong></h2></div>
                                <div class="col-xs-12 col-md-5"><h2 class="text-right">Número de Parcelas: <strong><?php echo $this->session->userdata('parcelas').' meses'; ?></strong></h2></div>
                            </div>
                            <div class="row margin-0">
                                <div class="col-xs-12 col-md-7 ns-portion">
                                    <p class="portion-value">
                                        Valor da Parcela: 
                                        <span>R$
                                            <strong><?php echo substr($this->session->userdata('valor_parcela'), 0, strlen($this->session->userdata('valor_parcela')) - 3); ?></strong>,
                                            <?php echo substr($this->session->userdata('valor_parcela'), strlen($this->session->userdata('valor_parcela')) - 2, 2);?> por mês
                                        </span>
                                    </p>
                                </div>
                                <div class="col-xs-12 col-md-5 ns-cpf">
                                    <p>Gostou? Inicie a contratação desse consórcio junto à nossa equipe de atendimento:</p>
                                    <form method="post" action="<?php echo base_url('/simulador/submit_con') ?>" id="formulario-simulador" class="form">
                                        <div class="input-group">
                                            <!-- Email -->
                                            <input type="text" name="cpf" class="form-control cpf" placeholder="PREENCHA SEU CPF">
                                            <input type="hidden" name="nome" value="<?php echo $this->session->userdata('nome'); ?>">
                                            <input type="hidden" name="email" value="<?php echo $this->session->userdata('email'); ?>">
                                            <input type="hidden" name="fone" value="<?php echo $this->session->userdata('telefone'); ?>">
                                            <input type="hidden" name="tipo" value="<?php echo $this->session->userdata('valor_parcela'); ?>">
                                            <input type="hidden" name="tp" value="<?php echo $this->session->userdata('propaganda'); ?>">
                                            <input type="hidden" name="valor" value="<?php echo $this->session->userdata('valor'); ?>">
                                            <input type="hidden" name="parcelas" value="<?php echo $this->session->userdata('parcelas'); ?>">
                                            <input type="hidden" name="val" value="<?php echo $this->session->userdata('valor'); ?>">
                                            <span class="input-group-btn"><button type="submit" name="submitcon" class="btn btn-primary">Contratar</button></span>
                                        </div><!-- /input-group -->
                                    </form>
                                </div>
                            </div>
                            <div class="row row-info"><div class="col-xs-12"></div></div>
                            <hr class="ns">
                        </div><!-- END NOVO BLOCO COM VALORES -->
                        <?php include APPPATH . 'views/includes/simulador-forms/simulador-parcelas.php'; ?>
                    </div>
                <?php } else { ?>
                    <?php include APPPATH . 'views/includes/simulador-forms/simulador-parcelas.php'; ?>
                <?php } ?>
            </div>
        </div>
    </section>
    <!-- End Section -->
    <?php include_once("includes/base.php"); ?>
</div>
<!-- End Page Wrap -->´