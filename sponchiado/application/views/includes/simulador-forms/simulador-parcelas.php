<h4 class="text-center aga4">Se desejar, você pode fazer uma nova simulação para encontrar a melhor parcela para você:</h4>
<!-- Contact Form -->
<div class="row mt-20">
    <div class="col-md-12">
        <form method="post" action="<?php echo base_url('/simulador/submit_plan') ?>" id="simulador-parcelas" class="form">
            <input type="hidden" name="return_url" value="<?php echo current_url() ?>" />
            <div class="fd_key"><input type="text" name="fd" value=""/></div>
            <div class="row">
                <div class="col-xs-10 col-xs-offset-1 col-md-6 col-md-offset-3">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="mb-30">
                                <select class="form-control input-lg" name="tipo" onchange="muda(this.value)">
                                    <option value="">SELECIONE O BEM</option>
                                    <option <?php if (isset($this->session->userdata['tipo']) && $this->session->userdata['tipo'] == 1) { ?> selected <?php } ?> value="1">Imóvel</option>
                                    <option <?php if (isset($this->session->userdata['tipo']) && $this->session->userdata['tipo'] == 2) { ?> selected <?php } ?> value="2">Automóvel</option>
                                    <option <?php if (isset($this->session->userdata['tipo']) && $this->session->userdata['tipo'] == 3) { ?> selected <?php } ?> value="3">Caminhões</option>
                                    <option <?php if (isset($this->session->userdata['tipo']) && $this->session->userdata['tipo'] == 4) { ?> selected <?php } ?> value="4">Máquinas Agrícolas</option>
                                </select>
                            </div>
                            <?php if (isset($this->session->userdata['tipo']) && $this->session->userdata['tipo'] == 1) { ?>
                                <div class="mb-30">
                                    <select class="form-control input-lg" name="parcelas" id="parcelas">
                                        <option value="">PARCELAS</option>
                                        <option <?php if (isset($this->session->userdata['parcelas']) && $this->session->userdata['parcelas'] == 180) { ?> selected <?php } ?>value="180">180 Meses</option>
                                    </select>
                                </div>
                            <?php } elseif (isset($this->session->userdata['tipo']) && $this->session->userdata['tipo'] == 2) { ?>
                                <div class="mb-30">
                                    <select class="form-control input-lg" name="parcelas" id="parcelas">
                                        <option value="">PARCELAS</option>
                                        <option <?php if (isset($this->session->userdata['parcelas']) && $this->session->userdata['parcelas'] == 60) { ?> selected <?php } ?> value="60">60 Meses</option>
                                        <option <?php if (isset($this->session->userdata['parcelas']) && $this->session->userdata['parcelas'] == 72) { ?> selected <?php } ?> value="72">72 Meses</option>
                                        <option <?php if (isset($this->session->userdata['parcelas']) && $this->session->userdata['parcelas'] == 84) { ?> selected <?php } ?> value="84">84 Meses</option>
                                        <option <?php if (isset($this->session->userdata['parcelas']) && $this->session->userdata['parcelas'] == 100) { ?> selected <?php } ?> value="100">100 Meses</option>
                                    </select>
                                </div>
                            <?php } elseif (isset($this->session->userdata['tipo']) && ($this->session->userdata['tipo'] == 3 || $this->session->userdata['tipo'] == 4)) { ?>
                                <div class="mb-30">
                                    <select class="form-control input-lg" name="parcelas" id="parcelas">
                                        <option value="">PARCELAS</option>
                                        <option <?php if (isset($this->session->userdata['parcelas']) && $this->session->userdata['parcelas'] == 80) { ?> selected <?php } ?> value="80">80 Meses</option>
                                        <option <?php if (isset($this->session->userdata['parcelas']) && $this->session->userdata['parcelas'] == 100) { ?> selected <?php } ?> value="100">100 Meses</option>
                                        <option <?php if (isset($this->session->userdata['parcelas']) && $this->session->userdata['parcelas'] == 120) { ?> selected <?php } ?> value="120">120 Meses</option>
                                    </select>
                                </div>
                            <?php } else { ?>
                                <div class="mb-30">
                                    <select class="form-control input-lg" name="parcelas" id="parcelas">
                                        <option value="na">PARCELAS</option>
                                    </select>
                                </div>
                            <?php } ?>
                            <div class="mb-30">
                                <input type="email" name="email" value="<?php echo $this->session->userdata['email']; ?>"
                                       class="form-control input-lg email" placeholder="E-MAIL" required
                                       oninvalid="setCustomValidity('Por favor, preencha seu E-mail')"
                                       onchange="try{setCustomValidity('');}catch(e){}">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="mb-30">
                                <input type="text" name="valor" value="<?php echo $this->session->userdata['valor']; ?>"
                                       class="form-control input-lg" placeholder="VALOR"
                                       onkeyup="mascara(this, mvalor);" required
                                       oninvalid="setCustomValidity('Por favor, preencha um Valor')"
                                       onchange="try{setCustomValidity('');}catch(e){}">
                            </div>
                            <div class="mb-30">
                                <input type="text" name="nome" value="<?php echo $this->session->userdata['nome']; ?>"
                                       class="form-control input-lg" placeholder="NOME" required
                                       oninvalid="setCustomValidity('Por favor, preencha seu Nome')"
                                       onchange="try{setCustomValidity('');}catch(e){}">
                            </div>
                            <div class="mb-30">
                                <input type="text" name="telefone" value="<?php echo $this->session->userdata['telefone']; ?>"
                                       class="form-control input-lg telefone" placeholder="TELEFONE"
                                       required
                                       oninvalid="setCustomValidity('Por favor, preencha seu Telefone')"
                                       onchange="try{setCustomValidity('');}catch(e){}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <input type="hidden" name="submitplan"/>
                        <div class="col-md-12">
                            <button id="simulador-parcelas" style="width:100%;" class="btn btn-mod btn-color btn-large btn-block"> Ver Parcelas </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- End Contact Form -->