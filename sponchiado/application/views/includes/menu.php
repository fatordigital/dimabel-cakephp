<!-- Navigation panel -->
	<nav class="main-nav navbar-fixed-top">
		<div class="container-fluid nwrapper-menu">
            <div class="container">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="<?php echo site_url('/') ?>" class="logo">
                            <img src="<?php echo getLink() ?>views/imagens/logo.png" alt="" />
                        </a>
                        <a href="" class="logo sponchiado">
                            <img src="<?php echo getLink() ?>views/imagens/sponchiado-logo.png" alt="Sponchiado" />
                        </a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="nwrapper-menu-text">
                        <p>Dúvidas sobre o consórcio ideal? Taxas? Formas de pagamento? Fale com nossa <strong>Central de Vendas</strong></p>
                    </li>
                    <li>
                        <a href="javascript:void(0)" class="nwrapper-menu-item-1"><img src="<?php echo getLink(); ?>views/imagens/nfooter-telefone.png" alt=""> <?php echo settings('Telefone') ?></a>
                    </li>
                    <?php /*<li>
                        <a href="javascript:void(0)" class="nwrapper-menu-item-2">
                            <img src="<?php echo getLink(); ?>views/imagens/nfooter-whatsapp.png" alt="">
                            <small>(51)</small> 98144.5555
                        </a>
                    </li> */ ;?>
                </ul>
            </div>
		</div>
	</nav>
	<!-- End Navigation panel -->
