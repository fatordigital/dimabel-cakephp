<?php

$cam = "";

$bt1 = 'active';
$bt2 = '';
$bt3 = '';
$bt4 = '';
$bt5 = '';
$bt6 = '';
$bt7 = '';
$bt8 = '';
$bt9 = '';

?>


<!-- Page Wrap -->
<div class="page" id="top">

    <?php include_once("includes/menu.php"); ?>

    <!-- Fullwidth Slider -->
    <div class="bg-dark relative" id="home">
        <div class="fullwidth-gallery">

            <!-- Slide Item -->
            <section class="home-section bg-scroll bg-dark-alfa-50"
                     data-background="<?php echo site_url('/') . 'application/'; ?>views/imagens/capa_1.jpg">
                <div class="js-height-full">
                </div>
            </section>
            <!-- End Slide Item -->

        </div>
        <!-- End Fullwidth Slider -->

        <!-- Header Content -->
        <div class="js-height-full fullwidth-galley-content">

            <!-- Hero Content -->
            <div class="home-content container">
                <div class="home-text">

                    <h1 class="hs-line-2 mt-0 mb-0 mb-xs-20">
                        SIMULE SEU CONSÓRCIO <span class="orange">SPONCHIADO</span> ONLINE
                    </h1>


                    <form method="post" action="<?php echo base_url('/simulador/submit_plan') ?>" id="form-5" class="form">
                        <input type="hidden" value="1" name="from_home" />
                        <input type="hidden" name="return_url" value="<?php echo current_url() ?>"/>

                        <div class="row">
                            <div class="fd_key">
                                <input type="text" name="fd" value="" />
                            </div>
                            <div class="col-xs-12 col-md-3 mb-20">
                                <select class="form-control input-lg" name="tipo" onchange="muda(this.value)">
                                    <option value="">SELECIONE O BEM</option>
                                    <option value="1">Imóvel</option>
                                    <option value="2">Automóvel</option>
                                    <?php /*
                                    <option value="3">Veículo Pesado</option>
                                    */ ?>
                                </select>
                            </div>
                            <div class="col-xs-12 col-md-3 mb-20">
                                <!-- Email -->
                                <input type="text" name="valor" class="form-control input-lg" placeholder="VALOR"
                                       onkeyup="mascara(this, mvalor);" required
                                       oninvalid="setCustomValidity('Por favor, preencha um Valor')"
                                       onchange="try{setCustomValidity('')}catch(e){}">
                            </div>
                            <div class="col-xs-12 col-md-3 mb-20">
                                <select class="form-control input-lg" name="parcelas" id="parcelas">
                                    <option value="na">PARCELAS</option>
                                </select>
                            </div>
                            <?php /* <div class="col-xs-12 col-md-3 text-right hidden-sm hidden-xs">
                                <span><small class="green">OU SIMULE PELO WHATSAPP</small><br /><small>(51)</small> 98144.5555</span>
                            </div> */ ;?>
                        </div>


                        <div class="row">
                            <div class="col-xs-12 col-md-3 mb-20">
                                <input type="text" name="nome" class="form-control input-lg" placeholder="SEU NOME">
                            </div>
                            <div class="col-xs-12 col-md-3 mb-20">
                                <input type="email" name="email" class="form-control input-lg" placeholder="SEU EMAIL">
                            </div>
                            <div class="col-xs-12 col-md-3 mb-20">
                                <input type="text" name="telefone" class="form-control input-lg telefone" placeholder="SEU TELEFONE">
                            </div>
                            <?php /* <div class="col-xs-12 col-md-3 text-right hidden-md hidden-lg">
                                <span><small class="green">OU SIMULE PELO WHATSAPP</small><br /><small>(51)</small> 99572.7345</span>
                            </div>*/ ;?>
                            <div class="col-xs-12 col-md-3 mb-20">
                                <div class="mb-10">
                                    <button style="width:100%" id="button-home-simulador" type="submit" name="submit"
                                            class="btn btn-mod btn-color btn-large">
                                        Continuar
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="row row-info">
                            <div class="col-xs-12">
                                <p>
                                    A forma planejada de adquirir seu imóvei ou veículo!
                                </p>
                            </div>
                        </div>

                    </form>

                    <div class="row hide">
                        <div class="col-xs-12 col-xs-offset-0 col-sm-offset-2 col-sm-10">
                            <p class="nhome-text-small">
                                Taxas: imóvel - 19% taxa de administração + 5% fundo de reserva<br/>
                                Automóveis - 17% taxa de administração + 3% fundo de reserva
                            </p>
                        </div>
                    </div>

                </div>
            </div>
            <!-- End Hero Content -->

            <!-- Scroll Down -->
            <div class="local-scroll hide">
                <a href="#entenda" class="scroll-down"><i class="fa fa-angle-down scroll-down-icon"></i></a>
            </div>
            <!-- End Scroll Down -->

        </div>
    </div>
    <!-- End Fullwidth Slider -->

    <!-- Blog Section -->
    <section class="page-section bg-gray" style="padding:80px 0" id="entenda">
        <div class="container relative">

            <div class="align-left">

                <h2 class="section-heading uppercase strong" style="color:#44ae88">
                    Entenda o Consórcio
                </h2>
                <!-- End Section Titles -->

                <div class="clearfix">
                    <div class="section-line left mt-0 mb-80 mb-xs-40"></div>
                </div>

            </div>


            <div class="row multi-columns-row">

                <!-- Post Item -->
                <div class="col-sm-12 mb-md-50 wow fadeIn" data-wow-delay="0.1s"
                     data-wow-duration="2s">

                    <div class="section-text mb-20" style="text-align:justify">
                        <h3 class="mb-10 mt-0">O que é consórcio e como funciona?</h3>
                        O consórcio é a reunião de pessoas físicas e/ou jurídicas, em grupo fechado, promovido por uma
                        Administradora, com prazo de duração previamente estabelecido, para propiciar aos seus
                        integrantes a aquisição de bem por meio de autofinanciamento.
                    </div>

<!--                    <div class="post-prev-more mt-10">-->
<!--                        --><?php //echo anchor('entenda', 'Saiba Mais', 'class="btn btn-mod btn-color btn-large"'); ?>
<!--                    </div>-->

                </div>
                <!-- End Post Item -->


                <?php /*
                <!-- Post Item -->
                <div class="col-sm-6 col-md-6 col-lg-6 mb-md-50 wow fadeIn" data-wow-delay="0.1s"
                     data-wow-duration="2s">

                    <div class="video">
                        <iframe width="640" height="360" src="https://www.youtube.com/embed/95WtLcFYHc0"
                                allowfullscreen></iframe>
                    </div>


                </div>
                <!-- End Post Item -->
                */ ?>


            </div>

        </div>
    </section>
    <!-- End Blog Section -->



    <!-- Blog Section -->
    <section class="page-section" style="padding:80px 0">
        <div class="container relative">

            <div class="align-left">

                <h2 class="section-heading uppercase strong" style="color:#44ae88">
                    Nossos Produtos
                </h2>
                <!-- End Section Titles -->

                <div class="clearfix">
                    <div class="section-line left mt-0 mb-80 mb-xs-40"></div>
                </div>

            </div>


            <div class="row multi-columns-row">

                <!-- Post Item -->
                <div class="col-sm-6 col-md-6 col-lg-6 mb-md-50 wow fadeIn" data-wow-delay="0.1s"
                     data-wow-duration="2s">

                    <div class="post-prev-img post-prev-border">
                        <img src="<?php echo site_url('/') . 'application/'; ?>views/imagens/capa_1.jpg" alt=""/>
                    </div>

                    <div class="post-prev-title uppercase strong">
                        Imobiliário
                    </div>

<!--                    <div class="post-prev-more mt-10">-->
<!--                        --><?php //echo anchor('imoveis', 'Saiba Mais', 'class="btn btn-mod btn-color"'); ?>
<!--                    </div>-->

                </div>
                <!-- End Post Item -->


                <!-- Post Item -->
                <div class="col-sm-6 col-md-6 col-lg-6 mb-md-50 wow fadeIn" data-wow-delay="0.1s"
                     data-wow-duration="2s">

                    <div class="post-prev-img post-prev-border">
                        <img src="<?php echo site_url('/') . 'application/'; ?>views/imagens/capa_2.jpg" alt=""/>
                    </div>

                    <div class="post-prev-title uppercase strong">
                        Veículos
                    </div>

<!--                    <div class="post-prev-more mt-10">-->
<!--                        --><?php //echo anchor('veiculos', 'Saiba Mais', 'class="btn btn-mod btn-color"'); ?>
<!--                    </div>-->

                </div>
                <!-- End Post Item -->


                <?php /*
				<!-- Post Item -->
				<div class="col-sm-6 col-md-4 col-lg-4 mb-md-50 wow fadeIn" data-wow-delay="0.1s" data-wow-duration="2s">

					<div class="post-prev-img post-prev-border">
						<a href="blog-single-sidebar-right.html"><img src="<?php echo '/'; ?>views/imagens/capa_3.jpg" alt="" /></a>
					</div>

					<div class="post-prev-title uppercase strong">
						<a href="">Veículos Pesados</a>
					</div>

					<div class="post-prev-more mt-10">
						<?php echo anchor('veiculos_pesados', 'Saiba Mais', 'class="btn btn-mod btn-color"'); ?>
					</div>

				</div>
				<!-- End Post Item -->
                */ ?>

            </div>

        </div>
    </section>
    <!-- End Blog Section -->


    <?php if(isset($posts) && $posts) { ?>
    <!-- Blog Section -->
    <section class="page-section" style="padding:80px 0">
        <div class="container relative">

            <div class="align-left">

                <h2 class="section-heading uppercase strong" style="color:#44ae88">
                    BLOG
                </h2>
                <!-- End Section Titles -->

                <div class="clearfix">
                    <div class="section-line left mt-0 mb-80 mb-xs-40"></div>
                </div>

            </div>


            <div class="row multi-columns-row">

                <?php foreach ($posts as $post) { ?>

                <!-- Post Item -->
                <div class="col-sm-6 col-md-6 col-lg-6 mb-md-50 wow fadeIn" data-wow-delay="0.1s"
                     data-wow-duration="2s">

                    <div class="post-prev-img post-prev-border">
                        <a href="<?php echo '/blog/'. $post->post_name; ?>">
                            <img src="<?php echo str_replace('http://', 'https://', image($post->ID, $db, 'guid')); ?>" alt="" />
                        </a>
                    </div>

                    <div class="post-prev-title uppercase strong">
                        <a href=""><?php echo $post->post_title ?></a>
                    </div>

                    <div class="post-prev-more mt-10">
                        <a href="<?php echo '/blog/' . $post->post_name ?>" class="btn btn-mod btn-color">
                            Saiba mais
                        </a>
                    </div>

                </div>
                <!-- End Post Item -->


                <?php } ?>

            </div>

        </div>
    </section>
    <!-- End Blog Section -->
    <?php } ?>


    <?php include_once("includes/base.php"); ?>

</div>
<!-- End Page Wrap -->
