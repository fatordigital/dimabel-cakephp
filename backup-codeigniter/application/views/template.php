<html>
<head>

    <title>DIMABEL Consórcios</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta charset="utf-8">
    <meta name="author" content="Roman Kirichik">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo getLink(); ?>views/favi/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo getLink(); ?>views/favi/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo getLink(); ?>views/favi/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo getLink(); ?>views/favi/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo getLink(); ?>views/favi/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo getLink(); ?>views/favi/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo getLink(); ?>views/favi/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo getLink(); ?>views/favi/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo getLink(); ?>views/favi/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo getLink(); ?>views/favi/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo getLink(); ?>views/favi/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo getLink(); ?>views/favi/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo getLink(); ?>views/favi/favicon-16x16.png">
    <link rel="manifest" href="<?php echo getLink(); ?>views/favi/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- CSS -->
    <link rel="stylesheet" href="<?php echo getLink(); ?>views/css/fonts.google.css">
    <link rel="stylesheet" href="<?php echo getLink(); ?>views/css/bootstrap.min.css">

    <!-- Toastr -->
    <link rel="stylesheet" href="<?php echo getLink(); ?>views/plugins/toastr/toastr.min.css">
    <!-- Toastr -->

    <link rel="stylesheet" href="<?php echo getLink(); ?>views/css/style.css?">
    <link rel="stylesheet" href="<?php echo getLink(); ?>views/css/style-responsive.css">
    <link rel="stylesheet" href="<?php echo getLink(); ?>views/css/animate.min.css">
    <link rel="stylesheet" href="<?php echo getLink(); ?>views/css/vertical-rhythm.min.css">
    <link rel="stylesheet" href="<?php echo getLink(); ?>views/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo getLink(); ?>views/css/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo getLink(); ?>views/css/colors/orange.css">

    <?php if (ENVIRONMENT != 'development') { ?>
        <script>
             (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-99254723-1', 'auto');
			ga('require', 'displayfeatures');
            ga('send', 'pageview');

        </script>


        <!--Start of Zendesk Chat Script-->
        <script type="text/javascript">
            window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
                d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
            _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
                $.src="https://v2.zopim.com/?4mavKOMvJmKiiTRy6TITxDLftQ9L00pk";z.t=+new Date;$.
                    type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
        </script>
        <!--End of Zendesk Chat Script-->

        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-TSV6NGX');</script>
        <!-- End Google Tag Manager -->

        <script>
            <?php
            if ($this->session->flashdata('goal_simulador')) {
                echo $this->session->flashdata('goal_simulador');
                $this->session->unset_userdata('goal_simulador');
            }
            ?>
        </script>

        <!-- Start of HubSpot Embed Code -->
        <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/3373993.js"></script>
        <!-- End of HubSpot Embed Code -->

    <?php } ?>


</head>


<body class="appear-animate">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TSV6NGX" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php echo $contents; ?>

<!-- JS -->
<script type="text/javascript" src="<?php echo getLink(); ?>views/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="<?php echo getLink(); ?>views/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="<?php echo getLink(); ?>views/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo getLink(); ?>views/js/swiffy-v7.4.js"></script>
<script type="text/javascript" src="<?php echo getLink(); ?>views/js/fator.js"></script>

<!-- Toastr -->
<script type="text/javascript" src="<?php echo getLink(); ?>views/plugins/toastr/toastr.min.js"></script>
<!-- Toastr -->

<script type="text/javascript">
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-full-width",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    <?php
        if ($this->session->flashdata('error')) {
            echo 'toastr.error("'.$this->session->flashdata('error').'")';
        }
    if ($this->session->flashdata('success')) {
        echo 'toastr.success("'.$this->session->flashdata('success').'")';
    }
    ?>
</script>

<script type="text/javascript" src="<?php echo getLink(); ?>views/js/SmoothScroll.js"></script>
<script type="text/javascript" src="<?php echo getLink(); ?>views/js/jquery.scrollTo.min.js"></script>
<script type="text/javascript" src="<?php echo getLink(); ?>views/js/jquery.localScroll.min.js"></script>
<script type="text/javascript" src="<?php echo getLink(); ?>views/js/jquery.viewport.mini.js"></script>
<script type="text/javascript" src="<?php echo getLink(); ?>views/js/jquery.countTo.js"></script>
<script type="text/javascript" src="<?php echo getLink(); ?>views/js/jquery.appear.js"></script>
<script type="text/javascript" src="<?php echo getLink(); ?>views/js/jquery.sticky.js"></script>
<script type="text/javascript" src="<?php echo getLink(); ?>views/js/jquery.parallax-1.1.3.js"></script>
<script type="text/javascript" src="<?php echo getLink(); ?>views/js/jquery.fitvids.js"></script>
<script type="text/javascript" src="<?php echo getLink(); ?>views/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo getLink(); ?>views/js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="<?php echo getLink(); ?>views/js/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="<?php echo getLink(); ?>views/js/jquery.magnific-popup.min.js"></script>
<!-- Replace test API Key "AIzaSyAZsDkJFLS0b59q7cmW0EprwfcfUA8d9dg" with your own one below
**** You can get API Key here - https://developers.google.com/maps/documentation/javascript/get-api-key -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZsDkJFLS0b59q7cmW0EprwfcfUA8d9dg"></script>
<script type="text/javascript" src="<?php echo getLink(); ?>views/js/gmap3.min.js"></script>
<script type="text/javascript" src="<?php echo getLink(); ?>views/js/wow.min.js"></script>
<script type="text/javascript" src="<?php echo getLink(); ?>views/js/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="<?php echo getLink(); ?>views/js/jquery.simple-text-rotator.min.js"></script>
<script type="text/javascript" src="<?php echo getLink(); ?>views/js/jquery.mask.min.js"></script>

<!-- Simulador -->
<script type="text/javascript" src="<?php echo getLink(); ?>views/js/simulador.js?time=<?php echo time() ?>"></script>
<!-- Simulador -->

<script type="text/javascript" src="<?php echo getLink(); ?>views/js/all.js?time=<?php echo time() ?>"></script>
<script type="text/javascript" src="<?php echo getLink(); ?>views/js/contact-form.js"></script>
<script type="text/javascript" src="<?php echo getLink(); ?>views/js/jquery.ajaxchimp.min.js"></script>
<!--[if lt IE 10]><script type="text/javascript" src="<?php echo getLink(); ?>views/js/placeholder.js"></script><![endif]-->

<?php if (ENVIRONMENT != 'development') { ?>
<script type="text/javascript" src="<?php echo getLink(); ?>views/includes/goals.js"></script>
<?php } ?>


</body>
</html>