<?php if ($this->session->flashdata('contato_home')) {?>
    <script>
        ga('send', 'pageview', '/goal/contato-home');
    </script>
<?php unset($goal_contato_home); } ?>

<?php if ($this->session->flashdata('contato')) {?>
    <script>
        ga('send', 'pageview', '/goal/contato');
    </script>
<?php unset($goal_contato); } ?>

<footer class="nfooter">
		<div class="topo">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-3">
						<p><big>FAÇA CONTATO</big></p>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-5 col-lg-4">
						<p>Dúvidas sobre o consórcio ideal? Taxas? Formas de pagamento? Fale com nossa <span>Central de Vendas</span></p>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-4">
						<p><img src="<?php echo getLink(); ?>views/imagens/nfooter-telefone.png" alt=""><?php echo settings('Telefone') ?></p>
					</div>
				</div>
			</div>
		</div>
		<div class="nmapa">
			<div class="container">
				<div class="nform">
					<h3>Entre em contato com a Dimabel Consórcios</h3>
                    <form id="form-base" action="<?=base_url('/home/contact')?>" method="post">
                        <div class="fd_key">
                            <input type="text" name="fd" value="" />
                        </div>
                        <input type="hidden" name="contato_home" />
						<div class="form-group">
							<input name="name" value="<?=set_value('name')?>" type="text" class="form-control" placeholder="Nome" required oninvalid="setCustomValidity('Por favor, preencha seu Nome')" onchange="try{setCustomValidity('')}catch(e){}">
						</div>
						<div class="form-group">
							<input name="email" value="<?=set_value('email')?>" type="email" class="form-control" placeholder="E-mail" required oninvalid="setCustomValidity('Por favor, preencha seu E-mail')" onchange="try{setCustomValidity('')}catch(e){}">
						</div>
						<div class="form-group">
							<input name="phone" value="<?=set_value('phone')?>" type="text" class="form-control" placeholder="Telefone" required oninvalid="setCustomValidity('Por favor, preencha seu Telefone')" onchange="try{setCustomValidity('')}catch(e){}" onkeyup="mascara(this, mtel);">
						</div>
						<div class="form-group">
							<textarea name="message" class="form-control" placeholder="Mensagem" required oninvalid="setCustomValidity('Por favor, preencha sua Mensagem')" onchange="try{setCustomValidity('')}catch(e){}"><?=set_value('message')?></textarea>
						</div>
                        <input type="hidden" name="return_url" value="<?php echo current_url() ?>">
                        <button
                            class="btn btn-submit-nform"
                            id="contato-home">
                            Enviar Mensagem
                        </button>
					</form>
				</div>
			</div>
			<div id="map"></div>
		</div>
		<div class="nbottom">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-3">
						<img src="<?php echo getLink(); ?>views/imagens/logo-footer.png" alt="DIMABEL" class="img-responsive">
						<p>CNPJ: 07.492.576/0001-62 - Dimabel Corretora de Seguros e Negócios Ltda</p>
					</div>
                    <div class="col-xs-12 col-sm-6 col-sm-pull-4 col-md-4 col-sm-push-5">
						<div class="nfooter-bg">
							<p>
								Consórcio é coisa séria! <span class="yellow">Denuncie Irregularidades</span> através da Ouvidoria da Caixa
							</p>
							<p>
								<small>0800 702 4240 de segunda a sexta - das 8h às 18h.</small>
							</p>
						</div>
					</div>
                    <div class="col-xs-12 col-sm-8 col-sm-offset-2  col-sm-pull-0 col-md-5 col-md-offset-0 col-md-pull-4">
						<p class="blue">Quer adquirir o consórcio pessoalmente?</p>
						<p class="orange">VISITE NOSSA MATRIZ EM SÃO LEOPOLDO/RS</p>
						<p>Av. João Correa, 503, sala 2 - São Leopoldo/RS - CEP 93040-035</p>
						<hr>
						<div class="row ncontato">
							<div class="col-xs-12 col-sm-6">
								<p><img src="<?php echo getLink(); ?>views/imagens/nfooter-telefone.png" alt=""> <span style="font-size: 21px;"><?php echo settings('Telefone') ?></span></p>
							</div>
							<?php /*<div class="col-xs-12 col-sm-6 no-padding-margin">
								<p><img src="<?php echo getLink(); ?>views/imagens/nfooter-whatsapp.png" alt=""> <small>(51)</small> 98144.5555</p>
							</div> */;?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>