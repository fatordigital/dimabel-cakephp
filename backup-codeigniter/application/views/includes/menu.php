<!-- Navigation panel -->
	<nav class="main-nav navbar-fixed-top">
		<div class="container-fluid nwrapper-menu">
            <div class="container">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="<?php echo site_url('/') ?>" class="logo" >
                            <img src="<?php echo getLink() ?>views/imagens/logo.png" alt="" />
                        </a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="nwrapper-menu-text">
                        <p>Dúvidas sobre o consórcio ideal? Taxas? Formas de pagamento? Fale com nossa <strong>Central de Vendas</strong></p>
                    </li>
                    <li>
                        <a href="javascript:void(0)" class="nwrapper-menu-item-1"><img src="<?php echo getLink(); ?>views/imagens/nfooter-telefone.png" alt=""> <?php echo settings('Telefone') ?></a>
                    </li>
                    <?php /*<li>
                        <a href="javascript:void(0)" class="nwrapper-menu-item-2">
                            <img src="<?php echo getLink(); ?>views/imagens/nfooter-whatsapp.png" alt="">
                            <small>(51)</small> 98144.5555
                        </a>
                    </li> */ ;?>
                </ul>
            </div>
		</div>
		<div class="container relative clearfix">
			<!-- Logo ( * your text or image into link tag *) -->
			<?php /*<div class="nav-logo-wrap local-scroll">
				<?php echo anchor('home', '<img src="'.$cam.'application/views/imagens/logo.png" alt="" />', 'class="logo"'); ?>
			</div> */; ?>
			<div class="mobile-nav">
				<i class="fa fa-bars"></i>
			</div>
			<!-- Main Menu -->
			<div class="inner-nav desktop-nav">
				<ul class="clearlist navbar-right">
					<li><?php echo anchor('home', 'Home', 'class="mn-has-sub '.$bt1.'"'); ?></li>
					<li><?php echo anchor('entenda', 'Entenda o Consórcio', 'class="mn-has-sub '.$bt2.'"'); ?></li>
					<li>
						<a href="#" class="mn-has-sub <?php echo $bt3; ?>">Nossos Produtos <i class="fa fa-angle-down"></i></a>

						<!-- Sub -->
						<ul class="mn-sub to-left">
							<li><?php echo anchor('imoveis', 'Consórcio Imobiliário', ''); ?></li>
							<li><?php echo anchor('veiculos', 'Consórcio de Veículos', ''); ?></li>
                            <?php /* ?>
							<li><?php echo anchor('veiculos_pesados', 'Consórcio de Veículos Pesados', ''); ?></li>
                            */ ?>
						</ul>
						<!-- End Sub -->
					</li>
					<li><?php echo anchor('quem', 'Quem Somos', 'class="mn-has-sub '.$bt4.'"'); ?></li>
					<li><?php echo anchor('atendimento', 'Atendimento', 'class="mn-has-sub '.$bt5.'"'); ?></li>
					<li><?php echo anchor('simulador', 'Simulador', 'class="mn-has-sub '.$bt6.'"'); ?></li>
				</ul>
			</div>
			<!-- End Main Menu -->
		</div>
	</nav>
	<!-- End Navigation panel -->
