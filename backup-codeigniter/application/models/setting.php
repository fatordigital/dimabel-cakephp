<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Model {

    public function get_and_config()
    {
        $result = $this->db->select('key, value')
            ->from('settings')
            ->get();
        $return = array();
        foreach ($result->result() as $item) {
            $return[$item->key] = $item->value;
        }
        return $return;
    }

}