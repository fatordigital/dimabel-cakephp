<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function getLink($string = '')
{

    $CI =& get_instance();
    return $CI->config->site_url('/') . $CI->config->_config_paths[0] . $string;
}

