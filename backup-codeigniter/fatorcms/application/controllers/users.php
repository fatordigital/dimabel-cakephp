<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

include 'cms.php';


class Users extends Cms
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('encrypt');
    }

    private function do_login()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (captcha_fator()) {
                if ($this->authenticate->attempt($this->input->post('username'), $this->input->post('password'))) {
                    response(array('error' => false, 'msg' => 'Usuário autenticado.'), 200);
                } else {
                    response(array('error' => true, 'msg' => 'Os dados estão inválidos.'), 406);
                }

            } else {
                response(array('error' => true, 'msg' => 'Não foi possível efetuar a requisição, tente novamente.'), 406);
            }
        }
    }

    public function login()
    {
        $this->authenticate->guest(true);

        $this->add_js(getLink('assets/plugins/jvalidate/jquery.validate.min.js'));
        $this->add_js(getLink('assets/js/login/index.js'));

        $this->do_login();

        $this->layout = 'template_login';
        $this->load_view('login');
    }


    public function logout()
    {
        if (!$this->authenticate->guest()) {
            $this->session->sess_destroy();
            redirect('/fatorcms');
        }
        redirect('/fatorcms');
    }
    
}