<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

include 'cms.php';

class Dashboards extends Cms
{

    public function index()
    {
        $this->load->model('Simulator');

        $paginate = $this->Simulator->paginate(isset($_GET['simulator']) ? $_GET['simulator'] : 0, 21);

        $paginador = isset($_GET['simulator']) ? $_GET['simulator'] : 2;

        $this->set_data('paginador', floor($paginador / 10) * 10 + 10);
        $this->set_data('simulators', $paginate['result']);
        $this->set_data('pages', (int)$paginate['paginas']);

        if (isset($_GET['exportar'])) {
            $this->exportar($paginate['result']);
        }

        $this->load_view('dashboard');
    }


    public function exportar($expotar)
    {
        $results = '';
        foreach ($expotar as $simulador) {

            $from = $simulador->from == '' ? 'dimabel' : $simulador->from;
            $type = utf8_decode($simulador->type == 1 ? 'Imóvel' : 'Veículo');

            $results .= '<tr>';
            $results .= '<td><b>' . $simulador->id . '</b></td>';
            $results .= '<td><b>' . $from . '</b></td>';
            $results .= '<td><b>' . $type . '</b></td>';
            $results .= '<td width="150"><b>R$' . $simulador->value . '</b></td>';
            $results .= '<td><b>' . $simulador->plots . 'x</b></td>';
            $results .= '<td><b>' . utf8_decode($simulador->name) . '</b></td>';
            $results .= '<td><b>' . $simulador->email . '</b></td>';
            $results .= '<td><b>' . $simulador->phone . '</b></td>';
            $results .= '<td><b>' . $simulador->br_date . '</b></td>';
            $results .= '</tr>';
        }

        $arquivo = date('d-m-Y') . '-simuladores.xls';

        $html = '';
        $html .= '<table border="1">';
        $html .= '<tr>';
        $html .= '<td><b>#</b></td>';
        $html .= '<td><b>De</b></td>';
        $html .= '<td><b>Tipo</b></td>';
        $html .= '<td width="150"><b>R$</b></td>';
        $html .= '<td><b>Parcelas</b></td>';
        $html .= '<td><b>Nome</b></td>';
        $html .= '<td><b>E-mail</b></td>';
        $html .= '<td><b>Telefone</b></td>';
        $html .= '<td><b>Data</b></td>';
        $html .= '</tr>';
        $html .= $results;
        $html .= '</table>';

        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
        header("Cache-Control: no-cache, must-revalidate");
        header("Pragma: no-cache");
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=\"{$arquivo}\"");
        header("Content-Description: PHP Generated Data");
        // Envia o conteúdo do arquivo
        echo $html;
        exit;
    }

}