<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


$GLOBALS['ci'] = 'B';

class Cms extends CI_Controller
{
    public $layout = 'template';

    public $data = array(
        'js' => array(),
        'css' => array()
    );

    public $settings;
    
    protected $email_settings = array(
        'smtp_host' => 'smtp.mandrillapp.com',
        'smtp_user' => 'mandrill@fatordigital.com.br',
        'smtp_pass' => 'WCnPLj4A1ATpyKvcf6tLhA'
    );

    public function load_settings()
    {
        $this->load->model('Setting');
        $this->settings = $this->Setting->get_and_config();
    }

    public function __construct()
    {
        parent::__construct();
        $this->load_settings();
        $this->load->model('User');
        $GLOBALS['ci'] = $this;
    }

    public function add_js($js)
    {
        $this->data['js'][] = '<script src="' . $js . '"></script>';
    }

    public function add_css($css)
    {
        $this->data['css'][] = '<link rel="stylesheet" href="' . $css . '" />';
    }

    public function set_data($key, $value)
    {
        $this->data[$key] = $value;
    }

    public function load_view($view)
    {
        return $this->template->load($this->layout, $view, $this->data);
    }

}