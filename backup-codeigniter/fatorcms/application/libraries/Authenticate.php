<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Authenticate
{

    public $redirectTo = 'fatorcms/dashboards';

    public $session;

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->session = $this->CI->session;
    }

    public function attempt($user, $password)
    {
        if (!$user || !$password)
            return false;

        $db = $this->CI->db->query('SELECT * FROM `users` WHERE `login` = ? AND `password` = ? LIMIT 1', array($user, md5($password)));
        if ($db->num_rows() > 0) {
            $this->CI->session->set_userdata(array('user' => $db->row_array()));
            return true;
        } else {
            return false;
        }
    }

    public function guest($redirect = false)
    {
        if ($this->session->userdata('user')) {
            if ($redirect == true) {
                redirect($this->redirectTo);
            }
            # está conectado
            return false;
        }
        # não conectado
        return true;
    }

    public function check()
    {
        if ($this->session->userdata('user')) {

        }
    }

    public function redirect()
    {

    }

}