<link rel="stylesheet" href="<?php echo getLink() ?>assets/css/login.css" />


<login inline-template>

    <div class="login">

        <div class="row" v-if="response_msg != ''">
            <div class="alert alert-danger center-block">
                {{ response_msg }}
            </div>
        </div>


        <h1>Acesso Restrito</h1>

        <form method="post" action="<?php echo $_SERVER['REQUEST_URI'] ?>" id="login">

            <div class="fd_key">
                <input type="hidden" name="fd" v-model="fd" value="" />
            </div>

            <div class="form-group">
                <input type="text" name="username" v-model="username" placeholder="E-mail *" v-bind:disabled="wait == 1" />
            </div>

            <div class="form-group">
                <input type="password" name="password" v-model="password" placeholder="Senha de acesso *"  v-bind:disabled="wait == 1" />
            </div>

            <button type="submit" class="btn btn-primary btn-block btn-large">
                {{ wait == 0 ? 'Acessar' : 'Aguarde...' }}
            </button>

        </form>
    </div>
</login>