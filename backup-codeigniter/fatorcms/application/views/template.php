

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Dimabel - Fator Digital</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?php echo getLink('assets/plugins/bootstrap/bootstrap.min.css') ?>" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo getLink('assets/plugins/font-awesome/css/font-awesome.min.css') ?>" />

    <link rel="stylesheet" href="<?php echo getLink('assets/plugins/toastr/toastr.min.css') ?>" />

    <!-- Custom Theme Style -->
    <link rel="stylesheet" href="<?php echo getLink('assets/css/custom.min.css') ?>" />
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="index.html" class="site_title"> <span>Dimabel</span></a>
                </div>

                <?php
                $user = $this->session->userdata('user');
                ?>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_pic">
<!--                        <img src="images/img.jpg" alt="..." class="img-circle profile_img">-->
                    </div>
                    <div class="profile_info">
                        <span>Bem-vindo,</span>
                        <h2><?php echo $user['name']; ?></h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>Geral</h3>
                        <ul class="nav side-menu">
                            <li>
                                <a href="<?php echo getLink('dashboards'); ?>">
                                    <i class="fa fa-home"></i> Página inicial
                                </a>
                            </li>
                        </ul>
                    </div>

                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Sair" href="<?php echo getLink('users/logout') ?>">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>


        <!-- page content -->
        <div class="right_col" role="main">

            <?php echo $contents; ?>

        </div>
        <!-- /page content -->

    </div>
</div>

<script src="<?php echo getLink('assets/plugins/jquery/jquery.min.js') ?>"></script>
<script src="<?php echo getLink('assets/plugins/bootstrap/bootstrap.min.js') ?>"></script>
<script src="<?php echo getLink('assets/plugins/jquery-mask/jquery.mask.min.js') ?>"></script>
<script src="<?php echo getLink('assets/plugins/toastr/toastr.min.js') ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.28/vue.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.3.4/vue-resource.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.date-mask').mask('00/00/0000');
    });
</script>


<script type="text/javascript">
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-full-width",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    <?php
    if ($this->session->flashdata('error')) {
        echo 'toastr.error("'.$this->session->flashdata('error').'")';
    }
    if ($this->session->flashdata('success')) {
        echo 'toastr.success("'.$this->session->flashdata('success').'")';
    }
    ?>
</script>

</body>
</html>