<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dimabel - Fator Digital</title>
    <link rel="stylesheet" href="<?php echo getLink('assets/plugins/bootstrap/bootstrap.min.css') ?>" />
</head>
<body>

<?php echo $contents; ?>


<script src="<?php echo getLink('assets/plugins/jquery/jquery.min.js') ?>"></script>
<script src="<?php echo getLink('assets/plugins/bootstrap/bootstrap.min.js') ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.28/vue.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.3.4/vue-resource.min.js"></script>

<?php
foreach ($js as $j) {
    echo $j;
}
?>

<script src="<?php echo getLink('assets/js/common.js') ?>"></script>

</body>
</html>
