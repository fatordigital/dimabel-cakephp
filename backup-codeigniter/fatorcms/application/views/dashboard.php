<div class="row">
    <div class="col-lg-12">
        <h3>Simuladores</h3>

        <form action="<?php echo getLink('dashboards') ?>" method="get">
            <div class="row">
                <div class="col-lg-2">
                    <div class="form-group">
                        <label for="a_partir">
                            De (dd/mm/AAAA):
                        </label>
                        <input type="text" name="a_partir" class="form-control date-mask" value="<?php echo isset($_GET['a_partir']) ? $_GET['a_partir'] : '' ?>" autocomplete="off" />
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <label for="ate">
                            Até (dd/mm/AAAA)
                        </label>
                        <input type="text" name="ate" class="form-control date-mask" value="<?php echo isset($_GET['ate']) ? $_GET['ate'] : '' ?>" autocomplete="off" />
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="nome">
                            Nome
                        </label>
                        <input type="text" name="nome" class="form-control" value="<?php echo isset($_GET['nome']) ? $_GET['nome'] : '' ?>" autocomplete="off" />
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="email">
                            E-mail
                        </label>
                        <input type="text" name="email" class="form-control" value="<?php echo isset($_GET['email']) ? $_GET['email'] : '' ?>" autocomplete="off" />
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">
                            Procurar <i class="fa fa-search"></i>
                        </button>
                        <a href="<?php echo $_SERVER['REQUEST_URI'] ?><?php echo isset($_GET['a_partir']) ? '&exportar' : '?exportar' ?>" class="btn btn-primary">
                            Exportar <i class="fa fa-download"></i>
                        </a>
                    </div>
                </div>
            </div>
        </form>

        <div class="row">
            <div class="col-lg-12">
                <table class="table">
                    <thead class="bg-success">
                    <tr>
                        <td>#</td>
                        <td>De</td>
                        <td><strong>Tipo</strong></td>
                        <td><strong>R$</strong></td>
                        <td><strong>Parcelas</strong></td>
                        <td><strong>Nome</strong></td>
                        <td><strong>E-mail</strong></td>
                        <td><strong>Telefone</strong></td>
                        <td><strong>Data</strong></td>
                    </tr>
                    </thead>
                    <tbody>

                    <?php foreach($simulators as $simulator) { ?>
                        <tr>
                            <td><?php echo $simulator->id ?></td>
                            <td><?php echo $simulator->from == '' ? 'Dimabel' : $simulator->from ?></td>
                            <td><?php echo $simulator->type == 1 ? 'Imóvel' : 'Automóvel' ?></td>
                            <td>R$ <?php echo $simulator->value ?></td>
                            <td><?php echo $simulator->plots ?>x</td>
                            <td><?php echo $simulator->name ?></td>
                            <td><?php echo $simulator->email ?></td>
                            <td><?php echo $simulator->phone ?></td>
                            <td><?php echo $simulator->br_date ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
    <div class="col-lg-12">
        <ul class="list-inline">
            <a href="<?php echo getLink('dashboards') ?>?simulator=<?php echo isset($_GET['simulator']) && $_GET['simulator'] > 1 ? $_GET['simulator'] - 1 : 1 ?>" class="btn btn-primary btn-xs<?php echo !isset($_GET['simulator']) || (isset($_GET['simulator']) && $_GET['simulator'] == 1) ? ' disabled' : '' ?>">
                <i class="glyphicon glyphicon-arrow-left"></i>
            </a>
            <?php for ($i = $paginador - 11; $i <= $paginador; $i++) { ?>
                <?php if ($i > -1) { ?>
                    <a href="<?php echo getLink('dashboards') ?>?simulator=<?php echo $i ?>" class="btn btn-success btn-xs<?php echo isset($_GET['simulator']) && $_GET['simulator'] == $i ? ' active' : '' ?>">
                        <?php echo $i + 1; ?>
                    </a>
                <?php } ?>
            <?php } ?>
            <a href="<?php echo getLink('dashboards') ?>?simulator=<?php echo isset($_GET['simulator']) ? $_GET['simulator'] + 1 : 1 ?>" class="btn btn-primary btn-xs<?php echo isset($_GET['simulator']) && $_GET['simulator'] >= $pages ? ' disabled' : '' ?>">
                <i class="glyphicon glyphicon-arrow-right"></i>
            </a>
        </ul>
    </div>
</div>