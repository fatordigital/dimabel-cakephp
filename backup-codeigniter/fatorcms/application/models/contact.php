<?php

class Contact extends CI_Model
{

    public $data, $errors, $error;

    public $rules = array(
        array(
            'field' => 'name',
            'label' => 'Nome',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'phone',
            'label' => 'Telefone',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'email',
            'label' => 'E-mail',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'message',
            'label' => 'Mensagem',
            'rules' => 'trim|required'
        )
    );

    protected $name, $email, $phone, $subject, $message, $json, $created_at, $updated_at;

    public function validate()
    {

        $this->load->library('form_validation');
        $this->form_validation->set_rules($this->rules);

        return $this->form_validation->run();
    }

    public function setData($_data)
    {
        $this->data = $_data;
    }

    public function fill($data)
    {
        $this->name = isset($data['name']) ? $data['name'] : '';
        $this->email = isset($data['email']) ? $data['email'] : '';
        $this->phone = isset($data['phone']) ? $data['phone'] : '';
        $this->subject = isset($data['subject']) ? $data['subject'] : '';
        $this->message = isset($data['message']) ? $data['message'] : '';
        $this->referer = isset($data['return_url']) ? $data['return_url'] : '';
        $this->json = json_encode($data);
    }

    public function find($key, $value)
    {
        $result = $this->db->select('*')
            ->from('contacts')
            ->where($key, $value)
            ->get();

        if ($result->num_rows)
            return $result->row();
        return false;
    }

    public function create()
    {
        if ($this->validate()) {

            $this->fill($this->input->post());

            $this->db->insert('contacts', array(
                'name' => $this->name,
                'email' => $this->email,
                'phone' => $this->phone,
                'subject' => $this->subject,
                'message' => $this->message,
                'referer' => $this->referer,
                'json' => $this->json,
                'created_at' => date('Y-m-d H:i:s'),
                'uodate_at' => date('Y-m-d H:i:s')
            ));

            return $this;
        }
        return false;
    }


}