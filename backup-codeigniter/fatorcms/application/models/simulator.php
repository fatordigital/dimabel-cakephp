<?php

class Simulator extends CI_Model
{

    public $data, $errors, $error;

    public $rules = array(
        array(
            'field' => 'name',
            'label' => 'Nome',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'phone',
            'label' => 'Telefone',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'email',
            'label' => 'E-mail',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'plots',
            'label' => 'Parcelas',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'message',
            'label' => 'Mensagem',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'message',
            'label' => 'Mensagem',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'message',
            'label' => 'Mensagem',
            'rules' => 'trim|required'
        )
    );

    protected $type, $value, $plots, $name, $email, $phone, $json, $created_at, $updated_at;

    public function validate()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules($this->rules);

        return $this->form_validation->run();
    }

    public function setData($_data)
    {
        $this->data = $_data;
    }

    public function fill($data)
    {
        $this->name = isset($data['name']) ? $data['name'] : '';
        $this->email = isset($data['email']) ? $data['email'] : '';
        $this->phone = isset($data['phone']) ? $data['phone'] : '';
        $this->subject = isset($data['subject']) ? $data['subject'] : '';
        $this->message = isset($data['message']) ? $data['message'] : '';
        $this->referer = isset($data['return_url']) ? $data['return_url'] : '';
        $this->json = json_encode($data);
    }

    public function find($key, $value)
    {
        $result = $this->db->select('*')
            ->from('contacts')
            ->where($key, $value)
            ->get();

        if ($result->num_rows)
            return $result->row();
        return false;
    }

    public function create()
    {
        if ($this->validate()) {
            $this->created_at = date('Y-m-d H:i:s');
            $this->updated_at = date('Y-m-d H:i:s');

            $this->fill($this->input->post());

            $this->db->insert('contacts', array(
                'name' => $this->name,
                'email' => $this->email,
                'phone' => $this->phone,
                'subject' => $this->subject,
                'message' => $this->message,
                'referer' => $this->referer,
                'json' => $this->json
            ));

            return $this;
        }
        return false;
    }

    public function paginate($off = 1, $length = 20)
    {
        $a_partir = isset($_GET['a_partir']) && !empty($_GET['a_partir']) ? $_GET['a_partir'] : '';
        if (!empty($a_partir)) {
            if (!typeDate($a_partir)) {
                alert('error', 'A data precisa ser uma data válida dd/mm/AAAA');
                redirect('/fatorcms/dashboards');
            }
        }
        $ate = isset($_GET['ate']) && !empty($_GET['ate']) ? $_GET['ate'] : '';
        if (!empty($ate)) {
            if (!typeDate($ate)) {
                alert('error', 'A data precisa ser uma data válida dd/mm/AAAA');
                redirect('/fatorcms/dashboards');
            }
        }

        $nome = isset($_GET['nome']) && !empty($_GET['nome']) ? $_GET['nome'] : '';
        $email = isset($_GET['email']) && !empty($_GET['email']) ? $_GET['email'] : '';

        $where = ' WHERE ';
        $exist = 0;
        if ($a_partir != '') {
            $a_partir = convert_date($a_partir);
            $where .= 'DATE(`created_at`) >= DATE("' . $a_partir . '")';
            $exist++;
        }

        if ($ate != '') {
            $ate = convert_date($ate);
            if ($exist > 0) {
                $where .= ' AND ';
            }
            $where .= 'DATE(`created_at`) <= DATE("' . $ate . '")';
        }


        if ($nome != '' && $email == '') {
            if ($exist > 0) {
                $where .= ' AND ';
            }
            $where .= '`name` LIKE "%' . $nome . '%"';
        }

        if ($email != '' && $nome == '') {
            if ($exist > 0) {
                $where .= ' AND ';
            }
            $where .= '`email` LIKE "%' . $email . '%"';
        }

        if ($email != '' && $nome != '') {
            if ($exist > 0) {
                $where .= ' OR ';
            }
            $where .= '(`name` LIKE "%' . $nome . '%"  OR `email` LIKE "%' . $email . '%")';
        }

        if ($where == ' WHERE ') {
            $where = '';
        }

        $off = $off * $length;

        if (isset($_GET['exportar'])) {
            $db = $this->db->query('SELECT *, DATE_FORMAT(`created_at`, "%d/%m/%Y") AS br_date FROM `simulators` ' . $where . ' ORDER BY id DESC');
        } else {
            $db = $this->db->query('SELECT *, DATE_FORMAT(`created_at`, "%d/%m/%Y") AS br_date FROM `simulators` ' . $where . ' ORDER BY id DESC LIMIT ' . $off . ',' . $length);
        }
        $count = $this->db->query('SELECT COUNT(id) AS contador FROM `simulators`' . $where);
        if ($count) {
            $count = $count->first_row();
            $count = $count->contador;
        }  else {
            $count = 1;
        }
        return array(
            'result' => $db ? $db->result() : array(),
            'paginas' => $count / $length
        );
    }

}