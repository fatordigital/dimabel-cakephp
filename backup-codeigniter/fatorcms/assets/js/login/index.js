var login = Vue.extend({
    data: function () {
        return {
            wait: 0,
            fd: '',
            username: '',
            password: '',
            response_msg: ''
        }
    },
    ready: function () {
        var self = this;
        $('#login').validate({
            rules: {
                username: {
                    required: true,
                    email: true
                },
                password: {
                    required: true
                }
            },
            messages: {
                username: {
                    required: 'Campo obrigatório.',
                    email: 'Digite um e-mail válido.'
                },
                password: {
                    required: 'Campo obrigatório.'
                }
            },
            submitHandler: function (form) {
                var validator = this;
                self.wait = 1;
                self.$http.post($(form).attr('action'), {
                    fd: self.fd,
                    username: self.username,
                    password: self.password
                }, {emulateJSON: true}).then(function (r) {
                    self.wait = 0;
                    if (r.body.error == false) {
                        form.submit();
                    }
                }, function (r) {
                    self.wait = 0;
                    self.response_msg = r.body.msg;
                    validator.showErrors({
                        username: 'Verifique se digitou o e-mail corretamente',
                        password: 'Verifique se digitou a senha corretamente'
                    });
                });
                return false;
            }
        });
    }
});

Vue.component('login', login);