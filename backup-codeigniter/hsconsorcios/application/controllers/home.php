<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include 'controller.php';

class Home extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->template->load('template', 'home');
    }


    public function contact()
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $this->load->model('Contact');
            if (captcha_fator() != false) {
                if (isset($_POST['contato_home']))
                    $this->session->set_flashdata('contato_home', true);

                if (isset($_POST['contato']))
                    $this->session->set_flashdata('contato', true);

                if ($this->Contact->create() != false) {
                    $body = '
                    HS Consórcios <br />
                     Nome: ' . $this->input->post('name') . ' <br />
                     E-mail: ' . $this->input->post('email') . ' <br />
                     Telefone: ' . $this->input->post('phone') . ' <br />
                     <br />
                     <strong>Mensagem</strong> <br /></br >
                     ' . $this->input->post('message') . '
                    ';

                    if ($this->send_php_mailer($this->input->post(), $body)) {
                        alert('success', 'Recebemos seu contato e retornaremos o mais rápido possível.');

                        $this->session->set_userdata(array('goal' => "ga('send', 'pageview', '/goal/hs-contato-home');"));

                        redirect($this->input->post('return_url'));
                    } else {
                        alert('error', 'Ops, tente novamente, um erro em nosso servidor aconteceu.');
                    }
                } else {
                    alert('error', 'Todos os campos do formulário são obrigatórios.');
                }
            }
        }
        $this->template->load('template', isset($_POST['view']) ? $_POST['view'] : 'home');
    }


}
