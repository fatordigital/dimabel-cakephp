<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include 'controller.php';

class Veiculos extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->template->load('template', 'veiculos');
    }


}
