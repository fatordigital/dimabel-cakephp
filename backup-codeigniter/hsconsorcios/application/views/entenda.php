<?php

$cam = "";

$bt1 = '';
$bt2 = 'active';
$bt3 = '';
$bt4 = '';
$bt5 = '';
$bt6 = '';
$bt7 = '';
$bt8 = '';
$bt9 = '';


?>
<html>
<head>

    <?php include_once("includes/head.php"); ?>

    <?php include_once("includes/analyticstracking.php"); ?>

</head>


<body class="appear-animate">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TSV6NGX" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- Page Wrap -->
<div class="page" id="top">

    <?php include_once("includes/menu.php"); ?>

    <!-- Head Section -->
    <section class="small-section bg-dark-alfa-90 bg-scroll"
             data-background="<?php echo getLink(); ?>views/imagens/entenda.jpg">
        <div class="relative container align-left">

            <div class="row">

                <div class="col-md-12">
                    <h1 class="section-heading mb-10 mb-xs-0">Entenda o Consórcio</h1>
                    <div class="hs-line-4 uppercase">
                        Suas dúvidas respondidas
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- End Head Section -->


    <!-- Section -->
    <section class="page-section" style="padding:80px 0;">
        <div class="relative container">

            <div class="section-text">

                <h4 style="margin:10px 0; font-weight:700; color:#CD071E;">O que é Carta de Crédito?</h4>
                <p class="text-justify">
                    É a denominação utilizada para identificar o crédito do consórcio. Trata-se de uma ordem de faturamento emitida pela administradora, com a qual o consorciado irá adquirir o bem de sua livre escolha. Para tanto, deverão ser apresentadas as garantias exigidas pela administradora, de forma a preservar os interesses dos próprios consorciados.
                </p>

                <h4 style="margin:10px 0; font-weight:700; color:#CD071E;">O que é uma administradora de consórcio?</h4>
                <p class="text-justify">
                    O consórcio tem como prestadora de serviço uma administradora, devidamente autorizada pelo Banco Central, órgão responsável pela regulamentação do setor, para gestão dos interesses do grupo de consorciados. A administradora cobra, pela prestação do serviço, uma taxa de administração que varia de acordo com cada empresa, modalidade de consórcio e prazo do plano. Cabe à administradora não apenas administrar como também zelar pela saúde financeira do grupo.
                </p>
                <h4 style="margin:10px 0; font-weight:700; color:#CD071E;">O que é Assembleia?</h4>
                <p class="text-justify">
                    A Assembleia Geral Ordinária, realizada em dia, hora e local informados pela administradora, destina-se à contemplação dos consorciados, bem como ao atendimento e à prestação de informações.
                </p>

                <h4 style="margin:10px 0; font-weight:700; color:#CD071E;">Como funciona a Contemplação?</h4>
                <p class="text-justify">
                    Por meio de Sorteios e Lances, realizadas nas assembleias mensais dos grupos de consórcio.
                </p>

                <h4 style="margin:10px 0; font-weight:700; color:#CD071E;">O que é Lance?</h4>
                <p class="text-justify">
                    É o direito do consorciado de concorrer à contemplação, mediante a antecipação de parcelas oferecidas por ocasião das assembleias dos grupos. Dependendo da disponibilidade de caixa do grupo, será contemplado o maior lance, de acordo com as regras contratuais.
                </p>

                <h4 style="margin:10px 0; font-weight:700; color:#CD071E;">Como funciona o Sorteio?</h4>
                <p class="text-justify">
                    De acordo com a disponibilidade da caixa, um ou mais participantes do grupo serão sorteados para receber sua Carta de Crédito, no valor do plano a que aderiu, independente do número de prestações que tenha pago. O Sorteio serve apenas para definição da ordem de recebimento do crédito, uma vez que todos os participantes do grupo receberão até o final do plano.
                </p>

                <h4 style="margin:10px 0; font-weight:700; color:#CD071E;">Dúvidas Sobre Consórcio</h4>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    1. O que é consórcio e como funciona?
                </h4>
                <p class="text-justify">
                    O consórcio é a reunião de pessoas físicas e/ou jurídicas, em grupo fechado, promovido por uma Administradora, com prazo de duração previamente estabelecido, para propiciar aos seus integrantes a aquisição de bem por meio de autofinanciamento.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    2. Existe uma idade limite para adquirir consórcio?
                </h4>
                <p class="text-justify">
                    Não. Nos produtos da Administradora Caixa Consórcios, a idade limite é apenas para efeito de seguro. Ou seja, para ter cobertura do Seguro Prestamista o somatório da idade do consorciado com o prazo do grupo não pode ultrapassar 80 anos.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    3. O que é o contrato de adesão?
                </h4>
                <p class="text-justify">
                    Contrato de adesão é o documento legal que formaliza a contratação do Consórcio, assinado pelo cliente e a Administradora. Contém as condições de funcionamento de um grupo de consórcio bem como os direitos e deveres do consorciado e da Administradora.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    4. O que é uma carta de crédito?
                </h4>
                <p class="text-justify">
                    É a denominação utilizada para identificar o crédito do consórcio. Trata-se de uma ordem de faturamento emitida pela administradora, com a qual o consorciado irá adquirir o imóvel de sua livre escolha. Para tanto, deverão ser apresentadas as garantias exigidas pela administradora, de forma a preservar os interesses dos próprios consorciados.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    5. Qual é a diferença do consórcio para o financiamento?
                </h4>
                <p class="text-justify">
                    Apesar de serem opções para aquisição de um bem, o consórcio e o financiamento são diferentes. No consórcio são vários membros que contribuem com um valor por mês para somar o valor total e sortear entre eles o custo de um bem. Como não se conhecem, há a necessidade de uma Administradora para garantir a segurança do investimento e fazer a gestão do grupo. Por isso o cliente contribuirá para a taxa de administração. Já no financiamento bancário o cliente paga sozinho e por isso são cobrados juros para que o banco antecipe o valor integral.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    6. Quem poderá adquirir consórcio?
                </h4>
                <p align="justify">
                    Pessoas Físicas capazes e Pessoas Jurídicas.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    7. O prazo Máximo independe de o consorciado ser pessoa física ou jurídica?
                </h4>
                <p align="justify">
                    Sim. O prazo máximo é de 200 meses.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    8. Cliente que mora no exterior poderá adquirir cotas de consórcio?
                </h4>
                <p align="justify">
                    Sim. Mas, a carta de crédito só tem abrangência nacional, ou seja, só poderá ser utilizada dentro do país.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    9. A adesão poderá ser feita por procuração?
                </h4>
                <p align="justify">
                    Sim, desde que seja uma procuração por instrumento público.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    10. Qual a forma de pagamento da primeira parcela?
                </h4>
                <p align="justify">
                    Boleto bancário ou débito automático. A última opção somente para consorciados que tem conta na Caixa Econômica Federal.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    11. Há taxa de adesão?
                </h4>
                <p align="justify">
                    Sim, que é de 1% sobre o valor da carta, dividido em todo o prazo do consórcio.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    12. Quais as vantagens do consórcio?
                </h4>
                <p align="justify">
                    • Por se tratar de autofinanciamento em que os recursos são gerados pelo próprio grupo o consórcio é a modalidade mais econômica para aquisição de um imóvel;<br />
                    • Administrado pela Caixa Consórcios S/A, credibilidade incomparável;<br />
                    • Rede nacional, com rápida formação de grupos, alta adimplência e muitas contemplações mensais;<br />
                    • Descontos nos Seguros Auto e Residencial, para os consorciados; - Facilidade para adquirir;<br />
                    • Portal de Serviços Online – resultado de Assembleia, oferta de lances, emissão de boleto, extrato mensal, informe de IR, entre outros serviços.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    13. O que é alienação fiduciária?
                </h4>
                <p align="justify">
                    É o instrumento legal pelo qual se garante o pagamento de uma dívida, mantendo o devedor na posse direta do bem e transferindo a propriedade resolúvel ao credor até a liquidação total das obrigações assumidas por ele. Se o devedor não cumprir as condições contratadas, pode perder o direito de reaver a propriedade do BEM e, mesmo assim, continuar obrigado a quitar o saldo devedor.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    14. Como é composta a parcela mensal do consórcio?
                </h4>
                <p align="justify">
                    A parcela mensal é composta pelo Fundo comum, Taxa administrativa, Fundo de reserva, seguros e demais encargos e obrigações previstas no contrato de adesão.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    15. Pessoa Jurídica tem desconto?
                </h4>
                <p align="justify">
                    Não. Pessoa Jurídica não paga o seguro de vida, por isso, a parcela fica um pouco mais baixa.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    16. Quais as modalidades do consórcio imobiliário para utilização da carta de crédito?
                </h4>
                <p align="justify">
                    • Aquisição de imóvel comercial ou residencial, novo ou usado;<br/>
                    • Aquisição de imóvel misto (residencial e comercial) com uma só matrícula no Cartório de Registro de Imóveis;<br/>
                    • Aquisição de terreno urbano;<br/>
                    • Aquisição de imóvel rural, sendo a garantia obrigatoriamente de imóvel urbano;<br/>
                    • Aquisição de terreno urbano e construção de imóvel, residencial ou comercial;<br/>
                    • Aquisição de imóvel na planta sendo a garantia, obrigatoriamente, outro imóvel urbano;<br/>
                    • Construção de imóvel residencial ou comercial, em terreno urbano próprio;<br/>
                    • Termino de construção já iniciada;<br/>
                    • Reforma e/ou ampliação de imóvel urbano próprio, residencial ou comercial;<br/>
                    • Compra e venda de imóvel com quitação de financiamento imobiliário do vendedor;<br/>
                    • Quitação de financiamento imobiliário próprio.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    17. Quais as modalidades do consórcio veículos automotores para utilização da carta?
                </h4>
                <p align="justify">
                    • Aquisição de Veículos automotores novos (0 km);<br/>
                    • Aquisição de Veículos automotores usados (com até 05 anos de fabricação, não incluindo o ano em curso);<br/>
                    • Quitação de financiamento próprio.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    18. É permitido adquirir o consórcio em conjunto com outra pessoa?
                </h4>
                <p align="justify">
                    Não. O consórcio é feito em nome de uma única pessoa física ou jurídica, cadastrado por CPF ou CNPJ.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    19. Quais são os prazos e valores previstos de cartas de crédito do consórcio?
                </h4>
                <p align="justify">
                    <strong>Imobiliário:</strong><br />
                    • Cotas com valores de R$ 70.000,00 à R$ 700.000,00 – prazo de 200 meses.<br />
                    <strong>Veículos automotores</strong><br />
                    • Cotas com valores de R$ 25.000,00 a 80.000,00 – prazo de 80 meses.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    20. Quem é correntista da Caixa Econômica tem algum plano diferenciado?
                </h4>
                <p align="justify">
                    Não. A vantagem neste caso são os débitos em conta para correntistas da Caixa Econômica Federal.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    21. O pagamento das parcelas poderá ser por débito em conta corrente ou poupança?
                </h4>
                <p align="justify">
                    Sim. Desde que o consorciado possua conta corrente ou poupança na Caixa Econômica Federal.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    22. Qual é o percentual da taxa de administração?
                </h4>
                <p align="justify">
                    Depende do valor de crédito contratado, conforme a seguir:<br /><br />
                    <strong>Veículos automotores:</strong><br />
                    Valores das cotas de R$ 25 mil a R$ 150 mil:<br />
                    • 15% para grupos abertos até 15/01/17 (antecipação de 4% nas 10 primeiras parcelas. A partir da 11ª, os 11% restantes são diluídos no prazo remanescente).<br />
                    • 17% para grupos abertos a partir de 16/01/17 (antecipação de 5% no primeiro ano e 2% no segundo ano. A partir da 25ª, os 10% restantes são diluídos no prazo remanescente).<br /><br />
                    <strong>Imobiliário:</strong><br />
                    Valores das cotas de R$ 70 mil a R$ 140 mil:<br />
                    • 18% para grupos abertos até 15/01/17 (antecipação de 4% nas 10 primeiras parcelas. A partir da 11ª, os 14% restantes são diluídos no prazo remanescente).<br />
                    • 20% para grupos abertos a partir de 16/01/17 (antecipação de 5% no primeiro ano e 2% no segundo ano. A partir da 25ª, os 13% restantes são diluídos no prazo remanescente).<br />
                    Valores das cotas de R$ 150 mil a R$ 300 mil:<br />
                    • 19% para grupos abertos até 15/01/17 (antecipação de 4% nas 10 primeiras parcelas. A partir da 11ª, os 15% restantes são diluídos no prazo remanescente).<br />
                    • 20% para grupos abertos a partir de 16/01/17 (antecipação de 5% no primeiro ano e 2% no segundo ano. A partir da 25ª, os 13% restantes são diluídos no prazo remanescente).<br />
                    Valores das cotas de R$ 400 mil a R$ 700 mil<br />
                    • 16% para grupos abertos até 15/01/17 (antecipação de 5% nas 12 primeiras parcelas. A partir da 13ª, os 11% restantes são diluídos no prazo remanescente).<br />
                    • 18% para grupos abertos a partir de 16/01/17 (antecipação de 5% no primeiro ano e 2% no segundo ano. A partir da 25ª, os 11% restantes são diluídos no prazo remanescente).

                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    23. Quando é feita a atualização da carta de crédito, do saldo devedor e das prestações, no caso de consórcio imobiliário e de veículos automotores?
                </h4>
                <p class="text-justify">
                    <strong>Imobiliário:</strong><br />
                    Enquanto não contemplada, a carta é atualizada pelo Índice Nacional de Preços ao Consumidor (INPC) a cada 12 Assembleias, contadas a partir da primeira.<br /><br />
                    <strong>Automotores:</strong><br />
                    Enquanto não contemplada, a carta é atualizada pelo Índice Nacional de Preços ao Consumidor (INPC) e pela tabela da Fundação Instituto de Pesquisas Econômicas (FIPE) a cada 12 Assembleias, contadas a partir da primeira, conforme a seguir:<br />
                    • Grupos antigos: terão os reajustes quando houver aumento do bem em relação à tabela da Fundação Instituto de Pesquisas Econômicas (FIPE) do mês anterior.<br />
                    • Grupos novos: terão os reajustes pelo Índice Nacional de Preços ao Consumidor (INPC) a cada 12 Assembleias, contadas a partir da primeira. Grupos a partir de 2001.<br />
                    Para as cartas contempladas utilizadas: não há atualização, somente parcelas e o saldo devedor pelo INPC.<br />
                    Para as cartas contempladas não utilizadas: o saldo devedor e parcelas continuam sendo atualizados pelo INPC e a carta é capitalizada diariamente em aplicações Financeiras.
                </p>


                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    24. É permitida a troca de bem, ou seja, alterar o valor da carta de crédito contratada?
                </h4>
                <p align="justify">
                    Sim. A alteração só poderá ocorrer após a realização da primeira Assembleia Geral Ordinária – AGO do grupo, e só é permitida para cotas adimplentes e não contempladas. Só serão permitidas duas trocas e somente por valores disponíveis no mesmo grupo, tanto para um valor maior ou menor.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    25. Cliente com restrição tem algum impedimento para aderir ao consórcio?
                </h4>
                <p align="justify">
                    Para aderir não, mas, após a contemplação e para utilização da carta de crédito será necessário que o consorciado não tenha restrições em seu nome, ou realize a transferência da cota para uma pessoas sem restrições.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    26. Há possibilidade de transferência de carta?
                </h4>
                <p align="justify">
                    Sim, tanto a carta contemplada quanto não contemplada. Porém há uma taxa de 1%, cobrada sobre a carta de crédito.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    27. O consorciado após aderir à cota poderá desistir?
                </h4>
                <p align="justify">
                    Sim. A solicitação de desistência poderá ser feita a qualquer momento, desde que não esteja contemplado, observando os seguintes critérios: - Em até 07 (sete) dias corridos, contados da data da assinatura do contrato de adesão, desde que o consorciado não tenha participado da 1ª Assembleia e a contratação tenha ocorrido fora de estabelecimento comercial, especialmente se por telefone ou em domicílio, conforme prevê a lei 8.078/1990, art. 49, do Código de Defesa do Consumidor. A devolução do valor pago será integral, devidamente corrigido por aplicação financeira.<br />
                    • Após 07 (sete) dias corridos, contados da data da assinatura do contrato de adesão, a devolução do valor pago será por sorteio mensal, através da AGO, ou após o encerramento do grupo, com incidência de multa de 10% (dez por cento) sobre os valores a serem devolvidos.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    28. É possível comprar um imóvel com a carta de crédito e colocar a documentação em nome de duas pessoas?
                </h4>
                <p align="justify">
                    Não. Somente o nome do consorciado deverá constar na matrícula, com exceção dos consorciados casados ou que vivam em união estável.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    29. A carta de crédito pode ser utilizada para compra de mais de 01 imóvel ou veículos automotores?
                </h4>
                <p align="justify">
                    Não.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    30. A carta de crédito poderá ser usada para arrematar imóveis em leilão?
                </h4>
                <p align="justify">
                    Não.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    31. Quais as condições para utilização do crédito para a modalidade de construção, reforma e/ou ampliação?
                </h4>
                <p align="justify">
                    • Apresentar como garantia o próprio terreno que deverá ser urbano; e<br />
                    • O terreno deverá estar quitado e livre de ônus.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    32. Ao adquirir um imóvel, como poderá ser composto o valor a ser pago?
                </h4>
                <p align="justify">
                    A compra do imóvel poderá ser composta pela Carta de crédito, recursos próprios e, caso necessário e o consorciado se enquadrando nas exigências da Caixa Econômica Federal poderá ser usado também o FGTS.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    33. Para análise de crédito o consorciado poderá compor a renda com mais alguém?
                </h4>
                <p align="justify">
                    Somente com o cônjuge ou companheiro desde que comprove a união estável.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    34. É permitida à unificação de cotas?
                </h4>
                <p align="justify">
                    Sim.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    35. O valor do imóvel a ser adquirido terá de ser o mesmo valor da Carta de crédito?
                </h4>
                <p align="justify">
                    Não. Se o valor do imóvel for menor que a carta, terá uma sobra de crédito que será abatida no saldo devedor, amortizando as parcelas ou o prazo. Se o valor do imóvel for maior que a carta, este deverá ser complementado com recursos próprios e/ou FGTS. Lembrando que para a utilização do FGTS devem ser observadas as exigências do fundo, e o processo não é de responsabilidade da Caixa Consórcios.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    36. O que é feito com a sobra de crédito?
                </h4>
                <p align="justify">
                    Será abatido no saldo devedor, amortizando as parcelas ou o prazo, a sobra de crédito poderá ser utilizada também para o pagamento de despesas cartorárias, não ultrapassando 10% do valor total da carta de crédito.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    37. É possível para o consorciado, após a contemplação, converter a carta em espécie?
                </h4>
                <p align="justify">
                    Sim, desde que: <br />
                    • A solicitação de conversão do crédito em espécie seja feita após 180 dias da contemplação e seja realizada a quitação do saldo devedor.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    38. Qual o procedimento após a escolha do imóvel pelo consorciado?
                </h4>
                <p align="justify">
                    Será feita uma vistoria no imóvel, pela área de engenharia da Caixa Consórcios, para verificar se o bem poderá ser aceito como garantia e para avaliação quanto ao seu valor.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    39. Essa vistoria tem custo e qual o valor?
                </h4>
                <p align="justify">
                    Sim, R$650,00 (seiscentos e cinquenta reais).
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    40. De quem é a responsabilidade pelo custo dessa vistoria?
                </h4>
                <p align="justify">
                    O custo será do consorciado.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    41. Qual o procedimento após a escolha do veículo automotor pelo consorciado?
                </h4>
                <p align="justify">
                    Veículo Automotor 0 km: - Apresentação da nota fiscal. Veículo Automotor Usado:<br />
                    - Será feita uma vistoria por uma empresa conveniada da Caixa Consórcios, para averiguar se o bem poderá ser aceito como garantia e para avaliação quanto ao seu valor.

                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    42. De quem é a responsabilidade pelo custo dessa vistoria?
                </h4>
                <p align="justify">
                    A primeira vistoria é custeada pela Caixa Consórcios. Caso seja necessária uma segunda, o custo será arcado pelo consorciado.
                </p>


                <h4 style="margin:4px 0; font-weight:700; color:#CD071E;">
                    Dúvidas Sobre Contemplação
                </h4>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    43. Quais as formas de contemplação?
                </h4>
                <p align="justify">
                    Sorteio, lance fixo e lance livre, necessariamente nessa ordem.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    44. O que é e como funcionam os lances livres?
                </h4>
                <p align="justify">
                    São definidos por percentual em relação ao valor do crédito atualizado, sendo considerado vencedor o consorciado que ofertar o maior percentual. Esse tipo de lance poderá ser pago com recursos próprios e/ou abatido da carta de crédito – lance embutido. Nesse último caso, o valor é limitado a 50% da carta.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    45. O que é e como funcionam os lances fixos?
                </h4>
                <p align="justify">
                    Corresponde a 20% do saldo devedor da cota do consorciado. No caso de existirem várias ofertas de lance fixo para a mesma AGO, será considerada vencedora a cota cujo número estiver mais próximo da cota sorteada.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    46. Qual é o maior percentual para oferta de lance livre?
                </h4>
                <p align="justify">
                    Limita-se ao valor do saldo devedor.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    47. Quais as formas de amortização, caso o consorciado seja contemplado?
                </h4>
                <p align="justify">
                    O valor pago no lance pode amortizar o saldo devedor, reduzindo as prestações ou o prazo.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    48. Em uma mesma assembleia, podem ser ofertados lance fixo e livre?
                </h4>
                <p align="justify">
                    Sim. Mas, para efeitos de contemplação só será acatada uma modalidade.
                </p>



                <h4 style="margin:10px 0; font-weight:700; color:#CD071E;">
                    Dúvidas Sobre Construção
                </h4>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    49. Como é liberado o crédito em caso de construção?
                </h4>
                <p align="justify">
                    O crédito será liberado em etapas de acordo com o Cronograma Físico Financeiro. São no mínimo 04 parcelas e no máximo 18, sendo que a liberação do crédito será condicionada à conclusão da etapa. A primeira liberação não poderá ser maior que 20% e a última menor que 10% do valor da obra.
                </p>

                <h4 style="margin:10px 0; font-weight:700; color:#CD071E;">
                    Dúvidas Sobre Contemplação
                </h4>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    50. Como é o processo de pagamento para as etapas de construção?
                </h4>
                <p align="justify">
                    As vistorias são pré-agendadas, e ao término de conclusão de cada etapa, a área de engenharia da Caixa Consórcios fará uma vistoria no imóvel para certificar se a etapa foi concluída, a qual será custeada pelo consorciado e o valor da vistoria será descontado do crédito a ser liberado. Com o de acordo do engenheiro, a parcela referente àquela etapa será liberada. Lembrando que a parcela relativa à última etapa será liberada após a averbação da construção e da liberação do habite-se.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    51. De quem é a responsabilidade pelo custo dessas vistorias?
                </h4>
                <p align="justify">
                    Do consorciado.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    52. A carta poderá ser utilizada para construção em área rural?
                </h4>
                <p align="justify">
                    Não.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#800414;">
                    53. Em caso de construção poderá ser dado outro imóvel em garantia?
                </h4>
                <p align="justify">
                    Não. Neste caso o imóvel a ser dado em garantia deverá ser o próprio terreno onde será construído, o qual deverá estar em nome do consorciado e livre de ônus.
                </p>

            </div>

        </div>
    </section>
    <!-- End Section -->


    <?php include_once("includes/base.php"); ?>

</div>
<!-- End Page Wrap -->


<?php include_once("includes/foot.php"); ?>



