<?php

$cam = "";

$bt1 = '';
$bt2 = '';
$bt3 = 'active';
$bt4 = '';
$bt5 = '';
$bt6 = '';
$bt7 = '';
$bt8 = '';
$bt9 = '';


?>
<!-- Page Wrap -->
<div class="page" id="top">

    <?php include_once("includes/menu.php"); ?>

    <!-- Head Section -->
    <section class="small-section bg-dark-alfa-90 bg-scroll"
             data-background="<?php echo getLink(); ?>views/imagens/capa_1.jpg">
        <div class="relative container align-left">

            <div class="row">

                <div class="col-md-12">
                    <h1 class="section-heading mb-10 mb-xs-0">Consórcio de Imóveis</h1>
                    <div class="hs-line-4 uppercase">
                        Nossos Produtos
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- End Head Section -->


    <!-- Section -->
    <section class="page-section" style="padding:80px 0;">
        <div class="relative container">
            <div class="section-text">
                <h4 style="margin:10px 0; font-weight:700; color:#CD071E;">Imóveis</h4>
                <p class="text-justify">
                    • Cartas com valores de R$ 70 mil a R$ 700 mil;<br />
                    • Prazo de 200 meses;<br />
                    • Poder de negociação: você paga o imóvel à vista, o que lhe garante mais poder para negociar descontos no preço;<br />
                    • Sem juros. As taxas de administração são diluídas no prazo contratado;<br />
                    • Unificação de cotas do mesmo grupo ou não;<br />
                    • Chances de contemplação por sorteio, lance fixo e lance livre;<br />
                    • Lance com recursos da carta: utilize até 50% de sua carta de crédito para ofertar um lance;<br />
                    • Portal de serviços ao cliente: fácil acesso aos resultados e assembleias, oferta de lances, emissão de boleto, informe de IR, envio de documentos, entre outros serviços;<br />
                    • Possiblidade de usar o FGTS para complementar o valor do imóvel, quitar ou amortizar o saldo devedor, conforme as regras do fundo.<br />
                    • Mudança de valor: enquanto não for contemplado, você pode alterar o valor da carta de crédito, duas vezes, dentro do mesmo grupo.
                </p>
            </div>
        </div>
    </section>
    <!-- End Section -->


    <?php include_once("includes/base.php"); ?>

</div>
<!-- End Page Wrap -->


