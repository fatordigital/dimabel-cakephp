
    <!-- JS -->
    <script type="text/javascript" src="<?php echo getLink(); ?>views/js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="<?php echo getLink(); ?>views/js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="<?php echo getLink(); ?>views/js/bootstrap.min.js"></script>

    <!-- Toastr -->
    <script type="text/javascript" src="<?php echo site_url('/application/views/plugins/toastr/toastr.min.js') ?>"></script>
    <!-- Toastr -->

    <script type="text/javascript" src="<?php echo getLink(); ?>views/js/SmoothScroll.js"></script>
    <script type="text/javascript" src="<?php echo getLink(); ?>views/js/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="<?php echo getLink(); ?>views/js/jquery.localScroll.min.js"></script>
    <script type="text/javascript" src="<?php echo getLink(); ?>views/js/jquery.viewport.mini.js"></script>
    <script type="text/javascript" src="<?php echo getLink(); ?>views/js/jquery.countTo.js"></script>
    <script type="text/javascript" src="<?php echo getLink(); ?>views/js/jquery.appear.js"></script>
    <script type="text/javascript" src="<?php echo getLink(); ?>views/js/jquery.sticky.js"></script>
    <script type="text/javascript" src="<?php echo getLink(); ?>views/js/jquery.parallax-1.1.3.js"></script>
    <script type="text/javascript" src="<?php echo getLink(); ?>views/js/jquery.fitvids.js"></script>
    <script type="text/javascript" src="<?php echo getLink(); ?>views/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="<?php echo getLink(); ?>views/js/isotope.pkgd.min.js"></script>
    <script type="text/javascript" src="<?php echo getLink(); ?>views/js/imagesloaded.pkgd.min.js"></script>
    <script type="text/javascript" src="<?php echo getLink(); ?>views/js/jquery.magnific-popup.min.js"></script>
    <!-- Replace test API Key "AIzaSyAZsDkJFLS0b59q7cmW0EprwfcfUA8d9dg" with your own one below
    **** You can get API Key here - https://developers.google.com/maps/documentation/javascript/get-api-key -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZsDkJFLS0b59q7cmW0EprwfcfUA8d9dg"></script>
    <script type="text/javascript" src="<?php echo getLink(); ?>views/js/gmap3.min.js"></script>
    <script type="text/javascript" src="<?php echo getLink(); ?>views/js/wow.min.js"></script>
    <script type="text/javascript" src="<?php echo getLink(); ?>views/js/masonry.pkgd.min.js"></script>
    <script type="text/javascript" src="<?php echo getLink(); ?>views/js/jquery.simple-text-rotator.min.js"></script>
    <script type="text/javascript" src="<?php echo getLink(); ?>views/js/jquery.mask.min.js"></script>

    <!-- Simulador -->
    <script type="text/javascript" src="<?php echo getLink(); ?>views/js/simulador.js?time=<?php echo time() ?>"></script>
    <!-- Simulador -->

    <script type="text/javascript" src="<?php echo getLink(); ?>views/js/all.js?time=<?php echo time() ?>"></script>
    <script type="text/javascript" src="<?php echo getLink(); ?>views/js/contact-form.js"></script>
    <script type="text/javascript" src="<?php echo getLink(); ?>views/js/jquery.ajaxchimp.min.js"></script>
    <!--[if lt IE 10]><script type="text/javascript" src="<?php echo getLink(); ?>views/js/placeholder.js"></script><![endif]-->
    <script type="text/javascript" src="<?php echo getLink(); ?>views/includes/goals.js"></script>


</body>
</html>