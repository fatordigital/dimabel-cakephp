var simulador_home = document.getElementById('button-home-simulador'),
    simulador_parcelas = document.getElementById('simulador-parcelas'),
    atendimento_button = document.getElementById('atendimento-form'),
    contato_home = document.getElementById('form-base');

$(document).ready(function(){
    if (simulador_home) {
        simulador_home.addEventListener('click', function () {
            ga('send', 'event', 'simulacao-hs', 'clique', 'envio');
        });
    }

    if (simulador_parcelas) {
        simulador_parcelas.addEventListener('submit', function () {
            ga('send', 'pageview', '/goal/hs-simulador');
        });
    }

    if (atendimento_button) {
        atendimento_button.addEventListener('submit', function () {
            ga('send', 'pageview', '/goal/hs-contato');
        });
    }

    if (contato_home) {
        contato_home.addEventListener('submit', function (e) {
            ga('send', 'pageview', '/goal/hs-contato-home');
        });
    }
});
