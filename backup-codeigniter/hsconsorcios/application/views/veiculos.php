<?php

$cam = "";

$bt1 = '';
$bt2 = '';
$bt3 = 'active';
$bt4 = '';
$bt5 = '';
$bt6 = '';
$bt7 = '';
$bt8 = '';
$bt9 = '';


?>


<!-- Page Wrap -->
<div class="page" id="top">

    <?php include_once("includes/menu.php"); ?>

    <!-- Head Section -->
    <section class="small-section bg-dark-alfa-90 bg-scroll"
             data-background="<?php echo getLink(); ?>views/imagens/capa_2.jpg">
        <div class="relative container align-left">

            <div class="row">

                <div class="col-md-12">
                    <h1 class="section-heading mb-10 mb-xs-0">Consórcio de Veículos</h1>
                    <div class="hs-line-4 uppercase">
                        Nossos Produtos
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- End Head Section -->


    <!-- Section -->
    <section class="page-section" style="padding:80px 0;">
        <div class="relative container">

            <div class="section-text">

                <h4 style="margin:10px 0; font-weight:700; color:#CD071E;">Veículos</h4>
                <p class="text-justify">
                    • Carta com valores: a partir de R$ 25 mil a R$ 150 mil;<br />
                    • Parcelas a partir de R$ 480,36 por mês;<br />
                    • Prazo de 80 meses;<br />
                    • Parcelas atraentes;<br />
                    • Sem juros. As taxas de administração são diluídas no prazo contratado;<br />
                    • Unificação de cotas do mesmo grupo ou não;<br />
                    • Chances de contemplação por sorteio, lance fixo e lance livre;<br />
                    • Lance com recursos da carta: utilize até 50% de sua carta de crédito para ofertar um lance;<br />
                    • Portal de serviços ao cliente: fácil acesso aos resultados e assembleias, oferta de lances, emissão de boleto, informe de IR, envio de documentos, entre outros serviços;<br />
                    • Mudança de valor: enquanto não for contemplado, você pode alterar o valor da carta de crédito, duas vezes, dentro do mesmo grupo;<br />
                    • Comprar um automóvel novo ou seminovo (de até 5 anos de fabricação) com a vantagem de pagar à vista o que lhe garante mais poder para negociar descontos no preço;<br />
                    • Quitar o financiamento de veículo com a carta de crédito;<br />
                    • Aquisição de caminhão e ônibus.<br />
                </p>

            </div>

        </div>
    </section>
    <!-- End Section -->


    <?php include_once("includes/base.php"); ?>

</div>
<!-- End Page Wrap -->
