<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'dimabel_site');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'k]B%*Un.j^}sWm?kb@SWZD7H>P8%QFYpa)lz9!_8EY`:W#NVBems,VmK}Za,iE9j');
define('SECURE_AUTH_KEY',  'H2TW9GNCbQU8zRDrC:v~xiHA7X4*7^>a]<lb me|oa%g5cW7Y!uUbYzKYUzJKl(s');
define('LOGGED_IN_KEY',    '(0*zEu<Bq`E9D,cVQ`IM3y5J813A{|H[K9?tT@Oxz5NE|1nh^X4<auc*s1 g&f1$');
define('NONCE_KEY',        'W|.sGseLqv%},NlO`7XU?^.y0(rr&u|t],IaxY>Cl-}1pD^k~a{TF!<8P1RL?g`d');
define('AUTH_SALT',        ')bc^Q.6.6C}QHhm:w1)yhcZm,>(>,p@wVZ?myvesBskSLBZ4-X|tsQ#)4LZD~y!_');
define('SECURE_AUTH_SALT', 'Y%/BK6-CNT]{xqVOFt@XaH s)[W9s.uX>Zbb{E,[-#7(*@#r}3l`Fb[%XE&W-o{ ');
define('LOGGED_IN_SALT',   'HKyUk#AIl_HYHm+Hr&@Mp*@aQl.mYopzOp@R%vC!3R@?^.RF[Me^)u#&^0{%1AcI');
define('NONCE_SALT',       'Ul F78dBi[l.u$TSwViTb=6f=t1,M6h(aTc<X[g,Gjp62#hHm9A/f;)keM[bnV]~');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
