<?php defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . 'views/phpmailer/class.phpmailer.php';
include APPPATH . 'views/phpmailer/class.smtp.php';

$GLOBALS['ci'] = 'B';

class Controller extends CI_Controller
{
    public $settings;
    protected $email_settings = array(
        'smtp_host' => 'smtp.mandrillapp.com',
        'smtp_user' => 'mandrill@fatordigital.com.br',
        'smtp_pass' => 'WCnPLj4A1ATpyKvcf6tLhA'
    );

    public function __construct()
    {
        parent::__construct();
        $this->load_settings();
        $this->load->library('email', $this->email_settings);
        $this->load->helper(array('url', 'form', 'google', 'fator_digital'));
        $GLOBALS['ci'] = $this;
    }

    public function load_settings()
    {
        $this->load->model('Setting');
        $this->settings = $this->Setting->get_and_config();
    }

    public function send_php_mailer($data, $body, $attributes = array())
    {

        $mail = new PHPMailer();

        $mail->IsSMTP();
        $mail->Host = $this->email_settings['smtp_host'];
        $mail->SMTPAuth = true;
        $mail->Username = $this->email_settings['smtp_user'];
        $mail->Password = $this->email_settings['smtp_pass'];
        $mail->Port = 587;
        $mail->SMTPSecure = 'tls';
        $mail->SMTPDebug = 0;

        $mail->From = $this->settings['email.contact'];
        $mail->Sender = $this->settings['email.contact'];
        $mail->FromName = $this->settings['email.from'];

        $mail->AddAddress($this->settings['email.contact']);
        $mail->AddBCC('andrew.rodrigues@fatordigital.com.br', 'Dsenvolvedor');

        $mail->addReplyTo($data['email'], $data['name']);

        $mail->IsHTML(true);
        $mail->CharSet = 'utf-8';

        $mail->Subject = isset($attributes['subject']) ? $attributes['subject'] : 'Contato - dimabel.com.br';
        $mail->Body = $body;

        $enviado = $mail->Send();

        $mail->ClearAllRecipients();
        $mail->ClearAttachments();


        return $enviado ? true : false;
    }

}
