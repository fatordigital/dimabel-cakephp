<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include 'controller.php';

class Home extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        $db = $this->db->query('
        SELECT wp_posts.ID, wp_term_taxonomy.term_taxonomy_id , wp_posts.post_title, wp_posts.post_name, wp_posts.post_date, wp_posts.post_content,
        wp_terms.`name` AS CATEGORY_NAME, wp_terms.slug AS CATEGORY_SLUG FROM wp_posts
        LEFT JOIN wp_term_relationships ON wp_posts.ID = wp_term_relationships.object_id
        LEFT JOIN wp_term_taxonomy ON wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id
        LEFT JOIN wp_terms ON wp_terms.term_id = wp_term_taxonomy.term_id
        WHERE wp_posts.post_status = "publish" GROUP BY ID ORDER BY ID DESC LIMIT 2
        ');

        $posts = $db->result();

        $db = $this->db;

        $this->template->load('template', 'home', compact('posts', 'db'));
    }


    public function contact()
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $this->load->model('Contact');
            if (captcha_fator() != false) {
                if (isset($_POST['contato_home']))
                    $this->session->set_flashdata('contato_home', true);

                if (isset($_POST['contato']))
                    $this->session->set_flashdata('contato', true);

                if ($this->Contact->create() != false) {
                    $body = '
                     Nome: ' . $this->input->post('name') . ' <br />
                     E-mail: ' . $this->input->post('email') . ' <br />
                     Telefone: ' . $this->input->post('phone') . ' <br />
                     <br />
                     <strong>Mensagem</strong> <br /></br >
                     ' . $this->input->post('message') . '
                    ';

                    if ($this->send_php_mailer($this->input->post(), $body)) {
                        alert('success', 'Recebemos seu contato e retornaremos o mais rápido possível.');
                        redirect($this->input->post('return_url'));
                    } else {
                        alert('error', 'Ops, tente novamente, um erro em nosso servidor aconteceu.');
                    }
                } else {
                    alert('error', 'Todos os campos do formulário são obrigatórios.');
                }
            }
        }
        $this->template->load('template', isset($_POST['view']) ? $_POST['view'] : 'home');
    }


}
