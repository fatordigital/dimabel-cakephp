<?php

$cam = "";

$bt1 = '';
$bt2 = '';
$bt3 = 'active';
$bt4 = '';
$bt5 = '';
$bt6 = '';
$bt7 = '';
$bt8 = '';
$bt9 = '';


?>
<html>
<head>

    <?php include_once("includes/head.php"); ?>

    <?php include_once("includes/analyticstracking.php"); ?>

</head>


<body class="appear-animate">

<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TSV6NGX" height="0" width="0"
            style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<?php if ($this->session->flashdata('success')) { ?>
    <div class="alert success">
        <?= $this->session->flashdata('success'); ?>
    </div>
<?php } ?>

<?php if ($this->session->flashdata('error')) { ?>
    <div class="alert error">
        <?= $this->session->flashdata('error'); ?>
    </div>
<?php } ?>

<!-- Page Wrap -->
<div class="page" id="top">

    <?php include_once("includes/menu.php"); ?>

    <!-- Head Section -->
    <section class="small-section bg-dark-alfa-90 bg-scroll"
             data-background="<?php echo getLink(); ?>views/imagens/capa_3.jpg">
        <div class="relative container align-left">

            <div class="row">

                <div class="col-md-12">
                    <h1 class="section-heading mb-10 mb-xs-0">Consórcio de Veículos Pesados</h1>
                    <div class="hs-line-4 uppercase">
                        Nossos Produtos
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- End Head Section -->


    <!-- Section -->
    <section class="page-section" style="padding:80px 0;">
        <div class="relative container">

            <div class="section-text">

                <h4 style="margin:10px 0; font-weight:700; color:#0072ab;">Veículos Pesados</h4>
                <p align="justify">
                    Para adquirir seu Caminhão ou Veículo Pesado, conte com o Consórcio da CAIXA. Assim, você não paga
                    juros e tem a flexibilidade para escolher, o modelo que mais convém ao seu negócio.
                    <br/>
                    <br/>
                    • Tudo isso com até 70 meses para pagar.
                    <br/>
                    • De forma flexível, você poderá aderir a um grupo em andamento e contratar um plano com prazo
                    menor.
                    <br/>
                    • O valor do crédito escolhido na proposta pode ser alterado antes da contemplação.
                    <br/>
                    • Parcelas atraentes que cabem no seu bolso.
                    <br/>
                    • Alto índice de contemplação nos grupos, com lance fixo, embutido e sorteio.

                </p>

            </div>

        </div>
    </section>
    <!-- End Section -->


    <?php include_once("includes/base.php"); ?>

</div>
<!-- End Page Wrap -->


<?php include_once("includes/foot.php"); ?>



