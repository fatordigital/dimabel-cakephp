<?php

$cam = "";

$bt1 = '';
$bt2 = '';
$bt3 = '';
$bt4 = '';
$bt5 = 'active';
$bt6 = '';
$bt7 = '';
$bt8 = '';
$bt9 = '';


?>


<!-- Page Wrap -->
<div class="page" id="top">

    <?php include_once("includes/menu.php"); ?>

    <!-- Head Section -->
    <section class="small-section bg-dark-alfa-90 bg-scroll"
             data-background="<?php echo getLink(); ?>views/imagens/contato.jpg">
        <div class="relative container align-left">

            <div class="row">

                <div class="col-md-12">
                    <h1 class="section-heading mb-10 mb-xs-0">Fale Conosco</h1>
                    <div class="hs-line-4 uppercase">
                        Espaço para Dúvidas, críticas e sugestões
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- End Head Section -->

    <!-- Section -->
    <section class="page-section" style="padding:80px 0;">
        <div class="relative container">

            <div class="section-text">


                <h4 style="margin:10px 0; font-weight:700; color:#0072ab;">Formulário de Contato</h4>


                <!-- Contact Form -->
                <div class="row mt-20">
                    <div class="col-md-12">

                        <?php echo form_open(site_url('/home/contact'), array('id' => 'atendimento-form', 'class' => 'form')) ?>

                            <input type="hidden" name="view" value="atendimento" />
                            <input type="hidden" name="return_url" value="<?php echo current_url() ?>"/>

                            <input type="hidden" name="contato" />

                            <div class="fd_key">
                                <input type="text" name="fd" value=""/>
                            </div>

                        <div class="clearfix">

                            <div class="cf-left-col">

                                <!-- Name -->
                                <div class="form-group">
                                    <input type="text" name="name" id="name" value="<?=set_value('name')?>" class="input-lg form-control" placeholder="Nome" pattern=".{3,100}" required />
                                </div>

                                <!-- Email -->
                                <div class="form-group">
                                    <input type="email" name="email" id="email" value="<?=set_value('email')?>" class="input-lg form-control" placeholder="Email" pattern=".{5,100}" required />
                                </div>

                                <div class="form-group">
                                    <input type="text" name="phone" id="phone" value="<?=set_value('phone')?>" class="input-lg form-control" placeholder="Telefone" required />
                                </div>

                            </div>

                            <div class="cf-right-col">

                                <!-- Message -->
                                <div class="form-group">
                                        <textarea name="message" id="message" class="input-lg form-control" style="height: 120px;" placeholder="Mensagem" required><?=set_value('message')?></textarea>
                                </div>

                            </div>


                        </div>

                        <div class="clearfix">

                            <div class="cf-left-col">

                                <!-- Inform Tip -->
                                <div class="form-tip pt-20">
                                    <i class="fa fa-info-circle"></i> Todos os campos são obrigatórios
                                </div>

                            </div>

                            <div class="cf-right-col">

                                <!-- Send Button -->
                                <div class="align-right pt-10">
                                    <button
                                        style="width:100%;"
                                        type="submit"
                                        class="btn btn-mod btn-color btn-large"
                                        name="submitAtendimento">
                                        Enviar Mensagem
                                    </button>
                                </div>

                            </div>

                        </div>

                        </form>

                    </div>
                </div>
                <!-- End Contact Form -->

            </div>

        </div>
    </section>
    <!-- End Section -->

    <?php include_once("includes/base.php"); ?>

</div>
<!-- End Page Wrap -->



