<?php

$cam = "";

$bt1 = '';
$bt2 = '';
$bt3 = '';
$bt4 = 'active';
$bt5 = '';
$bt6 = '';
$bt7 = '';
$bt8 = '';
$bt9 = '';


?>

<!-- Page Wrap -->
<div class="page" id="top">

    <?php include_once("includes/menu.php"); ?>

    <!-- Head Section -->
    <section class="small-section bg-dark-alfa-90 bg-scroll"
             data-background="<?php echo getLink(); ?>views/imagens/quem.jpg">
        <div class="relative container align-left">

            <div class="row">

                <div class="col-md-12">
                    <h1 class="section-heading mb-10 mb-xs-0">Quem Somos</h1>
                    <div class="hs-line-4 uppercase">
                        Conheça a Empresa
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- End Head Section -->


    <!-- Section -->
    <section class="page-section" style="padding:80px 0;">
        <div class="relative container">

            <div class="section-text">

                <h4 style="margin:10px 0; font-weight:700; color:#0072ab;">Dimabel Consórcios</h4>
                <p align="justify">
                    Somos uma empresa de soluções financeiras focada na necessidade global do cliente, constituída por
                    profissionais experientes e certificados, com forte atuação nas maiores instituições financeiras do
                    país.
                    <br/><br/>
                    A Dimabel Consórcios remete a uma filosofia de como fazer bem as coisas, cativando o seu público e
                    sempre procurando a excelência em seus serviços. Não nascemos para ser mais uma, nascemos para ser a
                    melhor.
                </p>

                <h4 style="margin:10px 0; font-weight:700; color:#0072ab;">Parceria CAIXA Consórcios</h4>
                <p align="justify">
                    A Dimabel Consórcios é parceira oficial da Caixa para comercialização e assessoria de seus
                    consórcios. A Caixa Seguradora é uma bem sucedida união entre duas instituições públicas: a CNP
                    Assurances, líder do mercado francês em seguros de pessoas, e a Caixa Econômica Federal.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#ff6600;">Missão</h4>
                <p align="justify">
                    Ofertar crédito nas várias modalidades com as melhores condições e segurança, visando o equilíbrio
                    financeiro e contribuindo para melhorar a qualidade de vida do cliente.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#ff6600;">Visão</h4>
                <p align="justify">
                    Planejamento financeiro acessível, com acompanhamento personalizado para alcançar seus objetivos.
                </p>

                <h4 style="margin:4px 0; font-weight:700; color:#ff6600;">Valores</h4>
                <p align="justify">
                    Sempre valorizar e respeitar o cliente, Conseguir excelência com simplicidade, Integridade e
                    confiança frente ao mercado.
                </p>

            </div>

        </div>
    </section>
    <!-- End Section -->


    <?php include_once("includes/base.php"); ?>

</div>
<!-- End Page Wrap -->



